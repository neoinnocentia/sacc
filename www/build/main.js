webpackJsonp([18],{

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirebaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loading__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__alert__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__data__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_take__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_take___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_take__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var FirebaseProvider = /** @class */ (function () {
    // Firebase Provider
    // This is the provider class for most of the Firebase updates in the app.
    function FirebaseProvider(angularDb, loadingProvider, alertProvider, dataProvider) {
        this.angularDb = angularDb;
        this.loadingProvider = loadingProvider;
        this.alertProvider = alertProvider;
        this.dataProvider = dataProvider;
    }
    /*  // Send friend request to userId.
     sendFriendRequest(userId) {
       let loggedInUserId = firebase.auth().currentUser.uid;
       this.loadingProvider.show();
   
       var requestsSent;
       // Use take(1) so that subscription will only trigger once.
       this.dataProvider.getRequests(loggedInUserId).take(1).subscribe((requests) => {
         requestsSent = requests.requestsSent;
         if (!requestsSent) {
           requestsSent = [userId];
         } else {
           if (requestsSent.indexOf(userId) == -1)
             requestsSent.push(userId);
         }
         // Add requestsSent information.
         this.angularDb.object('/requests/' + loggedInUserId).update({
           requestsSent: requestsSent
         }).then((success) => {
           var friendRequests;
           this.dataProvider.getRequests(userId).take(1).subscribe((requests) => {
             friendRequests = requests.friendRequests;
             if (!friendRequests) {
               friendRequests = [loggedInUserId];
             } else {
               if (friendRequests.indexOf(userId) == -1)
                 friendRequests.push(loggedInUserId);
             }
             // Add friendRequest information.
             this.angularDb.object('/requests/' + userId).update({
               friendRequests: friendRequests
             }).then((success) => {
               this.loadingProvider.hide();
               this.alertProvider.showFriendRequestSent();
             }).catch((error) => {
               this.loadingProvider.hide();
             });
           });
         }).catch((error) => {
           this.loadingProvider.hide();
         });
       });
     } */
    // Cancel friend request sent to userId.
    /*  cancelFriendRequest(userId) {
       let loggedInUserId = firebase.auth().currentUser.uid;
       this.loadingProvider.show();
   
       var requestsSent;
       this.dataProvider.getRequests(loggedInUserId).take(1).subscribe((requests) => {
         requestsSent = requests.requestsSent;
         requestsSent.splice(requestsSent.indexOf(userId), 1);
         // Update requestSent information.
         this.angularDb.object('/requests/' + loggedInUserId).update({
           requestsSent: requestsSent
         }).then((success) => {
           var friendRequests;
           this.dataProvider.getRequests(userId).take(1).subscribe((requests) => {
             friendRequests = requests.friendRequests;
             friendRequests.splice(friendRequests.indexOf(loggedInUserId), 1);
             // Update friendRequests information.
             this.angularDb.object('/requests/' + userId).update({
               friendRequests: friendRequests
             }).then((success) => {
               this.loadingProvider.hide();
               this.alertProvider.showFriendRequestRemoved();
             }).catch((error) => {
               this.loadingProvider.hide();
             });
           });
         }).catch((error) => {
           this.loadingProvider.hide();
         });
       });
     } */
    // Delete friend request.
    /*   deleteFriendRequest(userId) {
        let loggedInUserId = firebase.auth().currentUser.uid;
        this.loadingProvider.show();
    
        var friendRequests;
        this.dataProvider.getRequests(loggedInUserId).take(1).subscribe((requests) => {
          friendRequests = requests.friendRequests;
          friendRequests.splice(friendRequests.indexOf(userId), 1);
          // Update friendRequests information.
          this.angularDb.object('/requests/' + loggedInUserId).update({
            friendRequests: friendRequests
          }).then((success) => {
            var requestsSent;
            this.dataProvider.getRequests(userId).take(1).subscribe((requests) => {
              requestsSent = requests.requestsSent;
              requestsSent.splice(requestsSent.indexOf(loggedInUserId), 1);
              // Update requestsSent information.
              this.angularDb.object('/requests/' + userId).update({
                requestsSent: requestsSent
              }).then((success) => {
                this.loadingProvider.hide();
    
              }).catch((error) => {
                this.loadingProvider.hide();
              });
            });
          }).catch((error) => {
            this.loadingProvider.hide();
            //TODO ERROR
          });
        });
      } */
    // Accept friend request.
    /*   acceptFriendRequest(userId) {
        let loggedInUserId = firebase.auth().currentUser.uid;
        // Delete friend request.
        this.deleteFriendRequest(userId);
    
        this.loadingProvider.show();
        this.dataProvider.getUser(loggedInUserId).take(1).subscribe((account) => {
          var friends = account.friends;
          if (!friends) {
            friends = [userId];
          } else {
            friends.push(userId);
          }
          // Add both users as friends.
          this.dataProvider.getUser(loggedInUserId).update({
            friends: friends
          }).then((success) => {
            this.dataProvider.getUser(userId).take(1).subscribe((account) => {
              var friends = account.friends;
              if (!friends) {
                friends = [loggedInUserId];
              } else {
                friends.push(loggedInUserId);
              }
              this.dataProvider.getUser(userId).update({
                friends: friends
              }).then((success) => {
                this.loadingProvider.hide();
              }).catch((error) => {
                this.loadingProvider.hide();
              });
            });
          }).catch((error) => {
            this.loadingProvider.hide();
          });
        });
      } */
    // TimeLine
    FirebaseProvider.prototype.timeline = function (timelineId) {
        var _this = this;
        var loggedInUserId = __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid;
        this.dataProvider.getUser(loggedInUserId).take(1).subscribe(function (account) {
            var timeline = account.timeline;
            if (!timeline) {
                timeline = [timelineId];
            }
            else {
                timeline.push(timelineId);
            }
            // Add both users as friends.
            _this.dataProvider.getUser(loggedInUserId).update({
                timeline: timeline
            }).then(function (success) {
            }).catch(function (error) {
                _this.loadingProvider.hide();
            });
        });
    };
    // ==== Like postBy
    FirebaseProvider.prototype.likePost = function (key) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.dataProvider.postLike(key).take(1).subscribe(function (likes) {
                var likes = likes;
                if (!likes.length) {
                    likes = [__WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid];
                }
                else {
                    likes.push(__WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid);
                }
                // Add both users as friends.
                _this.dataProvider.postLike(key).update(likes).then(function (success) {
                    // alert('sc')
                    resolve(true);
                }).catch(function (error) {
                    _this.loadingProvider.hide();
                    reject(false);
                });
            });
        });
    };
    // ==== Like postBy
    FirebaseProvider.prototype.delikePost = function (key) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.dataProvider.postLike(key).take(1).subscribe(function (likes) {
                likes.splice(likes.indexOf(__WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid), 1);
                if (likes.length) {
                    //alert(likes.length)
                    _this.angularDb.object('likes/' + key).remove();
                    _this.dataProvider.postLike(key).update(likes).then(function (success) {
                        // alert('sc')
                        resolve(true);
                    }).catch(function (error) {
                        _this.loadingProvider.hide();
                        reject(false);
                    });
                }
                else {
                    _this.angularDb.object('likes/' + key).remove();
                }
            });
        });
    };
    // ====== Dislike
    FirebaseProvider.prototype.dislikePost = function (key) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.dataProvider.postdisLike(key).take(1).subscribe(function (dislikes) {
                var dislikes = dislikes;
                if (!dislikes.length) {
                    dislikes = [__WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid];
                }
                else {
                    dislikes.push(__WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid);
                }
                // Add both users as friends.
                _this.dataProvider.postdisLike(key).update(dislikes).then(function (success) {
                    // alert('sc')
                    resolve(true);
                }).catch(function (error) {
                    _this.loadingProvider.hide();
                    reject(false);
                });
            });
        });
    };
    // ===== Deldislike
    FirebaseProvider.prototype.dedislikePost = function (key) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.dataProvider.postdisLike(key).take(1).subscribe(function (dislikes) {
                dislikes.splice(dislikes.indexOf(__WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid), 1);
                if (dislikes.length) {
                    //alert(likes.length)
                    _this.angularDb.object('dislikes/' + key).remove();
                    _this.dataProvider.postdisLike(key).update(dislikes).then(function (success) {
                        // alert('sc')
                        resolve(true);
                    }).catch(function (error) {
                        _this.loadingProvider.hide();
                        reject(false);
                    });
                }
                else {
                    _this.angularDb.object('dislikes/' + key).remove();
                }
            });
        });
    };
    FirebaseProvider.prototype.commentPost = function (key, comment) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.dataProvider.getComments(key).take(1).subscribe(function (comments) {
                var comments = comments;
                if (!comments) {
                    comments = [comment];
                }
                else {
                    comments.push(comment);
                }
                // Add both users as friends.
                _this.dataProvider.postComments(key).update(comments).then(function (success) {
                    resolve(true);
                }).catch(function (error) {
                    _this.loadingProvider.hide();
                    reject(false);
                });
            });
        });
    };
    FirebaseProvider.prototype.reportPost = function (post, loginUser) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.dataProvider.getTimeline(post.$key).take(1).subscribe(function (suc) {
                console.log("timeline", suc);
                if (suc.postBy) {
                    _this.dataProvider.getReportPost(post.$key).take(1).subscribe(function (res) {
                        console.log("res", res);
                        var _res = res;
                        if (!_res.postId) {
                            _this.angularDb.object('/reportPost/' + post.$key).update({
                                dateCreated: new Date().toString(),
                                name: post.name,
                                postBy: post.postBy,
                                postText: post.postText,
                                image: post.image ? post.image : '',
                                location: post.location ? post.location : '',
                                avatar: post.avatar,
                                postId: post.$key,
                                reportedBy: [{
                                        name: loginUser.name,
                                        userId: loginUser.userId,
                                        image: loginUser.img,
                                        dateCreated: new Date().toString()
                                    }]
                            }).then(function (success) {
                                resolve(true);
                            }).catch(function (error) {
                                reject(false);
                            });
                        }
                        else {
                            var reportedBy = _res.reportedBy;
                            reportedBy.push({
                                name: loginUser.name,
                                userId: loginUser.userId,
                                image: loginUser.img,
                                dateCreated: new Date().toString()
                            });
                            _this.angularDb.object('/reportPost/' + post.$key).update({
                                reportedBy: reportedBy
                            }).then(function (success) {
                                resolve(true);
                            }).catch(function (error) {
                                reject(false);
                            });
                        }
                    });
                }
                else {
                    _this.alertProvider.showToast("Post alerday deleted by admin..");
                    reject(false);
                }
            });
        });
    };
    FirebaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_2__loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_3__alert__["a" /* AlertProvider */],
            __WEBPACK_IMPORTED_MODULE_4__data__["a" /* DataProvider */]])
    ], FirebaseProvider);
    return FirebaseProvider;
}());

//# sourceMappingURL=firebase.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BlockProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__friends_friends__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__request_request__ = __webpack_require__(90);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BlockProvider = /** @class */ (function () {
    function BlockProvider(afAuth, afDB, friendsProvider, requestProvider) {
        this.afAuth = afAuth;
        this.afDB = afDB;
        this.friendsProvider = friendsProvider;
        this.requestProvider = requestProvider;
        this.UserUid = window.localStorage.getItem('userid');
    }
    BlockProvider.prototype.blockUser = function (userDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Block List').child(userDetails.Id).push({
                Id: _this.UserUid
            }).then(function () {
                _this.afDB.database.ref('Block List').child(_this.UserUid).push({
                    Id: userDetails.Id
                }).then(function () {
                    _this.friendsProvider.unFriend(userDetails).then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            });
        });
        return promise;
    };
    BlockProvider.prototype.getBlockList = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            var Details = [];
            _this.afDB.database.ref('Users').once('value', function (snap) {
                var res = snap.val();
                var userDetails = [];
                for (var i in res) {
                    userDetails.push(res[i]);
                }
                _this.afDB.database.ref('Block List').child(_this.UserUid).once('value', function (snap) {
                    var res = snap.val();
                    var array = [];
                    for (var i in res) {
                        array.push(res[i]);
                    }
                    for (var ia in userDetails) {
                        for (var ii in array) {
                            if (userDetails[ia].Id === array[ii].Id) {
                                Details.push(userDetails[ia]);
                            }
                        }
                    }
                    resolve(Details);
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    BlockProvider.prototype.unBlock = function (userDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Block List').child(_this.UserUid).orderByChild('Id').equalTo(userDetails.Id).once('value', function (snap) {
                var res = snap.val();
                var tempstore = Object.keys(res);
                _this.afDB.database.ref('Block List').child(_this.UserUid).child(tempstore[0]).remove().then(function () {
                    _this.afDB.database.ref('Block List').child(userDetails.Id).orderByChild('Id').equalTo(_this.UserUid).once('value', function (snap) {
                        var res = snap.val();
                        var tempstore = Object.keys(res);
                        _this.afDB.database.ref('Block List').child(userDetails.Id).child(tempstore[0]).remove().then(function () {
                            resolve(true);
                        }).catch(function (err) {
                            reject(err);
                        });
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    BlockProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_3__friends_friends__["a" /* FriendsProvider */], __WEBPACK_IMPORTED_MODULE_4__request_request__["a" /* RequestProvider */]])
    ], BlockProvider);
    return BlockProvider;
}());

//# sourceMappingURL=block.js.map

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_request_request__ = __webpack_require__(90);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { SuperTabsController } from 'ionic2-super-tabs';
var RequestsPage = /** @class */ (function () {
    function RequestsPage(navCtrl, loadCtrl, platform, requestProvider, alertCtrl) {
        this.navCtrl = navCtrl;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.requestProvider = requestProvider;
        this.alertCtrl = alertCtrl;
        this.sentRequests = [];
        this.receivedRequests = [];
        this.looding = true;
    }
    RequestsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // this.superTabsCtrl.showToolbar(false)
        this.requestProvider.getReceivedRequests().then(function (res) {
            _this.looding = false;
            _this.receivedRequests = res;
        });
        this.requestProvider.getSentRequests().then(function (res) {
            _this.looding = false;
            _this.sentRequests = res;
        });
    };
    RequestsPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    RequestsPage.prototype.showSentRequestsConfirmation = function (userDetails) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Request',
            message: 'You have been sent a request to ' + userDetails.Name,
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        _this.showToast('Cancel');
                    }
                },
                {
                    text: 'Delete Request',
                    handler: function () {
                        _this.deleteSentRequest(userDetails);
                    }
                },
                {
                    text: 'Block ' + userDetails.Name,
                    handler: function () {
                        _this.blockSentRequest(userDetails);
                    }
                }
            ]
        });
        confirm.present();
    };
    RequestsPage.prototype.showReceivedRequestsConfirmation = function (userDetails) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Request',
            message: 'Request from ' + userDetails.Name,
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        _this.showToast('Cancel');
                    }
                },
                {
                    text: 'Delete Request',
                    handler: function () {
                        _this.deleteReceivedRequest(userDetails);
                    }
                },
                {
                    text: 'Block ' + userDetails.Name,
                    handler: function () {
                        _this.blockReceivedRequest(userDetails);
                    }
                },
                {
                    text: 'Accept Request',
                    handler: function () {
                        _this.acceptRequest(userDetails);
                    }
                }
            ]
        });
        confirm.present();
    };
    RequestsPage.prototype.deleteSentRequest = function (userDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Deleting Request'
        });
        load.present();
        this.requestProvider.deleteSentRequest(userDetails).then(function () {
            if (_this.sentRequests.length > 1) {
                _this.requestProvider.getSentRequests().then(function (res) {
                    _this.sentRequests = res;
                    load.dismiss();
                    _this.showToast('Request to ' + userDetails.Name + ' has been deleted');
                }).catch(function (err) {
                    load.dismiss();
                    _this.showToast(err);
                });
            }
            else {
                _this.sentRequests = [];
                load.dismiss();
                if (_this.receivedRequests.length < 1) {
                    _this.navCtrl.pop();
                }
                _this.showToast('Request to ' + userDetails.Name + ' has been deleted');
            }
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    RequestsPage.prototype.deleteReceivedRequest = function (userDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Deleting Request'
        });
        load.present();
        this.requestProvider.deleteReceivedRequest(userDetails).then(function () {
            if (_this.sentRequests.length > 1) {
                _this.requestProvider.getReceivedRequests().then(function (res) {
                    _this.receivedRequests = res;
                    load.dismiss();
                    _this.showToast('Request from ' + userDetails.Name + ' has been deleted');
                }).catch(function (err) {
                    load.dismiss();
                    _this.showToast(err);
                });
            }
            else {
                _this.receivedRequests = [];
                if (_this.sentRequests.length < 1) {
                    _this.navCtrl.pop();
                }
                load.dismiss();
                _this.showToast('Request from ' + userDetails.Name + ' has been deleted');
            }
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    RequestsPage.prototype.acceptRequest = function (userDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Accepting Request'
        });
        load.present();
        this.requestProvider.acceptRquest(userDetails).then(function () {
            if (_this.sentRequests.length > 1) {
                _this.requestProvider.getReceivedRequests().then(function (res) {
                    _this.receivedRequests = res;
                    load.dismiss();
                    _this.showToast('You and ' + userDetails.Name + " become friends");
                }).catch(function (err) {
                    load.dismiss();
                    _this.showToast(err);
                });
            }
            else {
                _this.receivedRequests = [];
                if (_this.sentRequests.length < 1) {
                    _this.navCtrl.pop();
                }
                load.dismiss();
                _this.showToast('You and ' + userDetails.Name + " become friends");
            }
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    RequestsPage.prototype.blockReceivedRequest = function (userDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Blocking ' + userDetails.Name + ' .....'
        });
        load.present();
        this.requestProvider.blockReceivedRequest(userDetails).then(function () {
            if (_this.sentRequests.length > 1) {
                _this.requestProvider.getReceivedRequests().then(function (res) {
                    _this.receivedRequests = res;
                    load.dismiss();
                    _this.showToast(userDetails.Name + ' has been blocked');
                }).catch(function (err) {
                    load.dismiss();
                    _this.showToast(err);
                });
            }
            else {
                _this.receivedRequests = [];
                if (_this.sentRequests.length < 1) {
                    _this.navCtrl.pop();
                }
                load.dismiss();
                _this.showToast(userDetails.Name + ' has been blocked');
            }
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    RequestsPage.prototype.blockSentRequest = function (userDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Blocking ' + userDetails.Name + ' .....'
        });
        load.present();
        this.requestProvider.blockSentRequest(userDetails).then(function () {
            if (_this.sentRequests.length > 1) {
                _this.requestProvider.getSentRequests().then(function (res) {
                    _this.sentRequests = res;
                    load.dismiss();
                    _this.showToast(userDetails.Name + ' has been blocked');
                }).catch(function (err) {
                    load.dismiss();
                    _this.showToast(err);
                });
            }
            else {
                _this.sentRequests = [];
                load.dismiss();
                if (_this.receivedRequests.length < 1) {
                    _this.navCtrl.pop();
                }
                _this.showToast(userDetails.Name + ' has been blocked');
            }
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    RequestsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-requests',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\requests\requests.html"*/'<ion-header>\n  <ion-navbar color="header">\n    <ion-title>Requests</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n\n<ion-spinner name="bubbles" *ngIf="looding" class="looding"></ion-spinner>\n\n\n<div *ngIf="sentRequests != 0">\n	<ion-list-header>Sent Requests</ion-list-header>\n	<ion-item *ngFor="let user of sentRequests" (click)="showSentRequestsConfirmation(user)">\n		<ion-avatar item-start>\n	      <img src="{{user.Photo}}">\n	    </ion-avatar>\n	    <h2>{{user.Name}}</h2>\n	</ion-item>\n</div>\n\n\n\n\n<div *ngIf="receivedRequests != 0">\n	<ion-list-header>Received Requests</ion-list-header>\n	<ion-item *ngFor="let user of receivedRequests" (click)="showReceivedRequestsConfirmation(user)">\n		<ion-avatar item-start>\n	      <img src="{{user.Photo}}">\n	    </ion-avatar>\n	    <h2>{{user.Name}}</h2>\n	</ion-item>\n</div>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\requests\requests.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__providers_request_request__["a" /* RequestProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], RequestsPage);
    return RequestsPage;
}());

//# sourceMappingURL=requests.js.map

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__tabs_tabs__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__register_register__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__reset_password_reset_password__ = __webpack_require__(327);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, authProvider, loadCtrl, platform) {
        this.navCtrl = navCtrl;
        this.authProvider = authProvider;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.userDetails = {
            Email: '',
            Password: ''
        };
        this.showPasswordText = false;
        this.isenabled = false;
        this.email = false;
        this.password = false;
        this.registerPage = __WEBPACK_IMPORTED_MODULE_4__register_register__["a" /* RegisterPage */];
    }
    LoginPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Processing ....'
        });
        load.present();
        this.authProvider.login(this.userDetails).then(function () {
            load.dismiss();
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__tabs_tabs__["a" /* TabsPage */]);
            _this.authProvider.onlineStatus();
            window.localStorage.setItem('userstate', 'logedIn');
            window.localStorage.setItem('userid', _this.authProvider.afAuth.auth.currentUser.uid);
        }).catch(function (err) {
            load.dismiss();
            var error = err.message;
            _this.showToast(error);
        });
    };
    LoginPage.prototype.signup = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__register_register__["a" /* RegisterPage */]);
    };
    LoginPage.prototype.forgrtPassword = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__reset_password_reset_password__["a" /* ResetPasswordPage */]);
    };
    LoginPage.prototype.showHide = function () {
        if (this.showPasswordText == false) {
            this.showPasswordText = true;
        }
        else {
            this.showPasswordText = false;
        }
    };
    LoginPage.prototype.emailInput = function (value) {
        if (this.userDetails.Email == '' || !this.userDetails.Email.match(/^[a-zA-Z0-9-_\.]+@[a-z]{2,}\.[a-z]{2,4}$/)) {
            this.email = false;
        }
        else {
            this.email = true;
        }
        if (this.userDetails.Email == '' || !this.userDetails.Email.match(/^[a-zA-Z0-9-_\.]+@[a-z]{2,}\.[a-z]{2,4}$/)) {
            this.isenabled = false;
        }
        else {
            if (this.userDetails.Password.length < 6) {
                this.isenabled = false;
            }
            else {
                this.isenabled = true;
            }
        }
    };
    LoginPage.prototype.passwordInput = function (value) {
        if (this.userDetails.Password.length < 6) {
            this.password = false;
        }
        else {
            this.password = true;
        }
        if (this.userDetails.Email == '' || !this.userDetails.Email.match(/^[a-zA-Z0-9-_\.]+@[a-z]{2,}\.[a-z]{2,4}$/)) {
            this.isenabled = false;
        }
        else {
            if (this.userDetails.Password.length < 6) {
                this.isenabled = false;
            }
            else {
                this.isenabled = true;
            }
        }
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\login\login.html"*/'\n<ion-content padding class ="bg-image">\n	<div >\n	 <ion-card-content>\n	  \n		<ion-list no-lines>\n		 \n		  <div  style="height:10vw; width:40vw;"></div>\n		<ion-col>\n		  \n			<p  class="title-custom" text-center style="font-size: 18px; font-weight: bold;" ion-text color="logo" >Welcome to </p>\n		   \n			<p class="title-custom" text-center style="font-size: 18px;"  ion-text color="logo" >South African</p>\n		   \n			<p class="title-custom" text-center  style="font-size: 18px;"  ion-text color="logo" >Council of Churches</p>\n		\n		  </ion-col> \n  \n		  <!--Username button-->\n		 <div  style="height: 20px;"></div> \n		 <button  ion-button round block outline color="logo" style="height: 10vw; width:200px; margin-left: 50px;" center >\n		  <ion-item center>\n			<ion-input type="email" placeholder="Username"  text-center [(ngModel)]="userDetails.Email"  ></ion-input>\n		  </ion-item>\n		 </button>\n  \n  \n		  <div class="spacer" style="height: 3px;"></div>\n		  <button  ion-button round block outline color="logo" style="height: 10vw; width:200px; margin-left: 50px;"  center>\n		  <ion-item center>\n			<ion-input type="password" placeholder="Password" text-center [(ngModel)]="userDetails.Password"></ion-input>\n		  </ion-item>\n		  </button>\n		  <div class="spacer" style="height: 3px;"></div>\n		  <div class="color"><button ion-button round  style="height:10vw; width:40vw; text-transform: none; " center color="logo"  (click)="login()">Login</button></div>\n		  <a (click)="forgrtPassword()"  ion-text color="logo" class="color" style="text-align: center;" center>Forgot login details ? <b> Get Help </b></a> \n		  \n		  <p style="text-align: center;" ion-text color="logo">Don\'t have an account?</p> \n		 <div class="color" ><button  [navPush]="registerPage" ion-button round style="height:10vw; width:40vw; text-transform: none; " center color="light"  ion-text-color="logo" (click)="signup()">Sign up</button></div> \n		  <div class="spacer" style="height: 3px;"></div>\n		</ion-list>\n	  \n	  </ion-card-content>\n   </div>\n	\n  \n  </ion-content>\n  '/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 121:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupchatchannelPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_groups_groupschannel__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_imghandler_imghandler__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__groupbuddieschannel_groupbuddieschannel__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__groupmemberschannel_groupmemberschannel__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__presence_presence__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__donation_donation__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the GroupchatchannelPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var GroupchatchannelPage = /** @class */ (function () {
    function GroupchatchannelPage(ngZone, platform, navCtrl, menu, navParams, groupProvider, actionSheetCtrl, events, imgstore, loadCtrl) {
        var _this = this;
        this.ngZone = ngZone;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.navParams = navParams;
        this.groupProvider = groupProvider;
        this.actionSheetCtrl = actionSheetCtrl;
        this.events = events;
        this.imgstore = imgstore;
        this.loadCtrl = loadCtrl;
        this.User_Uid = this.groupProvider.UserUid;
        this.newMessage = {
            body: ''
        };
        this.allMessages = [];
        this.allUsers = [];
        /*  var days = ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri']
         var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
     
         var today = new Date
         var toYear = today.getFullYear()
         var toMonth = 1 + today.getMonth()
         var toDays = today.getDate()
     
         var todayRes1 = toYear + '/' + toMonth + '/' + toDays
         var todayRes2 = toYear + '/' + toMonth
         var todayRes3 = toYear
     
     
      */
        this.events.subscribe('Messages', function () {
            _this.ngZone.run(function () {
                // this.looding = false
                _this.allMessages = _this.groupProvider.messages;
                _this.allUsers = _this.groupProvider.users;
                for (var key in _this.allMessages) {
                    var d = new Date(_this.allMessages[key].Time);
                    var years = d.getFullYear();
                    var month = d.getMonth();
                    var day = d.getDate();
                    var hours = d.getHours();
                    var minutes = '0' + d.getMinutes();
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    var formattedTime = monthNames[month] + "-" + day + "-" + hours + ":" + minutes.substr(-2);
                    _this.allMessages[key].Time = formattedTime;
                }
            });
        });
        this.groupDetails = this.navParams.get('groupDetails');
        this.events.subscribe('Messages', function () {
            _this.ngZone.run(function () {
                _this.allMessages = _this.groupProvider.messages;
                _this.allUsers = _this.groupProvider.users;
            });
        });
    }
    GroupchatchannelPage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('Messages');
    };
    GroupchatchannelPage.prototype.ionViewDidEnter = function () {
        this.groupProvider.getAllMessages();
    };
    GroupchatchannelPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    GroupchatchannelPage.prototype.showActionSheet = function () {
        var _this = this;
        if (this.groupDetails.Owner == this.groupProvider.UserUid) {
            var actionSheet = this.actionSheetCtrl.create({
                title: this.groupDetails.Name,
                buttons: [
                    {
                        text: 'Group Members',
                        icon: 'contacts',
                        handler: function () {
                            _this.showMembers(_this.groupDetails);
                        }
                    }, {
                        text: 'Add Member',
                        icon: 'add',
                        handler: function () {
                            _this.addMember(_this.groupDetails);
                        }
                    }, {
                        text: 'Delete Group',
                        icon: 'trash',
                        handler: function () {
                            _this.deleteGroup(_this.groupDetails);
                        }
                    }, {
                        text: 'Cancel',
                        icon: 'close',
                        handler: function () {
                            _this.showToast('Cancel clicked');
                        }
                    }
                ]
            });
            actionSheet.present();
        }
        else {
            var actionSheet = this.actionSheetCtrl.create({
                title: this.groupDetails.Name,
                buttons: [
                    {
                        text: 'Group Members',
                        icon: 'contacts',
                        handler: function () {
                            _this.showMembers(_this.groupDetails);
                        }
                    }, {
                        text: 'Leave Group',
                        icon: 'log-out',
                        handler: function () {
                            _this.leaveGroup(_this.groupDetails);
                        }
                    }, {
                        text: 'Cancel',
                        icon: 'close',
                        handler: function () {
                            _this.showToast('Cancel clicked');
                        }
                    }
                ]
            });
            actionSheet.present();
        }
    };
    GroupchatchannelPage.prototype.leaveGroup = function (groupDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Leaving Group ...'
        });
        load.present();
        this.groupProvider.leaveGroup(groupDetails).then(function () {
            load.dismiss();
            _this.showToast('You have been leaved');
            _this.navCtrl.pop();
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    GroupchatchannelPage.prototype.deleteGroup = function (groupDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Deleting Group ...'
        });
        load.present();
        this.groupProvider.deleteGroup(groupDetails).then(function () {
            load.dismiss();
            _this.showToast('Group has been deleted');
            _this.navCtrl.pop();
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    GroupchatchannelPage.prototype.showMembers = function (groupDetails) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__groupmemberschannel_groupmemberschannel__["a" /* GroupmembersChannelPage */], {
            groupDetails: groupDetails
        });
    };
    GroupchatchannelPage.prototype.addMember = function (groupDetails) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__groupbuddieschannel_groupbuddieschannel__["a" /* GroupbuddieschannelPage */], {
            groupDetails: groupDetails
        });
    };
    GroupchatchannelPage.prototype.donate = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__donation_donation__["a" /* DonationPage */]);
    };
    GroupchatchannelPage.prototype.groupInfo = function (groupDetails) {
    };
    GroupchatchannelPage.prototype.sendMessage = function () {
        var _this = this;
        var res = this.newMessage.body;
        var res1 = res.trim();
        if (res1 == '') {
            this.showToast("can't send empty message");
            this.newMessage.body = '';
        }
        else {
            this.groupProvider.sendMessage(this.newMessage, this.groupDetails).then(function () {
                _this.newMessage.body = '';
            }).catch(function (err) {
                console.log(err);
            });
        }
    };
    GroupchatchannelPage.prototype.opensettings = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__presence_presence__["a" /* PresencePage */]);
    };
    GroupchatchannelPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groupchatchannel',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\groupchatchannel\groupchatchannel.html"*/'<ion-header>\n\n\n\n  <ion-navbar color="logo">\n\n    <ion-title (click)="donate()">{{groupDetails.Name}}</ion-title>\n\n    <ion-buttons end>\n\n    	<button ion-button ion-icon icon-only icon-left (click)="showActionSheet()">\n\n    		<ion-icon name="more"></ion-icon>\n\n    	</button>\n\n	</ion-buttons>\n\n	<ion-buttons end>\n\n    	<button ion-button icon-left icon-only (click)="opensettings()">\n\n    		\n\n    		<ion-icon name="notifications"></ion-icon>\n\n    	</button>\n\n	</ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n\n\n\n\n\n\n<div class="chatwindow">\n\n	<ion-list no-lines>\n\n		<ion-item *ngFor="let message of allMessages; let i = index">\n\n\n\n\n\n			<ion-avatar item-right *ngIf="message.Id != User_Uid">\n\n				<img src="{{allUsers[i].Photo}}">\n\n			</ion-avatar>\n\n			<div class="bubble you" *ngIf="message.Id != User_Uid">\n\n				<h3 text-wrap ion-text color="light">{{message.Body}}</h3>\n\n				<div class="spacer" style="height: 10px;"></div>\n\n\n\n				<div><h3 text-wrap ion-text color="light" style="margin-left: 50px;" >{{message.Time | date:\'short\'}}</h3></div>\n\n\n\n			</div>\n\n\n\n\n\n			<ion-avatar item-left *ngIf="message.Id === User_Uid">\n\n				<img src="{{allUsers[i].Photo}}">\n\n			</ion-avatar>\n\n			<div class="bubble me" *ngIf="message.Id === User_Uid">\n\n				<h3 text-wrap ion-text color="light">{{message.Body}}</h3>\n\n				<div class="spacer" style="height: 10px;"></div>\n\n\n\n				<div><h3 text-wrap ion-text color="light" style="margin-left: 50px;" >{{message.Time | date:\'short\'}}</h3></div>\n\n\n\n			</div>\n\n\n\n\n\n		</ion-item>\n\n	</ion-list>\n\n</div>\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n</ion-content>\n\n\n\n\n\n<ion-footer fixed>\n\n	<ion-toolbar color="col">\n\n		<ion-textarea  round block outline color="light"  placeholder="type your message" ion-text color="light" type="text" [(ngModel)]="newMessage.body"></ion-textarea>\n\n		<ion-buttons end>\n\n			<button ion-button ion-icon icon-only (click)="sendMessage()">\n\n				<ion-icon name="send"></ion-icon>\n\n			</button>\n\n		</ion-buttons>\n\n	</ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\groupchatchannel\groupchatchannel.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_groups_groupschannel__["a" /* GroupschannelProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_3__providers_imghandler_imghandler__["a" /* ImghandlerProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */]])
    ], GroupchatchannelPage);
    return GroupchatchannelPage;
}());

//# sourceMappingURL=groupchatchannel.js.map

/***/ }),

/***/ 122:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__editprofile_editprofile__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tabs_tabs__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__accountprofilecot_accountprofilecot__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__chat_chat__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__panic_panic__ = __webpack_require__(216);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var PostPage = /** @class */ (function () {
    function PostPage(ngZone, events, camera, actionSheetCtrl, loadCtrl, platform, navCtrl, authProvider, navParams) {
        var _this = this;
        this.ngZone = ngZone;
        this.events = events;
        this.camera = camera;
        this.actionSheetCtrl = actionSheetCtrl;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.authProvider = authProvider;
        this.navParams = navParams;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_6__accountprofilecot_accountprofilecot__["a" /* AccountProfilePagecotcot */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_7__chat_chat__["a" /* ChatPage */];
        //tab3Root = PostPage;
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_8__panic_panic__["a" /* PanicPage */];
        this.userDetails = {
            Name: '',
            Email: '',
            Phone: '',
            Id: '',
            Status: '',
            proPhoto: '',
            bgPhoto: ''
        };
        this.details = {
            Name: '',
            Email: '',
            Phone: '',
            Id: '',
            Status: '',
            proPhoto: '',
            about: '',
            bgPhoto: ''
        };
        this.myProfile = false;
        this.userDetails = this.navParams.get('userDetails');
        this.events.subscribe('ProfileDetails', function () {
            _this.ngZone.run(function () {
                _this.details = _this.authProvider.ProfileDetails;
                if (_this.details.Id == _this.authProvider.UserUid) {
                    _this.myProfile = true;
                }
                else {
                    _this.myProfile = false;
                }
            });
        });
    }
    PostPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    PostPage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('ProfileDetails');
    };
    PostPage.prototype.ionViewDidEnter = function () {
        this.authProvider.getProfileDetails(this.userDetails);
    };
    PostPage.prototype.proPic = function () {
        var _this = this;
        if (this.details.Id == this.authProvider.UserUid) {
            var actionSheet = this.actionSheetCtrl.create({
                title: this.details.Name,
                buttons: [
                    {
                        text: 'View Profile Picture',
                        icon: 'person',
                        role: 'destructive',
                        handler: function () {
                            _this.viewProfilePicture(_this.details.proPhoto, _this.details.Name);
                        }
                    }, {
                        text: 'Select Profile Picture',
                        icon: 'albums',
                        handler: function () {
                            _this.selectProfilePicture();
                        }
                    }, {
                        text: 'Cancel',
                        icon: 'close',
                        role: 'cancel',
                        handler: function () {
                            _this.showToast('Cancel');
                        }
                    }
                ]
            });
            actionSheet.present();
        }
        else {
            this.viewProfilePicture(this.details.proPhoto, this.details.Name);
        }
    };
    PostPage.prototype.viewProfilePicture = function (photo, name) {
        var options = {
            share: true,
            closeButton: true,
            copyToReference: false // default is false
        };
        //this.photoViewer.show(photo, name, options);    
    };
    PostPage.prototype.selectProfilePicture = function () {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Uploading Picture ...'
        });
        var cameraOptions = {
            quality: 50,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            encodingType: this.camera.EncodingType.PNG,
            destinationType: this.camera.DestinationType.DATA_URL
        };
        this.camera.getPicture(cameraOptions).then(function (ImageData) {
            _this.captureDataUrl = 'data:image/jpeg;base64,' + ImageData;
            load.present();
            _this.authProvider.uploadProfilePhoto(_this.captureDataUrl).then(function () {
                _this.details.proPhoto = _this.captureDataUrl;
                load.dismiss();
                _this.showToast('Profile Picture has been updated');
            }).catch(function (err) {
                load.dismiss();
                _this.showToast(err);
            });
        }, function (err) {
            var error = JSON.stringify(err);
            _this.showToast(error);
        });
    };
    PostPage.prototype.editProfile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__editprofile_editprofile__["a" /* EditprofilePage */], {
            userDetails: this.details
        });
    };
    PostPage.prototype.tabs = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__tabs_tabs__["a" /* TabsPage */]);
    };
    PostPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-post',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\post\post.html"*/'\n<ion-header>\n\n  <ion-navbar color="logo">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Your Profile</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content>\n  \n<!--Profile Picture -->\n<div class="spacer" style="height: 10px;"></div>\n\n    <div class="spacer"  style="height: 5px;"></div>\n    <ion-item lines="none">\n    <ion-avatar item-start>\n    <img src="{{details.Photo}}" style="width: 80px; height: 80px; margin-left: 10px;" class="image" (click)="proPic()" >\n  </ion-avatar>\n    <p style="font-size: 18px; font-weight: bold; " ion-text color="logo">{{details.Name}}</p>\n  </ion-item>\n   <div class="spacer" style="height: 30px;"></div>\n\n   <div>\n		<button ion-button round outline color="logo"  style="margin-left: 20px; width: 300px; text-transform: none; " ion-text color="darkgrey">{{details.Email}}</button>\n  </div>\n  <div class="spacer" style="height: 3px;"></div>\n\n	<div>\n				<button ion-button round outline color="logo"  style="margin-left: 20px; width: 300px; text-transform: none; " ion-text color="darkgrey" >{{details.Phone}}</button>\n		\n  </div>\n  <div class="spacer" style="height: 10px;"></div>\n\n  <div class="back">\n    <button ion-button round color="standard" style="height:10vw; width: 150px; text-transform: none; " center (click)="editProfile()" >Update</button>\n    </div>\n    <div class="spacer" style="height: 3vw;"></div>\n    \n    <!--Profile Settings-->\n  \n  <div class="profile"><button ion-button round  style="height:10vw; width:300px; text-transform: none;margin-left: 20px; " center color="very"  >Account settings</button></div>  \n  \n\n\n\n</ion-content>\n<ion-footer>\n  <ion-toolbar color="col">\n    <ion-tabs color="col" (click)="tabs()" >\n      <ion-tab [root]="tab1Root"   tabIcon="customicon6" ></ion-tab>\n      <ion-tab [root]="tab2Root"   tabIcon="customicon7"></ion-tab>\n      <ion-tab [root]="tab3Root"   tabIcon="customicon8" (ionSelect)="profile()"></ion-tab>\n      <ion-tab [root]="tab4Root"   tabIcon="customicon9"></ion-tab>\n    </ion-tabs>\n    \n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\post\post.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], PostPage);
    return PostPage;
}());

//# sourceMappingURL=post.js.map

/***/ }),

/***/ 123:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewgroupchannelPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_groups_groupschannel__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_imghandler_imghandler__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__groupschannel_groupschannel__ = __webpack_require__(218);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the NewgroupchannelPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var NewgroupchannelPage = /** @class */ (function () {
    function NewgroupchannelPage(camera, navCtrl, platform, navParams, alertCtrl, groupProvider, imghandler, loadingCtrl) {
        this.camera = camera;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.groupProvider = groupProvider;
        this.imghandler = imghandler;
        this.loadingCtrl = loadingCtrl;
        this.group = {
            Name: 'Group Name',
            Picture: 'https://firebasestorage.googleapis.com/v0/b/chat-app-f6a12.appspot.com/o/user.png?alt=media&token=79419e48-423a-42c3-92b2-16027651b931'
        };
    }
    NewgroupchannelPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    NewgroupchannelPage.prototype.editName = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Channel Name',
            message: 'Enter a name for this channel',
            inputs: [
                {
                    name: 'Name',
                    placeholder: 'Group Name'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    cssClass: 'half-alert-button',
                    handler: function (data) {
                        _this.showToast('Cancel');
                    }
                },
                {
                    text: 'Save',
                    cssClass: 'half-alert-button',
                    handler: function (data) {
                        var res = data.Name;
                        var result = res.trim();
                        if (result == '') {
                            _this.showToast('Enter Channel Name');
                        }
                        else {
                            _this.group.Name = result;
                        }
                    }
                }
            ]
        });
        prompt.present();
    };
    NewgroupchannelPage.prototype.selectPicture = function () {
        var _this = this;
        var load = this.loadingCtrl.create({
            content: 'Uploading channel picture ....'
        });
        var cameraoptions = {
            quality: 50,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            encodingType: this.camera.EncodingType.PNG,
            destinationType: this.camera.DestinationType.DATA_URL
        };
        this.camera.getPicture(cameraoptions).then(function (ImageData) {
            _this.captureDataUrl = 'data:image/jpeg;base64,' + ImageData;
            load.present();
            _this.groupProvider.uploadGroupPicture(_this.captureDataUrl).then(function () {
                _this.group.Picture = _this.captureDataUrl;
                load.dismiss();
            }).catch(function (err) {
                load.dismiss();
                _this.showToast(err);
            });
        }, function (err) {
            var res = JSON.stringify(err);
            load.dismiss();
            _this.showToast(res);
        });
    };
    NewgroupchannelPage.prototype.createGroup = function () {
        var _this = this;
        var load = this.loadingCtrl.create({
            content: 'Creating Channel ....'
        });
        if (this.group.Picture == 'hsttps://firebasestorage.googleapis.com/v0/b/chat-app-f6a12.appspot.com/o/user.png?alt=media&token=79419e48-423a-42c3-92b2-16027651b931') {
            console.log('Select Channel Picture');
        }
        else {
            if (this.group.Name == 'Channel Name') {
                console.log('Enter Channel Name');
            }
            else {
                load.present();
                this.groupProvider.creaeGroup(this.group).then(function () {
                    load.dismiss();
                    console.log('Channel has been created');
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__groupschannel_groupschannel__["a" /* GroupschannelPage */]);
                }).catch(function (err) {
                    load.dismiss();
                    console.log(err);
                });
            }
        }
    };
    NewgroupchannelPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-newgroupchannel',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\newgroupchannel\newgroupchannel.html"*/'<!--\n\n  Generated template for the NewgroupchannelPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar color="logo">\n\n    <ion-title>Add a New Channel</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <div class="body-content">\n\n    <ion-list>\n\n  \n\n      <br>\n\n      <br>\n\n      \n\n      <ion-avatar>\n\n        <img src="{{group.Picture}}" (click)="selectPicture()">\n\n      </ion-avatar>\n\n  \n\n      <br>\n\n      <br>\n\n  \n\n  \n\n      <h2 (click)="editName()">{{group.Name}} <ion-icon color="header" name="create"></ion-icon></h2>\n\n  \n\n      <br>\n\n      <br>\n\n  \n\n      <div>\n\n        <p>Tap on group picture or name to change it</p>\n\n      </div>\n\n  \n\n  \n\n      <br>\n\n      <br>\n\n  \n\n      <button ion-button color="header" block (click)="createGroup()">\n\n        Create Channel\n\n      </button>\n\n  \n\n  \n\n  \n\n    </ion-list>\n\n  </div>\n\n  \n\n  \n\n  \n\n  \n\n  \n\n  \n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\newgroupchannel\newgroupchannel.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_groups_groupschannel__["a" /* GroupschannelProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_imghandler_imghandler__["a" /* ImghandlerProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */]])
    ], NewgroupchannelPage);
    return NewgroupchannelPage;
}());

//# sourceMappingURL=newgroupchannel.js.map

/***/ }),

/***/ 173:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the UserProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var UserProvider = /** @class */ (function () {
    function UserProvider(afireauth) {
        this.afireauth = afireauth;
        this.firedata = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/users');
    }
    /*
    Adds a new user to the system.
    Called from - signup.ts
    Inputs - The new user object containing the email, password and displayName.
    Outputs - Promise.
    
     */
    UserProvider.prototype.adduser = function (newuser) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afireauth.auth.createUserWithEmailAndPassword(newuser.email, newuser.password).then(function () {
                _this.afireauth.auth.currentUser.updateProfile({
                    displayName: newuser.displayName,
                    photoURL: 'https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/pictures%2Freport?alt=media&token=db066b34-ec95-4281-8a60-c74592e2b6bb'
                }).then(function () {
                    _this.firedata.child(_this.afireauth.auth.currentUser.uid).set({
                        uid: _this.afireauth.auth.currentUser.uid,
                        displayName: newuser.displayName,
                        photoURL: 'https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/pictures%2Freport?alt=media&token=db066b34-ec95-4281-8a60-c74592e2b6bb'
                    }).then(function () {
                        resolve({ success: true });
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    /*
    For resetting the password of the user.
    Called from - passwordreset.ts
    Inputs - email of the user.
    Output - Promise.
    
     */
    UserProvider.prototype.passwordreset = function (email) {
        var promise = new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().sendPasswordResetEmail(email).then(function () {
                resolve({ success: true });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    /*
    
    For updating the users collection and the firebase users list with
    the imageurl of the profile picture stored in firebase storage.
    Called from - profilepic.ts
    Inputs - Url of the image stored in firebase.
    OUtputs - Promise.
    
    */
    UserProvider.prototype.updateimage = function (imageurl) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afireauth.auth.currentUser.updateProfile({
                displayName: _this.afireauth.auth.currentUser.displayName,
                photoURL: imageurl
            }).then(function () {
                __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('/users/' + __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).update({
                    displayName: _this.afireauth.auth.currentUser.displayName,
                    photoURL: imageurl,
                    uid: __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid
                }).then(function () {
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.getuserdetails = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.child(__WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).once('value', function (snapshot) {
                resolve(snapshot.val());
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.updatedisplayname = function (newname) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afireauth.auth.currentUser.updateProfile({
                displayName: newname,
                photoURL: _this.afireauth.auth.currentUser.photoURL
            }).then(function () {
                _this.firedata.child(__WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).update({
                    displayName: newname,
                    photoURL: _this.afireauth.auth.currentUser.photoURL,
                    uid: _this.afireauth.auth.currentUser.uid
                }).then(function () {
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.updatel = function (newname) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('profile/' + __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).update({
                lastName: newname,
            }).then(function () {
                _this.firedata.child(__WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).update({
                    lastName: newname,
                    uid: _this.afireauth.auth.currentUser.uid
                }).then(function () {
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.updatefirst = function (newname) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('profile/' + __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).update({
                firstName: newname,
            }).then(function () {
                _this.firedata.child(__WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).update({
                    firstName: newname,
                    uid: _this.afireauth.auth.currentUser.uid
                }).then(function () {
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.updatea = function (newname) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('profile/' + __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).update({
                accountNo: newname,
            }).then(function () {
                _this.firedata.child(__WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).update({
                    accountNo: newname,
                    uid: _this.afireauth.auth.currentUser.uid
                }).then(function () {
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.updaten = function (newname) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('profile/' + __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).update({
                number: newname,
            }).then(function () {
                _this.firedata.child(__WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).update({
                    number: newname,
                    uid: _this.afireauth.auth.currentUser.uid
                }).then(function () {
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.updateidn = function (newname) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('profile/' + __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).update({
                idNo: newname,
            }).then(function () {
                _this.firedata.child(__WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).update({
                    idNo: newname,
                    uid: _this.afireauth.auth.currentUser.uid
                }).then(function () {
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.updateadd = function (newname) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.database().ref('profile/' + __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).update({
                address: newname,
            }).then(function () {
                _this.firedata.child(__WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).update({
                    address: newname,
                    uid: _this.afireauth.auth.currentUser.uid
                }).then(function () {
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider.prototype.getallusers = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.orderByChild('uid').once('value', function (snapshot) {
                var userdata = snapshot.val();
                var temparr = [];
                for (var key in userdata) {
                    temparr.push(userdata[key]);
                }
                resolve(temparr);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["a" /* AngularFireAuth */]])
    ], UserProvider);
    return UserProvider;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 179:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImghandlerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_file_chooser__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the ImghandlerProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ImghandlerProvider = /** @class */ (function () {
    function ImghandlerProvider(filechooser) {
        this.filechooser = filechooser;
        this.firestore = __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.storage();
    }
    /*
    
    For uploading an image to firebase storage.
   
    Called from - profilepic.ts
    Inputs - None.
    Outputs - The image url of the stored image.
     
     */
    ImghandlerProvider.prototype.uploadimage = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.filechooser.open().then(function (url) {
                window.FilePath.resolveNativePath(url, function (result) {
                    _this.nativepath = result;
                    window.resolveLocalFileSystemURL(_this.nativepath, function (res) {
                        res.file(function (resFile) {
                            var reader = new FileReader();
                            reader.readAsArrayBuffer(resFile);
                            reader.onloadend = function (evt) {
                                var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                                var imageStore = _this.firestore.ref('/profileimages').child(__WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid);
                                imageStore.put(imgBlob).then(function (res) {
                                    _this.firestore.ref('/profileimages').child(__WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).getDownloadURL().then(function (url) {
                                        resolve(url);
                                    }).catch(function (err) {
                                        reject(err);
                                    });
                                }).catch(function (err) {
                                    reject(err);
                                });
                            };
                        });
                    });
                });
            });
        });
        return promise;
    };
    ImghandlerProvider.prototype.grouppicstore = function (groupname) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.filechooser.open().then(function (url) {
                window.FilePath.resolveNativePath(url, function (result) {
                    _this.nativepath = result;
                    window.resolveLocalFileSystemURL(_this.nativepath, function (res) {
                        res.file(function (resFile) {
                            var reader = new FileReader();
                            reader.readAsArrayBuffer(resFile);
                            reader.onloadend = function (evt) {
                                var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                                var imageStore = _this.firestore.ref('/groupimages').child(__WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).child(groupname);
                                imageStore.put(imgBlob).then(function (res) {
                                    _this.firestore.ref('/profileimages').child(__WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).child(groupname).getDownloadURL().then(function (url) {
                                        resolve(url);
                                    }).catch(function (err) {
                                        reject(err);
                                    });
                                }).catch(function (err) {
                                    reject(err);
                                });
                            };
                        });
                    });
                });
            });
        });
        return promise;
    };
    ImghandlerProvider.prototype.picmsgstore = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.filechooser.open().then(function (url) {
                window.FilePath.resolveNativePath(url, function (result) {
                    _this.nativepath = result;
                    window.resolveLocalFileSystemURL(_this.nativepath, function (res) {
                        res.file(function (resFile) {
                            var reader = new FileReader();
                            reader.readAsArrayBuffer(resFile);
                            reader.onloadend = function (evt) {
                                var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                                var uuid = _this.guid();
                                var imageStore = _this.firestore.ref('/picmsgs').child(__WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid).child('picmsg' + uuid);
                                imageStore.put(imgBlob).then(function (res) {
                                    resolve(res.downloadURL);
                                }).catch(function (err) {
                                    reject(err);
                                })
                                    .catch(function (err) {
                                    reject(err);
                                });
                            };
                        });
                    });
                });
            });
        });
        return promise;
    };
    ImghandlerProvider.prototype.guid = function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };
    ImghandlerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_file_chooser__["a" /* FileChooser */]])
    ], ImghandlerProvider);
    return ImghandlerProvider;
}());

//# sourceMappingURL=imghandler.js.map

/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditprofilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EditprofilePage = /** @class */ (function () {
    function EditprofilePage(navCtrl, loadCtrl, platform, navParams, authProvider) {
        this.navCtrl = navCtrl;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.authProvider = authProvider;
        this.userDetails = {
            Name: '',
            Email: '',
            Phone: ''
        };
        this.userDetails = this.navParams.get('userDetails');
        var res = this.navParams.get('userDetails');
        this.userDetails.Name = res.Name;
        this.userDetails.Email = res.Email;
        this.userDetails.Phone = res.Phone;
    }
    EditprofilePage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    EditprofilePage.prototype.updateProfile = function () {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Processing ....'
        });
        if (this.userDetails.Name.length < 6) {
            this.showToast('Name should be at least 6 characters');
        }
        else {
            if (this.userDetails.Email == '') {
                this.showToast('Email should be xxxx@megatech.co.za');
            }
            else {
                if (this.userDetails.Phone == '') {
                    this.showToast('Number should be 10 digit');
                }
                else {
                    load.present();
                    this.authProvider.updateProfile(this.userDetails).then(function () {
                        load.dismiss();
                        _this.navCtrl.pop();
                        _this.showToast('Details have been updated');
                    }).catch(function (err) {
                        load.dismiss();
                        _this.showToast(err);
                    });
                }
            }
        }
    };
    EditprofilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-editprofile',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\editprofile\editprofile.html"*/'<ion-header>\n\n  <ion-navbar color="logo">\n    <ion-title>Edit Profile</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n\n	<ion-item>\n		<ion-label floating><ion-icon name="person"></ion-icon> Name</ion-label>\n		<ion-input type="text" [(ngModel)]="userDetails.Name" required></ion-input>\n	</ion-item>\n\n	<ion-item>\n		<ion-label floating><ion-icon name="mail"></ion-icon> Email</ion-label>\n		<ion-input type="text" [(ngModel)]="userDetails.Email" required></ion-input>\n	</ion-item>\n\n\n	<ion-item>\n		<ion-label floating><ion-icon name="call"></ion-icon>Phone</ion-label>\n		<ion-textarea type="number" [(ngModel)]="userDetails.Phone" required></ion-textarea>\n	</ion-item>\n\n\n	<br>\n	<br>\n	\n	<button ion-button block color="header" (click)="updateProfile()">\n		Update\n	</button>\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\editprofile\editprofile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */]])
    ], EditprofilePage);
    return EditprofilePage;
}());

//# sourceMappingURL=editprofile.js.map

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatbodyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_chat_chat__ = __webpack_require__(92);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { SuperTabsController } from 'ionic2-super-tabs';


var ChatbodyPage = /** @class */ (function () {
    function ChatbodyPage(loadCtrl, platform, chatProvider, alertCtrl, authProvider, navCtrl, navParams, ngZone, events) {
        var _this = this;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.chatProvider = chatProvider;
        this.alertCtrl = alertCtrl;
        this.authProvider = authProvider;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ngZone = ngZone;
        this.events = events;
        this.details = {};
        this.myDetails = {};
        this.Conversations = [];
        this.onlineStatus = 'Online';
        this.offlineStatus = 'Offline';
        this.newMessage = {
            body: ''
        };
        this.allMessages = [];
        /*    var days = ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri']
           var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
       
           var today = new Date
           var toYear = today.getFullYear()
           var toMonth = 1 + today.getMonth()
           var toDays = today.getDate()
       
           var todayRes1 = toYear + '/' + toMonth + '/' + toDays
           var todayRes2 = toYear + '/' + toMonth
           var todayRes3 = toYear
       
       
        */
        this.events.subscribe('Conversations', function () {
            _this.ngZone.run(function () {
                _this.looding = false;
                _this.allMessages = _this.chatProvider.allMessages;
                _this.allUsers = _this.chatProvider.buddyUsers;
                for (var key in _this.allMessages) {
                    var d = new Date(_this.allMessages[key].Time);
                    var years = d.getFullYear();
                    var month = d.getMonth();
                    var day = d.getDate();
                    var hours = d.getHours();
                    var minutes = '0' + d.getMinutes();
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    var formattedTime = monthNames[month] + "-" + day + "-" + hours + ":" + minutes.substr(-2);
                    _this.allMessages[key].Time = formattedTime;
                }
            });
        });
        this.userDetails = this.navParams.get('userDetails');
        this.events.subscribe('ProfileDetails', function () {
            _this.ngZone.run(function () {
                _this.details = _this.authProvider.ProfileDetails;
            });
        });
        this.events.subscribe('myDetails', function () {
            _this.ngZone.run(function () {
                _this.myDetails = _this.authProvider.myDetails;
            });
        });
        this.events.subscribe('messages', function () {
            _this.ngZone.run(function () {
                _this.allMessages = _this.chatProvider.allMessages;
            });
        });
        if (this.allMessages.length > 6) {
            setTimeout(function () {
                for (var i = 0; i < 10; i++) {
                    _this.allMessages[i];
                }
            }, 300);
        }
    }
    ChatbodyPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    /* callFunction(){
      this.content.scrollToBottom(0)
    } */
    ChatbodyPage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('ProfileDetails');
        this.events.subscribe('myDetails');
        this.events.subscribe('messages');
    };
    ChatbodyPage.prototype.ionViewDidEnter = function () {
        this.authProvider.getProfileDetails(this.userDetails);
        this.authProvider.getMyDetails();
        this.chatProvider.getMessages(this.userDetails);
    };
    ChatbodyPage.prototype.sendMessage = function () {
        var _this = this;
        var res = this.newMessage.body;
        var res1 = res.trim();
        if (res1 == '') {
            this.showToast("can't send empty message");
            this.newMessage.body = '';
        }
        else {
            this.chatProvider.sendMessage(this.newMessage).then(function () {
                _this.newMessage.body = '';
            }).catch(function (err) {
                console.log(err);
            });
        }
    };
    ChatbodyPage.prototype.showMessageConfirm = function (message, myDetails, friendDetails) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Message',
            message: 'Tap On Option',
            buttons: [
                {
                    text: 'Delete For Me',
                    handler: function () {
                        _this.deleteMessageForMe(message, myDetails, friendDetails);
                    }
                },
                {
                    text: 'Delete For All',
                    handler: function () {
                        _this.deleteMessageForAll(message, myDetails, friendDetails);
                    }
                },
                {
                    text: 'Cancel',
                    handler: function () {
                        _this.showToast('Cancel');
                    }
                }
            ]
        });
        confirm.present();
    };
    ChatbodyPage.prototype.copyMessage = function (message) {
        var _this = this;
        this.clipboard.copy(message.Body).then(function () {
            _this.showToast('Message coppied to clipboard');
        });
    };
    ChatbodyPage.prototype.deleteMessageForMe = function (message, myDetails, friendDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Deleting Message ...'
        });
        load.present();
        this.chatProvider.deleteMessageForMe(message, myDetails, friendDetails).then(function () {
            load.dismiss();
            _this.showToast('Message has been deleted');
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    ChatbodyPage.prototype.deleteMessageForAll = function (message, myDetails, friendDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Deleting Message ...'
        });
        load.present();
        this.chatProvider.deleteMessageForAll(message, myDetails, friendDetails).then(function () {
            load.dismiss();
            _this.showToast('Message has been deleted');
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], ChatbodyPage.prototype, "content", void 0);
    ChatbodyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-chatbody',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\chatbody\chatbody.html"*/'<ion-header>\n\n  <ion-navbar color="logo">\n    <ion-title>{{details.Name}}</ion-title>\n    <div class="online" *ngIf="details.Status == onlineStatus"></div>\n    <div class="offline" *ngIf="details.Status == offlineStatus"></div>\n    <span class="Status" style="margin-left: 70px;">{{details.Status}}</span>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content >\n\n<div>\n	<ion-list no-lines>\n		<ion-item *ngFor="let message of allMessages; let last = last" (press)="showMessageConfirm(message, myDetails, details)">\n\n			<ion-avatar item-right *ngIf="message.Id != details.Id">\n				<img src="{{myDetails.Photo}}">\n			</ion-avatar>\n			<div class="bubble you" *ngIf="message.Id != details.Id">\n				<h3 text-wrap ion-text color="light">{{message.Body}}</h3>\n				<div class="spacer" style="height: 10px;"></div>\n\n				<div><h3 text-wrap ion-text color="light" style="margin-left: 50px;" >{{message.Time | date:\'short\'}}</h3></div>\n\n			</div>\n\n\n			<ion-avatar item-left *ngIf="message.Id === details.Id">\n				<img src="{{details.Photo}}">\n			</ion-avatar>\n			<div class="bubble me" *ngIf="message.Id === details.Id">\n				<h3 text-wrap ion-text color="light">{{message.Body}}</h3>\n				<div class="spacer" style="height: 10px;"></div>\n\n				<h3 text-wrap ion-text color="light" style="margin-left: 50px;">{{message.Time | date:\'short\'}}</h3>\n\n\n			</div>\n\n\n		</ion-item>\n	</ion-list>\n</div>\n\n</ion-content>\n\n<ion-footer>\n	<ion-toolbar color="col">\n		<ion-textarea type="text"  round block outline color="light"  placeholder="type your message" ion-text color="light" [(ngModel)]="newMessage.body"></ion-textarea>\n		<ion-buttons end>\n			<button ion-button ion-icon icon-only (click)="sendMessage()">\n				<ion-icon name="send"></ion-icon>\n			</button>\n		</ion-buttons>\n	</ion-toolbar>\n</ion-footer>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\chatbody\chatbody.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__providers_chat_chat__["a" /* ChatProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], ChatbodyPage);
    return ChatbodyPage;
}());

//# sourceMappingURL=chatbody.js.map

/***/ }),

/***/ 183:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountProfilePagecotcot; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__timeline_timeline__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_reactions_reactions__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_opener__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__add_comment_add_comment__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angularfire2_database__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__tabs_tabs__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__donation_donation__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







//import { CommentPage } from '../comment/comment';




var AccountProfilePagecotcot = /** @class */ (function () {
    function AccountProfilePagecotcot(navCtrl, ngZone, events, menu, file, db, navParams, fileOpener, platform, socialSharing, popoverCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.ngZone = ngZone;
        this.events = events;
        this.menu = menu;
        this.file = file;
        this.db = db;
        this.navParams = navParams;
        this.fileOpener = fileOpener;
        this.socialSharing = socialSharing;
        this.popoverCtrl = popoverCtrl;
        this.modalCtrl = modalCtrl;
        this.stories = new Array();
        this.username = '';
        this.message = '';
        this.messages = [];
        window.addEventListener("contextmenu", function (e) { e.preventDefault(); });
        this.LikeValue = 0;
        this.Like = 0;
        this.comment = 6;
        this.comments = 6;
        // this.getQuotes();
        var storyItem1 = {
            userPicture: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.04.jpeg?alt=media&token=377986ea-ce66-460a-b56e-3a15d10a42a1",
            userId: 1,
            userName: "Bishop Joshua",
            currentItem: 0,
            items: [{
                    date: "12/08/20",
                    duration: "3",
                    id: "3",
                    media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.25.58.jpeg?alt=media&token=b5a120a4-2977-4256-b93c-8f54dad845f8",
                    seen: false,
                    type: "0",
                    views: 5
                }],
            seen: true
        };
        var storyItem2 = {
            userPicture: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.00.jpeg?alt=media&token=ffc0d97e-5432-4b18-874e-be0b10424c45",
            userId: 2,
            userName: "Pastor James",
            currentItem: 0,
            seen: false,
            items: [{
                    date: "12/08/20",
                    duration: "4",
                    id: "64",
                    media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.02%20(1).jpeg?alt=media&token=930ddfd3-f935-4633-a551-a91fe8eb16be",
                    seen: false,
                    type: "0",
                    views: null
                }, {
                    date: "12/08/20",
                    duration: "3",
                    id: "74",
                    media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.02.jpeg?alt=media&token=35c25663-4614-4f9e-bb78-09362891427d",
                    seen: false,
                    type: "0",
                    views: null
                }, {
                    date: "11/08/20",
                    duration: null,
                    id: "84",
                    media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.03%20(1).jpeg?alt=media&token=268fe7e6-3d4d-497b-9361-dae30eb6636f",
                    seen: false,
                    type: "1",
                    views: null
                }]
        };
        var storyItem3 = {
            userPicture: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.03%20(2).jpeg?alt=media&token=44a21fa6-db47-4c64-979c-0f475b815af1",
            userId: 1,
            userName: "prophet MJB",
            currentItem: 0,
            items: [{
                    date: "20 minutes",
                    duration: "5",
                    id: "3",
                    media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.03.jpeg?alt=media&token=de137fc9-466c-41bf-b2f5-c506f3cea61e",
                    seen: true,
                    type: "0",
                    views: 5
                }],
            seen: true
        };
        var storyItem4 = {
            userPicture: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.04%20(1).jpeg?alt=media&token=af620806-2cb6-4603-9418-d9f337e3bc65",
            userId: 2,
            userName: "Pastor TK",
            currentItem: 0,
            seen: false,
            items: [{
                    date: "30 minutes",
                    duration: "4",
                    id: "64",
                    media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.04%20(2).jpeg?alt=media&token=959ae903-261c-4dbc-b860-3f14652d1286",
                    seen: false,
                    type: "0",
                    views: null
                }, {
                    date: "há 30 minutos",
                    duration: "3",
                    id: "74",
                    media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.04%20(3).jpeg?alt=media&token=552e4beb-b249-4461-82e8-63d43b9506fe",
                    seen: false,
                    type: "0",
                    views: null
                }, {
                    date: "há 1 hora",
                    duration: null,
                    id: "84",
                    media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.03.jpeg?alt=media&token=de137fc9-466c-41bf-b2f5-c506f3cea61e",
                    seen: false,
                    type: "1",
                    views: null
                }]
        };
        var storyItem5 = {
            userPicture: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.04%20(2).jpeg?alt=media&token=959ae903-261c-4dbc-b860-3f14652d1286",
            userId: 1,
            userName: "AFM Youth",
            currentItem: 0,
            items: [{
                    date: "há 20 minutos",
                    duration: "5",
                    id: "3",
                    media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.04%20(1).jpeg?alt=media&token=af620806-2cb6-4603-9418-d9f337e3bc65",
                    seen: true,
                    type: "0",
                    views: 5
                }],
            seen: true
        };
        var storyItem6 = {
            userPicture: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.03%20(1).jpeg?alt=media&token=268fe7e6-3d4d-497b-9361-dae30eb6636f",
            userId: 2,
            userName: "Pastor Moses",
            currentItem: 0,
            seen: false,
            items: [{
                    date: "há 30 minutos",
                    duration: "4",
                    id: "64",
                    media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.04.jpeg?alt=media&token=377986ea-ce66-460a-b56e-3a15d10a42a1",
                    seen: false,
                    type: "0",
                    views: null
                }, {
                    date: "há 30 minutos",
                    duration: "3",
                    id: "74",
                    media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.25.51%20(1).jpeg?alt=media&token=13e1427e-5105-4715-85e8-9c9b80c5ec3a",
                    seen: false,
                    type: "0",
                    views: null
                }, {
                    date: "há 1 hora",
                    duration: null,
                    id: "84",
                    media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.03%20(1).jpeg?alt=media&token=268fe7e6-3d4d-497b-9361-dae30eb6636f",
                    seen: false,
                    type: "1",
                    views: null
                }]
        };
        this.stories.push(storyItem1);
        this.stories.push(storyItem2);
        this.stories.push(storyItem3);
        this.stories.push(storyItem4);
        this.stories.push(storyItem5);
        this.stories.push(storyItem6);
        this.reorderStories();
    }
    AccountProfilePagecotcot.prototype.openStoryViewer = function (index) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__timeline_timeline__["a" /* TimelinePage */], { stories: this.stories, tapped: index });
        modal.onDidDismiss(function () {
            _this.reorderStories();
        });
        modal.present();
    };
    AccountProfilePagecotcot.prototype.reorderStories = function () {
        this.stories.sort(function (a, b) {
            if (a.seen)
                return 1;
            if (b.seen)
                return -1;
            return 0;
        });
    };
    AccountProfilePagecotcot.prototype.showReactions = function (ev) {
        var reactions = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__pages_reactions_reactions__["a" /* ReactionsPage */]);
        reactions.present({
            ev: ev
        });
    };
    AccountProfilePagecotcot.prototype.handleLike = function () {
        this.LikeValue++;
    };
    AccountProfilePagecotcot.prototype.handlingLike = function () {
        this.Like++;
    };
    AccountProfilePagecotcot.prototype.saveImg = function () {
        var _this = this;
        var imageName = "logo.jpg";
        var ROOT_DIRECTORY = 'file:///sdcard//';
        var downloadFolderName = 'tempDownloadFolder';
        //Create a folder in memory location
        this.file.createDir(ROOT_DIRECTORY, downloadFolderName, true)
            .then(function (entries) {
            //Copy our asset/img/FreakyJolly.jpg to folder we created
            _this.file.copyFile(_this.file.applicationDirectory + "www/assets/imgs/", imageName, ROOT_DIRECTORY + downloadFolderName + '//', imageName)
                .then(function (entries) {
                //Open copied file in device's default viewer
                _this.fileOpener.open(ROOT_DIRECTORY + downloadFolderName + "/" + imageName, 'image/jpeg')
                    .then(function () { return console.log('File is opened'); })
                    .catch(function (e) { return alert('Error' + JSON.stringify(e)); });
            })
                .catch(function (error) {
                alert('error ' + JSON.stringify(error));
            });
        })
            .catch(function (error) {
            alert('error' + JSON.stringify(error));
        });
    };
    AccountProfilePagecotcot.prototype.shareImg = function () {
        var _this = this;
        var imageName = "logo.jpg";
        var ROOT_DIRECTORY = 'file:///sdcard//';
        var downloadFolderName = 'tempDownloadFolder';
        //Create a folder in memory location
        this.file.createDir(ROOT_DIRECTORY, downloadFolderName, true)
            .then(function (entries) {
            //Copy our asset/img/FreakyJolly.jpg to folder we created
            _this.file.copyFile(_this.file.applicationDirectory + "www/assets/imgs/", imageName, ROOT_DIRECTORY + downloadFolderName + '//', imageName)
                .then(function (entries) {
                //Common sharing event will open all available application to share
                _this.socialSharing.share("Message", "Subject", ROOT_DIRECTORY + downloadFolderName + "/" + imageName, imageName)
                    .then(function (entries) {
                    console.log('success ' + JSON.stringify(entries));
                })
                    .catch(function (error) {
                    alert('error ' + JSON.stringify(error));
                });
            })
                .catch(function (error) {
                alert('error ' + JSON.stringify(error));
            });
        })
            .catch(function (error) {
            alert('error ' + JSON.stringify(error));
        });
    };
    AccountProfilePagecotcot.prototype.shareImgg = function () {
        var _this = this;
        var imageName = "gender.jpg";
        var ROOT_DIRECTORY = 'file:///sdcard//';
        var downloadFolderName = 'tempDownloadFolder';
        //Create a folder in memory location
        this.file.createDir(ROOT_DIRECTORY, downloadFolderName, true)
            .then(function (entries) {
            //Copy our asset/img/FreakyJolly.jpg to folder we created
            _this.file.copyFile(_this.file.applicationDirectory + "www/assets/imgs/", imageName, ROOT_DIRECTORY + downloadFolderName + '//', imageName)
                .then(function (entries) {
                //Common sharing event will open all available application to share
                _this.socialSharing.share("Message", "Subject", ROOT_DIRECTORY + downloadFolderName + "/" + imageName, imageName)
                    .then(function (entries) {
                    console.log('success ' + JSON.stringify(entries));
                })
                    .catch(function (error) {
                    alert('error ' + JSON.stringify(error));
                });
            })
                .catch(function (error) {
                alert('error ' + JSON.stringify(error));
            });
        })
            .catch(function (error) {
            alert('error ' + JSON.stringify(error));
        });
    };
    AccountProfilePagecotcot.prototype.toCommentSection = function () {
        var commentsModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__add_comment_add_comment__["a" /* AddCommentPage */]);
        commentsModal.present();
        this.comments++;
    };
    AccountProfilePagecotcot.prototype.toCommentSections = function () {
        var commentsModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__add_comment_add_comment__["a" /* AddCommentPage */]);
        commentsModal.present();
        this.comment++;
    };
    AccountProfilePagecotcot.prototype.call = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__tabs_tabs__["a" /* TabsPage */]);
    };
    AccountProfilePagecotcot.prototype.donate = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__donation_donation__["a" /* DonationPage */]);
    };
    AccountProfilePagecotcot = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-accountprofilecot',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\accountprofilecot\accountprofilecot.html"*/'<ion-header>\n\n	<ion-navbar color="logo">\n\n	 <button ion-button menuToggle>\n\n	  <ion-icon name="menu"></ion-icon>\n\n	 </button>\n\n	 <ion-title>\n\n        Posts\n\n	 </ion-title>\n\n	</ion-navbar>\n\n   </ion-header>\n\n\n\n<ion-content>\n\n  <ion-searchbar  outline color="logo" \n\n  [(ngModel)]="searchTerm" \n\n  (ionChange)="setFilteredItems()"   placeholder="Search posts by name or post content" \n\n></ion-searchbar>\n\n  <ion-scroll scrollX="true" text-nowrap margin-top>\n\n    <div class="story" text-center *ngFor="let story of stories; let index = index" [ngClass]="!story.seen ? \'unseen\' : \'\'" (click)="openStoryViewer(index)">\n\n      <img [src]="story.userPicture" [alt]="\'Story de\' + story.userName">\n\n<div>\n\n	 <span class="block">{{ story.userName }}</span>\n\n</div>\n\n     \n\n	</div>\n\n	\n\n  </ion-scroll>\n\n  <ion-label color="col" style="font-size: 18px; font-weight: bold; margin-left: 15px;">Latest Posts</ion-label>\n\n  <ion-card>\n\n	 \n\n\n\n	  <ion-item>\n\n	    <ion-avatar item-start>\n\n	      <img src="https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.25.58.jpeg?alt=media&token=b5a120a4-2977-4256-b93c-8f54dad845f8">\n\n	    </ion-avatar>\n\n	    <h2>Prophet Mike</h2>\n\n	    <p>August 10, 20</p>\n\n	  </ion-item>\n\n\n\n	  <img src="/assets/imgs/WhatsApp Image 2020-08-12 at 07.26.04 (3).jpeg" />\n\n\n\n	  <ion-card-content>\n\n	    <p>Wait a minute. Wait a minute, Doc. Uhhh... Are you telling me that you built a time machine... out of a DeLorean?! Whoa. This is heavy.</p>\n\n	  </ion-card-content>\n\n\n\n	  <ion-row>\n\n	    <ion-col>\n\n        <button (click)="handleLike()" (press)="showReactions($event)" ion-button icon-left clear small>\n\n			<img src="assets/icons8-heart.png" style="height: 25px; width: 25px;" />{{LikeValue}}\n\n	      </button>\n\n	    </ion-col>\n\n	    <ion-col style="margin-right: -30px;">\n\n	      <button ion-button icon-left clear small (click)="toCommentSection()" >\n\n			<img src="assets/icons8-comments.png" style="height: 20px; width: 20px; " />\n\n	      </button>\n\n	    </ion-col>\n\n	    <ion-col  style="margin-right: -100px;">\n\n        <button ion-button icon-left clear small (click)="shareImg(img)"  style="margin-right: 20px;">\n\n			<img src="assets/icons8-share.png" style="height: 20px; width: 20px; " />\n\n	       \n\n	      </button>\n\n	    </ion-col>\n\n	  </ion-row>\n\n\n\n  </ion-card>\n\n  <ion-card>\n\n\n\n	  <ion-item>\n\n	    <ion-avatar item-start>\n\n	      <img src="https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.00.jpeg?alt=media&token=ffc0d97e-5432-4b18-874e-be0b10424c45">\n\n	    </ion-avatar>\n\n	    <h2>AFM Youth</h2>\n\n	    <p>August 11, 20</p>\n\n	  </ion-item>\n\n\n\n	  <img src="/assets/imgs/WhatsApp Image 2020-08-12 at 07.26.05.jpeg" />\n\n\n\n	  <ion-card-content>\n\n	    <p>Gender-based violence (GBV) is violence that is directed at an individual based on his or her biological sex OR gender identity. It includes physical, sexual, verbal, emotional, and psychological abuse, threats, coercion, and economic or educational deprivation, whether occurring in public or private life. </p>\n\n	  </ion-card-content>\n\n\n\n	  <ion-row>\n\n	    <ion-col>\n\n        <button (click)="handlingLike()" (press)="showReactions($event)" ion-button icon-left clear small>\n\n			<img src="assets/icons8-heart.png" style="height: 25px; width: 25px;" />{{Like}}\n\n\n\n	      </button>\n\n	    </ion-col>\n\n	    <ion-col style="margin-right: -30px;">\n\n	      <button ion-button icon-left clear small (click)="toCommentSection()">\n\n			<img src="assets/icons8-comments.png" style="height: 20px; width: 20px;" />\n\n	      </button>\n\n	    </ion-col>\n\n	    <ion-col style="margin-right: -100px;">\n\n        <button ion-button icon-left clear small (click)="shareImgg(img)">\n\n			<img src="assets/icons8-share.png" style="height: 20px; width: 20px;" />\n\n	       \n\n	      </button>\n\n	    </ion-col>\n\n	  </ion-row>\n\n\n\n  </ion-card>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\accountprofilecot\accountprofilecot.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* MenuController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_8_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_opener__["a" /* FileOpener */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ModalController */]])
    ], AccountProfilePagecotcot);
    return AccountProfilePagecotcot;
}());

//# sourceMappingURL=accountprofilecot.js.map

/***/ }),

/***/ 185:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_friends_friends__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__users_users__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__chatbody_chatbody__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__ = __webpack_require__(92);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { SuperTabsController } from 'ionic2-super-tabs';





var ChatPage = /** @class */ (function () {
    function ChatPage(chatProvider, myModal, authProvider, ngZone, events, friendsProvider, navCtrl, navParams) {
        var _this = this;
        this.chatProvider = chatProvider;
        this.myModal = myModal;
        this.authProvider = authProvider;
        this.ngZone = ngZone;
        this.events = events;
        this.friendsProvider = friendsProvider;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Friends = [];
        this.allUsers = [];
        this.Conversations = [];
        this.onlineStatus = 'Online';
        this.offlineStatus = "Offline";
        this.looding = true;
        this.events.subscribe('Friends', function () {
            _this.ngZone.run(function () {
                _this.Friends = _this.friendsProvider.Friends;
            });
        });
        this.events.subscribe('Conversations', function () {
            _this.ngZone.run(function () {
                _this.looding = false;
                _this.Conversations = _this.chatProvider.Conversations;
                _this.allUsers = _this.chatProvider.buddyUsers;
                console.log(_this.Conversations);
                //Time: new Date().toString()
                for (var key in _this.Conversations) {
                    var d = new Date(_this.Conversations[key].Time);
                    var month = d.getMonth();
                    var day = d.getDate();
                    var hours = d.getHours();
                    var minutes = '0' + d.getMinutes();
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    var formattedTime = monthNames[month] + "-" + day + "-" + hours + ":" + minutes.substr(-2);
                    // day + "/" + monthNames[month] + "/" + hours + " : " + minutes.substr(-2) 
                    _this.Conversations[key].Time = formattedTime;
                }
            });
        });
    }
    ChatPage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('Friends');
        this.events.subscribe('Conversations');
    };
    ChatPage.prototype.ionViewDidEnter = function () {
        this.friendsProvider.getFriends();
        this.chatProvider.getConversations();
    };
    ChatPage.prototype.openUsersPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__users_users__["a" /* UsersPage */]);
    };
    ChatPage.prototype.openChatBody = function (userDetails) {
        this.chatProvider.initialize(userDetails);
        var modal = this.myModal.create(__WEBPACK_IMPORTED_MODULE_4__chatbody_chatbody__["a" /* ChatbodyPage */], {
            userDetails: userDetails
        });
        modal.present();
    };
    ChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-chat',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\chat\chat.html"*/'<ion-content class="back">\n	<ion-label style="font-size: 18px; text-align: center; font-weight: bold;" color="light">Direct Messages</ion-label>\n	<div class="spacer" style="height: 5px;"></div>\n	<ion-searchbar color="logo"\n	[(ngModel)]="searchTerm" \n	(ionChange)="setFilteredItems()"   placeholder="Search posts by name or post content" color="logo" style="margin-right: 50px; width: 250px;"></ion-searchbar>\n  <div class="spacer" style="height: 5px;"></div>\n  <div class="centered" ><button class="spacer"  ion-button round  center  style="height: 40px; width:250px;  text-transform: none; " (click)="openUsersPage()" ion-text color="logo"> Invite +<ion-icon name="person-add" color="light" style="margin-right: 50px;"></ion-icon></button></div>\n<div class="spacer" style="height: 5px;"></div>\n<ion-spinner name="bubbles" *ngIf="looding" class="looding"></ion-spinner>\n	<div *ngIf="!looding">\n\n\n		<div class="no-items" *ngIf="blockList == 0">\n			<ion-icon name="person"></ion-icon>\n			<p>No Chats Found</p>\n		</div>\n		<ion-list no-lines>\n			<ion-item class="border" *ngFor="let item of Conversations.reverse(); let i = index" (click)="openChatBody(allUsers[i])" block round outline color="light" >\n				<ion-avatar item-left>\n					<img src="{{allUsers[i].Photo}}">\n					<div class="online" *ngIf="allUsers[i].Status == onlineStatus"></div>\n				</ion-avatar>\n				<div>\n					<h3 text-left style="font-size: 16px; font-weight: bold;" ion-text color="logo">{{allUsers[i].Name}}</h3>\n					<h3 ion-text color="logo">{{item.Body}}</h3>\n					<span style="margin-left: 40px; font-size: 16px; font-weight: bold;" ion-text color="logo">{{item.Time}}</span>\n				</div>\n			</ion-item>\n		</ion-list>\n	</div>\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\chat\chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__["a" /* ChatProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ModalController */], __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_2__providers_friends_friends__["a" /* FriendsProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], ChatPage);
    return ChatPage;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 186:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_users_users__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_request_request__ = __webpack_require__(90);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UsersPage = /** @class */ (function () {
    function UsersPage(navCtrl, platform, loadCtrl, usersProvider, alertCtrl, requestProvider) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.loadCtrl = loadCtrl;
        this.usersProvider = usersProvider;
        this.alertCtrl = alertCtrl;
        this.requestProvider = requestProvider;
        this.users = [];
        this.looding = true;
    }
    UsersPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    UsersPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        //this.superTabsCtrl.showToolbar(false)
        this.usersProvider.getAllUsers().then(function (res) {
            _this.looding = false;
            _this.users = res;
        }).catch(function (err) {
            console.log(err);
        });
    };
    UsersPage.prototype.showUserConfirmation = function (userDetails) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Send Request',
            message: 'The request will be sent to ' + userDetails.Name,
            buttons: [
                {
                    text: 'Cancel',
                    cssClass: 'half-alert-button',
                    handler: function () {
                        _this.showToast('Cancel');
                    }
                },
                {
                    text: 'Send',
                    cssClass: 'half-alert-button',
                    handler: function () {
                        _this.agreeFun(userDetails);
                    }
                }
            ]
        });
        confirm.present();
    };
    UsersPage.prototype.agreeFun = function (userDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Sending Request to ' + userDetails.Name,
            duration: 5000
        });
        load.present();
        this.requestProvider.makeRequest(userDetails).then(function () {
            if (_this.users.length > 1) {
                _this.usersProvider.getAllUsers().then(function (res) {
                    _this.users = res;
                    load.dismiss();
                    _this.showToast('Request has been sent to ' + userDetails.Name);
                }).catch(function (err) {
                    load.dismiss();
                    _this.showToast(err);
                });
            }
            else {
                load.dismiss();
                _this.navCtrl.pop();
                _this.showToast('Request has been sent to ' + userDetails.Name);
            }
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    UsersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-users',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\users\users.html"*/'<ion-header>\n\n  <ion-navbar color="logo">\n    <ion-title>Suggested for you</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n\n<ion-spinner name="bubbles" *ngIf="looding" class="looding"></ion-spinner>\n\n\n\n	<div *ngIf="!looding">\n\n\n		<div class="no-items" *ngIf="blockList == 0">\n			<ion-icon name="person"></ion-icon>\n			<p>No Users found</p>\n		</div>\n\n\n		<ion-item *ngFor="let user of users" (click)="showUserConfirmation(user)">\n			<ion-avatar item-start>\n		      <img src="{{user.Photo}}">\n		    </ion-avatar>\n		    <h2>{{user.Name}}</h2>\n		</ion-item>\n\n	</div>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\users\users.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__providers_users_users__["a" /* UsersProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__providers_request_request__["a" /* RequestProvider */]])
    ], UsersPage);
    return UsersPage;
}());

//# sourceMappingURL=users.js.map

/***/ }),

/***/ 187:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewgroupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_group_group__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__group_group__ = __webpack_require__(188);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { SuperTabsController } from 'ionic2-super-tabs';


var NewgroupPage = /** @class */ (function () {
    function NewgroupPage(camera, platform, loadCtrl, alertCtrl, navCtrl, groupProvider, navParams) {
        this.camera = camera;
        this.platform = platform;
        this.loadCtrl = loadCtrl;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.groupProvider = groupProvider;
        this.navParams = navParams;
        this.group = {
            Name: 'Group Name',
            Picture: 'https://firebasestorage.googleapis.com/v0/b/chat-app-f6a12.appspot.com/o/user.png?alt=media&token=79419e48-423a-42c3-92b2-16027651b931'
        };
    }
    NewgroupPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    NewgroupPage.prototype.editName = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Group Name',
            message: 'Enter a name for this group',
            inputs: [
                {
                    name: 'Name',
                    placeholder: 'Group Name'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    cssClass: 'half-alert-button',
                    handler: function (data) {
                        _this.showToast('Cancel');
                    }
                },
                {
                    text: 'Save',
                    cssClass: 'half-alert-button',
                    handler: function (data) {
                        var res = data.Name;
                        var result = res.trim();
                        if (result == '') {
                            _this.showToast('Enter Group Name');
                        }
                        else {
                            _this.group.Name = result;
                        }
                    }
                }
            ]
        });
        prompt.present();
    };
    NewgroupPage.prototype.selectPicture = function () {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Uploading group picture ....'
        });
        var cameraoptions = {
            quality: 50,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            encodingType: this.camera.EncodingType.PNG,
            destinationType: this.camera.DestinationType.DATA_URL
        };
        this.camera.getPicture(cameraoptions).then(function (ImageData) {
            _this.captureDataUrl = 'data:image/jpeg;base64,' + ImageData;
            load.present();
            _this.groupProvider.uploadGroupPicture(_this.captureDataUrl).then(function () {
                _this.group.Picture = _this.captureDataUrl;
                load.dismiss();
            }).catch(function (err) {
                load.dismiss();
                _this.showToast(err);
            });
        }, function (err) {
            var res = JSON.stringify(err);
            load.dismiss();
            _this.showToast(res);
        });
    };
    NewgroupPage.prototype.createGroup = function () {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Creating Group ....'
        });
        if (this.group.Picture == 'hsttps://firebasestorage.googleapis.com/v0/b/chat-app-f6a12.appspot.com/o/user.png?alt=media&token=79419e48-423a-42c3-92b2-16027651b931') {
            console.log('Select Group Picture');
        }
        else {
            if (this.group.Name == 'Group Name') {
                console.log('Enter Group Name');
            }
            else {
                load.present();
                this.groupProvider.creaeGroup(this.group).then(function () {
                    load.dismiss();
                    console.log('Group has been created');
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__group_group__["a" /* GroupPage */]);
                }).catch(function (err) {
                    load.dismiss();
                    console.log(err);
                });
            }
        }
    };
    NewgroupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-newgroup',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\newgroup\newgroup.html"*/'<ion-header>\n\n  <ion-navbar color="logo">\n    <ion-title>Create Group</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n\n\n\n<div class="body-content">\n	<ion-list>\n\n		<br>\n		<br>\n		\n		<ion-avatar>\n			<img src="{{group.Picture}}" (click)="selectPicture()">\n		</ion-avatar>\n\n		<br>\n		<br>\n\n\n		<h2 (click)="editName()">{{group.Name}} <ion-icon color="header" name="create"></ion-icon></h2>\n\n		<br>\n		<br>\n\n		<div>\n			<p>Tap on group picture or name to change it</p>\n		</div>\n\n\n		<br>\n		<br>\n\n		<button ion-button color="header" block (click)="createGroup()">\n			Create Group\n		</button>\n\n\n\n	</ion-list>\n</div>\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\newgroup\newgroup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_group_group__["a" /* GroupProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], NewgroupPage);
    return NewgroupPage;
}());

//# sourceMappingURL=newgroup.js.map

/***/ }),

/***/ 188:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__newgroup_newgroup__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__groupbody_groupbody__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_group_group__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { SuperTabsController } from 'ionic2-super-tabs';



var GroupPage = /** @class */ (function () {
    function GroupPage(ngZone, myModal, events, groupProvider, navCtrl, navParams) {
        var _this = this;
        this.ngZone = ngZone;
        this.myModal = myModal;
        this.events = events;
        this.groupProvider = groupProvider;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Groups = [];
        this.User_Uid = this.groupProvider.UserUid;
        this.looding = true;
        this.events.subscribe('Groups', function () {
            _this.ngZone.run(function () {
                _this.looding = false;
                _this.Groups = _this.groupProvider.Groups;
            });
        });
    }
    GroupPage.prototype.openNewGroupPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__newgroup_newgroup__["a" /* NewgroupPage */]);
    };
    GroupPage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('Groups');
    };
    GroupPage.prototype.ionViewDidEnter = function () {
        this.groupProvider.getGroups();
    };
    GroupPage.prototype.openGroupBody = function (groupDetails) {
        this.groupProvider.initialize(groupDetails);
        var modal = this.myModal.create(__WEBPACK_IMPORTED_MODULE_3__groupbody_groupbody__["a" /* GroupbodyPage */], {
            groupDetails: groupDetails
        });
        modal.present();
    };
    GroupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-group',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\group\group.html"*/'<ion-header>\n\n  <ion-navbar color="logo">\n    <ion-title>Group</ion-title>\n    <ion-buttons end>\n    	<button ion-button icon-left icon-only (click)="openNewGroupPage()">\n    		<ion-icon name="add"></ion-icon>\n    		<ion-icon name="contacts"></ion-icon>\n    	</button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n\n\n\n<ion-spinner name="bubbles" *ngIf="looding" class="looding"></ion-spinner>\n\n\n    <div *ngIf="!looding">\n\n\n        <div class="no-items" *ngIf="blockList == 0">\n            <ion-icon name="contacts"></ion-icon>\n            <p>No Groups Found</p>\n        </div>\n\n\n\n        <div *ngIf="Groups != 0">\n        	<ion-item *ngFor="let group of Groups" (click)="openGroupBody(group)">\n\n\n        		<ion-avatar item-start *ngIf="group.Owner === User_Uid">\n        	      <img src="{{group.Picture}}">\n        	  </ion-avatar>\n        	  <h2 *ngIf="group.Owner === User_Uid">{{group.Name}}</h2>\n            <ion-note *ngIf="group.Owner === User_Uid">By Me</ion-note>\n\n\n\n\n            <ion-avatar item-start *ngIf="group.Owner != User_Uid">\n                <img src="{{group.Picture}}">\n            </ion-avatar>\n            <h2 *ngIf="group.Owner != User_Uid">{{group.Name}}</h2>\n\n\n        	</ion-item>\n        </div>\n\n\n    </div>\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\group\group.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_4__providers_group_group__["a" /* GroupProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], GroupPage);
    return GroupPage;
}());

//# sourceMappingURL=group.js.map

/***/ }),

/***/ 189:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupbodyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__groupaddmember_groupaddmember__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__groupmember_groupmember__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_groups_groupschannel__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__newgroupchannel_newgroupchannel__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__groupchatchannel_groupchatchannel__ = __webpack_require__(121);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { GroupProvider } from '../../providers/group/group';





var GroupbodyPage = /** @class */ (function () {
    function GroupbodyPage(ngZone, myModal, events, loadCtrl, platform, actionSheetCtrl, navCtrl, groupProvider, navParams) {
        var _this = this;
        this.ngZone = ngZone;
        this.myModal = myModal;
        this.events = events;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.actionSheetCtrl = actionSheetCtrl;
        this.navCtrl = navCtrl;
        this.groupProvider = groupProvider;
        this.navParams = navParams;
        this.Groups = [];
        this.User_Uid = this.groupProvider.UserUid;
        this.looding = true;
        this.groupDetails = this.navParams.get('groupDetails');
        this.events.subscribe('Channels', function () {
            _this.ngZone.run(function () {
                _this.looding = false;
                _this.Groups = _this.groupProvider.Groups;
            });
        });
    }
    GroupbodyPage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('Channels');
    };
    GroupbodyPage.prototype.ionViewDidEnter = function () {
        this.groupProvider.getGroups();
    };
    GroupbodyPage.prototype.openGroupBody = function (groupDetails) {
        this.groupProvider.initialize(groupDetails);
        var modal = this.myModal.create(__WEBPACK_IMPORTED_MODULE_6__groupchatchannel_groupchatchannel__["a" /* GroupchatchannelPage */], {
            groupDetails: groupDetails
        });
        modal.present();
    };
    GroupbodyPage.prototype.openNewGroupPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__newgroupchannel_newgroupchannel__["a" /* NewgroupchannelPage */]);
    };
    GroupbodyPage.prototype.showActionSheet = function () {
        var _this = this;
        if (this.groupDetails.Owner == this.groupProvider.UserUid) {
            var actionSheet = this.actionSheetCtrl.create({
                title: this.groupDetails.Name,
                buttons: [
                    {
                        text: 'Group Members',
                        icon: 'contacts',
                        handler: function () {
                            _this.showMembers(_this.groupDetails);
                        }
                    }, {
                        text: 'Add Member',
                        icon: 'add',
                        handler: function () {
                            _this.addMember(_this.groupDetails);
                        }
                    }, {
                        text: 'Group Info',
                        icon: 'alert',
                        handler: function () {
                            _this.groupInfo(_this.groupDetails);
                        }
                    }, {
                        text: 'Delete Group',
                        icon: 'trash',
                        handler: function () {
                            _this.deleteGroup(_this.groupDetails);
                        }
                    }, {
                        text: 'Cancel',
                        icon: 'close',
                        handler: function () {
                            _this.showToast('Cancel clicked');
                        }
                    }
                ]
            });
            actionSheet.present();
        }
        else {
            var actionSheet = this.actionSheetCtrl.create({
                title: this.groupDetails.Name,
                buttons: [
                    {
                        text: 'Group Members',
                        icon: 'contacts',
                        handler: function () {
                            _this.showMembers(_this.groupDetails);
                        }
                    }, {
                        text: 'Group Info',
                        icon: 'alert',
                        handler: function () {
                            _this.groupInfo(_this.groupDetails);
                        }
                    }, {
                        text: 'Leave Group',
                        icon: 'log-out',
                        handler: function () {
                            _this.leaveGroup(_this.groupDetails);
                        }
                    }, {
                        text: 'Cancel',
                        icon: 'close',
                        handler: function () {
                            _this.showToast('Cancel clicked');
                        }
                    }
                ]
            });
            actionSheet.present();
        }
    };
    GroupbodyPage.prototype.leaveGroup = function (groupDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Leaving Group ...'
        });
        load.present();
        this.groupProvider.leaveGroup(groupDetails).then(function () {
            load.dismiss();
            _this.showToast('You have been leaved');
            _this.navCtrl.pop();
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    GroupbodyPage.prototype.groupInfo = function (groupDetails) {
    };
    GroupbodyPage.prototype.deleteGroup = function (groupDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Deleting Group ...'
        });
        load.present();
        this.groupProvider.deleteGroup(groupDetails).then(function () {
            load.dismiss();
            _this.showToast('Group has been deleted');
            _this.navCtrl.pop();
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    GroupbodyPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    GroupbodyPage.prototype.showMembers = function (groupDetails) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__groupmember_groupmember__["a" /* GroupmemberPage */], {
            groupDetails: groupDetails
        });
    };
    GroupbodyPage.prototype.addMember = function (groupDetails) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__groupaddmember_groupaddmember__["a" /* GroupaddmemberPage */], {
            groupDetails: groupDetails
        });
    };
    GroupbodyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groupbody',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\groupbody\groupbody.html"*/'<ion-header>\n\n  <ion-navbar color="logo">\n	<button ion-button menuToggle>\n		<ion-icon name="menu"></ion-icon>\n	   </button>\n    <ion-title>{{groupDetails.Name}}</ion-title>\n    <ion-buttons end>\n    	<button ion-button ion-icon icon-only icon-left (click)="showActionSheet()">\n    		<ion-icon name="more"></ion-icon>\n		</button>\n		\n	</ion-buttons>\n	<ion-buttons end>\n    	<button ion-button icon-left icon-only (click)="openNewGroupPage()">\n    		<ion-icon name="add"></ion-icon>\n    	</button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n	<ion-spinner name="bubbles" *ngIf="looding" class="looding"></ion-spinner>\n\n\n	<div *ngIf="!looding">\n\n\n        <div class="no-items" *ngIf="blockList == 0">\n            <ion-icon name="contacts"></ion-icon>\n            <p>No Groups Found</p>\n        </div>\n\n\n\n        <div *ngIf="Groups != 0">\n			<ion-item *ngFor="let group of Groups" (click)="openGroupBody(group)">\n\n\n        		<ion-avatar item-start *ngIf="group.Owner === User_Uid">\n        	      <img src="{{group.Picture}}">\n        	  </ion-avatar>\n        	  <h2 *ngIf="group.Owner === User_Uid">#{{group.Name}}</h2>\n            <ion-note *ngIf="group.Owner === User_Uid">By Me</ion-note>\n\n\n\n\n            <ion-avatar item-start *ngIf="group.Owner != User_Uid">\n                <img src="{{group.Picture}}">\n            </ion-avatar>\n            <h2 *ngIf="group.Owner != User_Uid">#{{group.Name}}</h2>\n\n\n        	</ion-item>\n        </div>\n\n\n    </div>\n\n</ion-content>\n\n\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\groupbody\groupbody.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__providers_groups_groupschannel__["a" /* GroupschannelProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], GroupbodyPage);
    return GroupbodyPage;
}());

//# sourceMappingURL=groupbody.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaceDirectionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_connectivity_connectivity__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_locations_locations__ = __webpack_require__(93);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PlaceDirectionPage = /** @class */ (function () {
    function PlaceDirectionPage(location_handler, navCtrl, navParams, connectionListerner) {
        this.location_handler = location_handler;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.connectionListerner = connectionListerner;
        this.map_initialised = false;
        this.apiKey = "AIzaSyBkK4dKGUIh5qkHR6FacFJTKbLR_nyEftk"; //API KEY
        this.destination_name = "";
        this.distance = "";
        this.place_directions = {
            place_origin: null,
            place_destination: null,
            place_travel_mode: "WALKING",
        };
        var data = this.navParams.get('direction');
        if (data) {
            this.place_directions.place_origin = data.current_location;
            this.place_directions.place_destination = data;
            this.destination_name = data.place_name;
            this.distance = data.place_distance;
        }
    }
    PlaceDirectionPage.prototype.ionViewDidLoad = function () {
        //load map now
        this.loadGoogleMap();
    };
    /*
      @Author:Dieudonne Dengun
      @Date:12/05/2018
      @Description:Load map for
  
    */
    PlaceDirectionPage.prototype.loadGoogleMap = function () {
        var _this = this;
        this.addConnectivityListeners();
        if (typeof google == "undefined" || typeof google.maps == "undefined") {
            console.log("Google maps JavaScript needs to be loaded.");
            if (this.connectionListerner.isOnline()) {
                //Load the SDK
                window['mapInit'] = function () {
                    _this.startDestinationNavigation();
                    _this.enableMap();
                };
                this.map_initialised = true;
                var script = document.createElement("script");
                script.id = "googleMaps";
                if (this.apiKey) {
                    script.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
                }
                else {
                    script.src = 'https://maps.googleapis.com/maps/api/js?callback=mapInit';
                }
                document.body.appendChild(script);
            }
        }
        else {
            if (this.connectionListerner.isOnline()) {
                this.startDestinationNavigation();
                this.enableMap();
            }
            else {
                this.disableMap();
            }
        }
    };
    /*
       @Author:Dieudonne Dengun
       @Date:12/04/2018
       @Description:Disable the display map if there is no internet and alert user of network change
     */
    PlaceDirectionPage.prototype.disableMap = function () {
        var title = "Offline Status";
        var message = "No connection.Please connect to the internet to continue";
        this.location_handler.showSimpleAlertDialog(title, message);
    };
    /*
      @Author:Dieudonne Dengun
      @Date:12/04/2018
      @Description:Enable and render map if the user internet has been restored after reconnection
    */
    PlaceDirectionPage.prototype.enableMap = function () {
        this.location_handler.showToastMessage("You are currently online", "bottom", 3000);
    };
    /*
    @Author:Dieudonne Dengun
    @Date:12/04/2018
    @Description:add network change listener to monitor user connection fluctuation
    */
    PlaceDirectionPage.prototype.addConnectivityListeners = function () {
        var _this = this;
        //determine of the user phone is connected to the internet
        var onOnline = function () {
            setTimeout(function () {
                if (typeof google == "undefined" || typeof google.maps == "undefined") {
                    if (!_this.map_initialised) {
                        //reintialised the map again on the dom
                        _this.loadGoogleMap();
                    }
                }
                else {
                    if (_this.map_initialised) {
                        _this.startDestinationNavigation();
                        _this.enableMap();
                    }
                }
            }, 2000);
        };
        //this means the user is offline, so disabled the map
        var onOffline = function () {
            _this.disableMap();
        };
        //add online and offline network listeners to the dom to monitor network changes
        document.addEventListener('online', onOnline, false);
        document.addEventListener('offline', onOffline, false);
    };
    /*
     @Author:Dieudonne Dengun
     @Date: 12/05/2018
     @Description:start navigation routing
    */
    PlaceDirectionPage.prototype.startDestinationNavigation = function () {
        var _this = this;
        //initialize map 
        var place_origin = this.place_directions.place_origin;
        var place_destination = this.place_directions.place_destination;
        var origin_latLng = new google.maps.LatLng(place_origin.lat, place_origin.lng);
        var destination_latLng = new google.maps.LatLng(place_destination.latitude, place_destination.longitude);
        //set origin and destination objects to be used by the marker service
        var origin_latlng = {
            lat: place_origin.lat,
            lng: place_origin.lng
        };
        var destination_latlng = {
            lat: place_destination.latitude,
            lng: place_destination.longitude
        };
        //map options
        var mapOptions = {
            center: origin_latLng,
            zoom: 15,
            zoomControl: false,
            fullscreenControl: false,
            gestureHandling: 'none',
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapDiv.nativeElement, mapOptions);
        //set and initialise directions api 
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({ suppressMarkers: true });
        directionsDisplay.setMap(this.map);
        directionsDisplay.setPanel(this.directionsPanel.nativeElement);
        //display the nagivation on map
        directionsService.route({
            origin: origin_latLng,
            destination: destination_latLng,
            travelMode: google.maps.TravelMode[this.place_directions.place_travel_mode]
        }, (function (res, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(res);
                var _route = res.routes[0].legs[0];
                //set markers for the origin and destination on the map
                var origin_marker = new google.maps.Marker({
                    map: _this.map,
                    animation: google.maps.Animation.DROP,
                    position: _route.start_location,
                    label: {
                        text: "C",
                        color: "white",
                    },
                    icon: {
                        path: google.maps.SymbolPath.CIRCLE,
                        fillColor: 'green',
                        fillOpacity: .6,
                        scale: 20,
                        strokeColor: 'white',
                        strokeWeight: .5
                    }
                });
                var destination_marker = new google.maps.Marker({
                    map: _this.map,
                    animation: google.maps.Animation.DROP,
                    position: _route.end_location,
                    label: {
                        text: "D",
                        color: "white",
                    },
                    icon: {
                        path: google.maps.SymbolPath.CIRCLE,
                        fillColor: 'red',
                        fillOpacity: .6,
                        scale: 20,
                        strokeColor: 'white',
                        strokeWeight: .5
                    }
                });
            }
            else {
                console.warn(status);
            }
        }).bind(this));
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], PlaceDirectionPage.prototype, "mapDiv", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('directionsPanel'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], PlaceDirectionPage.prototype, "directionsPanel", void 0);
    PlaceDirectionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-place-direction',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\place-direction\place-direction.html"*/'\n\n<ion-header>\n\n\n\n  <ion-navbar color="logo">\n\n    <ion-title text-wrap>Heading to {{destination_name}}, <br/> <small>about {{distance}}m from current location</small></ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n\n\n   \n\n        <div class="direction-card" #directionsPanel></div>\n\n        <div #map id="map"></div> \n\n    \n\n   \n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\place-direction\place-direction.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_locations_locations__["a" /* LocationsProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_connectivity_connectivity__["a" /* ConnectivityProvider */]])
    ], PlaceDirectionPage);
    return PlaceDirectionPage;
}());

//# sourceMappingURL=place-direction.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__donation_donation__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PaymentsPage = /** @class */ (function () {
    function PaymentsPage(navCtrl, navParams, userservice, loadingCtrl, afDatabase, fire, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.userservice = userservice;
        this.loadingCtrl = loadingCtrl;
        this.afDatabase = afDatabase;
        this.fire = fire;
        this.toastCtrl = toastCtrl;
        this.selectedLeave = '';
        this.profile = {};
        this.newuser = {
            email: '',
            password: '',
            amount: '',
            number: '',
            displayName: ''
        };
    }
    PaymentsPage.prototype.pay = function () {
        var _this = this;
        var toaster = this.toastCtrl.create({
            duration: 3000,
            position: 'bottom'
        });
        if (this.newuser.email == '' || this.newuser.amount == '' || this.newuser.displayName == '') {
            toaster.setMessage('All fields are required dude');
            toaster.present();
        }
        else if (this.newuser.amount.length >= 10) {
            toaster.setMessage('Password is not strong. Try giving more than six characters');
            toaster.present();
        }
        else {
            var loader_1 = this.loadingCtrl.create({
                content: 'Please wait'
            });
            loader_1.present();
            this.userservice.adduser(this.newuser).then(function (res) {
                loader_1.dismiss();
                if (res.success) {
                    _this.fire.authState.take(1).subscribe(function (auth) {
                        _this.afDatabase.object("profile/" + auth.uid);
                    });
                    //this.navCtrl.push(MenugpPage);
                }
                else {
                    alert('Error' + res);
                }
            });
        }
        this.fire.authState.take(1).subscribe(function (auth) {
            _this.afDatabase.object("profile/" + auth.uid);
        });
    };
    PaymentsPage.prototype.goback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__donation_donation__["a" /* DonationPage */]);
    };
    PaymentsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-payments',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\payments\payments.html"*/'\n<ion-header>\n  <ion-navbar color="purple">\n    <ion-title>Payment</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n<div class="spacer" style="height: 3vw;"></div>\n\n<ion-item class="item-leave-height">\n <ion-label>Top Up Type</ion-label> \n  <ion-select [(ngModel)]="selectedLeave">\n      <ion-option value="Donation">Donation</ion-option>\n      <ion-option value="Tyth">Tyth</ion-option>\n      <ion-option value="Offerings">Offerings</ion-option>\n  </ion-select>\n</ion-item>\n\n\n<div class="spacer" style="height: 3vw;"></div>\n<button ion-button outline round style="height:10vw; width:200px;" >\n  <ion-item>\n    <ion-input type="number" placeholder="Contact no." text-left [(ngModel)]="newuser.number" ></ion-input>\n  </ion-item>\n  </button>\n\n\n<div class="spacer" style="height: 3vw;"></div>\n  <button ion-button outline round style="height:10vw; width:200px;" >\n    <ion-item >\n      <ion-input type="text" placeholder="Amount" class="text" text-left [(ngModel)]="newuser.amount" ></ion-input>\n    </ion-item>\n  </button>\n\n<div class="spacer" style="height: 3vw;"></div>\n<button ion-button outline round style="height:10vw; width:200px;" >\n<ion-item >\n  <ion-input type="text" placeholder="Last name" class="text" [(ngModel)]="newuser.lastName" ></ion-input>\n</ion-item>\n</button>\n\n\n<div class="spacer" style="height: 3vw;"></div>\n<button ion-button outline round style="height:10vw; width:200px;" >\n  <ion-item>\n    <ion-input type="email" placeholder="Email" text-center [(ngModel)]="newuser.email"></ion-input>\n  </ion-item>\n  </button>\n\n<div class="spacer" style="height: 3vw;"></div>\n<div class="color">\n<p>Payment Method</p>\n\n</div>\n<div class="spacer" style="height: 3vw;"></div>\n<div class="color"><button ion-button round  style="height:10vw; width:40vw; text-transform: none; " center color="standard"  (click)="pay()">Bank Card</button></div>\n<div class="color"><button ion-button round  style="height:10vw; width:40vw; text-transform: none; " center color="standard"  (click)="payment()">Voucher</button></div>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\payments\payments.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* ToastController */]])
    ], PaymentsPage);
    return PaymentsPage;
}());

//# sourceMappingURL=payments.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommentsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_firebase__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_data__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CommentsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var CommentsPage = /** @class */ (function () {
    function CommentsPage(navCtrl, navParams, viewCtrl, firebaseProvider, dataProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.firebaseProvider = firebaseProvider;
        this.dataProvider = dataProvider;
        this.postKey = this.navParams.get('postKey');
    }
    CommentsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.dataProvider.getComments(this.postKey).subscribe(function (comments) {
            if (_this.comments) {
                var tempComments = comments[comments.length - 1];
                var tempData_1 = {};
                tempData_1 = tempComments;
                _this.dataProvider.getUser(tempComments.commentBy).subscribe(function (user) {
                    tempData_1.avatar = user.img;
                    tempData_1.name = user.name;
                });
                // this.addOrUpdateTimeline(tempData)
                _this.comments.push(tempData_1);
            }
            else {
                _this.comments = [];
                comments.forEach(function (comment) {
                    if (comment.$exists()) {
                        var tempComment = comment;
                        var tempData_2 = {};
                        tempData_2 = tempComment;
                        _this.dataProvider.getUser(tempComment.commentBy).subscribe(function (user) {
                            tempData_2.avatar = user.img;
                            tempData_2.name = user.name;
                        });
                        // this.addOrUpdateTimeline(tempData)
                        _this.comments.push(tempData_2);
                    }
                });
            }
        });
    };
    CommentsPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    CommentsPage.prototype.postComment = function () {
        var _this = this;
        var comment = {
            dateCreated: new Date().toString(),
            commentBy: __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().currentUser.uid,
            commentText: this.commentText,
        };
        this.firebaseProvider.commentPost(this.postKey, comment).then(function (res) {
            _this.commentText = '';
        });
    };
    CommentsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-comments',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\comments\comments.html"*/'<!--\n  Generated template for the Comment page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-toolbar color="custom">\n\n    <ion-buttons start>\n      <button ion-button (click)="dismiss()">\n        <ion-icon name="md-close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>\n      Comment\n    </ion-title>\n\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content >\n\n  <ion-list *ngIf="comments">\n    <ion-item  *ngFor="let item of comments">\n\n      <ion-avatar item-left >\n        <img src="{{item.avatar}}" >\n      </ion-avatar>\n      <h2>{{item.name}}</h2>\n      <p>{{item.commentText}} </p>\n      <ion-note item-right>{{item.dateCreated | date:\'short\'}}</ion-note>\n    </ion-item>\n    <!-- <div class="nocomment" *ngIf="!commentList.length">\n      <img src="assets/no-record.png">\n\n    </div> -->\n  </ion-list>\n\n</ion-content>\n<ion-footer >\n  <ion-grid>\n    <ion-row>\n        <!--<ion-col col-12>\n       <button ion-button icon-only color="custom" (click)="imageShare()">\n          <ion-icon name=\'camera\'></ion-icon>\n        </button>\n      </ion-col>-->\n      <ion-col col-10>\n        <ion-textarea  placeholder="Enter your comment" [(ngModel)]="commentText"></ion-textarea>\n\n      </ion-col>\n      <ion-col col-2>\n        <button ion-button icon-only  color="custom" (click)="postComment()" [disabled]="!commentText">\n          <ion-icon name=\'send\'></ion-icon>\n\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-footer>\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\comments\comments.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_firebase__["a" /* FirebaseProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_data__["a" /* DataProvider */]])
    ], CommentsPage);
    return CommentsPage;
}());

//# sourceMappingURL=comments.js.map

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupbuddieschannelPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_groups_groupschannel__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile_profile__ = __webpack_require__(68);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the GroupbuddieschannelPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var GroupbuddieschannelPage = /** @class */ (function () {
    function GroupbuddieschannelPage(navCtrl, loadCtrl, platform, ngZone, navParams, alertCtrl, events, groupProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.ngZone = ngZone;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.groupProvider = groupProvider;
        this.Friends = [];
        this.looding = true;
        this.groupDetails = this.navParams.get('groupDetails');
        this.events.subscribe('AllFriends', function () {
            _this.ngZone.run(function () {
                _this.looding = false;
                _this.Friends = _this.groupProvider.AllFriends;
            });
        });
    }
    GroupbuddieschannelPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    GroupbuddieschannelPage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('AllFriends');
    };
    GroupbuddieschannelPage.prototype.ionViewDidEnter = function () {
        this.groupProvider.getAllFriends(this.groupDetails);
    };
    GroupbuddieschannelPage.prototype.showFriendsConfirmation = function (userDetails) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: userDetails.Name,
            message: 'Tap on option',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        _this.showToast('Cancel');
                    }
                },
                {
                    text: 'Add ' + userDetails.Name + ' to Group',
                    handler: function () {
                        _this.addMember(userDetails, _this.groupDetails);
                    }
                }, {
                    text: 'View Profile',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__profile_profile__["a" /* ProfilePage */], {
                            userDetails: userDetails
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    GroupbuddieschannelPage.prototype.addMember = function (userDetails, groupDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Adding ' + userDetails.Name + ' to group ...'
        });
        load.present();
        this.groupProvider.addMember(userDetails, groupDetails).then(function () {
            load.dismiss();
            _this.showToast(userDetails.Name + ' has been added to group');
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    GroupbuddieschannelPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groupbuddieschannel',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\groupbuddieschannel\groupbuddieschannel.html"*/'<ion-header>\n\n  <ion-navbar color="logo">\n\n    <ion-title>Add Member</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n\n\n\n\n\n\n\n\n\n\n<ion-spinner name="bubbles" *ngIf="looding" class="looding"></ion-spinner>\n\n\n\n\n\n\n\n<div *ngIf="Friends != 0">\n\n	<ion-item *ngFor="let user of Friends" (click)="showFriendsConfirmation(user)">\n\n		<ion-avatar item-start>\n\n	      <img src="{{user.Photo}}">\n\n	    </ion-avatar>\n\n	    <h2>{{user.Name}}</h2>\n\n	</ion-item>\n\n</div>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\groupbuddieschannel\groupbuddieschannel.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_2__providers_groups_groupschannel__["a" /* GroupschannelProvider */]])
    ], GroupbuddieschannelPage);
    return GroupbuddieschannelPage;
}());

//# sourceMappingURL=groupbuddieschannel.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupmembersChannelPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_groups_groupschannel__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile_profile__ = __webpack_require__(68);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the GroupmembersChannelPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var GroupmembersChannelPage = /** @class */ (function () {
    function GroupmembersChannelPage(navCtrl, alertCtrl, ngZone, loadCtrl, platform, navParams, groupProvider, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.ngZone = ngZone;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.groupProvider = groupProvider;
        this.events = events;
        this.GroupMembers = [];
        this.looding = true;
        this.User_Uid = this.groupProvider.UserUid;
        this.groupDetails = this.navParams.get('groupDetails');
        this.events.subscribe('GroupMembers', function () {
            _this.ngZone.run(function () {
                _this.looding = false;
                _this.GroupMembers = _this.groupProvider.groupMembers;
            });
        });
    }
    GroupmembersChannelPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    GroupmembersChannelPage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('GroupMembers');
    };
    GroupmembersChannelPage.prototype.ionViewDidEnter = function () {
        this.groupProvider.getGroupMembers(this.groupDetails);
    };
    GroupmembersChannelPage.prototype.showMemberConfirmation = function (userDetails) {
        var _this = this;
        if (this.groupDetails.Owner = this.groupProvider.UserUid) {
            if (this.groupDetails.Owner == userDetails.Id) {
                console.log('You are the admin');
            }
            else {
                var confirm_1 = this.alertCtrl.create({
                    title: userDetails.Name,
                    message: 'Tap on option',
                    buttons: [
                        {
                            text: 'Cancel',
                            handler: function () {
                                _this.showToast('Cancel');
                            }
                        },
                        {
                            text: 'Delete ' + userDetails.Name + ' from Group',
                            handler: function () {
                                _this.deleteMember(userDetails, _this.groupDetails);
                            }
                        }, {
                            text: 'View Profile',
                            handler: function () {
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__profile_profile__["a" /* ProfilePage */], {
                                    userDetails: userDetails
                                });
                            }
                        }
                    ]
                });
                confirm_1.present();
            }
        }
        else {
            console.log('You are not admin');
        }
    };
    GroupmembersChannelPage.prototype.deleteMember = function (userDetails, groupDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Deleting ' + userDetails.Name + ' ...'
        });
        load.present();
        this.groupProvider.deleteMember(userDetails, groupDetails).then(function () {
            load.dismiss();
            _this.showToast(userDetails.Name + ' has been deleted');
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    GroupmembersChannelPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groupmemberschannel',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\groupmemberschannel\groupmemberschannel.html"*/'<!--\n\n  Generated template for the GroupmembersChannelPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar color="logo">\n\n    <ion-title>Channel Members</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <ion-spinner name="bubbles" *ngIf="looding" class="looding"></ion-spinner>\n\n\n\n\n\n	<ion-item *ngFor="let user of GroupMembers" (click)="showMemberConfirmation(user)">\n\n		<ion-avatar item-start>\n\n	      <img src="{{user.Photo}}">\n\n	    </ion-avatar>\n\n	    <h2>{{user.Name}}</h2>\n\n	    <ion-note *ngIf="groupDetails.Owner === user.Id">Admin</ion-note>\n\n	</ion-item>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\groupmemberschannel\groupmemberschannel.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_groups_groupschannel__["a" /* GroupschannelProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], GroupmembersChannelPage);
    return GroupmembersChannelPage;
}());

//# sourceMappingURL=groupmemberschannel.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PanicPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_call_number__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__(556);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__ = __webpack_require__(559);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_storage_storage__ = __webpack_require__(324);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var PanicPage = /** @class */ (function () {
    function PanicPage(navCtrl, storageService, alertCtrl, toastController, navParams, callNumber) {
        this.navCtrl = navCtrl;
        this.storageService = storageService;
        this.alertCtrl = alertCtrl;
        this.toastController = toastController;
        this.navParams = navParams;
        this.callNumber = callNumber;
        this.contacts = [];
        this.Data = [];
        this.items = [];
        this.newItem = {};
        this.loadItems();
    }
    PanicPage.prototype.doRefresh = function (event) {
        console.log('Begin async operation');
        setTimeout(function () {
            console.log('Async operation has ended');
            event.target.complete();
        }, 2000);
    };
    // CREATE
    PanicPage.prototype.addItem = function () {
        var _this = this;
        this.newItem.modified = Date.now();
        this.newItem.id = Date.now();
        this.storageService.addItem(this.newItem).then(function (item) {
            _this.newItem = {};
            _this.showToast('Item added!');
            _this.loadItems(); // Or add it to the array directly
        });
    };
    // READ
    PanicPage.prototype.loadItems = function () {
        var _this = this;
        this.storageService.getItems().then(function (items) {
            _this.items = items;
            console.log(_this.items);
        });
    };
    PanicPage.prototype.deleteItem = function (item) {
        var _this = this;
        this.storageService.deleteItem(item.id).then(function (item) {
            _this.showToast('Contact removed!');
            _this.loadItems(); // Or splice it from the array directly
        });
    };
    PanicPage.prototype.police = function () {
        this.callNumber.callNumber("0117589100", true)
            .then(function (res) { return console.log('Launched dialer!', res); })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    PanicPage.prototype.ambulance = function () {
        this.callNumber.callNumber("084124", true)
            .then(function (res) { return console.log('Launched dialer!', res); })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    PanicPage.prototype.fire = function () {
        this.callNumber.callNumber("0114580911", true)
            .then(function (res) { return console.log('Launched dialer!', res); })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    PanicPage.prototype.child = function () {
        this.callNumber.callNumber("0114524110", true)
            .then(function (res) { return console.log('Launched dialer!', res); })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    PanicPage.prototype.coronaCare = function () {
        this.callNumber.callNumber("0828839920", true)
            .then(function (res) { return console.log('Launched dialer!', res); })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    PanicPage.prototype.church = function () {
        this.callNumber.callNumber("01188422987", true)
            .then(function (res) { return console.log('Launched dialer!', res); })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    PanicPage.prototype.call = function (Phone) {
        this.callNumber.callNumber(Phone, true)
            .then(function (res) { return console.log('Launched dialer!', res); })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    PanicPage.prototype.showToast = function (msg) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: msg,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    PanicPage.prototype.presentPrompt = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Add Emergency Contact',
            inputs: [
                {
                    name: 'Name',
                    placeholder: 'Name'
                },
                {
                    name: 'Phone',
                    placeholder: 'Phone Number',
                    type: 'string'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Save',
                    handler: function (data) {
                        _this.newItem.modified = Date.now();
                        _this.newItem.id = Date.now();
                        _this.newItem.Name = data.Name;
                        _this.newItem.Number = data.Phone;
                        _this.storageService.addItem(_this.newItem).then(function (item) {
                            _this.newItem = {};
                            _this.showToast('Contact added!');
                            _this.loadItems(); // Or add it to the array directly
                        });
                        //console.log(data.Name,data.phone);
                    }
                }
            ]
        });
        alert.present();
    };
    PanicPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PanicPage');
    };
    PanicPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-panic',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\panic\panic.html"*/'<ion-header>\n\n  <ion-navbar color="logo" > \n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title >Panic Button</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding >\n\n\n\n<!--   <h4 style="padding: 1em  5% 30px 5px;">Your Utility Bills</h4> -->\n\n\n\n \n\n\n\n  <ion-fab  bottom center >\n\n    <button ion-fab end (click)="presentPrompt()" color="lightgray"  class="mypopo" >  \n\n      <ion-icon ios="ios-add" md="md-add" ></ion-icon>\n\n  </button>\n\n  </ion-fab>\n\n \n\n \n\n<ion-grid >\n\n  <ion-row>\n\n    <ion-col col-6 color="light">\n\n          <div   (click)="police()"  > \n\n              <img style="height: 130px; width: 130px; " src="assets/imgs/saps.png" />\n\n          </div>\n\n            \n\n\n\n    </ion-col>\n\n      <ion-col col-6>\n\n            <div   (click)="ambulance()" color="light" >\n\n                <img style="height: 110px; width: 120px;  margin-top: 5px;"  src="assets/imgs/ambulance.png" />\n\n            </div>\n\n  \n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col col-6>\n\n              <div (click)="church()"   color="light" >\n\n                  <img style="height: 130px; width: 130px; "  src="assets/imgs/church.png" />\n\n              </div>\n\n                \n\n    \n\n        </ion-col>\n\n          <ion-col col-6>\n\n            <div (click)="coronaCare()" color="light" >\n\n              <img  style="height: 130px; width: 130px; "  src="assets/imgs/corona.png">\n\n            </div>\n\n      \n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col col-6>\n\n                <div (click)="security()"   color="light" >\n\n                    <img style="height: 150px; width: 200px; margin-bottom: -60px;"  src="assets/imgs/connexme.jpg" />\n\n                </div>\n\n                  \n\n      \n\n          </ion-col>\n\n          <ion-list *ngFor="let item of items; let i = index">\n\n\n\n            <ion-col col-6>\n\n          <ion-item no-lines >\n\n            <ion-col (click)="call(item.Number)">\n\n              <img src="assets/imgs/contact_avatar_fill.png" style="width: 120px; height: 130px;"/><br/>\n\n              <b>{{item.Name}}</b>\n\n                </ion-col>\n\n          <button ion-button icon-only clear (click)="deleteItem(item)"><ion-icon name="ios-trash" color="danger" ></ion-icon></button>\n\n          </ion-item>\n\n            </ion-col>\n\n           \n\n        </ion-list>\n\n          </ion-row>\n\n</ion-grid>\n\n\n\n<!-- <ion-row>\n\n \n\n<ion-list *ngFor="let item of items; let i = index">\n\n\n\n    <ion-col col-6>\n\n  <ion-item no-lines >\n\n    <ion-card (click)="call(item.Number)">\n\n          <img src="assets/imgs/emerge.png"  /><br/>\n\n          <b>{{item.Name}}</b>\n\n        </ion-card>\n\n  <button ion-button icon-only clear (click)="deleteItem(item)"><ion-icon name="ios-trash" color="danger" ></ion-icon></button>\n\n  </ion-item>\n\n    </ion-col>\n\n   \n\n</ion-list>\n\n</ion-row> -->\n\n\n\n \n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\panic\panic.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_5__providers_storage_storage__["a" /* StorageProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["u" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1__ionic_native_call_number__["a" /* CallNumber */]])
    ], PanicPage);
    return PanicPage;
}());

//# sourceMappingURL=panic.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimetimelinePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__comments_comments__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_alert__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_data__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_database__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_firebase__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_lodash__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__reactions_reactions__ = __webpack_require__(95);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import {AddPostPage} from "../add-post/add-post";



//mport * as firebase from "firebase";



var TimetimelinePage = /** @class */ (function () {
    function TimetimelinePage(navCtrl, navParams, loadingProvider, angularDb, dataProvider, firebaseProvider, modalCtrl, alertCtrl, actionSheetCtrl, alertProvider, platform) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingProvider = loadingProvider;
        this.angularDb = angularDb;
        this.dataProvider = dataProvider;
        this.firebaseProvider = firebaseProvider;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.alertProvider = alertProvider;
        this.platform = platform;
        this.platform.ready().then(function () {
            _this.platform.pause.subscribe(function () {
                //this.isFirstTime = false;
                if (_this.user.userId) {
                    // Update userData on Database.
                    _this.angularDb
                        .object('Users' + _this.user.userId)
                        .update({
                        isOnline: false
                    })
                        .then(function (success) { })
                        .catch(function (error) {
                        //this.alertProvider.showErrorMessage('profile/error-update-profile');
                    });
                }
            });
            _this.platform.resume.subscribe(function () {
                _this.isFirstTime = false;
                if (_this.user.userId) {
                    // Update userData on Database.
                    _this.angularDb
                        .object('Users' + _this.user.userId)
                        .update({
                        isOnline: true
                    })
                        .then(function (success) { })
                        .catch(function (error) {
                        //this.alertProvider.showErrorMessage('profile/error-update-profile');
                    });
                }
            });
        });
    }
    TimetimelinePage.prototype.ionViewDidLoad = function () {
        // this.isFirstTime = true;
        this.getTimeline();
    };
    TimetimelinePage.prototype.getTimeline = function () {
        var _this = this;
        // Observe the userData on database to be used by our markup html.
        // Whenever the userData on the database is updated, it will automatically reflect on our user variable.
        this.loadingProvider.show();
        //this.createUserData();
        var userData = this.dataProvider.getCurrentUser().subscribe(function (user) {
            _this.user = user;
            console.log("timeline user", _this.user);
            _this.dataProvider.setData("userData", _this.user);
            userData.unsubscribe();
            //  Update userData on Database.
        });
        // Get Friend  List
        this.dataProvider.getFriends().subscribe(function (friends) {
            // Get timeline by user
            _this.dataProvider.getTimelinePost().subscribe(function (post) {
                _this.loadingProvider.hide();
                if (_this.timelineData) {
                    var timeline_1 = post[post.length - 1];
                    var tempData_1 = {};
                    tempData_1 = timeline_1;
                    var friendIndex = __WEBPACK_IMPORTED_MODULE_9_lodash__["findKey"](friends, function (data) {
                        var _tempData = data;
                        return _tempData.$value == timeline_1.postBy;
                    });
                    if (friendIndex ||
                        timeline_1.postBy == __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid) {
                        _this.dataProvider.getUser(timeline_1.postBy).subscribe(function (user) {
                            tempData_1.avatar = user.img;
                            tempData_1.name = user.name;
                        });
                        // Check Locaion
                        if (timeline_1.location) {
                            var tempLocaion = JSON.parse(timeline_1.location);
                            tempData_1.lat = tempLocaion.lat;
                            tempData_1.long = tempLocaion.long;
                            // tempData.location="https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=500x200&maptype=roadmap&markers=color:red|label:S|"+tempLocaion.lat+","+tempLocaion.long+"&key=AIzaSyAt0edUAx4S2d7z8wh1Xe04yE9Xml1ZLPY"
                            tempData_1.location =
                                "https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=500x200&maptype=roadmap&markers=color:red|label:S|40.702147,-74.015794&key=AIzaSyAt0edUAx4S2d7z8wh1Xe04yE9Xml1ZLPY";
                        }
                        //  ===== check like and commnets ===
                        _this.dataProvider.getLike(tempData_1.$key).subscribe(function (likes) {
                            tempData_1.likes = likes.length;
                            var isLike = __WEBPACK_IMPORTED_MODULE_9_lodash__["findKey"](likes, function (like) {
                                var _tempLike = like;
                                return _tempLike.$value == __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                            });
                            if (isLike) {
                                tempData_1.isLike = true;
                            }
                            else {
                                tempData_1.isLike = false;
                            }
                        });
                        //  ===== check dilike
                        _this.dataProvider.getdisLike(tempData_1.$key).subscribe(function (dislikes) {
                            tempData_1.dislikes = dislikes.length;
                            // Check post like or not
                            var isdisLike = __WEBPACK_IMPORTED_MODULE_9_lodash__["findKey"](dislikes, function (dislike) {
                                var _tempLike = dislike;
                                return _tempLike.$value == __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid;
                            });
                            if (isdisLike) {
                                tempData_1.isdisLike = true;
                            }
                            else {
                                tempData_1.isdisLike = false;
                            }
                        });
                        //  ===== check commnets
                        _this.dataProvider.getComments(tempData_1.$key).subscribe(function (comments) {
                            tempData_1.comments = comments.length;
                            // Check post like or not
                            var isComments = __WEBPACK_IMPORTED_MODULE_9_lodash__["findKey"](comments, function (comment) {
                                var _tempComment = comment;
                                return (_tempComment.commentBy == __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid);
                            });
                            if (isComments) {
                                tempData_1.isComment = true;
                            }
                            else {
                                tempData_1.isComment = false;
                            }
                        });
                        // this.addOrUpdateTimeline(tempData)
                        _this.timelineData.unshift(tempData_1);
                    }
                }
                else {
                    _this.timelineData = [];
                    post.forEach(function (data) {
                        _this.dataProvider.getTimeline(data.$key).subscribe(function (timeline) {
                            if (timeline.$exists()) {
                                var tempData_2 = {};
                                tempData_2 = timeline;
                                var friendIndex = __WEBPACK_IMPORTED_MODULE_9_lodash__["findKey"](friends, function (data) {
                                    var _tempData = data;
                                    return _tempData.$value == timeline.postBy;
                                });
                                if (friendIndex ||
                                    timeline.postBy == __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid) {
                                    _this.dataProvider.getUser(timeline.postBy).subscribe(function (user) {
                                        tempData_2.avatar = user.img;
                                        tempData_2.name = user.name;
                                    });
                                    // Check Location
                                    if (timeline.location) {
                                        var tempLocaion = JSON.parse(timeline.location);
                                        tempData_2.lat = tempLocaion.lat;
                                        tempData_2.long = tempLocaion.long;
                                        tempData_2.location =
                                            "https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=500x300&maptype=roadmap&markers=color:red|label:S|" +
                                                tempLocaion.lat +
                                                "," +
                                                tempLocaion.long;
                                    }
                                    //  ===== check like
                                    _this.dataProvider.getLike(tempData_2.$key).subscribe(function (likes) {
                                        tempData_2.likes = likes.length;
                                        // Check post like or not
                                        var isLike = __WEBPACK_IMPORTED_MODULE_9_lodash__["findKey"](likes, function (like) {
                                            var _tempLike = like;
                                            return (_tempLike.$value == __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid);
                                        });
                                        if (isLike) {
                                            tempData_2.isLike = true;
                                        }
                                        else {
                                            tempData_2.isLike = false;
                                        }
                                    });
                                    //  ===== check dilike
                                    _this.dataProvider
                                        .getdisLike(tempData_2.$key)
                                        .subscribe(function (dislikes) {
                                        tempData_2.dislikes = dislikes.length;
                                        // Check post like or not
                                        var isdisLike = __WEBPACK_IMPORTED_MODULE_9_lodash__["findKey"](dislikes, function (dislike) {
                                            var _tempLike = dislike;
                                            return (_tempLike.$value == __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid);
                                        });
                                        if (isdisLike) {
                                            tempData_2.isdisLike = true;
                                        }
                                        else {
                                            tempData_2.isdisLike = false;
                                        }
                                    });
                                    //  ===== check commnets
                                    _this.dataProvider
                                        .getComments(tempData_2.$key)
                                        .subscribe(function (comments) {
                                        tempData_2.comments = comments.length;
                                        // Check post like or not
                                        var isComments = __WEBPACK_IMPORTED_MODULE_9_lodash__["findKey"](comments, function (comment) {
                                            var _tempComment = comment;
                                            return (_tempComment.commentBy ==
                                                __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.auth().currentUser.uid);
                                        });
                                        if (isComments) {
                                            tempData_2.isComment = true;
                                        }
                                        else {
                                            tempData_2.isComment = false;
                                        }
                                    });
                                    _this.timelineData.unshift(tempData_2);
                                }
                            }
                        });
                    });
                }
            });
        });
    };
    TimetimelinePage.prototype.reportPost = function (item) {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: "Report post to admin",
            buttons: [
                {
                    text: "Report",
                    role: "destructive",
                    handler: function () {
                        console.log(" report Post ", item);
                        _this.loadingProvider.show();
                        _this.firebaseProvider.reportPost(item, _this.user).then(function (res) {
                            _this.loadingProvider.hide();
                            _this.alertProvider.showToast("Post reported successfully");
                        }, function (err) {
                            _this.loadingProvider.hide();
                        });
                    }
                },
                {
                    text: "Cancel",
                    role: "cancel",
                    handler: function () {
                        console.log("Cancel clicked");
                    }
                }
            ]
        });
        actionSheet.present();
    };
    TimetimelinePage.prototype.addOrUpdateTimeline = function (timeline) {
        if (!this.timelineData) {
            this.timelineData = [timeline];
        }
        else {
            var index = -1;
            for (var i = 0; i < this.timelineData.length; i++) {
                if (this.timelineData[i].$key == timeline.$key) {
                    index = i;
                }
            }
            if (index > -1) {
                this.timelineData[index] = timeline;
            }
            else {
                this.timelineData.unshift(timeline);
            }
        }
    };
    TimetimelinePage.prototype.addPost = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__reactions_reactions__["a" /* ReactionsPage */]);
    };
    TimetimelinePage.prototype.likePost = function (post) {
        this.firebaseProvider.likePost(post.$key);
    };
    TimetimelinePage.prototype.delikePost = function (post) {
        this.firebaseProvider.delikePost(post.$key);
    };
    TimetimelinePage.prototype.disikePost = function (post) {
        this.firebaseProvider.dislikePost(post.$key);
    };
    TimetimelinePage.prototype.dedislikePost = function (post) {
        this.firebaseProvider.dedislikePost(post.$key);
    };
    TimetimelinePage.prototype.commentPost = function (post) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__comments_comments__["a" /* CommentsPage */], { postKey: post.$key });
        modal.present();
    };
    TimetimelinePage.prototype.openMap = function (lat, long) {
        window.open("http://maps.google.com/maps?q=" + lat + "," + long, "_system", "location=yes");
    };
    TimetimelinePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-timetimeline',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\timetimeline\timetimeline.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Post</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content >\n  <ion-card *ngIf="user">\n    <ion-item class="post" (click)="addPost()" >\n      <ion-thumbnail item-left  >\n        <img src="{{user.img}}"  >\n      </ion-thumbnail>\n      <p >What\'s on your mind?</p>\n    </ion-item>\n  </ion-card>\n  <ion-card *ngFor="let item of timelineData">\n    <ion-item >\n      <ion-avatar item-left>\n        <img src="{{item.avatar}}"  >\n      </ion-avatar>\n      <h2>{{item.name}}</h2>\n      <p>{{item.dateCreated | date:\'short\'}}</p>\n      <ion-icon  item-right  name="more" (click)="reportPost(item)"></ion-icon>\n\n    </ion-item>\n\n    <ion-card-content>\n      <p>{{item.postText}}</p>\n    </ion-card-content>\n    <!-- <img *ngIf="p.isType==2"   src="{{p.mapUrl}}" style="200px" (click)="openMap(p.mapData.lat,p.mapData.long)"> -->\n    <img src="{{item.image}}" style="height:200px" *ngIf="item.image" (click)="enlargeImage(item.image)">\n    <img src="https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=500x200&maptype=roadmap&markers=color:red|label:S|{{item.lat}},{{item.long}}&key=AIzaSyAt0edUAx4S2d7z8wh1Xe04yE9Xml1ZLPY" style="height:200px" *ngIf="item.location" (click)="openMap(item.lat,item.long)">\n    <ion-row text-center>\n      <ion-col col-4>\n        <button ion-button icon-left clear small color="dark" (click)="likePost(item)" *ngIf="!item.isLike">\n          <ion-icon name="thumbs-up" ></ion-icon>\n          <div> {{item.likes}} Like </div>\n        </button>\n        <button ion-button icon-left clear small color="custom" (click)="delikePost(item)" *ngIf="item.isLike">\n          <ion-icon name="thumbs-up" ></ion-icon>\n          <div>{{item.likes}} Like</div>\n        </button>\n      </ion-col>\n      <ion-col col-4>\n        <button ion-button icon-left clear small color="dark" (click)="disikePost(item)" *ngIf="!item.isdisLike">\n          <ion-icon name="thumbs-down" ></ion-icon>\n          <div> {{item.dislikes}} Dislike </div>\n        </button>\n        <button ion-button icon-left clear small color="custom" (click)="dedislikePost(item)" *ngIf="item.isdisLike">\n          <ion-icon name="thumbs-down" ></ion-icon>\n          <div>{{item.dislikes}} Dislike</div>\n        </button>\n      </ion-col>\n      <ion-col col-4>\n        <button ion-button icon-left clear small color="dark" (click)="commentPost(item)" *ngIf="!item.isComment">\n          <ion-icon name="text"></ion-icon>\n          <div>{{item.comments}} Comment</div>\n        </button>\n        <button ion-button icon-left clear small color="custom" (click)="commentPost(item)" *ngIf="item.isComment" >\n          <ion-icon name="text"></ion-icon>\n          <div>{{item.comments}} Comment</div>\n        </button>\n      </ion-col>\n    \n    </ion-row>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\timetimeline\timetimeline.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_7_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_6__providers_data__["a" /* DataProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_firebase__["a" /* FirebaseProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_alert__["a" /* AlertProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */]])
    ], TimetimelinePage);
    return TimetimelinePage;
}());

//# sourceMappingURL=timetimeline.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupschannelPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_groups_groupschannel__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__newgroupchannel_newgroupchannel__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__groupchatchannel_groupchatchannel__ = __webpack_require__(121);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GroupschannelPage = /** @class */ (function () {
    function GroupschannelPage(navCtrl, ngZone, navParams, events, myModal, groupProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.ngZone = ngZone;
        this.navParams = navParams;
        this.events = events;
        this.myModal = myModal;
        this.groupProvider = groupProvider;
        this.Groups = [];
        this.User_Uid = this.groupProvider.UserUid;
        this.looding = true;
        this.events.subscribe('Channels', function () {
            _this.ngZone.run(function () {
                _this.looding = false;
                _this.Groups = _this.groupProvider.Groups;
            });
        });
    }
    GroupschannelPage.prototype.openNewGroupPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__newgroupchannel_newgroupchannel__["a" /* NewgroupchannelPage */]);
    };
    GroupschannelPage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('Channels');
    };
    GroupschannelPage.prototype.ionViewDidEnter = function () {
        this.groupProvider.getGroups();
    };
    GroupschannelPage.prototype.openGroupBody = function (groupDetails) {
        this.groupProvider.initialize(groupDetails);
        var modal = this.myModal.create(__WEBPACK_IMPORTED_MODULE_4__groupchatchannel_groupchatchannel__["a" /* GroupchatchannelPage */], {
            groupDetails: groupDetails
        });
        modal.present();
    };
    GroupschannelPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groupschannel',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\groupschannel\groupschannel.html"*/'<ion-header>\n\n\n\n  <ion-navbar color="logo">\n\n    <ion-title>Group</ion-title>\n\n    <ion-buttons end>\n\n    	<button ion-button icon-left icon-only (click)="openNewGroupPage()">\n\n    		<ion-icon name="add"></ion-icon>\n\n    		<ion-icon name="contacts"></ion-icon>\n\n    	</button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n\n\n\n\n\n\n\n\n<ion-spinner name="bubbles" *ngIf="looding" class="looding"></ion-spinner>\n\n\n\n\n\n    <div *ngIf="!looding">\n\n\n\n\n\n        <div class="no-items" *ngIf="blockList == 0">\n\n            <ion-icon name="contacts"></ion-icon>\n\n            <p>No Groups Found</p>\n\n        </div>\n\n\n\n\n\n\n\n        <div *ngIf="Groups != 0">\n\n        	<ion-item *ngFor="let group of Groups" (click)="openGroupBody(group)">\n\n\n\n\n\n        		<ion-avatar item-start *ngIf="group.Owner === User_Uid">\n\n        	      <img src="{{group.Picture}}">\n\n        	  </ion-avatar>\n\n        	  <h2 *ngIf="group.Owner === User_Uid">{{group.Name}}</h2>\n\n            <ion-note *ngIf="group.Owner === User_Uid">By Me</ion-note>\n\n\n\n\n\n\n\n\n\n            <ion-avatar item-start *ngIf="group.Owner != User_Uid">\n\n                <img src="{{group.Picture}}">\n\n            </ion-avatar>\n\n            <h2 *ngIf="group.Owner != User_Uid">{{group.Name}}</h2>\n\n\n\n\n\n        	</ion-item>\n\n        </div>\n\n\n\n\n\n    </div>\n\n\n\n\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\groupschannel\groupschannel.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2__providers_groups_groupschannel__["a" /* GroupschannelProvider */]])
    ], GroupschannelPage);
    return GroupschannelPage;
}());

//# sourceMappingURL=groupschannel.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenugpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tabs_tabs__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__newgroup_newgroup__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__chat_chat__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__groupbody_groupbody__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_group_group__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__group_group__ = __webpack_require__(188);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { LoggedinPage } from '../loggedin/loggedin';

//import { AboutPage } from '../about/about';
//import { CojpaymentsPage } from '../cojpayments/cojpayments';
//import { CojhistoryPage } from '../cojhistory/cojhistory';
//import { AboutPage } from '../about/about';
//import { ImghandlerProvider } from '../../providers/imghandler/imghandler';
//import { UserProvider } from '../../providers/user/user';



//import { UpdateProfilePage } from '../update-profile/update-profile';
//import { SignupPage } from '../signup/signup';
//import { ChatsPage } from '../chats/chats';
//import { BuddiesPage } from '../buddies/buddies';
//import { BuddychatPage } from '../buddychat/buddychat';
//import { GroupsProvider } from '../../providers/groups/groups';


//import { GroupinfoPage } from '../groupinfo/groupinfo';
//import { ChatProvider } from '../../providers/chat/chat';
//import { GroupchatchannelPage } from '../groupchatchannel/groupchatchannel';



var MenugpPage = /** @class */ (function () {
    function MenugpPage(navCtrl, groupProvider, platform, myModal, loadingCtrl, events, menu, afAuth, database, alertCtrl, zone, navParams, InAppBrowser) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.groupProvider = groupProvider;
        this.platform = platform;
        this.myModal = myModal;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
        this.menu = menu;
        this.afAuth = afAuth;
        this.database = database;
        this.alertCtrl = alertCtrl;
        this.zone = zone;
        this.navParams = navParams;
        this.InAppBrowser = InAppBrowser;
        this.Groups = [];
        this.User_Uid = this.groupProvider.UserUid;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_7__chat_chat__["a" /* ChatPage */];
        //tab2Root = NewgroupPage;
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_10__group_group__["a" /* GroupPage */];
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__tabs_tabs__["a" /* TabsPage */];
        this.pages = [
            { title: 'Chats', pageName: __WEBPACK_IMPORTED_MODULE_7__chat_chat__["a" /* ChatPage */], icon: 'assets/icons8-chat.png' },
            { title: 'Group Chats', pageName: __WEBPACK_IMPORTED_MODULE_6__newgroup_newgroup__["a" /* NewgroupPage */], icon: 'assets/icons8-user_group.png' }
        ];
        this.events.subscribe('Groups', function () {
            _this.zone.run(function () {
                _this.Groups = _this.groupProvider.Groups;
            });
        });
    }
    MenugpPage.prototype.newgroup = function () {
        var modal = this.myModal.create(__WEBPACK_IMPORTED_MODULE_6__newgroup_newgroup__["a" /* NewgroupPage */]);
        modal.present();
    };
    MenugpPage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('Groups');
    };
    MenugpPage.prototype.ionViewDidEnter = function () {
        this.groupProvider.getGroups();
    };
    MenugpPage.prototype.openGroupBody = function (groupDetails) {
        this.groupProvider.initialize(groupDetails);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__groupbody_groupbody__["a" /* GroupbodyPage */], {
            groupDetails: groupDetails
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Nav */])
    ], MenugpPage.prototype, "nav", void 0);
    MenugpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-menugp',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\menugp\menugp.html"*/'<!-- left menu -->\n\n<ion-menu [content]="content"  >\n\n    <ion-tabs color="logo" class="help"  #myTabs>\n\n      <ion-tab [root]="tab1Root"   tabIcon="chatboxes" class="menuicon ">\n\n      </ion-tab>     \n\n    \n\n      <ion-tab [root]="tab3Root" tabIcon="contact" class="menuicon " ></ion-tab>\n\n\n\n      <ion-tab [root]="tab2Root" tabIcon="add-circle" (ionSelect)="newgroup()" class="menuicon " ></ion-tab>\n\n          \n\n    \n\n    </ion-tabs>\n\n \n\n     \n\n</ion-menu>\n\n\n\n\n\n<!-- right menu -->\n\n<ion-menu side="right" [content]="content">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title>Secondary Menu</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n  \n\n  <ion-content>\n\n  <ion-list>\n\n    <button menuClose="right" ion-item *ngFor="let p of pages" (click)="openPage(p)">\n\n      {{p.title}}\n\n    </button>\n\n  </ion-list>\n\n  </ion-content>\n\n</ion-menu>\n\n<ion-nav #content [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\menugp\menugp.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_9__providers_group_group__["a" /* GroupProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* MenuController */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
    ], MenugpPage);
    return MenugpPage;
}());

//# sourceMappingURL=menugp.js.map

/***/ }),

/***/ 251:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 251;

/***/ }),

/***/ 252:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return snapshotToArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(584);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2__ = __webpack_require__(585);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_login_login__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_register_register__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_profile_profile__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angularfire2_database__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_geolocation__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_network__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_http__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_camera__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_chat_chat__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_auth_auth__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_file__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_file_chooser__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_file_path__ = __webpack_require__(586);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_device__ = __webpack_require__(587);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_diagnostic__ = __webpack_require__(588);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_location_accuracy__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_in_app_browser__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__angular_common_http__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_accountprofilecot_accountprofilecot__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_call_number__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_panic_panic__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__angular_forms__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31_ionic_selectable__ = __webpack_require__(589);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__ionic_native_native_storage__ = __webpack_require__(590);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_preference_preference__ = __webpack_require__(591);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_testing_testing__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_timeline_timeline_module__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_social_sharing__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_reactions_reactions__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__ionic_native_file_opener__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_add_comment_add_comment__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_newgroup_newgroup__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_presence_presence__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_settings_settings__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_users_users__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__providers_notification_notification__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__providers_request_request__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__providers_block_block__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__providers_users_users__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__providers_friends_friends__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__providers_group_group__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_editprofile_editprofile__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__pages_notification_unread_notification_unread__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_notification_read_notification_read__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_groupaddmember_groupaddmember__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__pages_groupmember_groupmember__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__pages_chat_chat__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__pages_group_group__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__pages_reset_password_reset_password__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__pages_friends_friends__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__pages_block_block__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__pages_requests_requests__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__pages_notifications_notifications__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__pages_chatbody_chatbody__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__pages_groupbody_groupbody__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__ionic_storage__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__ionic_native_date_picker__ = __webpack_require__(592);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__ionic_native_toast__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__pages_menugp_menugp__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68__providers_storage_storage__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_69__pages_post_post__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_70__providers_imghandler_imghandler__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_71__providers_groups_groupschannel__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_72__pages_groupbuddieschannel_groupbuddieschannel__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_73__pages_groupchatchannel_groupchatchannel__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_74__pages_groupinfochannel_groupinfochannel__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_75__pages_groupmemberschannel_groupmemberschannel__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_76__pages_groupschannel_groupschannel__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_77__pages_newgroupchannel_newgroupchannel__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_78__pages_donation_donation__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_79__pages_payments_payments__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_80__providers_user_user__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_81__providers_locations_locations__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_82__providers_connectivity_connectivity__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_83__providers_google_maps_google_maps__ = __webpack_require__(597);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_84__pages_nearby_nearby__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_85__pages_booked_booked__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_86__pages_bookings_bookings__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_87__pages_sars_sars__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_88__pages_nearcentre_nearcentre__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_89__pages_school_school__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_90__pages_restaurants_restaurants__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_91__pages_place_details_place_details__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_92__pages_place_direction_place_direction__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_93__providers_data__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_94__pages_timetimeline_timetimeline__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_95__providers_alert__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_96__providers_firebase__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_97__providers_image__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_98__providers_loading__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_99__pages_comments_comments__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_100__pages_image_modal_image_modal__ = __webpack_require__(599);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_101__ionic_native_google_maps__ = __webpack_require__(319);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



































































































//import { VideoProvider } from '../providers/video';



//import { RequestsProvider } from '../providers/requests/requests';
//import { Clipboard } from '@ionic-native/clipboard';
//import { ClipboardOriginal } from '@ionic-native/clipboard';
//import { PhotoViewer } from '@ionic-native/photo-viewer';
//mport { ClipboardOriginal } from '@ionic-native/clipboard';
var firebaseAuth = {
    apiKey: "AIzaSyB2NbbCFdW5z5Mh_YNY_K1bOip8sKMrPM8",
    authDomain: "municipality-732a0.firebaseapp.com",
    databaseURL: "https://municipality-732a0.firebaseio.com",
    projectId: "municipality-732a0",
    storageBucket: "municipality-732a0.appspot.com",
    messagingSenderId: "1033486321710"
};
var snapshotToArray = function (snapshot) {
    var returnArray = [];
    snapshot.forEach(function (element) {
        var item = element.val();
        item.key = element.key;
        returnArray.push(item);
    });
    return returnArray;
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_add_comment_add_comment__["a" /* AddCommentPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_reactions_reactions__["a" /* ReactionsPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_preference_preference__["a" /* PreferencePage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_accountprofilecot_accountprofilecot__["a" /* AccountProfilePagecotcot */],
                __WEBPACK_IMPORTED_MODULE_29__pages_panic_panic__["a" /* PanicPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_testing_testing__["a" /* TestingPage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_newgroup_newgroup__["a" /* NewgroupPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_presence_presence__["a" /* PresencePage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_users_users__["a" /* UsersPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_editprofile_editprofile__["a" /* EditprofilePage */],
                __WEBPACK_IMPORTED_MODULE_51__pages_notification_unread_notification_unread__["a" /* NotificationUnreadPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_notification_read_notification_read__["a" /* NotificationReadPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_groupaddmember_groupaddmember__["a" /* GroupaddmemberPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_groupmember_groupmember__["a" /* GroupmemberPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_group_group__["a" /* GroupPage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_reset_password_reset_password__["a" /* ResetPasswordPage */],
                __WEBPACK_IMPORTED_MODULE_58__pages_friends_friends__["a" /* FriendsPage */],
                __WEBPACK_IMPORTED_MODULE_59__pages_block_block__["a" /* BlockPage */],
                __WEBPACK_IMPORTED_MODULE_60__pages_requests_requests__["a" /* RequestsPage */],
                __WEBPACK_IMPORTED_MODULE_61__pages_notifications_notifications__["a" /* NotificationsPage */],
                __WEBPACK_IMPORTED_MODULE_62__pages_chatbody_chatbody__["a" /* ChatbodyPage */],
                __WEBPACK_IMPORTED_MODULE_63__pages_groupbody_groupbody__["a" /* GroupbodyPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_groupaddmember_groupaddmember__["a" /* GroupaddmemberPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_groupmember_groupmember__["a" /* GroupmemberPage */],
                __WEBPACK_IMPORTED_MODULE_67__pages_menugp_menugp__["a" /* MenugpPage */],
                __WEBPACK_IMPORTED_MODULE_69__pages_post_post__["a" /* PostPage */],
                __WEBPACK_IMPORTED_MODULE_72__pages_groupbuddieschannel_groupbuddieschannel__["a" /* GroupbuddieschannelPage */],
                __WEBPACK_IMPORTED_MODULE_73__pages_groupchatchannel_groupchatchannel__["a" /* GroupchatchannelPage */],
                __WEBPACK_IMPORTED_MODULE_74__pages_groupinfochannel_groupinfochannel__["a" /* GroupinfochannelPage */],
                __WEBPACK_IMPORTED_MODULE_75__pages_groupmemberschannel_groupmemberschannel__["a" /* GroupmembersChannelPage */],
                __WEBPACK_IMPORTED_MODULE_76__pages_groupschannel_groupschannel__["a" /* GroupschannelPage */],
                __WEBPACK_IMPORTED_MODULE_77__pages_newgroupchannel_newgroupchannel__["a" /* NewgroupchannelPage */],
                __WEBPACK_IMPORTED_MODULE_78__pages_donation_donation__["a" /* DonationPage */],
                __WEBPACK_IMPORTED_MODULE_79__pages_payments_payments__["a" /* PaymentsPage */],
                __WEBPACK_IMPORTED_MODULE_84__pages_nearby_nearby__["a" /* NearbyPage */],
                __WEBPACK_IMPORTED_MODULE_85__pages_booked_booked__["a" /* BookedPage */],
                __WEBPACK_IMPORTED_MODULE_86__pages_bookings_bookings__["a" /* BookingsPage */],
                __WEBPACK_IMPORTED_MODULE_87__pages_sars_sars__["a" /* SarsPage */],
                __WEBPACK_IMPORTED_MODULE_88__pages_nearcentre_nearcentre__["a" /* NearcentrePage */],
                __WEBPACK_IMPORTED_MODULE_89__pages_school_school__["a" /* SchoolPage */],
                __WEBPACK_IMPORTED_MODULE_90__pages_restaurants_restaurants__["a" /* RestaurantsPage */],
                __WEBPACK_IMPORTED_MODULE_91__pages_place_details_place_details__["a" /* PlaceDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_92__pages_place_direction_place_direction__["a" /* PlaceDirectionPage */],
                __WEBPACK_IMPORTED_MODULE_94__pages_timetimeline_timetimeline__["a" /* TimetimelinePage */],
                __WEBPACK_IMPORTED_MODULE_99__pages_comments_comments__["a" /* CommentsPage */],
                __WEBPACK_IMPORTED_MODULE_100__pages_image_modal_image_modal__["a" /* ImageModalPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/donation/donation.module#DonationPageModule', name: 'DonationPage', segment: 'donation', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/comments/comments.module#CommentsPageModule', name: 'CommentsPage', segment: 'comments', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupchatchannel/groupchatchannel.module#GroupchatchannelPageModule', name: 'GroupchatchannelPage', segment: 'groupchatchannel', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupbuddieschannel/groupbuddieschannel.module#GroupbuddieschannelPageModule', name: 'GroupbuddieschannelPage', segment: 'groupbuddieschannel', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupinfochannel/groupinfochannel.module#GroupinfochannelPageModule', name: 'GroupinfochannelPage', segment: 'groupinfochannel', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupmemberschannel/groupmemberschannel.module#GroupmembersChannelPageModule', name: 'GroupmembersChannelPage', segment: 'groupmemberschannel', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/nearby/nearby.module#NearbyPageModule', name: 'NearbyPage', segment: 'nearby', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupschannel/groupschannel.module#GroupschannelPageModule', name: 'GroupschannelPage', segment: 'groupschannel', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/newgroupchannel/newgroupchannel.module#NewgroupchannelPageModule', name: 'NewgroupchannelPage', segment: 'newgroupchannel', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/panic/panic.module#PanicPageModule', name: 'PanicPage', segment: 'panic', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/payments/payments.module#PaymentsPageModule', name: 'PaymentsPage', segment: 'payments', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/post/post.module#PostPageModule', name: 'PostPage', segment: 'post', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menugp/menugp.module#MenugpPageModule', name: 'MenugpPage', segment: 'menugp', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reactions/reactions.module#ReactionsPageModule', name: 'ReactionsPage', segment: 'reactions', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/testing/testing.module#TestingPageModule', name: 'TestingPage', segment: 'testing', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/timetimeline/timetimeline.module#TimetimelinePageModule', name: 'TimetimelinePage', segment: 'timetimeline', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/timecomment/timecomment.module#TimecommentPageModule', name: 'TimecommentPage', segment: 'timecomment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/timeline/timeline.module#TimelinePageModule', name: 'TimelinePage', segment: 'timeline', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_7_angularfire2__["a" /* AngularFireModule */].initializeApp(firebaseAuth),
                __WEBPACK_IMPORTED_MODULE_64__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_8_angularfire2_auth__["b" /* AngularFireAuthModule */],
                __WEBPACK_IMPORTED_MODULE_30__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_12_angularfire2_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_26__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_15__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_31_ionic_selectable__["a" /* IonicSelectableModule */],
                __WEBPACK_IMPORTED_MODULE_35__pages_timeline_timeline_module__["TimelinePageModule"],
                __WEBPACK_IMPORTED_MODULE_26__angular_common_http__["b" /* HttpClientModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_reactions_reactions__["a" /* ReactionsPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_preference_preference__["a" /* PreferencePage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_accountprofilecot_accountprofilecot__["a" /* AccountProfilePagecotcot */],
                __WEBPACK_IMPORTED_MODULE_29__pages_panic_panic__["a" /* PanicPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_testing_testing__["a" /* TestingPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_add_comment_add_comment__["a" /* AddCommentPage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_newgroup_newgroup__["a" /* NewgroupPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_presence_presence__["a" /* PresencePage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_users_users__["a" /* UsersPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_editprofile_editprofile__["a" /* EditprofilePage */],
                __WEBPACK_IMPORTED_MODULE_51__pages_notification_unread_notification_unread__["a" /* NotificationUnreadPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_notification_read_notification_read__["a" /* NotificationReadPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_group_group__["a" /* GroupPage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_reset_password_reset_password__["a" /* ResetPasswordPage */],
                __WEBPACK_IMPORTED_MODULE_58__pages_friends_friends__["a" /* FriendsPage */],
                __WEBPACK_IMPORTED_MODULE_59__pages_block_block__["a" /* BlockPage */],
                __WEBPACK_IMPORTED_MODULE_60__pages_requests_requests__["a" /* RequestsPage */],
                __WEBPACK_IMPORTED_MODULE_61__pages_notifications_notifications__["a" /* NotificationsPage */],
                __WEBPACK_IMPORTED_MODULE_62__pages_chatbody_chatbody__["a" /* ChatbodyPage */],
                __WEBPACK_IMPORTED_MODULE_63__pages_groupbody_groupbody__["a" /* GroupbodyPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_groupaddmember_groupaddmember__["a" /* GroupaddmemberPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_groupmember_groupmember__["a" /* GroupmemberPage */],
                __WEBPACK_IMPORTED_MODULE_67__pages_menugp_menugp__["a" /* MenugpPage */],
                __WEBPACK_IMPORTED_MODULE_69__pages_post_post__["a" /* PostPage */],
                __WEBPACK_IMPORTED_MODULE_72__pages_groupbuddieschannel_groupbuddieschannel__["a" /* GroupbuddieschannelPage */],
                __WEBPACK_IMPORTED_MODULE_73__pages_groupchatchannel_groupchatchannel__["a" /* GroupchatchannelPage */],
                __WEBPACK_IMPORTED_MODULE_74__pages_groupinfochannel_groupinfochannel__["a" /* GroupinfochannelPage */],
                __WEBPACK_IMPORTED_MODULE_75__pages_groupmemberschannel_groupmemberschannel__["a" /* GroupmembersChannelPage */],
                __WEBPACK_IMPORTED_MODULE_76__pages_groupschannel_groupschannel__["a" /* GroupschannelPage */],
                __WEBPACK_IMPORTED_MODULE_77__pages_newgroupchannel_newgroupchannel__["a" /* NewgroupchannelPage */],
                __WEBPACK_IMPORTED_MODULE_78__pages_donation_donation__["a" /* DonationPage */],
                __WEBPACK_IMPORTED_MODULE_79__pages_payments_payments__["a" /* PaymentsPage */],
                __WEBPACK_IMPORTED_MODULE_84__pages_nearby_nearby__["a" /* NearbyPage */],
                __WEBPACK_IMPORTED_MODULE_85__pages_booked_booked__["a" /* BookedPage */],
                __WEBPACK_IMPORTED_MODULE_86__pages_bookings_bookings__["a" /* BookingsPage */],
                __WEBPACK_IMPORTED_MODULE_87__pages_sars_sars__["a" /* SarsPage */],
                __WEBPACK_IMPORTED_MODULE_88__pages_nearcentre_nearcentre__["a" /* NearcentrePage */],
                __WEBPACK_IMPORTED_MODULE_89__pages_school_school__["a" /* SchoolPage */],
                __WEBPACK_IMPORTED_MODULE_90__pages_restaurants_restaurants__["a" /* RestaurantsPage */],
                __WEBPACK_IMPORTED_MODULE_91__pages_place_details_place_details__["a" /* PlaceDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_92__pages_place_direction_place_direction__["a" /* PlaceDirectionPage */],
                __WEBPACK_IMPORTED_MODULE_94__pages_timetimeline_timetimeline__["a" /* TimetimelinePage */],
                __WEBPACK_IMPORTED_MODULE_99__pages_comments_comments__["a" /* CommentsPage */],
                __WEBPACK_IMPORTED_MODULE_100__pages_image_modal_image_modal__["a" /* ImageModalPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_38__ionic_native_file_opener__["a" /* FileOpener */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_file_chooser__["a" /* FileChooser */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_66__ionic_native_toast__["a" /* Toast */],
                __WEBPACK_IMPORTED_MODULE_65__ionic_native_date_picker__["a" /* DatePicker */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_call_number__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_32__ionic_native_native_storage__["a" /* NativeStorage */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_17__providers_chat_chat__["a" /* ChatProvider */],
                __WEBPACK_IMPORTED_MODULE_18__providers_auth_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_device__["a" /* Device */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_diagnostic__["a" /* Diagnostic */],
                __WEBPACK_IMPORTED_MODULE_47__providers_users_users__["a" /* UsersProvider */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_36__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_44__providers_notification_notification__["a" /* NotificationProvider */],
                __WEBPACK_IMPORTED_MODULE_45__providers_request_request__["a" /* RequestProvider */],
                __WEBPACK_IMPORTED_MODULE_46__providers_block_block__["a" /* BlockProvider */],
                __WEBPACK_IMPORTED_MODULE_47__providers_users_users__["a" /* UsersProvider */],
                __WEBPACK_IMPORTED_MODULE_48__providers_friends_friends__["a" /* FriendsProvider */],
                __WEBPACK_IMPORTED_MODULE_49__providers_group_group__["a" /* GroupProvider */],
                __WEBPACK_IMPORTED_MODULE_68__providers_storage_storage__["a" /* StorageProvider */],
                __WEBPACK_IMPORTED_MODULE_70__providers_imghandler_imghandler__["a" /* ImghandlerProvider */],
                __WEBPACK_IMPORTED_MODULE_71__providers_groups_groupschannel__["a" /* GroupschannelProvider */],
                __WEBPACK_IMPORTED_MODULE_80__providers_user_user__["a" /* UserProvider */],
                __WEBPACK_IMPORTED_MODULE_82__providers_connectivity_connectivity__["a" /* ConnectivityProvider */],
                __WEBPACK_IMPORTED_MODULE_83__providers_google_maps_google_maps__["a" /* GoogleMapsProvider */],
                __WEBPACK_IMPORTED_MODULE_81__providers_locations_locations__["a" /* LocationsProvider */],
                __WEBPACK_IMPORTED_MODULE_93__providers_data__["a" /* DataProvider */],
                __WEBPACK_IMPORTED_MODULE_95__providers_alert__["a" /* AlertProvider */],
                __WEBPACK_IMPORTED_MODULE_96__providers_firebase__["a" /* FirebaseProvider */],
                __WEBPACK_IMPORTED_MODULE_97__providers_image__["a" /* ImageProvider */],
                __WEBPACK_IMPORTED_MODULE_98__providers_loading__["a" /* LoadingProvider */],
                //VideoProvider
                __WEBPACK_IMPORTED_MODULE_101__ionic_native_google_maps__["a" /* GoogleMaps */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 27:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthProvider = /** @class */ (function () {
    function AuthProvider(afAuth, afDB, evente) {
        this.afAuth = afAuth;
        this.afDB = afDB;
        this.evente = evente;
        this.UserUid = window.localStorage.getItem('userid');
    }
    AuthProvider.prototype.getUserDetails = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Users').child(_this.UserUid).once('value', function (snap) {
                var res = snap.val();
                resolve(res);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    AuthProvider.prototype.updateProfile = function (userDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Users').child(_this.UserUid).update({
                Name: userDetails.Name,
                Phone: userDetails.Phone,
                about: userDetails.about,
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    AuthProvider.prototype.getMyDetails = function () {
        var _this = this;
        this.afDB.database.ref('Users').child(this.UserUid).on('value', function (snap) {
            _this.myDetails;
            _this.myDetails = snap.val();
            _this.evente.publish('myDetails');
        });
    };
    AuthProvider.prototype.getProfileDetails = function (userDetails) {
        var _this = this;
        this.afDB.database.ref('Users').child(userDetails.Id).on('value', function (snap) {
            _this.ProfileDetails;
            _this.ProfileDetails = snap.val();
            _this.evente.publish('ProfileDetails');
        });
    };
    AuthProvider.prototype.uploadProfilePhoto = function (picURL) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.storage().ref('Profile Picture').child(_this.UserUid + '.jpg').putString(picURL, __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.storage.StringFormat.DATA_URL).then(function (snapshot) {
                snapshot.ref.getDownloadURL().then(function (downloadURL) {
                    _this.afDB.database.ref('Users').child(_this.UserUid).update({
                        Photo: downloadURL
                    }).then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    AuthProvider.prototype.uploadCover = function (picURL) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.storage().ref('Covers').child(_this.UserUid + '.jpg').putString(picURL, __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.storage.StringFormat.DATA_URL).then(function (snapshot) {
                snapshot.ref.getDownloadURL().then(function (downloadURL) {
                    _this.afDB.database.ref('Users').child(_this.UserUid).update({
                        bgPhoto: downloadURL
                    }).then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    AuthProvider.prototype.login = function (userDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afAuth.auth.signInWithEmailAndPassword(userDetails.Email, userDetails.Password).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    AuthProvider.prototype.forgetPassword = function (userDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afAuth.auth.sendPasswordResetEmail(userDetails.Email).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    AuthProvider.prototype.register = function (userDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afAuth.auth.createUserWithEmailAndPassword(userDetails.Email, userDetails.Password).then(function () {
                _this.afDB.database.ref('Users').child(_this.afAuth.auth.currentUser.uid).set({
                    Name: userDetails.Name,
                    Email: userDetails.Email,
                    Phone: userDetails.Phone,
                    Id: _this.afAuth.auth.currentUser.uid,
                    Status: 'Offline',
                    about: "Hey there! I'm using Chat App.",
                    Photo: 'https://firebasestorage.googleapis.com/v0/b/chat-app-f6a12.appspot.com/o/user.png?alt=media&token=79419e48-423a-42c3-92b2-16027651b931'
                }).then(function () {
                    resolve(true);
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    AuthProvider.prototype.logOut = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afAuth.auth.signOut().then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    AuthProvider.prototype.onlineStatus = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Users').child(_this.UserUid).update({
                Status: 'Online'
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    AuthProvider.prototype.offlineStatuss = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Users').child(_this.UserUid).update({
                Status: 'Offline'
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    AuthProvider.prototype.offlineStatus = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            var ref = _this.afDB.database.ref('Users').child(_this.UserUid);
            ref.onDisconnect().update({
                Status: 'Offline'
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 294:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/comments/comments.module": [
		601,
		17
	],
	"../pages/donation/donation.module": [
		600,
		16
	],
	"../pages/groupbuddieschannel/groupbuddieschannel.module": [
		603,
		15
	],
	"../pages/groupchatchannel/groupchatchannel.module": [
		602,
		14
	],
	"../pages/groupinfochannel/groupinfochannel.module": [
		604,
		13
	],
	"../pages/groupmemberschannel/groupmemberschannel.module": [
		605,
		12
	],
	"../pages/groupschannel/groupschannel.module": [
		607,
		11
	],
	"../pages/menugp/menugp.module": [
		612,
		10
	],
	"../pages/nearby/nearby.module": [
		606,
		9
	],
	"../pages/newgroupchannel/newgroupchannel.module": [
		608,
		8
	],
	"../pages/panic/panic.module": [
		609,
		7
	],
	"../pages/payments/payments.module": [
		610,
		6
	],
	"../pages/post/post.module": [
		611,
		5
	],
	"../pages/reactions/reactions.module": [
		613,
		4
	],
	"../pages/tabs/tabs.module": [
		615,
		3
	],
	"../pages/testing/testing.module": [
		614,
		2
	],
	"../pages/timecomment/timecomment.module": [
		617,
		0
	],
	"../pages/timeline/timeline.module": [
		333
	],
	"../pages/timetimeline/timetimeline.module": [
		616,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 294;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PresencePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_chat_chat__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__settings_settings__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_notification_notification__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__notifications_notifications__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_request_request__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_auth_auth__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_friends_friends__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__users_users__ = __webpack_require__(186);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import { RequestProvider } from '../../providers/requests/requests';

//import { NotificationsPage } from '../notifications/notifications';






/**
 * Generated class for the PresencePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PresencePage = /** @class */ (function () {
    function PresencePage(requestservice, chatProvider, authProvider, friendsProvider, navCtrl, notificationsProvider, events, ngZone, chatservice, afDatabase, afAuth, navParams) {
        var _this = this;
        this.requestservice = requestservice;
        this.chatProvider = chatProvider;
        this.authProvider = authProvider;
        this.friendsProvider = friendsProvider;
        this.navCtrl = navCtrl;
        this.notificationsProvider = notificationsProvider;
        this.events = events;
        this.ngZone = ngZone;
        this.chatservice = chatservice;
        this.afDatabase = afDatabase;
        this.afAuth = afAuth;
        this.navParams = navParams;
        this.buttonClicked = false;
        this.Friends = [];
        this.allUsers = [];
        this.Conversations = [];
        this.onlineStatus = 'Online';
        this.offlineStatus = "Offline";
        this.looding = true;
        this.events.subscribe('Notifications', function () {
            _this.ngZone.run(function () {
                var res = _this.notificationsProvider.NotificationsUnread;
                _this.notifications = res.length;
            });
        });
        this.events.subscribe('Friends', function () {
            _this.ngZone.run(function () {
                _this.Friends = _this.friendsProvider.Friends;
            });
        });
        var days = ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
        var today = new Date;
        var toYear = today.getFullYear();
        var toMonth = 1 + today.getMonth();
        var toDays = today.getDate();
        var todayRes1 = toYear + '/' + toMonth + '/' + toDays;
        var todayRes2 = toYear + '/' + toMonth;
        var todayRes3 = toYear;
        this.events.subscribe('Conversations', function () {
            _this.ngZone.run(function () {
                _this.looding = false;
                _this.Conversations = _this.chatProvider.Conversations;
                _this.allUsers = _this.chatProvider.buddyUsers;
                for (var key in _this.Conversations) {
                    var d = new Date(_this.Conversations[key].Time);
                    var years = d.getFullYear();
                    var month = 1 + d.getMonth();
                    var day = d.getDate();
                    var hours = d.getHours();
                    var minutes = '0' + d.getMinutes();
                    var messageTime1 = years + '/' + month + '/' + day;
                    var messageTime2 = years + '/' + month;
                    var messageTime3 = years;
                    var DN = toDays - day;
                    if (messageTime1 == todayRes1) {
                        _this.Conversations[key].Time = hours + ":" + minutes.substr(-2);
                    }
                    else {
                        if (messageTime2 == todayRes2) {
                            if (DN == 1) {
                                _this.Conversations[key].Time = 'Yestersay,' + hours + ":" + minutes.substr(-2);
                            }
                            else if (DN < 7) {
                                _this.Conversations[key].Time = days[DN] + hours + ":" + minutes.substr(-2);
                            }
                            else if (DN > 7) {
                                _this.Conversations[key].Time = months[month] + ',' + day + ',' + years;
                            }
                        }
                        else {
                            _this.Conversations[key].Time = months[month] + ',' + day + ',' + years;
                        }
                    }
                }
            });
        });
    }
    PresencePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.requestservice.getReceivedRequests();
        this.requestservice.getReceivedRequests();
        this.myfriends = [];
        this.events.subscribe('gotrequests', function () {
            _this.myrequests = [];
            _this.myrequests = _this.requestservice.myDetails;
        });
        this.events.subscribe('friends', function () {
            _this.myfriends = [];
            _this.ngZone.run(function () {
                _this.myfriends = _this.requestservice.getReceivedRequests;
            });
        });
    };
    PresencePage.prototype.ionViewDidLeave = function () {
        this.events.unsubscribe('gotrequests');
        this.events.unsubscribe('friends');
        this.events.subscribe('friends');
        this.events.subscribe('Friends');
        this.events.subscribe('Conversations');
        this.events.subscribe('Notifications');
    };
    PresencePage.prototype.ionViewDidEnter = function () {
        this.friendsProvider.getFriends();
        this.chatProvider.getConversations();
        this.requestservice.getReceivedRequests();
        this.notificationsProvider.getMyNotificationsUnread();
    };
    PresencePage.prototype.notification = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__notifications_notifications__["a" /* NotificationsPage */]);
    };
    PresencePage.prototype.settings = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__settings_settings__["a" /* SettingsPage */]);
    };
    PresencePage.prototype.openUsersPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__users_users__["a" /* UsersPage */]);
    };
    PresencePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-presence',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\presence\presence.html"*/'<!--\n  Generated template for the PresencePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="logo">\n    <ion-title>User Dashboard</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="back" >\n  <div class="spacer" style="height: 20px;"></div>\n\n  <div class="center" ><button class="spacer"  ion-button round  center  style="height: 40px; width:300px;  text-transform: none; " (click)="openUsersPage()" ion-text color="logo" center><p ion-text color="light"> Invite Member +</p> </button></div>\n  <div class="spacer" style="height: 20px;"></div>\n\n  <ion-row style="margin-left: 20px;">\n    <ion-col size="6"  >\n      <img src="assets/icons8-search.png" (click)="openUsersPage()"  style="height: 30px; width: 30px; margin-left: 10px;">\n    </ion-col>\n    <ion-col>\n      <img src="assets/icons8-pin.png" style="height: 30px; width: 30px;">\n    </ion-col>\n    <ion-col>\n      <img src="assets/icons8-notification.png"   (click)="notification()" style="height: 30px; width: 30px;"><ion-badge item-end>{{notifications}}</ion-badge>\n\n    </ion-col>\n    <ion-col>\n      <img src="assets/icons8-settings.png" style="height: 30px; width: 30px;" (click)="settings()">\n    </ion-col>\n  </ion-row>\n  <div class="spacer" style="height: 10px;"></div>\n\n        <ion-label color="light" style="font-size: 20px;  margin-left: 20px;" >\n         LEADEARS\n        </ion-label>\n        <ion-list no-lines style="margin-left: 20px;">\n          <ion-item class="back" *ngFor="let item of Conversations.reverse(); let i = index" (click)="openChatBody(allUsers[i])" block round outline color="light" >\n            <ion-avatar item-left>\n              <img src="{{allUsers[i].Photo}}">\n              <h3 text-left style="font-size: 16px; font-weight: bold;" ion-text color="light">{{allUsers[i].Name}} </h3>\n              <h3 text-left style="font-size: 16px; font-weight: bold;" ion-text color="light">Admin</h3>\n\n            \n\n\n            </ion-avatar>\n          </ion-item>\n        </ion-list> \n        \n\n        <ion-list no-lines style="margin-left: 20px;">\n          <ion-item  class="back" *ngFor="let item of myfriends" (click)="buddychat(item)" block round outline color="light"  >\n            <ion-avatar item-left>\n              <img src={{item.photoURL}}>\n            </ion-avatar>\n            <h3 ion-text color="light" style="font-size: 16px;">{{item.Name}}</h3>\n          \n          </ion-item>\n        </ion-list> \n        <div class="spacer" style="height: 20px;"></div>\n\n        <ion-label color="light" style="font-size: 20px; margin-left: 20px;">\n          ONLINE/OFFLINE\n        </ion-label>\n        <ion-list no-lines style="margin-left: 20px;">\n          <ion-item class="back" *ngFor="let item of Conversations.reverse(); let i = index" (click)="openChatBody(allUsers[i])" block round outline color="light" >\n            <ion-avatar item-left>\n              <img src="{{allUsers[i].Photo}}">\n              <h3 text-left style="font-size: 16px; font-weight: bold;" ion-text color="light">{{allUsers[i].Name}} </h3>\n              <div class="online" *ngIf="allUsers[i].Status == onlineStatus" style="margin-right: 20px;"></div>\n              <div class="offline" *ngIf="allUsers[i].Status == offlineStatus" style="margin-right: 20px;"></div>\n\n\n            </ion-avatar>\n          </ion-item>\n        </ion-list> \n\n</ion-content>\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\presence\presence.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8__providers_request_request__["a" /* RequestProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_chat_chat__["a" /* ChatProvider */], __WEBPACK_IMPORTED_MODULE_9__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_10__providers_friends_friends__["a" /* FriendsProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_6__providers_notification_notification__["a" /* NotificationProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_4__providers_chat_chat__["a" /* ChatProvider */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], PresencePage);
    return PresencePage;
}());

//# sourceMappingURL=presence.js.map

/***/ }),

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__friends_friends__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__block_block__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__requests_requests__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__post_post__ = __webpack_require__(122);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









//import { SuperTabsController } from 'ionic2-super-tabs';
var SettingsPage = /** @class */ (function () {
    function SettingsPage(navCtrl, navParams, authProvider, afAuth) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.authProvider = authProvider;
        this.afAuth = afAuth;
        this.userDetails = {
            Name: '',
            Email: '',
            Phone: '',
            Id: '',
            Status: '',
            proPhoto: '',
            bgPhoto: ''
        };
    }
    SettingsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // this.superTabsCtrl.showToolbar(true)
        this.authProvider.getUserDetails().then(function (res) {
            _this.userDetails.Name = res.Name;
            _this.userDetails.proPhoto = res.Photo;
            _this.userDetails.Phone = res.Phone;
            _this.userDetails.Status = res.Status;
            _this.userDetails.Id = res.Id;
            _this.userDetails.bgPhoto = res.bgPhoto;
            _this.userDetails.Email = res.Email;
        }).catch(function (err) {
            console.log(err);
        });
    };
    SettingsPage.prototype.openProfilePage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__post_post__["a" /* PostPage */], {
            userDetails: this.userDetails
        });
    };
    SettingsPage.prototype.openFriendsPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__friends_friends__["a" /* FriendsPage */]);
    };
    SettingsPage.prototype.openRequestsPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__requests_requests__["a" /* RequestsPage */]);
    };
    SettingsPage.prototype.openBlockPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__block_block__["a" /* BlockPage */]);
    };
    SettingsPage.prototype.logOut = function () {
        var _this = this;
        this.afAuth.auth.signOut();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* LoginPage */]);
        this.authProvider.offlineStatuss().then(function () {
            _this.authProvider.logOut().then(function () {
                _this.navCtrl.parent.parent.setRoot(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* LoginPage */]);
                window.localStorage.clear();
            }).catch(function (err) {
                console.log(err);
            });
        }).catch(function (err) {
            console.log(err);
        });
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-settings',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\settings\settings.html"*/'<ion-header>\n\n  <ion-navbar color="logo">\n	  \n    <ion-title>Settings</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n\n<ion-item (click)="openProfilePage()">\n	<ion-avatar item-start>\n	    <img src="{{userDetails.proPhoto}}">\n	</ion-avatar>\n	<h2>{{userDetails.Name}}</h2>\n	<p>View Profile</p>\n</ion-item>\n\n\n<ion-item (click)="openFriendsPage()">\n	Friends\n</ion-item>\n\n<ion-item (click)="openRequestsPage()">\n	Requests\n</ion-item>\n\n<ion-item (click)="openBlockPage()">\n	Block\n</ion-item>\n\n\n<br>\n\n\n\n\n<button ion-button block color="logo" (click)="logOut()">\n	LogOut\n</button>\n\n\n\n\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\settings\settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__["a" /* AngularFireAuth */]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FriendsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_friends_friends__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_block_block__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profile_profile__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__chatbody_chatbody__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__ = __webpack_require__(92);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







//import { SuperTabsController } from 'ionic2-super-tabs';
var FriendsPage = /** @class */ (function () {
    function FriendsPage(chatProvider, navCtrl, loadCtrl, platform, blockProvider, alertCtrl, friendsProvider) {
        this.chatProvider = chatProvider;
        this.navCtrl = navCtrl;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.blockProvider = blockProvider;
        this.alertCtrl = alertCtrl;
        this.friendsProvider = friendsProvider;
        this.Friends = [];
        this.looding = true;
        this.userDetailss = this.chatProvider.UserUid;
    }
    FriendsPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    FriendsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        //this.superTabsCtrl.showToolbar(false)
        this.friendsProvider.getAllFriends().then(function (res) {
            _this.looding = false;
            _this.Friends = res;
        }).catch(function (err) {
            console.log(err);
        });
    };
    FriendsPage.prototype.showFriendsConfirmation = function (userDetails) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: userDetails.Name,
            message: 'Tap on option',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        _this.showToast('Cancel');
                    }
                },
                {
                    text: 'Unfriend ' + userDetails.Name,
                    handler: function () {
                        _this.unfriend(userDetails);
                    }
                },
                {
                    text: 'Block ' + userDetails.Name,
                    handler: function () {
                        _this.block(userDetails);
                    }
                },
                {
                    text: 'Chat With' + userDetails.Name,
                    handler: function () {
                        _this.openChatBody(userDetails);
                    }
                },
                {
                    text: 'View Profile',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__profile_profile__["a" /* ProfilePage */], {
                            userDetails: userDetails
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    FriendsPage.prototype.unfriend = function (userDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Unfriending ' + userDetails.Name + ' ...'
        });
        load.present();
        this.friendsProvider.unFriend(userDetails).then(function () {
            if (_this.Friends.length > 1) {
                _this.friendsProvider.getAllFriends().then(function (res) {
                    _this.Friends = res;
                }).then(function () {
                    load.dismiss();
                    _this.showToast(userDetails.Name + ' has been unfriended');
                }).catch(function (err) {
                    load.dismiss();
                    _this.showToast(err);
                });
            }
            else {
                load.dismiss();
                _this.navCtrl.pop();
                _this.showToast(userDetails.Name + ' has been unfriended');
            }
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    FriendsPage.prototype.block = function (userDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Blocking ' + userDetails.Name + ' ...'
        });
        load.present();
        this.blockProvider.blockUser(userDetails).then(function () {
            if (_this.Friends.length > 1) {
                _this.friendsProvider.getAllFriends().then(function (res) {
                    _this.Friends = res;
                }).then(function () {
                    load.dismiss();
                    _this.showToast(userDetails.Name + ' has been blocked');
                }).catch(function (err) {
                    load.dismiss();
                    _this.showToast(err);
                });
            }
            else {
                load.dismiss();
                _this.navCtrl.pop();
                _this.showToast(userDetails.Name + ' has been blocked');
            }
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    FriendsPage.prototype.openChatBody = function (userDetails) {
        this.chatProvider.initialize(userDetails);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__chatbody_chatbody__["a" /* ChatbodyPage */], {
            userDetails: userDetails
        });
    };
    FriendsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-friends',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\friends\friends.html"*/'<ion-header>\n\n  <ion-navbar color="logo">\n    <ion-title>Friends</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-spinner name="bubbles" *ngIf="looding" class="looding"></ion-spinner>\n\n\n\n	<div *ngIf="!looding">\n\n\n		<div class="no-items" *ngIf="blockList == 0">\n			<ion-icon name="person"></ion-icon>\n			<p>No Friends yet</p>\n		</div>\n\n\n		<div *ngIf="Friends != 0">\n			<ion-list-header>Friends</ion-list-header>\n			<ion-item *ngFor="let user of Friends" (click)="showFriendsConfirmation(user)">\n				<ion-avatar item-start>\n			      <img src="{{user.Photo}}">\n			    </ion-avatar>\n			    <h2>{{user.Name}}</h2>\n			</ion-item>\n		</div>\n		\n		\n\n\n	</div>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\friends\friends.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__providers_chat_chat__["a" /* ChatProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__providers_block_block__["a" /* BlockProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__providers_friends_friends__["a" /* FriendsProvider */]])
    ], FriendsPage);
    return FriendsPage;
}());

//# sourceMappingURL=friends.js.map

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BlockPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_block_block__ = __webpack_require__(113);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { SuperTabsController } from 'ionic2-super-tabs';
var BlockPage = /** @class */ (function () {
    function BlockPage(navCtrl, loadCtrl, platform, alertCtrl, blockProvider) {
        this.navCtrl = navCtrl;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.blockProvider = blockProvider;
        this.blockList = [];
        this.looding = true;
    }
    BlockPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    BlockPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // this.superTabsCtrl.showToolbar(false)
        this.blockProvider.getBlockList().then(function (res) {
            _this.looding = false;
            _this.blockList = res;
        }).catch(function (err) {
            console.log(err);
        });
    };
    BlockPage.prototype.showBlockConfirmation = function (userDetails) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: userDetails.Name,
            message: 'Unblock ' + userDetails.Name + ' for chatting together again.',
            buttons: [
                {
                    text: 'Cancel',
                    cssClass: 'half-alert-button',
                    handler: function () {
                        _this.showToast('Cancel');
                    }
                },
                {
                    text: 'Unblock',
                    cssClass: 'half-alert-button',
                    handler: function () {
                        _this.unBlock(userDetails);
                    }
                }
            ]
        });
        confirm.present();
    };
    BlockPage.prototype.unBlock = function (userDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Unblocking ' + userDetails.Name + ' ...'
        });
        load.present();
        this.blockProvider.unBlock(userDetails).then(function () {
            if (_this.blockList.length > 1) {
                _this.blockProvider.getBlockList().then(function (res) {
                    _this.blockList = res;
                }).then(function () {
                    load.dismiss();
                    _this.showToast(userDetails.Name + ' has been unblocked');
                }).catch(function (err) {
                    load.dismiss();
                    _this.showToast(err);
                });
            }
            else {
                load.dismiss();
                _this.navCtrl.pop();
                _this.showToast(userDetails.Name + ' has been unblocked');
            }
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    BlockPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-block',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\block\block.html"*/'<ion-header>\n\n  <ion-navbar color="logo">\n    <ion-title>Block List</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n\n\n	<ion-spinner name="bubbles" *ngIf="looding" class="looding"></ion-spinner>\n\n\n	<div *ngIf="!looding">\n\n\n		<div class="no-items" *ngIf="blockList == 0">\n			<ion-icon name="person"></ion-icon>\n			<p>No Users Found</p>\n		</div>\n\n\n		<div *ngIf="blockList != 0">\n			<ion-list-header>Block List</ion-list-header>\n			<ion-item *ngFor="let user of blockList" (click)="showBlockConfirmation(user)">\n				<ion-avatar item-start>\n			      <img src="{{user.Photo}}">\n			    </ion-avatar>\n			    <h2>{{user.Name}}</h2>\n			</ion-item>\n		</div>\n\n\n	</div>\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\block\block.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__providers_block_block__["a" /* BlockProvider */]])
    ], BlockPage);
    return BlockPage;
}());

//# sourceMappingURL=block.js.map

/***/ }),

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimelinePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TimelinePage = /** @class */ (function () {
    function TimelinePage(navCtrl, navParams, viewCtrl, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.stories = new Array();
        this.userId = 1;
        this.isPaused = false;
        this.isWaiting = false;
        this.stories = this.navParams.get("stories");
        this.tapped = this.navParams.get("tapped");
    }
    Object.defineProperty(TimelinePage.prototype, "progressElement", {
        set: function (progress) {
            var _this = this;
            if (progress) {
                progress = progress.nativeElement;
                progress.addEventListener("animationend", function () { _this.nextStoryItem(); });
                progress.addEventListener("webkitAnimationEnd", function () { _this.nextStoryItem(); });
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TimelinePage.prototype, "videoElement", {
        set: function (video) {
            var _this = this;
            if (video) {
                this.video = video.nativeElement;
                this.video.onwaiting = function () { _this.isWaiting = true; };
                this.video.onready = this.video.onload = this.video.onplaying = this.video.oncanplay = function () { _this.isWaiting = false; };
                this.video.addEventListener("loadedmetadata", function () {
                    var storyVideo = _this.getCurrentStoryItem();
                    if (_this.video.duration && !storyVideo.duration)
                        storyVideo.duration = _this.video.duration;
                    _this.video.play();
                });
            }
            else {
                if (this.video)
                    this.video = null;
            }
        },
        enumerable: true,
        configurable: true
    });
    TimelinePage.prototype.ngAfterViewInit = function () {
        this.slides.initialSlide = this.tapped;
    };
    TimelinePage.prototype.closeStoryViewer = function () {
        this.viewCtrl.dismiss();
    };
    TimelinePage.prototype.getCurrentStory = function () {
        return this.stories[this.slides.getActiveIndex()];
    };
    TimelinePage.prototype.nextStoryItem = function () {
        if (this.getCurrentStory().currentItem < this.getCurrentStory().items.length - 1) {
            this.getCurrentStory().currentItem++;
            this.setStorySeen();
        }
        else {
            if (this.slides.isEnd()) {
                this.closeStoryViewer();
            }
            else {
                this.slides.slideNext();
            }
        }
    };
    TimelinePage.prototype.pauseStory = function () {
        this.isPaused = true;
        if (this.video)
            this.video.pause();
    };
    TimelinePage.prototype.playStory = function () {
        this.isPaused = false;
        if (this.video)
            this.video.play();
    };
    TimelinePage.prototype.getCurrentStoryItem = function () {
        var currentStory = this.getCurrentStory();
        if (currentStory)
            return currentStory.items[currentStory.currentItem];
    };
    TimelinePage.prototype.isLoading = function (indexStory) {
        var storyItem = this.getCurrentStoryItem();
        if (storyItem) {
            return !storyItem.duration && storyItem.type == 1 && indexStory === this.slides.getActiveIndex();
        }
        else {
            return true;
        }
    };
    TimelinePage.prototype.changeStoryItem = function (event, story) {
        if (event.center.y < 70 || event.center.y > this.platform.height() - 70)
            return;
        if (event.center.x < this.platform.width() / 2) {
            if (story.currentItem > 0) {
                story.currentItem--;
                this.setStorySeen();
            }
            else {
                this.slides.slidePrev();
            }
        }
        else {
            this.nextStoryItem();
        }
    };
    TimelinePage.prototype.onSwipeUp = function () {
        console.log("Swipe Up!");
    };
    TimelinePage.prototype.setStorySeen = function () {
        var story = this.getCurrentStory();
        var storyItem = this.getCurrentStoryItem();
        if (!storyItem.seen) {
            if (story.currentItem === story.items.length - 1)
                story.seen = true;
            storyItem.seen = true;
        }
    };
    TimelinePage.prototype.ionViewDidEnter = function () {
        this.setStorySeen();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* Slides */])
    ], TimelinePage.prototype, "slides", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("progress"),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], TimelinePage.prototype, "progressElement", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("video"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], TimelinePage.prototype, "videoElement", null);
    TimelinePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-timeline',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\timeline\timeline.html"*/'<ion-content (press)="pauseStory()" (pressup)="playStory()" (panstart)="pauseStory()" (panend)="playStory()" swipe-vertical (onSwipeUp)="onSwipeUp()" (onSwipeDown)="closeStoryViewer()">\n\n  <ion-slides (ionSlideDidChange)="setStorySeen()">\n\n    <ion-slide *ngFor="let story of stories; let indexStory = index" [ngClass]="isPaused || isLoading(indexStory) || isWaiting ? \'isPaused\' : \'\'" (tap)="changeStoryItem($event, story)">\n\n      <ion-grid class="pagination">\n\n        <ion-row>\n\n          <ion-col *ngFor="let items of story.items; let index = index">\n\n            <div class="seen" [hidden]="index >= story.currentItem"></div>\n\n            <div #progress class="progress" *ngIf="story.currentItem === index && slides.getActiveIndex() === indexStory" [ngStyle]="{\'animation-duration\': story.items[story.currentItem].duration + \'s\'}"></div>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n\n\n      <div class="header" padding>\n\n        <div>\n\n          <img [src]="story.userPicture" [alt]="\'Story de\' + story.userName">\n\n\n\n          <div class="info" text-left>\n\n            <span>{{ story.userName }}</span>\n\n            <span>{{ story.items[story.currentItem].date }}</span>\n\n          </div>\n\n        </div>\n\n\n\n        <div>\n\n          <button ion-button clear icon-only no-margin no-padding (tap)="closeStoryViewer()">\n\n            <ion-icon name="close"></ion-icon>\n\n          </button>\n\n        </div>\n\n      </div>\n\n\n\n      <div class="item" *ngFor="let item of story.items; let index = index" [hidden]="story.currentItem != index">\n\n        <ion-spinner [hidden]="item.type == 0 || (!isLoading(indexStory) && !isWaiting)"></ion-spinner>\n\n\n\n        <img [src]="item.media" [alt]="\'Story de\' + story.userName" [hidden]="item.type != 0">\n\n\n\n        <video #video webkit-playsinline playsinline preload="metadata" [src]="item.media" *ngIf="item.type == 1 && story.currentItem === index && slides.getActiveIndex() === indexStory"></video>\n\n      </div>\n\n    </ion-slide>\n\n  </ion-slides>\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\timeline\timeline.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */]])
    ], TimelinePage);
    return TimelinePage;
}());

//# sourceMappingURL=timeline.js.map

/***/ }),

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__alert__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__loading__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ImageProvider = /** @class */ (function () {
    function ImageProvider(angularDb, alertProvider, loadingProvider, camera, alertCtrl) {
        this.angularDb = angularDb;
        this.alertProvider = alertProvider;
        this.loadingProvider = loadingProvider;
        this.camera = camera;
        this.alertCtrl = alertCtrl;
        this.profilePhotoOptions = {
            quality: 50,
            targetWidth: 384,
            targetHeight: 384,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true
        };
        this.photoMessageOptions = {
            quality: 50,
            targetWidth: 384,
            targetHeight: 384,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: false,
            allowEdit: true,
        };
        this.groupPhotoOptions = {
            quality: 50,
            targetWidth: 384,
            targetHeight: 384,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true
        };
    }
    // Function to convert dataURI to Blob needed by Firebase
    ImageProvider.prototype.imgURItoBlob = function (dataURI) {
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {
            type: mimeString
        });
    };
    // Generate a random filename of length for the image to be uploaded
    ImageProvider.prototype.generateFilename = function () {
        var length = 8;
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text + ".jpg";
    };
    // Set ProfilePhoto given the user and the cameraSourceType.
    // This function processes the imageURI returned and uploads the file on Firebase,
    // Finally the user data on the database is updated.
    ImageProvider.prototype.setProfilePhoto = function (user, sourceType) {
        var _this = this;
        this.profilePhotoOptions.sourceType = sourceType;
        this.loadingProvider.show();
        // Get picture from camera or gallery.
        this.camera.getPicture(this.profilePhotoOptions).then(function (imageData) {
            // Process the returned imageURI.
            var imgBlob = _this.imgURItoBlob("data:image/jpeg;base64," + imageData);
            var metadata = {
                'contentType': imgBlob.type
            };
            // Generate filename and upload to Firebase Storage.
            __WEBPACK_IMPORTED_MODULE_5_firebase__["storage"]().ref().child('images/' + user.userId + '/' + _this.generateFilename()).put(imgBlob, metadata).then(function (snapshot) {
                // Delete previous profile photo on Storage if it exists.
                _this.deleteImageFile(user.img);
                // URL of the uploaded image!
                var url = snapshot.metadata.downloadURLs[0];
                var profile = {
                    displayName: user.name,
                    photoURL: url
                };
                // Update Firebase User.
                __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.updateProfile(profile)
                    .then(function (success) {
                    // Update User Data on Database.
                    _this.angularDb.object('Users' + user.userId).update({
                        img: url
                    }).then(function (success) {
                        _this.loadingProvider.hide();
                        _this.alertProvider.showProfileUpdatedMessage();
                    }).catch(function (error) {
                        _this.loadingProvider.hide();
                        //this.alertProvider.showErrorMessage('profile/error-change-photo');
                    });
                })
                    .catch(function (error) {
                    _this.loadingProvider.hide();
                    //this.alertProvider.showErrorMessage('profile/error-change-photo');
                });
            }).catch(function (error) {
                _this.loadingProvider.hide();
                //this.alertProvider.showErrorMessage('image/error-image-upload');
            });
        }).catch(function (error) {
            _this.loadingProvider.hide();
        });
    };
    // Upload and set the group object's image.
    ImageProvider.prototype.setGroupPhoto = function (group, sourceType) {
        var _this = this;
        this.groupPhotoOptions.sourceType = sourceType;
        this.loadingProvider.show();
        // Get picture from camera or gallery.
        this.camera.getPicture(this.groupPhotoOptions).then(function (imageData) {
            // Process the returned imageURI.
            var imgBlob = _this.imgURItoBlob("data:image/jpeg;base64," + imageData);
            var metadata = {
                'contentType': imgBlob.type
            };
            __WEBPACK_IMPORTED_MODULE_5_firebase__["storage"]().ref().child('images/' + __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid + '/' + _this.generateFilename()).put(imgBlob, metadata).then(function (snapshot) {
                _this.deleteImageFile(group.img);
                // URL of the uploaded image!
                var url = snapshot.metadata.downloadURLs[0];
                group.img = url;
                _this.loadingProvider.hide();
            }).catch(function (error) {
                _this.loadingProvider.hide();
                //this.alertProvider.showErrorMessage('image/error-image-upload');
            });
        }).catch(function (error) {
            _this.loadingProvider.hide();
        });
    };
    // Set group photo and return the group object as promise.
    ImageProvider.prototype.setGroupPhotoPromise = function (group, sourceType) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.groupPhotoOptions.sourceType = sourceType;
            _this.loadingProvider.show();
            // Get picture from camera or gallery.
            _this.camera.getPicture(_this.groupPhotoOptions).then(function (imageData) {
                // Process the returned imageURI.
                var imgBlob = _this.imgURItoBlob("data:image/jpeg;base64," + imageData);
                var metadata = {
                    'contentType': imgBlob.type
                };
                __WEBPACK_IMPORTED_MODULE_5_firebase__["storage"]().ref().child('images/' + __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid + '/' + _this.generateFilename()).put(imgBlob, metadata).then(function (snapshot) {
                    _this.deleteImageFile(group.img);
                    // URL of the uploaded image!
                    var url = snapshot.metadata.downloadURLs[0];
                    group.img = url;
                    _this.loadingProvider.hide();
                    resolve(group);
                }).catch(function (error) {
                    _this.loadingProvider.hide();
                    //this.alertProvider.showErrorMessage('image/error-image-upload');
                });
            }).catch(function (error) {
                _this.loadingProvider.hide();
            });
        });
    };
    //Delete the image given the url.
    ImageProvider.prototype.deleteImageFile = function (path) {
        var fileName = path.substring(path.lastIndexOf('%2F') + 3, path.lastIndexOf('?'));
        __WEBPACK_IMPORTED_MODULE_5_firebase__["storage"]().ref().child('images/' + __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid + '/' + fileName).delete().then(function () { }).catch(function (error) { });
    };
    //Delete the user.img given the user.
    ImageProvider.prototype.deleteUserImageFile = function (user) {
        var fileName = user.img.substring(user.img.lastIndexOf('%2F') + 3, user.img.lastIndexOf('?'));
        __WEBPACK_IMPORTED_MODULE_5_firebase__["storage"]().ref().child('images/' + user.userId + '/' + fileName).delete().then(function () { }).catch(function (error) { });
    };
    // Delete group image file on group storage reference.
    ImageProvider.prototype.deleteGroupImageFile = function (groupId, path) {
        var fileName = path.substring(path.lastIndexOf('%2F') + 3, path.lastIndexOf('?'));
        __WEBPACK_IMPORTED_MODULE_5_firebase__["storage"]().ref().child('images/' + groupId + '/' + fileName).delete().then(function () { }).catch(function (error) { });
    };
    // Upload photo message and return the url as promise.
    ImageProvider.prototype.uploadPhotoMessage = function (conversationId, sourceType) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.photoMessageOptions.sourceType = sourceType;
            _this.loadingProvider.show();
            // Get picture from camera or gallery.
            _this.camera.getPicture(_this.photoMessageOptions).then(function (imageData) {
                // Process the returned imageURI.
                var imgBlob = _this.imgURItoBlob("data:image/jpeg;base64," + imageData);
                var metadata = {
                    'contentType': imgBlob.type
                };
                // Generate filename and upload to Firebase Storage.
                __WEBPACK_IMPORTED_MODULE_5_firebase__["storage"]().ref().child('images/' + conversationId + '/' + _this.generateFilename()).put(imgBlob, metadata).then(function (snapshot) {
                    // URL of the uploaded image!
                    var url = snapshot.metadata.downloadURLs[0];
                    _this.loadingProvider.hide();
                    resolve(url);
                }).catch(function (error) {
                    _this.loadingProvider.hide();
                    //this.alertProvider.showErrorMessage('image/error-image-upload');
                });
            }).catch(function (error) {
                _this.loadingProvider.hide();
            });
        });
    };
    // Upload group photo message and return a promise as url.
    ImageProvider.prototype.uploadGroupPhotoMessage = function (groupId, sourceType) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.photoMessageOptions.sourceType = sourceType;
            _this.loadingProvider.show();
            // Get picture from camera or gallery.
            _this.camera.getPicture(_this.photoMessageOptions).then(function (imageData) {
                // Process the returned imageURI.
                var imgBlob = _this.imgURItoBlob("data:image/jpeg;base64," + imageData);
                var metadata = {
                    'contentType': imgBlob.type
                };
                // Generate filename and upload to Firebase Storage.
                __WEBPACK_IMPORTED_MODULE_5_firebase__["storage"]().ref().child('images/' + groupId + '/' + _this.generateFilename()).put(imgBlob, metadata).then(function (snapshot) {
                    // URL of the uploaded image!
                    var url = snapshot.metadata.downloadURLs[0];
                    _this.loadingProvider.hide();
                    resolve(url);
                }).catch(function (error) {
                    _this.loadingProvider.hide();
                    // this.alertProvider.showErrorMessage('image/error-image-upload');
                });
            }).catch(function (error) {
                _this.loadingProvider.hide();
            });
        });
    };
    // ======== set post image ========
    ImageProvider.prototype.setImage = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.alert = _this.alertCtrl.create({
                title: 'Send Photo Message',
                message: 'Do you want to take a photo or choose from your photo gallery?',
                buttons: [
                    {
                        text: 'Cancel',
                        handler: function (data) { }
                    },
                    {
                        text: 'Choose from Gallery',
                        handler: function () {
                            _this.photoMessageOptions.sourceType = _this.camera.PictureSourceType.PHOTOLIBRARY;
                            _this.camera.getPicture(_this.photoMessageOptions).then(function (imageData) {
                                resolve("data:image/jpeg;base64," + imageData);
                            });
                        }
                    },
                    {
                        text: 'Take Photo',
                        handler: function () {
                            _this.photoMessageOptions.sourceType = _this.camera.PictureSourceType.CAMERA;
                            _this.camera.getPicture(_this.photoMessageOptions).then(function (imageData) {
                                resolve("data:image/jpeg;base64," + imageData);
                            });
                        }
                    }
                ]
            }).present();
        });
    };
    // ======= upload image in post folder ====
    ImageProvider.prototype.uploadPostImage = function (url) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var imgBlob = _this.imgURItoBlob(url);
            var metadata = {
                'contentType': imgBlob.type
            };
            // Generate filename and upload to Firebase Storage.
            __WEBPACK_IMPORTED_MODULE_5_firebase__["storage"]().ref().child('images/post/' + _this.generateFilename()).put(imgBlob, metadata).then(function (snapshot) {
                // URL of the uploaded image!
                var url = snapshot.metadata.downloadURLs[0];
                resolve(url);
            }).catch(function (error) {
                //this.alertProvider.showErrorMessage('image/error-image-upload');
                reject(error);
            });
        });
    };
    ImageProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_1__alert__["a" /* AlertProvider */],
            __WEBPACK_IMPORTED_MODULE_3__loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */]])
    ], ImageProvider);
    return ImageProvider;
}());

//# sourceMappingURL=image.js.map

/***/ }),

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddCommentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_module__ = __webpack_require__(252);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { CommentsProvider } from '../../providers/comments/comments';

//import { UserProvidercot } from '../../providers/user/usercot';


/**
 * Generated class for the AddCommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddCommentPage = /** @class */ (function () {
    function AddCommentPage(db, navCtrl, navParams) {
        var _this = this;
        this.db = db;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.items = [];
        this.ref = __WEBPACK_IMPORTED_MODULE_3_firebase___default.a.database().ref('items/');
        this.inputText = '';
        this.ref.on('value', function (resp) {
            _this.items = Object(__WEBPACK_IMPORTED_MODULE_4__app_app_module__["b" /* snapshotToArray */])(resp);
        });
    }
    AddCommentPage.prototype.addItem = function (item) {
        if (item !== undefined && item !== null) {
            var newItem = this.ref.push();
            newItem.set(item);
            this.inputText = '';
        }
    };
    AddCommentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-comment',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\add-comment\add-comment.html"*/'<ion-header>\n\n  <ion-navbar color="logo">\n    <ion-title style="text-align: center;">Comments</ion-title>\n   \n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <!-- first comment grid -->\n    <ion-row class="post-content">\n      <ion-col col-2>\n        <ion-avatar item-start>\n          <img class="icon-photo" src="https://res.cloudinary.com/og-tech/image/upload/s--Ivs1sp-J--/c_scale,w_100/v1529311900/og_icon.jpg">\n        </ion-avatar>\n      </ion-col>\n      <ion-col col-10>\n        <div>\n          <!-- this will contain the main post content -->\n          <p><strong>capt.og:</strong> Zoned Out 💥</p>\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <!-- this rows will represent sample comments -->\n    <ion-row class="user-comments">\n      <ion-col col-2>\n        <ion-avatar item-start>\n          <img class="icon-photo" src="https://api.adorable.io/avatars/100/big_dawg@adorable.png">\n        </ion-avatar>\n      </ion-col>\n      <ion-col col-10>\n        <div>\n          <!-- this will contain the main post content -->\n          <p><strong>big_dawg:</strong> Haha! I feel you bruva! Stay grinding 💯</p>\n        </div>\n      </ion-col>\n    </ion-row> \n\n    <ion-row class="user-comments">\n        <ion-col col-2>\n          <ion-avatar item-start>\n            <img class="icon-photo" src="https://api.adorable.io/avatars/100/broda_shagi@adorable.png">\n          </ion-avatar>\n        </ion-col>\n        <ion-col col-10>\n          <div>\n            <!-- this will contain the main post content -->\n            <p><strong>broda_shagi:</strong> Better make sure you write tests 😏</p>\n          </div>\n        </ion-col>\n    </ion-row> \n    <ion-row class="user-comments" *ngFor="let item of items">\n     \n      <ion-col col-2>\n        <ion-list no-lines>\n        <ion-avatar item-start>\n          <img class="icon-photo" src="https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/profileimages%2Fneo.jpg?alt=media&token=7197c484-2439-4ae4-af91-51f7aafc564c">\n        </ion-avatar>\n      </ion-list>\n      </ion-col>\n      <ion-col col-10>\n        <div>\n          <!-- this will contain the main post content -->\n          <p><strong>Precious:</strong>{{item.name}}</p>\n        </div>\n      </ion-col>\n   \n  </ion-row> \n\n  </ion-grid>\n\n\n</ion-content>\n<ion-footer>\n  <!-- add the input field fixed to the bottom of the screen -->\n  <ion-grid>\n      <ion-row class="comment-area">\n        <ion-col col-9>\n          <ion-textarea  [(ngModel)]="inputText" placeholder="Enter your comment..."></ion-textarea>\n        </ion-col>\n        <ion-col col-3 >\n          <button ion-button color="logo" class="comment-button" (click)="addItem({name:inputText})">\n            <ion-icon name="paper-plane"></ion-icon>\n          </button>\n          \n        </ion-col>\n      </ion-row>\n    </ion-grid>\n</ion-footer>\n\n \n  '/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\add-comment\add-comment.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], AddCommentPage);
    return AddCommentPage;
}());

//# sourceMappingURL=add-comment.js.map

/***/ }),

/***/ 323:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UsersProvider = /** @class */ (function () {
    function UsersProvider(afAuth, afDB) {
        this.afAuth = afAuth;
        this.afDB = afDB;
        this.UserUid = window.localStorage.getItem('userid');
    }
    UsersProvider.prototype.getAllUsers = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Users').once('value', function (snap) {
                var res = snap.val();
                var array = [];
                for (var i in res) {
                    array.push(res[i]);
                }
                for (var aa = array.length - 1; aa >= 0; aa--) {
                    if (array[aa].Id === _this.UserUid) {
                        array.splice(aa, 1);
                    }
                }
                _this.afDB.database.ref('Requests').child(_this.UserUid).child('Sent Requests').once('value', function (snap) {
                    var res = snap.val();
                    var array2 = [];
                    for (var i in res) {
                        array2.push(res[i]);
                    }
                    for (var aa = array.length - 1; aa >= 0; aa--) {
                        for (var bb = 0; bb < array2.length; bb++) {
                            if (array[aa].Id === array2[bb].Id) {
                                array.splice(aa, 1);
                            }
                        }
                    }
                    _this.afDB.database.ref('Requests').child(_this.UserUid).child('Received Requests').once('value', function (snap) {
                        var res = snap.val();
                        var array3 = [];
                        for (var i in res) {
                            array3.push(res[i]);
                        }
                        for (var aa = array.length - 1; aa >= 0; aa--) {
                            for (var bb = array3.length - 1; bb >= 0; bb--) {
                                if (array[aa].Id === array3[bb].Id) {
                                    array.splice(aa, 1);
                                }
                            }
                        }
                        _this.afDB.database.ref('Friends').child(_this.UserUid).once('value', function (snap) {
                            var res = snap.val();
                            var array4 = [];
                            for (var i in res) {
                                array4.push(res[i]);
                            }
                            for (var aa = array.length - 1; aa >= 0; aa--) {
                                for (var bb = 0; bb < array4.length; bb++) {
                                    if (array[aa].Id === array4[bb].Id) {
                                        array.splice(aa, 1);
                                    }
                                }
                            }
                            _this.afDB.database.ref('Block List').child(_this.UserUid).once('value', function (snap) {
                                var res = snap.val();
                                var array5 = [];
                                for (var i in res) {
                                    array5.push(res[i]);
                                }
                                for (var aa = array.length - 1; aa >= 0; aa--) {
                                    for (var bb = 0; bb < array5.length; bb++) {
                                        if (array[aa].Id === array5[bb].Id) {
                                            array.splice(aa, 1);
                                        }
                                    }
                                }
                                resolve(array);
                            }).catch(function (err) {
                                reject(err);
                            });
                        }).catch(function (err) {
                            reject(err);
                        });
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UsersProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], UsersProvider);
    return UsersProvider;
}());

//# sourceMappingURL=users.js.map

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StorageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(178);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ITEMS_KEY = 'my-items';
var StorageProvider = /** @class */ (function () {
    function StorageProvider(http, storage) {
        this.http = http;
        this.storage = storage;
    }
    StorageProvider.prototype.addItem = function (item) {
        var _this = this;
        return this.storage.get(ITEMS_KEY).then(function (items) {
            if (items) {
                items.push(item);
                return _this.storage.set(ITEMS_KEY, items);
            }
            else {
                return _this.storage.set(ITEMS_KEY, [item]);
            }
        });
    };
    // READ
    StorageProvider.prototype.getItems = function () {
        return this.storage.get(ITEMS_KEY);
    };
    // UPDATE
    StorageProvider.prototype.updateItem = function (item) {
        var _this = this;
        return this.storage.get(ITEMS_KEY).then(function (items) {
            if (!items || items.length === 0) {
                return null;
            }
            var newItems = [];
            for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
                var i = items_1[_i];
                if (i.id === item.id) {
                    newItems.push(item);
                }
                else {
                    newItems.push(i);
                }
            }
            return _this.storage.set(ITEMS_KEY, newItems);
        });
    };
    // DELETE
    StorageProvider.prototype.deleteItem = function (id) {
        var _this = this;
        return this.storage.get(ITEMS_KEY).then(function (items) {
            if (!items || items.length === 0) {
                return null;
            }
            var toKeep = [];
            for (var _i = 0, items_2 = items; _i < items_2.length; _i++) {
                var i = items_2[_i];
                if (i.id !== id) {
                    toKeep.push(i);
                }
            }
            return _this.storage.set(ITEMS_KEY, toKeep);
        });
    };
    StorageProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], StorageProvider);
    return StorageProvider;
}());

//# sourceMappingURL=storage.js.map

/***/ }),

/***/ 326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__tabs_tabs__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, authProvider, loadCtrl, platform) {
        this.navCtrl = navCtrl;
        this.authProvider = authProvider;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.userDetails = {
            Email: '',
            Password: '',
            Phone: '',
            Name: '',
            agreeWithPolicy: false
        };
        this.showPasswordText = false;
        this.isenabled = false;
        this.email = false;
        this.password = false;
        this.name = false;
        this.phone = false;
        this.agreeWithPolicy = false;
    }
    RegisterPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    RegisterPage.prototype.register = function () {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Creating Account ...'
        });
        load.present();
        this.authProvider.register(this.userDetails).then(function () {
            load.dismiss();
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__tabs_tabs__["a" /* TabsPage */]);
            window.localStorage.setItem('userstate', 'logedIn');
            window.localStorage.setItem('userid', _this.authProvider.afAuth.auth.currentUser.uid);
        }).catch(function (err) {
            load.dismiss();
            var error = err.message;
            _this.showToast(error);
        });
    };
    RegisterPage.prototype.showHide = function () {
        if (this.showPasswordText == false) {
            this.showPasswordText = true;
        }
        else {
            this.showPasswordText = false;
        }
    };
    RegisterPage.prototype.nameInput = function (value) {
        if (this.userDetails.Name.length < 6) {
            this.name = false;
        }
        else {
            this.name = true;
        }
        if (this.userDetails.Name.length < 6) {
            this.isenabled = false;
        }
        else {
            if (this.userDetails.Email == '' || !this.userDetails.Email.match(/^[a-zA-Z0-9-_\.]+@[a-z]{8}\.[a-z-.]{1,5}$/)) {
                this.isenabled = false;
            }
            else {
                if (this.userDetails.Password.length < 6) {
                    this.isenabled = false;
                }
                else {
                    if (this.userDetails.Phone.length < 10) {
                        this.isenabled = false;
                    }
                    else {
                        if (this.userDetails.agreeWithPolicy == false) {
                            this.isenabled = false;
                        }
                        else {
                            this.isenabled = true;
                        }
                    }
                }
            }
        }
    };
    RegisterPage.prototype.emailInput = function (value) {
        if (this.userDetails.Email == '' || !this.userDetails.Email.match(/^[a-zA-Z0-9-_\.]+@[a-z]{8}\.[a-z-.]{1,5}$/)) {
            this.email = false;
        }
        else {
            this.email = true;
        }
        if (this.userDetails.Name.length < 6) {
            this.isenabled = false;
        }
        else {
            if (this.userDetails.Email == '' || !this.userDetails.Email.match(/^[a-zA-Z0-9-_\.]+@[a-z]{8}\.[a-z-.]{1,5}$/)) {
                this.isenabled = false;
            }
            else {
                if (this.userDetails.Password.length < 6) {
                    this.isenabled = false;
                }
                else {
                    if (this.userDetails.Phone.length < 10) {
                        this.isenabled = false;
                    }
                    else {
                        if (this.userDetails.agreeWithPolicy == false) {
                            this.isenabled = false;
                        }
                        else {
                            this.isenabled = true;
                        }
                    }
                }
            }
        }
    };
    RegisterPage.prototype.passwordInput = function (value) {
        if (this.userDetails.Password.length < 6) {
            this.password = false;
        }
        else {
            this.password = true;
        }
        if (this.userDetails.Name.length < 6) {
            this.isenabled = false;
        }
        else {
            if (this.userDetails.Email == '' || !this.userDetails.Email.match(/^[a-zA-Z0-9-_\.]+@[a-z]{8}\.[a-z-.]{1,5}$/)) {
                this.isenabled = false;
            }
            else {
                if (this.userDetails.Password.length < 6) {
                    this.isenabled = false;
                }
                else {
                    if (this.userDetails.Phone.length < 10) {
                        this.isenabled = false;
                    }
                    else {
                        if (this.userDetails.agreeWithPolicy == false) {
                            this.isenabled = false;
                        }
                        else {
                            this.isenabled = true;
                        }
                    }
                }
            }
        }
    };
    RegisterPage.prototype.phoneInput = function (value) {
        if (this.userDetails.Phone.length < 10) {
            this.phone = false;
        }
        else {
            this.phone = true;
        }
        if (this.userDetails.Name.length < 6) {
            this.isenabled = false;
        }
        else {
            if (this.userDetails.Email == '' || !this.userDetails.Email.match(/^[a-zA-Z0-9-_\.]+@[a-z]{8}\.[a-z-.]{1,5}$/)) {
                this.isenabled = false;
            }
            else {
                if (this.userDetails.Password.length < 6) {
                    this.isenabled = false;
                }
                else {
                    if (this.userDetails.Phone.length < 10) {
                        this.isenabled = false;
                    }
                    else {
                        if (this.userDetails.agreeWithPolicy == false) {
                            this.isenabled = false;
                        }
                        else {
                            this.isenabled = true;
                        }
                    }
                }
            }
        }
    };
    RegisterPage.prototype.agreeWithPolicyInput = function (value) {
        if (this.userDetails.agreeWithPolicy == false) {
            this.agreeWithPolicy = false;
        }
        else {
            this.agreeWithPolicy = true;
        }
        if (this.userDetails.Name.length < 6) {
            this.isenabled = false;
        }
        else {
            if (this.userDetails.Email == '' || !this.userDetails.Email.match(/^[a-zA-Z0-9-_\.]+@[a-z]{8}\.[a-z-.]{1,5}$/)) {
                this.isenabled = false;
            }
            else {
                if (this.userDetails.Password.length < 6) {
                    this.isenabled = false;
                }
                else {
                    if (this.userDetails.Phone.length < 10) {
                        this.isenabled = false;
                    }
                    else {
                        if (this.userDetails.agreeWithPolicy == false) {
                            this.isenabled = false;
                        }
                        else {
                            this.isenabled = true;
                        }
                    }
                }
            }
        }
    };
    RegisterPage.prototype.backToLogin = function () {
        this.navCtrl.pop();
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\register\register.html"*/'<ion-content class="bg-image">\n\n\n\n	<div >\n \n		<ion-card-content>\n		\n		  <ion-list no-lines>\n		   \n			<div  style="height:30vw; width:40vw;"></div>\n\n	\n\n			<button  ion-button round block outline color="logo" style="height: 10vw; width:200px; margin-left: 50px;" >\n				<ion-label floating><ion-icon name="person"></ion-icon> Name</ion-label>\n				<ion-item center>\n				<ion-input (ionChange)=\'nameInput($event)\' placeholder="Name" text-center type="text" [(ngModel)]="userDetails.Name" required></ion-input>\n\n				</ion-item>\n		</button>\n		<ion-item-divider *ngIf="!name">Name should be at least 6 characters</ion-item-divider>\n		<ion-item-divider *ngIf="name"><ion-icon color="secondary" name="checkmark"></ion-icon> Name should be at least 6 characters</ion-item-divider>\n\n		<div class="spacer" style="height: 3vw;"></div>\n\n		\n\n		<button ion-button outline round color="logo" style="height:10vw; width:200px; margin-left: 50px;" >\n			<ion-label floating><ion-icon name="mail"></ion-icon> Email</ion-label>\n			<ion-item center>\n			<ion-input (ionChange)=\'emailInput($event)\' placeholder="Email"  text-center type="email" [(ngModel)]="userDetails.Email" required></ion-input>\n\n			</ion-item>\n		</button>\n		<ion-item-divider *ngIf="!email">Email should be xxxx@megatech.co.za</ion-item-divider>\n		<ion-item-divider *ngIf="email"><ion-icon color="secondary" name="checkmark"></ion-icon> Email should be xxxx@megatech.co.za</ion-item-divider>\n\n		\n		<div class="spacer" style="height: 3vw;"></div>\n\n\n		<button ion-button outline round color="logo" style="height:10vw; width:200px; margin-left: 50px;" >\n			<ion-label floating><ion-icon name="lock"></ion-icon> Password</ion-label>\n			<ion-item center>\n			<ion-input (ionChange)=\'passwordInput($event)\' placeholder="Password"  text-center type="password" *ngIf="!showPasswordText" [(ngModel)]="userDetails.Password" required></ion-input>\n			<ion-input (ionChange)=\'passwordInput($event)\'  text-center type="text" *ngIf="showPasswordText" [(ngModel)]="userDetails.Password" required></ion-input>\n\n			</ion-item>\n			<button ion-button clear item-right (click)="showHide()"> <ion-icon name="eye"> </ion-icon> </button>\n        </button>\n		<ion-item-divider *ngIf="!password">Password should be at least 6 characters</ion-item-divider>\n		<ion-item-divider *ngIf="password"><ion-icon color="secondary" name="checkmark"></ion-icon> Password should be at least 6 characters</ion-item-divider>\n\n\n        <div class="spacer" style="height: 3vw;"></div>\n\n\n		<button ion-button outline round color="logo" style="height:10vw; width:200px; margin-left: 50px;" >\n			<ion-label floating><ion-icon name="call"></ion-icon> Mobile</ion-label>\n			<ion-item center>\n			<ion-input (ionChange)=\'phoneInput($event)\' placeholder="Mobile"  text-center type="number" [(ngModel)]="userDetails.Phone" required></ion-input>\n\n			</ion-item>\n        </button>\n		<ion-item-divider *ngIf="!phone">Phone should be 10 numbers</ion-item-divider>\n		<ion-item-divider *ngIf="phone"><ion-icon color="secondary" name="checkmark"></ion-icon> Phone should be 10 numbers</ion-item-divider>\n\n		<div class="spacer" style="height: 3vw;"></div>\n\n\n\n		<ion-item>\n			<ion-label>I accept the terms and privacy policy</ion-label>\n			<ion-checkbox (ionChange)=\'agreeWithPolicyInput($event)\' color="header" checked="false" [(ngModel)]="userDetails.agreeWithPolicy" required></ion-checkbox>\n		</ion-item>\n		<ion-item-divider *ngIf="!agreeWithPolicy">Accept the terms and privacy policy</ion-item-divider>\n		<ion-item-divider *ngIf="agreeWithPolicy"><ion-icon color="secondary" name="checkmark"></ion-icon> Accept the terms and privacy policy</ion-item-divider>\n\n\n		<div class="spacer" style="height: 3vw;"></div>\n	  \n		<div class="color"><button ion-button [disabled]=\'!isenabled\'  round style="height:10vw; width:40vw; text-transform: none; " color="logo"  (click)="register()">Sign Up</button></div> \n        <div class="spacer" style="height: 3vw;"></div>\n        \n        <div class="color"> <button ion-button round style="height:10vw; width:40vw; text-transform: none; " color="light"  ion-text-color="logo" (click)="backToLogin()">Login</button></div> \n      \n        </ion-list>\n  \n    </ion-card-content>\n</div>\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\register\register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 327:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ResetPasswordPage = /** @class */ (function () {
    function ResetPasswordPage(navCtrl, authProvider, loadCtrl, platform) {
        this.navCtrl = navCtrl;
        this.authProvider = authProvider;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.userDetails = {
            Email: ''
        };
        this.isenabled = false;
        this.email = false;
    }
    ResetPasswordPage.prototype.ionViewWillEnter = function () {
        this.userDetails.Email = '';
    };
    ResetPasswordPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    ResetPasswordPage.prototype.forgrtPassword = function () {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Reseting Password ...'
        });
        load.present();
        this.authProvider.forgetPassword(this.userDetails).then(function () {
            load.dismiss();
            _this.showToast('Follow your email instructions');
            _this.navCtrl.pop();
        }).catch(function (err) {
            load.dismiss();
            var error = err.message;
            _this.showToast(error);
        });
    };
    ResetPasswordPage.prototype.backToLogin = function () {
        this.navCtrl.pop();
    };
    ResetPasswordPage.prototype.emailInput = function (value) {
        if (this.userDetails.Email == '' || !this.userDetails.Email.match(/^[a-zA-Z0-9-_\.]+@[a-z]{8}\.[a-z-.]{1,5}$/)) {
            this.email = false;
            this.isenabled = false;
        }
        else {
            this.email = true;
            this.isenabled = true;
        }
    };
    ResetPasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-reset-password',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\reset-password\reset-password.html"*/'<ion-content class="bg-image">\n\n\n	<div >\n\n		<div  style="height:30vw; width:40vw;"></div>\n		<ion-item>\n			<ion-label floating><ion-icon name="mail"></ion-icon> Email</ion-label>\n			<ion-input (ionChange)=\'emailInput($event)\' type="email" [(ngModel)]="userDetails.Email" required></ion-input>\n		</ion-item>\n		<ion-item-divider *ngIf="!email">Email should be xxxx@megatech.co.za</ion-item-divider>\n		<ion-item-divider *ngIf="email"><ion-icon color="secondary" name="checkmark"></ion-icon> Email should be xxxx@xxxx.co.za</ion-item-divider>\n\n		<br>\n\n		<button color="header" ion-button block (click)="forgrtPassword()" [disabled]=\'!isenabled\'>\n			Reset Password\n		</button>\n\n		<br>\n\n		<p block class="login" (click)="backToLogin()">\n			Back to login !\n		</p>\n\n	</div>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\reset-password\reset-password.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */]])
    ], ResetPasswordPage);
    return ResetPasswordPage;
}());

//# sourceMappingURL=reset-password.js.map

/***/ }),

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notification_read_notification_read__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__notification_unread_notification_unread__ = __webpack_require__(330);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NotificationsPage = /** @class */ (function () {
    function NotificationsPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__notification_unread_notification_unread__["a" /* NotificationUnreadPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__notification_read_notification_read__["a" /* NotificationReadPage */];
    }
    NotificationsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-notifications',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\notifications\notifications.html"*/'<ion-content >\n	<button ion-button menuToggle>\n		<ion-icon name="menu"></ion-icon>\n	   </button>\n\n<ion-tabs color="logo" tabsPlacement="top">\n	<ion-tab [root]="tab1Root" tabsHideOnSubPages=\'true\' tabTitle="Unread" tabIcon="notifications"></ion-tab>\n	<ion-tab [root]="tab2Root" tabsHideOnSubPages=\'true\' tabTitle="Read" tabIcon="notifications-outline"></ion-tab>\n</ion-tabs>\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\notifications\notifications.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */]])
    ], NotificationsPage);
    return NotificationsPage;
}());

//# sourceMappingURL=notifications.js.map

/***/ }),

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationReadPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_notification_notification__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__requests_requests__ = __webpack_require__(114);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { SuperTabsController } from 'ionic2-super-tabs';
var NotificationReadPage = /** @class */ (function () {
    function NotificationReadPage(notificationsProvider, navCtrl, ngZone, events) {
        var _this = this;
        this.notificationsProvider = notificationsProvider;
        this.navCtrl = navCtrl;
        this.ngZone = ngZone;
        this.events = events;
        this.Notifications = [];
        this.allUsers = [];
        this.looding = true;
        this.events.subscribe('Notifications', function () {
            _this.ngZone.run(function () {
                _this.looding = false;
                _this.Notifications = _this.notificationsProvider.NotificationsRead;
                _this.allUsers = _this.notificationsProvider.buddyUsersRead;
            });
        });
    }
    NotificationReadPage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('Notifications');
    };
    NotificationReadPage.prototype.ionViewDidEnter = function () {
        this.notificationsProvider.getMyNotificationsRead();
    };
    NotificationReadPage.prototype.showNotificationsConfirmation = function (details) {
        if (details.Type == 'Request') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__requests_requests__["a" /* RequestsPage */]);
        }
        else if (details.Type == 'Friend') {
        }
        else {
        }
    };
    NotificationReadPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-notification-read',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\notification-read\notification-read.html"*/'\n\n<ion-content padding color="logo">\n\n\n\n\n	<ion-spinner name="bubbles" *ngIf="looding" class="looding"></ion-spinner>\n\n\n\n	<div *ngIf="!looding">\n\n\n		<div class="no-items" *ngIf="blockList == 0">\n			<ion-icon name="person"></ion-icon>\n			<p>No Notification yet</p>\n		</div>\n\n\n		<div *ngIf="Notifications != 0">\n			<ion-item *ngFor="let item of Notifications; let i = index" (click)="showNotificationsConfirmation(item)">\n				<ion-avatar item-start>\n			      <img src="{{allUsers[i].Photo}}">\n			    </ion-avatar>\n			    <h2 *ngIf="item.Type == \'Request\'">New request from {{allUsers[i].Name}}</h2>\n			    <h2 *ngIf="item.Type == \'Friend\'">You and {{allUsers[i].Name}} are now friends</h2>\n			</ion-item>\n		</div>\n\n\n	</div>\n\n\n\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\notification-read\notification-read.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_notification_notification__["a" /* NotificationProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], NotificationReadPage);
    return NotificationReadPage;
}());

//# sourceMappingURL=notification-read.js.map

/***/ }),

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationUnreadPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_notification_notification__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__requests_requests__ = __webpack_require__(114);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { SuperTabsController } from 'ionic2-super-tabs';


var NotificationUnreadPage = /** @class */ (function () {
    function NotificationUnreadPage(notificationsProvider, navCtrl, ngZone, events) {
        var _this = this;
        this.notificationsProvider = notificationsProvider;
        this.navCtrl = navCtrl;
        this.ngZone = ngZone;
        this.events = events;
        this.Notifications = [];
        this.allUsers = [];
        this.looding = true;
        this.events.subscribe('Notifications', function () {
            _this.ngZone.run(function () {
                _this.looding = false;
                _this.Notifications = _this.notificationsProvider.NotificationsUnread;
                _this.allUsers = _this.notificationsProvider.buddyUsersUnread;
            });
        });
    }
    NotificationUnreadPage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('Notifications');
    };
    NotificationUnreadPage.prototype.ionViewDidEnter = function () {
        this.notificationsProvider.getMyNotificationsUnread();
    };
    NotificationUnreadPage.prototype.showNotificationsConfirmation = function (details) {
        if (details.Type == 'Request') {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__requests_requests__["a" /* RequestsPage */]);
        }
        else if (details.Type == 'Friend') {
        }
        else {
        }
        this.notificationsProvider.makeNotificationAsRead(details);
    };
    NotificationUnreadPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-notification-unread',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\notification-unread\notification-unread.html"*/'\n<ion-content padding color="logo">\n	<ion-spinner name="bubbles" *ngIf="looding" class="looding"></ion-spinner>\n\n\n\n	<div *ngIf="!looding">\n\n\n		<div class="no-items" *ngIf="blockList == 0">\n			<ion-icon name="person"></ion-icon>\n			<p>No Notification yet</p>\n		</div>\n\n\n		<div *ngIf="Notifications != 0">\n			<ion-item *ngFor="let item of Notifications; let i = index" (click)="showNotificationsConfirmation(item)">\n				<ion-avatar item-start>\n			      <img src="{{allUsers[i].Photo}}">\n			    </ion-avatar>\n			    <h2 *ngIf="item.Type == \'Request\'">New request from {{allUsers[i].Name}}</h2>\n			    <h2 *ngIf="item.Type == \'Friend\'">You and {{allUsers[i].Name}} are now friends</h2>\n			</ion-item>\n		</div>\n\n\n	</div>\n\n\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\notification-unread\notification-unread.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_notification_notification__["a" /* NotificationProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], NotificationUnreadPage);
    return NotificationUnreadPage;
}());

//# sourceMappingURL=notification-unread.js.map

/***/ }),

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupaddmemberPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_group_group__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile_profile__ = __webpack_require__(68);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GroupaddmemberPage = /** @class */ (function () {
    function GroupaddmemberPage(navCtrl, ngZone, events, groupProvider, navParams, loadCtrl, platform, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.ngZone = ngZone;
        this.events = events;
        this.groupProvider = groupProvider;
        this.navParams = navParams;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.Friends = [];
        this.looding = true;
        this.groupDetails = this.navParams.get('groupDetails');
        this.events.subscribe('AllFriends', function () {
            _this.ngZone.run(function () {
                _this.looding = false;
                _this.Friends = _this.groupProvider.AllFriends;
            });
        });
    }
    GroupaddmemberPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    GroupaddmemberPage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('AllFriends');
    };
    GroupaddmemberPage.prototype.ionViewDidEnter = function () {
        this.groupProvider.getAllFriends(this.groupDetails);
    };
    GroupaddmemberPage.prototype.showFriendsConfirmation = function (userDetails) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: userDetails.Name,
            message: 'Tap on option',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        _this.showToast('Cancel');
                    }
                },
                {
                    text: 'Add ' + userDetails.Name + ' to Group',
                    handler: function () {
                        _this.addMember(userDetails, _this.groupDetails);
                    }
                }, {
                    text: 'View Profile',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__profile_profile__["a" /* ProfilePage */], {
                            userDetails: userDetails
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    GroupaddmemberPage.prototype.addMember = function (userDetails, groupDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Adding ' + userDetails.Name + ' to group ...'
        });
        load.present();
        this.groupProvider.addMember(userDetails, groupDetails).then(function () {
            load.dismiss();
            _this.showToast(userDetails.Name + ' has been added to group');
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    GroupaddmemberPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groupaddmember',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\groupaddmember\groupaddmember.html"*/'<ion-header>\n  <ion-navbar color="logo">\n    <ion-title>Add Member</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n\n\n\n\n\n\n<ion-spinner name="bubbles" *ngIf="looding" class="looding"></ion-spinner>\n\n\n\n<div *ngIf="Friends != 0">\n	<ion-item *ngFor="let user of Friends" (click)="showFriendsConfirmation(user)">\n		<ion-avatar item-start>\n	      <img src="{{user.Photo}}">\n	    </ion-avatar>\n	    <h2>{{user.Name}}</h2>\n	</ion-item>\n</div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\groupaddmember\groupaddmember.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_2__providers_group_group__["a" /* GroupProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], GroupaddmemberPage);
    return GroupaddmemberPage;
}());

//# sourceMappingURL=groupaddmember.js.map

/***/ }),

/***/ 332:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupmemberPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_group_group__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile_profile__ = __webpack_require__(68);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GroupmemberPage = /** @class */ (function () {
    function GroupmemberPage(navCtrl, loadCtrl, platform, alertCtrl, ngZone, events, groupProvider, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.ngZone = ngZone;
        this.events = events;
        this.groupProvider = groupProvider;
        this.navParams = navParams;
        this.GroupMembers = [];
        this.looding = true;
        this.User_Uid = this.groupProvider.UserUid;
        this.groupDetails = this.navParams.get('groupDetails');
        this.events.subscribe('GroupMembers', function () {
            _this.ngZone.run(function () {
                _this.looding = false;
                _this.GroupMembers = _this.groupProvider.groupMembers;
            });
        });
    }
    GroupmemberPage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    GroupmemberPage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('GroupMembers');
    };
    GroupmemberPage.prototype.ionViewDidEnter = function () {
        this.groupProvider.getGroupMembers(this.groupDetails);
    };
    GroupmemberPage.prototype.showMemberConfirmation = function (userDetails) {
        var _this = this;
        if (this.groupDetails.Owner = this.groupProvider.UserUid) {
            if (this.groupDetails.Owner == userDetails.Id) {
                console.log('You are the admin');
            }
            else {
                var confirm_1 = this.alertCtrl.create({
                    title: userDetails.Name,
                    message: 'Tap on option',
                    buttons: [
                        {
                            text: 'Cancel',
                            handler: function () {
                                _this.showToast('Cancel');
                            }
                        },
                        {
                            text: 'Delete ' + userDetails.Name + ' from Group',
                            handler: function () {
                                _this.deleteMember(userDetails, _this.groupDetails);
                            }
                        }, {
                            text: 'View Profile',
                            handler: function () {
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__profile_profile__["a" /* ProfilePage */], {
                                    userDetails: userDetails
                                });
                            }
                        }
                    ]
                });
                confirm_1.present();
            }
        }
        else {
            console.log('You are not admin');
        }
    };
    GroupmemberPage.prototype.deleteMember = function (userDetails, groupDetails) {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Deleting ' + userDetails.Name + ' ...'
        });
        load.present();
        this.groupProvider.deleteMember(userDetails, groupDetails).then(function () {
            load.dismiss();
            _this.showToast(userDetails.Name + ' has been deleted');
        }).catch(function (err) {
            load.dismiss();
            _this.showToast(err);
        });
    };
    GroupmemberPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groupmember',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\groupmember\groupmember.html"*/'<ion-header>\n\n  <ion-navbar color="logo">\n    <ion-title>Group Members</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n<ion-spinner name="bubbles" *ngIf="looding" class="looding"></ion-spinner>\n\n\n	<ion-item *ngFor="let user of GroupMembers" (click)="showMemberConfirmation(user)">\n		<ion-avatar item-start>\n	      <img src="{{user.Photo}}">\n	    </ion-avatar>\n	    <h2>{{user.Name}}</h2>\n	    <ion-note *ngIf="groupDetails.Owner === user.Id">Admin</ion-note>\n	</ion-item>\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\groupmember\groupmember.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_2__providers_group_group__["a" /* GroupProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], GroupmemberPage);
    return GroupmemberPage;
}());

//# sourceMappingURL=groupmember.js.map

/***/ }),

/***/ 333:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimelinePageModule", function() { return TimelinePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__timeline__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__directives_directives_module__ = __webpack_require__(565);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var TimelinePageModule = /** @class */ (function () {
    function TimelinePageModule() {
    }
    TimelinePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__timeline__["a" /* TimelinePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__timeline__["a" /* TimelinePage */]),
                __WEBPACK_IMPORTED_MODULE_3__directives_directives_module__["a" /* DirectivesModule */]
            ],
        })
    ], TimelinePageModule);
    return TimelinePageModule;
}());

//# sourceMappingURL=timeline.module.js.map

/***/ }),

/***/ 378:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BookedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__bookings_bookings__ = __webpack_require__(379);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BookedPage = /** @class */ (function () {
    function BookedPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    BookedPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BookedPage');
    };
    BookedPage.prototype.goback = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__bookings_bookings__["a" /* BookingsPage */]);
    };
    BookedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-booked',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\booked\booked.html"*/'<ion-header>\n  <ion-navbar color="purple">\n    <ion-title>Nearby</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <div style="height:50px; width: fit-content; margin-top: -20px; margin-left: -20px;margin-right: -20px;">\n      <img src="assets/sarss.png" />\n     \n  </div>\n  <p ion-text color="white" style="text-align: end;">Open 08:00 - 5:00</p>\n\n  <div  style="height: 25vw;"></div>\n  <div>\n  <ion-card class="card2">    \n  <ion-card-header class="card-header2" (click)="toggleAccordion()">\n  <p ion-text color="white" style="font-size: x-small;" >You have been successfully queued</p>\n  </ion-card-header >\n </ion-card> \n \n <div class="text">\n <p ion-text color="very">You have been queued at SARS for </p>\n <label ion-text color="very">Today 14:00</label>\n <p ion-text color="very">Your ticket number </p>\n <label  ion-text color="purple" style="font-size: medium;font-weight: bold;">#567789</label>\n</div>\n <div  style="height: 25vw;"></div>\n <div > <button ion-button round style="height:10vw; width:40vw; text-transform: none; " color="purple" (click)="goback()" >Done</button></div>\n \n\n  </div>  \n\n\n\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\booked\booked.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], BookedPage);
    return BookedPage;
}());

//# sourceMappingURL=booked.js.map

/***/ }),

/***/ 379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BookingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sars_sars__ = __webpack_require__(380);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BookingsPage = /** @class */ (function () {
    function BookingsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    BookingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BookingsPage');
    };
    BookingsPage.prototype.sars = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__sars_sars__["a" /* SarsPage */]);
    };
    BookingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-bookings',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\bookings\bookings.html"*/'\n<ion-header >\n  <ion-navbar  color="purple">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    <ion-title  style="text-transform: none;" >Nearby</ion-title>\n  </button>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="backkcolor">\n    \n    <button ion-button round outline style="height:10vw; width:200vw; text-transform: none; " center color="purple"  (click)="signin()">\n      <img src="../assets/icons8-search.png" style="margin-left: -12px;"/>\n   <p style="font-size:xx-small; margin-left: 2px;" color= "purple">Search for municipality facility,office,center etc</p>\n  </button>\n  </div>\n \n <div class="update" style="height: 2vw;"></div>\n <div class="service" >\n  <p ion text-color="purple" style="font-weight: bold;font-size: large;">Service Centres</p>\n</div>\n <div>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col >\n       \n         <div  (click)="sars()" style="width:34vw;height:24vw ;margin-left: -18px; " >\n        <img src="assets/sars.png" />\n      </div>\n      \n      </ion-col>\n      <ion-col style="bottom: -7px;">\n      \n         <div  style="width:34vw;height:24vw;margin:-6px;"center>\n        <img  src="assets/office.png"  />\n        <p></p>\n      </div>\n       \n      </ion-col>\n      <ion-col>\n      \n         <div style="width:34vw;height:24vw;margin-right: -66px; ">\n        <img  src="assets/license.png" />\n      </div>\n     \n      </ion-col>\n    </ion-row>\n  </ion-grid>\n </div>\n  \n\n  \n</ion-content>\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\bookings\bookings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], BookingsPage);
    return BookingsPage;
}());

//# sourceMappingURL=bookings.js.map

/***/ }),

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SarsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__booked_booked__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__nearcentre_nearcentre__ = __webpack_require__(381);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SarsPage = /** @class */ (function () {
    function SarsPage(renderers, toast, navCtrl, zone, userservice, alertCtrl) {
        this.renderers = renderers;
        this.toast = toast;
        this.navCtrl = navCtrl;
        this.zone = zone;
        this.userservice = userservice;
        this.alertCtrl = alertCtrl;
        this.accordionExpanded = false;
        // @ViewChild("lc") listContent :any;
        this.icon = "arrow-forward";
        /* var maxV=10000
        var minV=1000
        this.rawRandomNumber = Math.random() * (maxV - minV) + minV
        this.randomNumberFloor =Math.floor(this.rawRandomNumber)
        this.toastOptions = {
        message: ' Appointment send!',
        duration: 3000 ;*/
    }
    SarsPage.prototype.OnInit = function () {
        console.log(this.cardContent.nativeElement);
        //console.log(this.listContent.nativeElement);
    };
    SarsPage.prototype.toggleAccordion = function () {
        if (this.accordionExpanded) {
            this.renderers.setElementStyle(this.cardContent.nativeElement, "max-height", "0px");
        }
        else {
            this.renderers.setElementStyle(this.cardContent.nativeElement, "max-height", "500px");
        }
        this.accordionExpanded = !this.accordionExpanded;
    };
    SarsPage.prototype.alert = function (message) {
        this.alertCtrl.create({
            title: 'Appointment!',
            message: 'Your appointment is successfully Scheduled. ',
            subTitle: message,
            buttons: ['OK']
        }).present();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__booked_booked__["a" /* BookedPage */]);
    };
    SarsPage.prototype.map = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__nearcentre_nearcentre__["a" /* NearcentrePage */]);
    };
    SarsPage.prototype.popup = function (message) {
        this.alertCtrl.create({
            message: 'Tax Service Debt Management:Taxpayers can be assisted with Debt arrangments.Dedicated Small,Micro and Medium Enterprices (SMMEs) service deskTo Qualify as an SMME,you need to be the actual owner with a per annum business turnover of between R0 and R20M.Where to park:There is ample parking space at the new branch',
            subTitle: message,
            buttons: ['OK']
        }).present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("cc"),
        __metadata("design:type", Object)
    ], SarsPage.prototype, "cardContent", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */])
    ], SarsPage.prototype, "childNavCtrl", void 0);
    SarsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sars',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\sars\sars.html"*/'\n<ion-header>\n  <ion-navbar color="purple">\n    <ion-title>Nearby</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <div style="height:50px; width: fit-content; margin-top: -20px; margin-left: -20px;margin-right: -20px;">\n      <img src="assets/sarss.png" />\n  </div>\n\n  <p ion-text color="white" style="text-align: end;">Open 08:00 - 5:00</p>\n\n <div>\n  <div style="height: 15vw;margin-top: 30px;"></div>\n   <div >\n  <ion-card >    \n    <ion-card-header >\n     \n    <!--   <button ion-button clear small icon-only item-right> -->\n        <p ion-text color="white" (click)="popup()" >Available Services</p>\n       <!--  <ion-icon color="light" [name]="icon"></ion-icon>\n      </button> -->\n    </ion-card-header >\n\n  </ion-card> \n</div> \n\n    \n  <div  style="height: 5vw;"></div>\n  <div>\n  <ion-card class="card2">    \n  <ion-card-header class="card-header2" (click)="toggleAccordion()">\n  <p ion-text color="white" >Book An Appointment</p>\n  </ion-card-header >\n  <ion-card-content  #cc>\n    <div  style="height: 5vw;"></div>\n    <p style=" width: 85vw; font-size: medium;  margin-left: -15vw; font-weight: bold; text-align: right;">Time </p>\n\n    <p style=" width: 85vw;  font-size: medium;margin-top: -28px;  margin-left: 1vw; text-align: left; font-weight: bold;">Day </p>\n    <ion-item  class="day" color="gray" style=" height:12vw; width:35vw; margin-left: -2vw;">\n     <ion-label >Today</ion-label>\n  </ion-item>\n  <ion-label style="font-size: x-small; text-align: left; margin-left: 5vw;">Only same day bookings allowed</ion-label>\n     <ion-item class="day" color="gray" style="width: 35vw; margin-left: 45vw; margin-top: -80px;">\n       <ion-datetime color="standard"   displayFormat="h:mm A " pickerFormat="h mm A"  ></ion-datetime>\n   </ion-item>\n   <div class="spacer" style="height: 80px;" ></div>\n      <div class="bOtn">\n      <button ion-button round color="standard" (click)="alert()" center style="text-transform: none; height:12vw; width:50vw; ">Book/Queue</button>\n    </div>\n    <div class="spacer" style="height: 10px;" ></div>\n  </ion-card-content>\n </ion-card> \n  </div>  \n\n\n      <div  style="height: 1vw;"></div>\n       <button ion-button block  round  color="yellow"style="height:16vw; text-transform: none;" (click)="map()">\n     <p ion-text color="white" style="text-align: left;">Navigate</p>\n     <img src="../assets/imgs/navigation.png" style="margin-right: -20px;">\n      </button>\n    </div>\n \n\n  </ion-content>\n\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\sars\sars.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Renderer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], SarsPage);
    return SarsPage;
}());

//# sourceMappingURL=sars.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NearcentrePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_connectivity_connectivity__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__place_details_place_details__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__place_direction_place_direction__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__restaurants_restaurants__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__school_school__ = __webpack_require__(384);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { MapPage } from '../map/map';
//import { ListPage } from '../list/list';




//import { SupermarketPage } from '../supermarket/supermarket';
//import { HospitalPage } from '../hospital/hospital';
//import { PharmacyPage } from '../pharmacy/pharmacy';
//import { ChurchPage } from '../church/church';

//import { PostooficePage } from '../postoofice/postoofice';
var NearcentrePage = /** @class */ (function () {
    function NearcentrePage(connectionService, navCtrl) {
        var _this = this;
        this.connectionService = connectionService;
        this.navCtrl = navCtrl;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_5__restaurants_restaurants__["a" /* RestaurantsPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_6__school_school__["a" /* SchoolPage */];
        this.rating = 0;
        this.showFooter = true; //show and hide footer using the Fab Icon
        this.connectionService.selectedPlaceObserve$.subscribe(function (place) {
            _this.selected_place = place;
            if (_this.selected_place)
                _this.rating = _this.selected_place.rating;
            //this.rating=4;
        });
    }
    /*
      @Author:Dieudonne Dengun
      @Date:03/05/2018
      @Description:Navigate to Place Details Page
    */
    NearcentrePage.prototype.showPlaceDetailsPage = function (place) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__place_details_place_details__["a" /* PlaceDetailsPage */], { place: place });
    };
    /*
     @Author:Dieudonne Dengun
     @Date:03/05/2018
     @Description:Navigate to Place Direction Page
   */
    NearcentrePage.prototype.showDirectionPage = function (place) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__place_direction_place_direction__["a" /* PlaceDirectionPage */], { direction: place });
    };
    NearcentrePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-nearcentre',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\nearcentre\nearcentre.html"*/'\n\n<ion-header >\n\n  <ion-navbar color="dark">\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-buttons class="btns" right >\n\n      <button class="alert-default" left ion-button icon-only clear large (click)="showPlaceDetailsPage(selected_place)" padding margin-right style=" font-size: 16px; text-transform: none;">\n\n          <ion-icon name="list"></ion-icon>\n\n          More Info\n\n      </button>\n\n  \n\n      <button class="alert-info" right ion-button icon-only clear large (click)="showDirectionPage(selected_place)" padding margin-left style="font-size: 16px; text-transform: none;">\n\n          <ion-icon name="navigate"></ion-icon>\n\n          Directions\n\n      </button>\n\n  </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content color="standard">\n\n \n\n  <ion-tabs color="standard">\n\n    <ion-tab [root]="tab6Root"  tabTitle="Schools" tabIcon="neo1"></ion-tab>\n\n    <ion-tab [root]="tab1Root"  tabTitle="Post Office" tabIcon="neo" ></ion-tab>\n\n   <!--  <ion-tab [root]="tab2Root"  tabTitle="Shopping Center" tabIcon="neo4"></ion-tab>\n\n    <ion-tab [root]="tab5Root"  tabTitle="Church"  tabIcon="neo2" ></ion-tab>\n\n    <ion-tab [root]="tab6Root"  tabTitle="Schools" tabIcon="neo1"></ion-tab>\n\n    <ion-tab [root]="tab7Root"  tabTitle="Banks" tabIcon="neo3"></ion-tab> -->\n\n  </ion-tabs>\n\n\n\n</ion-content>\n\n\n\n\n\n\n\n\n\n\n\n \n\n\n\n\n\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\nearcentre\nearcentre.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_connectivity_connectivity__["a" /* ConnectivityProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */]])
    ], NearcentrePage);
    return NearcentrePage;
}());

//# sourceMappingURL=nearcentre.js.map

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaceDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_connectivity_connectivity__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_locations_locations__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_browser_tab__ = __webpack_require__(598);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__place_direction_place_direction__ = __webpack_require__(211);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var PlaceDetailsPage = /** @class */ (function () {
    function PlaceDetailsPage(navCtrl, browserTab, iab, social_sharing, location_handler, navParams, connectionListerner) {
        this.navCtrl = navCtrl;
        this.browserTab = browserTab;
        this.iab = iab;
        this.social_sharing = social_sharing;
        this.location_handler = location_handler;
        this.navParams = navParams;
        this.connectionListerner = connectionListerner;
        this.map_initialised = false;
        this.tab1 = __WEBPACK_IMPORTED_MODULE_7__place_direction_place_direction__["a" /* PlaceDirectionPage */];
        this.apiKey = "AIzaSyBkK4dKGUIh5qkHR6FacFJTKbLR_nyEftk"; //API KEY
        this.show_place_photos = false; //determine if the place photos sliders be shown or not
        this.show_place_map = false; //determine if the current place location be displayed on map 
        var place = navParams.get('place');
        //check if the details were gotten
        if (place) {
            this.place = place;
            this.rating = place.rating;
            var place_photos = place.place_photos;
            if (place.place_photos) {
                if (place_photos.length > 0) {
                    //show the photos slider and hide the map at the header section
                    this.show_place_photos = true;
                    this.place_photos = [];
                    for (var i = 0; i < place_photos.length; i++) {
                        var image = place.place_photos[i].getUrl({
                            maxHeight: 250,
                            maxWeight: 1000
                        });
                        var photo = {
                            link: image
                        };
                        this.place_photos.push(photo);
                    }
                }
            }
            else {
                //show the map instead since there are photos to display
                this.show_place_map = true;
            }
        }
    }
    PlaceDetailsPage.prototype.ionViewDidLoad = function () {
        this.loadGoogleMap();
    };
    /*
       @Author:Dieudonne Dengun
       @Date:12/05/2018
       @Description:Load map for
   
     */
    PlaceDetailsPage.prototype.loadGoogleMap = function () {
        var _this = this;
        this.addConnectivityListeners();
        if (typeof google == "undefined" || typeof google.maps == "undefined") {
            console.log("Google maps JavaScript needs to be loaded.");
            if (this.connectionListerner.isOnline()) {
                //Load the SDK
                window['mapInit'] = function () {
                    _this.renderMap();
                    _this.enableMap();
                };
                this.map_initialised = true;
                var script = document.createElement("script");
                script.id = "googleMaps";
                //https://maps.googleapis.com/maps/api/js?key=AIzaSyBkK4dKGUIh5qkHR6FacFJTKbLR_nyEftk&callback=initMap
                if (this.apiKey) {
                    script.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
                }
                else {
                    script.src = 'https://maps.googleapis.com/maps/api/js?callback=mapInit';
                }
                document.body.appendChild(script);
            }
        }
        else {
            if (this.connectionListerner.isOnline()) {
                this.renderMap();
                this.enableMap();
            }
            else {
                this.disableMap();
            }
        }
    };
    /*
       @Author:Dieudonne Dengun
       @Date:12/04/2018
       @Description:Disable the display map if there is no internet and alert user of network change
     */
    PlaceDetailsPage.prototype.disableMap = function () {
        var title = "Offline Status";
        var message = "You are currently offline.Please connect to the internet to continue";
        this.location_handler.showSimpleAlertDialog(title, message);
    };
    /*
      @Author:Dieudonne Dengun
      @Date:12/04/2018
      @Description:Enable and render map if the user internet has been restored after reconnection
    */
    PlaceDetailsPage.prototype.enableMap = function () {
        this.location_handler.showToastMessage("You are currently online", "bottom", 3000);
    };
    /*
    @Author:Dieudonne Dengun
    @Date:12/04/2018
    @Description:add network change listener to monitor user connection fluctuation
    */
    PlaceDetailsPage.prototype.addConnectivityListeners = function () {
        var _this = this;
        //determine of the user phone is connected to the internet
        var onOnline = function () {
            setTimeout(function () {
                if (typeof google == "undefined" || typeof google.maps == "undefined") {
                    if (!_this.map_initialised) {
                        //reintialised the map again on the dom
                        _this.loadGoogleMap();
                    }
                }
                else {
                    if (_this.map_initialised) {
                        _this.renderMap();
                        _this.enableMap();
                    }
                }
            }, 2000);
        };
        //this means the user is offline, so disabled the map
        var onOffline = function () {
            _this.disableMap();
        };
        //add online and offline network listeners to the dom to monitor network changes
        document.addEventListener('online', onOnline, false);
        document.addEventListener('offline', onOffline, false);
    };
    /*
       @Description:Render place on map
    
    */
    PlaceDetailsPage.prototype.renderMap = function () {
        var latLng = new google.maps.LatLng(this.place.latitude, this.place.longitude);
        var mapOptions = {
            center: latLng,
            zoom: 16,
            zoomControl: true,
            fullscreenControl: false,
            gestureHandling: 'none',
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var place_lagLng = {
            lat: this.place.latitude,
            lng: this.place.longitude
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.comtent_map = new google.maps.Map(this.contentMapElement.nativeElement, mapOptions);
        var map_marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: place_lagLng,
            label: {
                text: "C",
                color: "white",
            },
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                fillColor: 'Blue',
                fillOpacity: .6,
                scale: 20,
                strokeColor: 'white',
                strokeWeight: .5
            }
        });
        var content_map_marker = new google.maps.Marker({
            map: this.comtent_map,
            animation: google.maps.Animation.BOUNCE,
            position: place_lagLng,
            label: {
                text: "C",
                color: "white",
            },
        });
    };
    /*
  
    */
    /*
      @Description: Save a place on local storage as favorite
    */
    /*
     @Description:Show direction for the place
    */
    PlaceDetailsPage.prototype.showDirection = function (place) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__place_direction_place_direction__["a" /* PlaceDirectionPage */], { direction: place });
    };
    /*
      @Author:Dieudonne Dengun
      @Date: 02/06/2018
      @Description:Open a selected place website
    */
    PlaceDetailsPage.prototype.openPlaceWebsite = function (url) {
        //open url from the browser's service
        var _this = this;
        this.browserTab.isAvailable()
            .then(function (isAvailable) {
            //check if browsertab is supported or available for the device
            if (isAvailable) {
                _this.browserTab.openUrl(url).then(function (success) {
                    if (success) {
                        //this means the browser was successfully open
                    }
                });
            }
            else {
                // open URL with InAppBrowser instead since browsertab not available
                _this.iab.create(url, "_system", "location=true");
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('contentMap'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], PlaceDetailsPage.prototype, "contentMapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], PlaceDetailsPage.prototype, "mapElement", void 0);
    PlaceDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-place-details',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\place-details\place-details.html"*/'<ion-header>\n\n\n\n  <ion-navbar color="logo">\n\n    <ion-title>\n\n      <small text-wrap>{{place.place_name}} </small>\n\n      <br/>\n\n      <small>\n\n        <i text-wrap>About {{place.place_distance}}m from here</i>\n\n      </small>\n\n    </ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n  <!-- Place photos or map depending on wether there are/is photos -->\n\n\n\n  <div #map [style.display]="show_place_map ? \'block\' : \'none\'" class="place-map">\n\n\n\n  </div>\n\n  <!-- This means selected place got no picture at the moment -->\n\n  <div [style.display]="show_place_photos ? \'block\' : \'none\'">\n\n    <div class="slider-banner">\n\n      <ion-slides no-margin pager="true" loop="true" *ngIf="place_photos" slidesPerView="1" autoplay="2000" speed="500">\n\n\n\n        <ion-slide *ngFor=" let photo of place_photos; let j=index" class="slide" (click)="showImages(place)">\n\n          <img class="slider-image" no-padding src="{{photo.link}}" />\n\n        </ion-slide>\n\n\n\n      </ion-slides>\n\n    </div>\n\n  </div>\n\n\n\n  <!-- Place details secion -->\n\n\n\n  <div class="ion-card">\n\n    <!-- Place head section -->\n\n    <ion-item class="item-selected">\n\n      <ion-avatar item-left>\n\n        <img class="footer-image" src="{{place.place_url}}">\n\n      </ion-avatar>\n\n      <h3 class="segment-title" text-wrap>{{place.place_name}}</h3>\n\n\n\n      \n\n    </ion-item>\n\n\n\n    <br/>\n\n    <!-- List of possible details about the place -->\n\n    <ion-list>\n\n\n\n      <ion-item>\n\n        <ion-icon class="item-color" name="locate" item-start></ion-icon>\n\n        {{place.place_address}}\n\n      </ion-item>\n\n\n\n      <ion-item *ngIf="place.place_formatted_number">\n\n        <ion-icon class="item-color" name="call" item-start></ion-icon>\n\n        {{place.place_formatted_number}}\n\n      </ion-item>\n\n\n\n      <ion-item *ngIf="place.place_website" (click)="openPlaceWebsite(place.place_website)"> \n\n        <ion-icon class="item-color" name="call" item-start></ion-icon>\n\n        {{place.place_website}}\n\n      </ion-item>\n\n\n\n      <ion-item text-wrap>\n\n        <ion-icon class="item-color" name="time" item-start></ion-icon>\n\n        {{place.place_status}}\n\n      </ion-item>\n\n\n\n    </ion-list>\n\n\n\n\n\n  </div>\n\n  <br/>\n\n  <div #contentMap [style.display]="show_place_photos ? \'block\' : \'none\'" class="place-map ion-card"></div>\n\n</ion-content>\n\n\n\n<ion-footer text-center *ngIf="place">\n\n\n\n  <ion-toolbar color="primary" class="ion-card">\n\n    <!-- <ion-tabs>\n\n      <ion-tab tabIcon="navigate" tabTitle="DIRECTION" [root]="tab1"></ion-tab>\n\n      <ion-tab (ionSelect)="savePlace()" tabIcon="bookmark" tabTitle="SAVE" [root]="tab1"></ion-tab>\n\n      <ion-tab (ionSelect)="callPlace()" tabIcon="call" tabTitle="CALL" [root]="tab1"></ion-tab>\n\n      <ion-tab (ionSelect)="sharePlace()" tabIcon="share" tabTitle="SHARE PLACE" [root]="tab1"></ion-tab>\n\n    </ion-tabs> -->\n\n\n\n    <ion-row>\n\n\n\n      <ion-col text-center (click)="showDirection(place)">\n\n        <ion-icon name="navigate"></ion-icon>\n\n        <span class="icon">Direction</span>\n\n      </ion-col>\n\n      <ion-col text-center *ngIf="place.place_formatted_number">\n\n        <a href="tel:{{place.place_formatted_number}}">\n\n          <ion-icon name="call"></ion-icon>\n\n          <span class="icon">Call</span>\n\n        </a>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-toolbar>\n\n\n\n</ion-footer>\n\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\place-details\place-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_browser_tab__["a" /* BrowserTab */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_3__providers_locations_locations__["a" /* LocationsProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_connectivity_connectivity__["a" /* ConnectivityProvider */]])
    ], PlaceDetailsPage);
    return PlaceDetailsPage;
}());

//# sourceMappingURL=place-details.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestaurantsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_connectivity_connectivity__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_locations_locations__ = __webpack_require__(93);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RestaurantsPage = /** @class */ (function () {
    function RestaurantsPage(navCtrl, platform, location_handler, geolocation, navParams, connectivityService) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.location_handler = location_handler;
        this.geolocation = geolocation;
        this.navParams = navParams;
        this.connectivityService = connectivityService;
        this.mapInitialised = false;
        this.map_loaded = false;
        this.apiKey = "AIzaSyBkK4dKGUIh5qkHR6FacFJTKbLR_nyEftk";
        this.place_view_option = "map";
        this.show_map = true;
        this.current_window = null;
        this.selected_place = null;
        this.offline_dialog = false;
    }
    RestaurantsPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        setTimeout(function () {
            _this.location_handler.showLoader("Loading nearby Post Office....");
            //render the map and nearby services
            _this.renderNearbyServices();
        }, 10000);
    };
    RestaurantsPage.prototype.segmentChanged = function (event_object) {
        console.log(event_object);
        if (event_object._value === "list") {
            this.show_map = false;
        }
        else if (event_object._value === "map") {
            this.show_map = true;
        }
    };
    /*
      @Author:Dieudonne Dengun
      @Date:12/04/2018
      @Description:Initialize google maps and load nearby services with marker on it
    */
    RestaurantsPage.prototype.renderNearbyServices = function () {
        var _this = this;
        this.addConnectivityListeners();
        if (typeof google == "undefined" || typeof google.maps == "undefined") {
            console.log("Google maps JavaScript needs to be loaded.");
            // this.disableMap();
            if (this.connectivityService.isOnline()) {
                console.log("online, loading map");
                //Load the SDK
                window['mapInit'] = function () {
                    _this.initMap();
                    _this.enableMap();
                };
                this.map_loaded = true;
                var script = document.createElement("script");
                script.id = "googleMaps";
                if (this.apiKey) {
                    script.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
                }
                else {
                    script.src = 'https://maps.googleapis.com/maps/api/js?callback=mapInit';
                }
                document.body.appendChild(script);
            }
        }
        else {
            if (this.connectivityService.isOnline()) {
                this.initMap();
                this.enableMap();
            }
            else {
                this.disableMap();
            }
        }
    };
    /*
    @Author:Dieudonne Dengun
    @Date:02/05/2018
  
    */
    /*
      @Author:Dieudonne Dengun
      @Description:Initialize google map and start its rendering on the UI
     */
    RestaurantsPage.prototype.initMap = function () {
        var _this = this;
        this.mapInitialised = true;
        var current;
        this.geolocation.getCurrentPosition().then(function (position) {
            var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            var mapOptions = {
                center: latLng,
                zoom: 16,
                zoomControl: true,
                fullscreenControl: false,
                gestureHandling: 'auto',
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            _this.map = new google.maps.Map(_this.mapElement.nativeElement, mapOptions);
            _this.current_location = latLng; //set user current latitude and longitude to the current context to be used later
            //set the current user location object to be used later
            current = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            //set marker for user current location
            var marker = new google.maps.Marker({
                map: _this.map,
                animation: google.maps.Animation.DROP,
                position: current,
                label: {
                    text: "C",
                    color: "white",
                },
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    fillColor: 'blue',
                    fillOpacity: .6,
                    scale: 20,
                    strokeColor: 'white',
                    strokeWeight: .5
                }
            });
            //show user current location on map
            var infoWindow = new google.maps.InfoWindow;
            infoWindow.setPosition(current);
            infoWindow.setContent('You are here.');
            infoWindow.open(_this.map);
            _this.map.setCenter(current);
            //  setInterval((() {
            //   if (this.mapInitialised) {
            //    self.getNearbyActivities("restaurant", this.current_location, current);
            //   }
            // }).bind(this), 5000000);
            _this.getNearbyActivities("post office", _this.current_location, current);
        });
        this.current_location_object = current;
    };
    /*
      @Author:Dieudonne Dengun
      @Date:12/04/2018
      @Description:Disable the display map if there is no internet and alert user of network change
    */
    RestaurantsPage.prototype.disableMap = function () {
        var title = "Offline Status";
        var message = "You are currently offline.Please connect to the internet to continue";
        // this.location_handler.showSimpleAlertDialog(title,message);
    };
    /*
      @Author:Dieudonne Dengun
      @Date:12/04/2018
      @Description:Enable and render map if the user internet has been restored after reconnection
    */
    RestaurantsPage.prototype.enableMap = function () {
        this.location_handler.showToastMessage("You are currently online", "bottom", 3000);
    };
    /*
    @Author:Dieudonne Dengun
    @Date:12/04/2018
    @Description:add network change listener to monitor user connection fluctuation
    */
    RestaurantsPage.prototype.addConnectivityListeners = function () {
        var _this = this;
        //determine of the user phone is connected to the internet
        var onOnline = function () {
            setTimeout(function () {
                if (typeof google == "undefined" || typeof google.maps == "undefined") {
                    _this.renderNearbyServices();
                }
                else {
                    if (!_this.mapInitialised) {
                        _this.initMap();
                    }
                    _this.enableMap();
                }
            }, 2000);
        };
        //this means the user is offline, so disabled the map
        var onOffline = function () {
            _this.disableMap();
        };
        //add online and offline network listeners to the dom to monitor network changes
        document.addEventListener('online', onOnline, false);
        document.addEventListener('offline', onOffline, false);
    };
    /*
      @Author:Dieudonne Dengun
      @Date:12/04/2018
      @Description: get and set up a list and map view of nearby services based on a user current location
      @param: place_type, user_current_location
    */
    RestaurantsPage.prototype.getNearbyActivities = function (place_type, user_current_location, current_location) {
        //build the search nearby request object to be searched
        var request = {
            location: user_current_location,
            radius: '1000',
            type: [place_type]
        };
        var user_location = current_location;
        console.log(current_location);
        var nearby_places = []; //hold the list of nearby places
        var place_service = new google.maps.places.PlacesService(this.map);
        var map_id = this.map;
        var window = this.current_window;
        var self = this;
        place_service.nearbySearch(request, (function (results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                console.log(results);
                var _loop_1 = function () {
                    place = results[i];
                    var photo = place.icon;
                    if (place.photos) {
                        var photos_list = place.photos;
                        photo = photos_list[0].getUrl({ 'maxWidth': 35, 'maxHeight': 35 });
                    }
                    var marker = new google.maps.Marker({
                        map: map_id,
                        animation: google.maps.Animation.DROP,
                        position: place.geometry.location,
                        title: place.name,
                        label: {
                            text: "R",
                            color: "white",
                        },
                    });
                    //set custom features to the marker
                    //  marker.setLabel("R");
                    // check and determine is  a service is still available
                    var state_postOffice = "";
                    if (place.opening_hours) {
                        if (place.opening_hours.open_now) {
                            state_postOffice = "Service is currently on and still open";
                        }
                        else {
                            state_postOffice = "Service has currently closed for the day";
                        }
                    }
                    else {
                        state_postOffice = "Service has currently closed for the day";
                    }
                    //build the modal details for a clicked place marker
                    // let cont = "<div class='card'>" +
                    //   "<img src='" + photo + "' alt='' class='place-logo'>" +
                    //   "<h1 class='title-window'>" + place.name + "</h1>" +
                    //   "<p >Located at '<b>" + place.vicinity + "</b></p>" +
                    //   "<p>" + state_restuarant + "</p>" +
                    //   "</div";
                    // // let content = "<div><p> "+"<b>"+place.name+" restaurant</b> located at '"+place.vicinity+"'</p> <br/> <i>'"+state_restuarant+"'</i></div>";          
                    // let infoWindow = new google.maps.InfoWindow({
                    //   content: cont
                    // });
                    var lng = place.geometry.location.lng();
                    var lat = place.geometry.location.lat();
                    //calculate the distance of this place from that of the user
                    var units = 'm';
                    var earthRadius = {
                        miles: 3958.8,
                        m: 6378137
                    };
                    var R = earthRadius[units];
                    var lat1 = user_location.lat;
                    var lon1 = user_location.lng;
                    var lat2 = lat;
                    var lon2 = lng;
                    var dLat = (lat2 - lat1) * (Math.PI / 180);
                    var dLon = (lon2 - lon1) * (Math.PI / 180);
                    ;
                    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                        Math.cos((lat1) * (Math.PI / 180)) * Math.cos((lat2) * (Math.PI / 180)) *
                            Math.sin(dLon / 2) *
                            Math.sin(dLon / 2);
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                    var d = Math.round((R * c));
                    //toFixed(2);
                    // get the detail place details 
                    placeDetails = {
                        placeId: place.place_id
                    };
                    var selected_place = place;
                    var place_rating = 0;
                    var place_formatted_address = "";
                    var place_formatted_number = "";
                    var place_url = "";
                    var place_website = "";
                    var place_photos = [];
                    var getPlaceDetailService = new google.maps.places.PlacesService(map_id);
                    getPlaceDetailService.getDetails(placeDetails, (function (place, status) {
                        var _this = this;
                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                            place_formatted_number = place.international_phone_number;
                            place_formatted_address = place.formatted_address;
                            place_url = place.url;
                            place_website = place.place_website;
                            place_photos = place.photos;
                            //  console.log("Lat :"+lat +" Log :"+lng);
                            //push the places as object to the nearby array
                            var place_object_1 = {
                                latitude: lat,
                                longitude: lng,
                                place_name: selected_place.name,
                                place_vicinity: selected_place.vicinity,
                                place_url: photo,
                                place_status: state_postOffice,
                                place_distance: d,
                                current_location: current_location,
                                rating: place.rating,
                                place_website: place_website,
                                place_map_url: place_url,
                                place_address: place_formatted_address,
                                place_formatted_number: place_formatted_number,
                                place_photos: place_photos
                            };
                            nearby_places.push(place_object_1);
                            //add a listener of click event for the location markers
                            google.maps.event.addListener(marker, 'mouseover', function () {
                                //   if (window != null) {
                                //     // window.close();
                                //   //  this.selected_place=null;
                                // } 
                                // window=infoWindow;
                                // this.selected_place=place_object;
                                _this.setSelectedPlace(place_object_1);
                                // infoWindow.open(map_id, marker);
                                // console.log( this.selected_place);
                            });
                            google.maps.event.addListener(marker, 'click', function () {
                                //   if (window != null) {
                                //     // window.close();
                                //     // selected_place_object=null;
                                // } 
                                // window=infoWindow;
                                _this.setSelectedPlace(place_object_1);
                                // infoWindow.open(map_id, marker);
                                //set the footer place as selected place
                            });
                        }
                        else {
                            console.log("Unable to get place details " + selected_place);
                        }
                    }).bind(this_1));
                };
                var this_1 = this, place, placeDetails;
                for (var i = 0; i < results.length; i++) {
                    _loop_1();
                }
            }
        }).bind(this));
        console.log(nearby_places);
        //update the global places array.
        this.places = nearby_places;
        //stop the loading animation now
        this.location_handler.closeLoader();
        console.log(this.selected_place);
        //end of forloop of places
        //sort the places in order of closeness
        // let sort_places: any = this.location_handler.calculateNearbyPlacesByApplyHaversine(nearby_places,this.current_location_object);
        // if (sort_places) {
        // }
    };
    /*
    @Author:Dieudonne Dengun
    @Date:03/0
    @Description:Update selected place variable
    */
    RestaurantsPage.prototype.setSelectedPlace = function (place) {
        if (place != null)
            this.connectivityService.setSelectedPlace(place);
    };
    //  icon: photo,
    //  label: {
    //   text: place.name,
    //   color: "#212121",
    //   fontWeight: "italic",
    //   fontSize: "8px"
    // }
    RestaurantsPage.prototype.handleResultsCallback = function (results, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
                var place = results[i];
                console.log(place.name);
                var marker = new google.maps.Marker({
                    map: this.map,
                    animation: google.maps.Animation.DROP,
                    position: place.geometry.location
                });
            }
        }
    };
    /*
      @Author:Dieudonne Dengun
      @Date: 09/04/2018
      @Description: add google marker for a search place on Google Map
    */
    RestaurantsPage.prototype.addMarker = function (place) {
        var marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: place.geometry.location
        });
        // this.markers.push(marker);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], RestaurantsPage.prototype, "mapElement", void 0);
    RestaurantsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-restaurants',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\restaurants\restaurants.html"*/'\n\n  <ion-content>\n\n\n\n  <ion-segment [(ngModel)]="place_view_option" color="" (ionChange)="segmentChanged($event)" >\n\n\n\n    <ion-segment-button value="map" class="segment">\n\n      <ion-icon name="map"></ion-icon>\n\n      Map View\n\n    </ion-segment-button>\n\n\n\n    <!-- Contact Tab for the student contact information -->\n\n    <ion-segment-button value="list">\n\n      <ion-icon name="list"></ion-icon>\n\n      List View\n\n    </ion-segment-button>\n\n\n\n  </ion-segment>\n\n \n\n\n\n \n\n     <h3 class="segment-title" padding [style.display]="show_map ? \'block\' : \'none\'" >Post Office  nearby</h3>\n\n    <!-- Display Map here  -->\n\n    <!-- <p>Hello Dengun</p> -->\n\n    <div #map id="map" [style.display]="show_map ? \'block\' : \'none\'" ></div>\n\n  \n\n<!-- Build a switch to check the three segment click -->\n\n<div>\n\n  \n\n  <!-- This means, the student has clicked on the Map Tab Segment -->\n\n  <div  [style.display]="place_view_option == \'map\' ? \'block\' : \'none\'"  class="segment-item">\n\n   \n\n  </div>\n\n\n\n  <!-- This means, the student has clicked on List View Tab Segment -->\n\n  <div [style.display]="place_view_option == \'list\' ? \'block\' : \'none\'"  class="segment-item">\n\n    <div class="segment-item">\n\n\n\n      <h3 class="segment-title" padding>\n\n       Post Office nearby\n\n      </h3>\n\n\n\n      <!-- Display a list of nearby services to the user -->\n\n     <ion-list *ngIf="places" no-lines>\n\n      \n\n        <ion-item *ngFor="let place of places; let j=index">\n\n            <ion-thumbnail item-start>\n\n              <img src="{{place.place_url}}">\n\n            </ion-thumbnail>\n\n            <h3 class="segment-title">{{place.place_name}}</h3>\n\n          <p text-wrap>About <b>{{place.place_distance}}</b> metres from your current location</p>\n\n          <small><i>{{place.place_vicinity}}</i></small>\n\n            <button ion-button clear item-end>\n\n                <ion-icon name="pin"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n      </ion-list>\n\n\n\n    </div>\n\n  </div>\n\n</div>\n\n\n\n \n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\restaurants\restaurants.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_4__providers_locations_locations__["a" /* LocationsProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_connectivity_connectivity__["a" /* ConnectivityProvider */]])
    ], RestaurantsPage);
    return RestaurantsPage;
}());

//# sourceMappingURL=restaurants.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SchoolPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_locations_locations__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_connectivity_connectivity__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SchoolPage = /** @class */ (function () {
    function SchoolPage(navCtrl, location_handler, navParams, geolocation, connectivityService) {
        this.navCtrl = navCtrl;
        this.location_handler = location_handler;
        this.navParams = navParams;
        this.geolocation = geolocation;
        this.connectivityService = connectivityService;
        this.mapInitialised = false;
        this.apiKey = "AIzaSyBkK4dKGUIh5qkHR6FacFJTKbLR_nyEftk";
        this.place_view_option = "map";
        this.show_map = true;
        this.current_window = null;
        this.offline_dialog = false;
    }
    SchoolPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        setTimeout(function () {
            _this.location_handler.showLoader("Loading nearby schools....");
            //render the map and nearby services
            _this.renderNearbyServices();
        }, 10000);
    };
    SchoolPage.prototype.segmentChanged = function (event_object) {
        console.log(event_object);
        if (event_object._value === "list") {
            this.show_map = false;
        }
        else if (event_object._value === "map") {
            this.show_map = true;
        }
    };
    /*
      @Author:Dieudonne Dengun
      @Date:12/04/2018
      @Description:Initialize google maps and load nearby services with marker on it
    */
    SchoolPage.prototype.renderNearbyServices = function () {
        var _this = this;
        this.addConnectivityListeners();
        if (typeof google == "undefined" || typeof google.maps == "undefined") {
            console.log("Google maps JavaScript needs to be loaded.");
            this.disableMap();
            if (this.connectivityService.isOnline()) {
                console.log("online, loading map");
                //Load the SDK
                window['mapInit'] = function () {
                    _this.initMap();
                    //this.enableMap();
                };
                // let script = document.createElement("script");
                // script.id = "googleMaps";
                // if (this.apiKey) {
                //   script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&libraries=places&callback=mapInit';
                // } else {
                //   script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
                // }
                // document.body.appendChild(script);
            }
        }
        else {
            if (this.connectivityService.isOnline()) {
                this.initMap();
                //this.enableMap();
            }
            else {
                this.disableMap();
            }
        }
    };
    /*
       @Author:Dieudonne Dengun
       @Description:Initialize google map and start its rendering on the UI
      */
    SchoolPage.prototype.initMap = function () {
        var _this = this;
        this.mapInitialised = true;
        var current;
        this.geolocation.getCurrentPosition().then(function (position) {
            var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            var mapOptions = {
                center: latLng,
                zoom: 16,
                // zoomControl: false,
                fullscreenControl: false,
                gestureHandling: 'auto',
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            _this.map = new google.maps.Map(_this.mapElement.nativeElement, mapOptions);
            _this.current_location = latLng; //set user current latitude and longitude to the current context to be used later
            //set the current user location object to be used later
            current = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            //set marker for user current location
            var marker = new google.maps.Marker({
                map: _this.map,
                animation: google.maps.Animation.DROP,
                position: current,
                label: {
                    text: "C",
                    color: "white",
                },
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    fillColor: 'blue',
                    fillOpacity: .6,
                    scale: 20,
                    strokeColor: 'white',
                    strokeWeight: .5
                }
            });
            //show user current location on map
            var infoWindow = new google.maps.InfoWindow;
            infoWindow.setPosition(current);
            infoWindow.setContent('You are here.');
            infoWindow.open(_this.map);
            _this.map.setCenter(current);
            //'public_university','private_university'
            var types = ['SARS'];
            _this.getNearbyActivities(types, _this.current_location, current);
        });
        this.current_location_object = current;
    };
    /*
      @Author:Dieudonne Dengun
      @Date:12/04/2018
      @Description:Disable the display map if there is no internet and alert user of network change
    */
    SchoolPage.prototype.disableMap = function () {
        if (this.offline_dialog) {
        }
        else {
            this.offline_dialog = true;
            var title = "Offline Status";
            var message = "You are currently offline.Please connect to the internet to continue";
            this.location_handler.showSimpleAlertDialog(title, message);
        }
        // this.location_handler.showSimpleAlertDialog(title,message);
    };
    /*
      @Author:Dieudonne Dengun
      @Date:12/04/2018
      @Description:Enable and render map if the user internet has been restored after reconnection
    
    enableMap() {
  
      this.location_handler.showToastMessage("You are currently online", "bottom", 3000);
    } */
    /*
    @Author:Dieudonne Dengun
    @Date:12/04/2018
    @Description:add network change listener to monitor user connection fluctuation
    */
    SchoolPage.prototype.addConnectivityListeners = function () {
        var _this = this;
        //determine of the user phone is connected to the internet
        var onOnline = function () {
            setTimeout(function () {
                if (typeof google == "undefined" || typeof google.maps == "undefined") {
                    _this.renderNearbyServices();
                }
                else {
                    if (!_this.mapInitialised) {
                        _this.initMap();
                    }
                    //this.enableMap();
                }
            }, 2000);
        };
        //this means the user is offline, so disabled the map
        var onOffline = function () {
            _this.disableMap();
        };
        //add online and offline network listeners to the dom to monitor network changes
        document.addEventListener('online', onOnline, false);
        document.addEventListener('offline', onOffline, false);
    };
    /*
     @Author:Dieudonne Dengun
     @Date:12/04/2018
     @Description: get and set up a list and map view of nearby services based on a user current location
     @param: place_type, user_current_location
   */
    SchoolPage.prototype.getNearbyActivities = function (place_type, user_current_location, current_location) {
        //build the search nearby request object to be searched
        var request = {
            location: user_current_location,
            radius: '1000',
            type: place_type
        };
        var user_location = current_location;
        console.log(current_location);
        var nearby_places = []; //hold the list of nearby places
        var place_service = new google.maps.places.PlacesService(this.map);
        var map_id = this.map;
        var window = this.current_window;
        var self = this;
        place_service.nearbySearch(request, (function (results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                console.log(results);
                var _loop_1 = function () {
                    place = results[i];
                    var photo = place.icon;
                    if (place.photos) {
                        var photos_list = place.photos;
                        photo = photos_list[0].getUrl({ 'maxWidth': 35, 'maxHeight': 35 });
                    }
                    var marker = new google.maps.Marker({
                        map: map_id,
                        animation: google.maps.Animation.DROP,
                        position: place.geometry.location,
                        title: place.name,
                        label: {
                            text: "S",
                            color: "white",
                        },
                    });
                    //set custom features to the marker
                    //  marker.setLabel("R");
                    // check and determine is  a service is still available
                    var state_restuarant = "";
                    if (place.opening_hours) {
                        if (place.opening_hours.open_now) {
                            state_restuarant = "Service is currently on and still open";
                        }
                        else {
                            state_restuarant = "Service has currently closed for the day";
                        }
                    }
                    else {
                        state_restuarant = "Service has currently closed for the day";
                    }
                    //build the modal details for a clicked place marker
                    // let cont = "<div class='card'>" +
                    //   "<img src='" + photo + "' alt='' class='place-logo'>" +
                    //   "<h1 class='title-window'>" + place.name + "</h1>" +
                    //   "<p >Located at '<b>" + place.vicinity + "</b></p>" +
                    //   "<p>" + state_restuarant + "</p>" +
                    //   "</div";
                    // // let content = "<div><p> "+"<b>"+place.name+" restaurant</b> located at '"+place.vicinity+"'</p> <br/> <i>'"+state_restuarant+"'</i></div>";          
                    // let infoWindow = new google.maps.InfoWindow({
                    //   content: cont
                    // });
                    var lng = place.geometry.location.lng();
                    var lat = place.geometry.location.lat();
                    //calculate the distance of this place from that of the user
                    var units = 'm';
                    var earthRadius = {
                        miles: 3958.8,
                        m: 6378137
                    };
                    var R = earthRadius[units];
                    var lat1 = user_location.lat;
                    var lon1 = user_location.lng;
                    var lat2 = lat;
                    var lon2 = lng;
                    var dLat = (lat2 - lat1) * (Math.PI / 180);
                    var dLon = (lon2 - lon1) * (Math.PI / 180);
                    ;
                    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                        Math.cos((lat1) * (Math.PI / 180)) * Math.cos((lat2) * (Math.PI / 180)) *
                            Math.sin(dLon / 2) *
                            Math.sin(dLon / 2);
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                    var d = Math.round((R * c));
                    //toFixed(2);
                    //  console.log("Lat :"+lat +" Log :"+lng);
                    placeDetails = {
                        placeId: place.place_id
                    };
                    var selected_place = place;
                    var place_rating = 0;
                    var place_formatted_address = "";
                    var place_formatted_number = "";
                    var place_url = "";
                    var place_website = "";
                    var place_photos = [];
                    var getPlaceDetailService = new google.maps.places.PlacesService(map_id);
                    getPlaceDetailService.getDetails(placeDetails, (function (place, status) {
                        var _this = this;
                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                            console.log(place);
                            place_formatted_number = place.international_phone_number;
                            place_formatted_address = place.formatted_address;
                            place_url = place.url;
                            place_website = place.place_website;
                            place_photos = place.photos;
                            //  console.log("Lat :"+lat +" Log :"+lng);
                            //push the places as object to the nearby array
                            var place_object_1 = {
                                latitude: lat,
                                longitude: lng,
                                place_name: selected_place.name,
                                place_vicinity: selected_place.vicinity,
                                place_url: photo,
                                place_status: state_restuarant,
                                place_distance: d,
                                current_location: current_location,
                                rating: place.rating,
                                place_website: place_website,
                                place_map_url: place_url,
                                place_address: place_formatted_address,
                                place_formatted_number: place_formatted_number,
                                place_reviews: place.reviews,
                                place_photos: place_photos
                            };
                            nearby_places.push(place_object_1);
                            //add a listener of click event for the location markers
                            google.maps.event.addListener(marker, 'mouseover', function () {
                                //   if (window != null) {
                                //     // window.close();
                                //   //  this.selected_place=null;
                                // } 
                                // window=infoWindow;
                                // this.selected_place=place_object;
                                _this.setSelectedPlace(place_object_1);
                                // infoWindow.open(map_id, marker);
                                // console.log( this.selected_place);
                            });
                            google.maps.event.addListener(marker, 'click', function () {
                                //   if (window != null) {
                                //     // window.close();
                                //     // selected_place_object=null;
                                // } 
                                // window=infoWindow;
                                _this.setSelectedPlace(place_object_1);
                                // infoWindow.open(map_id, marker);
                            });
                        }
                        else {
                            console.log("Unable to get place details " + selected_place);
                        }
                    }).bind(this_1));
                };
                var this_1 = this, place, placeDetails;
                for (var i = 0; i < results.length; i++) {
                    _loop_1();
                }
            }
        }).bind(this));
        console.log(nearby_places);
        //update the global places array.
        this.places = nearby_places;
        //stop the loading animation now
        this.location_handler.closeLoader();
        //end of forloop of places
        //sort the places in order of closeness
        // let sort_places: any = this.location_handler.calculateNearbyPlacesByApplyHaversine(nearby_places,this.current_location_object);
        // if (sort_places) {
        // }
    };
    /*
    @Author:Dieudonne Dengun
    @Date:03/0
    @Description:Update selected place variable
    */
    SchoolPage.prototype.setSelectedPlace = function (place) {
        if (place != null)
            this.connectivityService.setSelectedPlace(place);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], SchoolPage.prototype, "mapElement", void 0);
    SchoolPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-school',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\school\school.html"*/'\n\n  <ion-content>\n\n\n\n    <ion-segment [(ngModel)]="place_view_option" color="" (ionChange)="segmentChanged($event)" >\n\n  \n\n       <ion-segment-button value="map" class="segment">\n\n        <ion-icon name="map"></ion-icon>\n\n        Map View\n\n      </ion-segment-button> \n\n  \n\n      <!-- Contact Tab for the student contact information -->\n\n      <ion-segment-button value="list">\n\n        <ion-icon name="list"></ion-icon>\n\n        List View\n\n      </ion-segment-button>\n\n  \n\n    </ion-segment>\n\n   \n\n  \n\n   \n\n       <h3 class="segment-title" padding [style.display]="show_map ? \'block\' : \'none\'" > Schools and training centers around</h3>\n\n      <!-- Display Map here  -->\n\n      <!-- <p>Hello Dengun</p> -->\n\n      <div #map id="map" [style.display]="show_map ? \'block\' : \'none\'" ></div>\n\n    \n\n  <!-- Build a switch to check the three segment click -->\n\n  <div>\n\n    \n\n    <!-- This means, the student has clicked on the Map Tab Segment -->\n\n    <div  [style.display]="place_view_option == \'map\' ? \'block\' : \'none\'"  class="segment-item">\n\n     \n\n    </div>\n\n  \n\n    <!-- This means, the student has clicked on List View Tab Segment -->\n\n    <div [style.display]="place_view_option == \'list\' ? \'block\' : \'none\'"  class="segment-item">\n\n      <div class="segment-item">\n\n  \n\n        <h3 class="segment-title" padding>\n\n          Schools and training centers around - List View\n\n        </h3>\n\n  \n\n        <!-- Display a list of nearby services to the user -->\n\n       <ion-list *ngIf="places" >\n\n        \n\n          <ion-item *ngFor="let place of places; let j=index">\n\n              <ion-thumbnail item-start>\n\n                <img src="{{place.place_url}}">\n\n              </ion-thumbnail>\n\n              <h3 class="segment-title">{{place.place_name}}</h3>\n\n              <p text-wrap>About <b>{{place.place_distance}}</b> metres from your current location</p>\n\n            <small><i>{{place.place_vicinity}}</i></small>\n\n              <button ion-button clear item-end>\n\n                  <ion-icon name="pin"></ion-icon>\n\n              </button>\n\n            </ion-item>\n\n        </ion-list>\n\n  \n\n      </div>\n\n    </div>\n\n  </div>\n\n  \n\n   \n\n  </ion-content>\n\n  '/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\school\school.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_locations_locations__["a" /* LocationsProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_4__providers_connectivity_connectivity__["a" /* ConnectivityProvider */]])
    ], SchoolPage);
    return SchoolPage;
}());

//# sourceMappingURL=school.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupinfochannelPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the GroupinfochannelPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var GroupinfochannelPage = /** @class */ (function () {
    function GroupinfochannelPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    GroupinfochannelPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-groupinfochannel',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\groupinfochannel\groupinfochannel.html"*/'<!--\n\n  Generated template for the GroupinfochannelPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar color="hcolor">\n\n    <ion-title>Group Info</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n \n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\groupinfochannel\groupinfochannel.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], GroupinfochannelPage);
    return GroupinfochannelPage;
}());

//# sourceMappingURL=groupinfochannel.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NearbyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NearbyPage = /** @class */ (function () {
    function NearbyPage(navCtrl, navParams, geolocation, ngzone) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.geolocation = geolocation;
        this.ngzone = ngzone;
    }
    NearbyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-nearby',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\nearby\nearby.html"*/''/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\nearby\nearby.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */]])
    ], NearbyPage);
    return NearbyPage;
}());

/*  updateSearchResults(){
   if (this.autocomplete.input == '') {
     this.autocompleteItems = [];
     return;
   }
   this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
   (predictions, status) => {
     this.autocompleteItems = [];
     this.ngzone.run(() => {
       predictions.forEach((prediction) => {
         this.autocompleteItems.push(prediction);
       });
     });
   });
 }
 selectSearchResult(item){
   this.clearMarkers();
   this.autocompleteItems = [];
 
   this.geocoder.geocode({'placeId': item.place_id}, (results, status) => {
     if(status === 'OK' && results[0]){
       let position = {
           lat: results[0].geometry.location.lat,
           lng: results[0].geometry.location.lng
       };
       let marker = new google.maps.Marker({
         position: results[0].geometry.location,
         map: this.map,
       });
       this.markers.push(marker);
       this.map.setCenter(results[0].geometry.location);
     }
   })
 }
 
 tryGeolocation(){
   this.clearMarkers();
   this.geolocation.getCurrentPosition().then((resp) => {
     let pos = {
       lat: resp.coords.latitude,
       lng: resp.coords.longitude
     };
     let marker = new google.maps.Marker({
       position: pos,
       map: this.map,
       title: 'I am here!'
     });
     this.markers.push(marker);
     this.map.setCenter(pos);
   }).catch((error) => {
     console.log('Error getting location', error);
   });
 }

 clearMarkers():void {
   let marker = new google.maps.Marker({
     map: this.map,
     animation: google.maps.Animation.DROP,
     position: this.map.getCenter()
   });

   let content = "<h4>Information!</h4>";

  this.addInfoWindow(marker, content);

 }
 appointment()
 {
   this.navCtrl.push(BookingsPage)
 }
 

 ionViewDidLoad() {
  // this.tryGeolocation();
  this.map = new google.maps.Map(document.getElementById('map'), {
     center: { lat:-26.107567, lng: 28.056702},
     zoom: 15
   });
  // this.findUserLocation();
 }
 

}
*/ 
//# sourceMappingURL=nearby.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TestingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TestingPage = /** @class */ (function () {
    function TestingPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.buttonClicked = false;
    }
    TestingPage.prototype.onButtonClick = function () {
        this.buttonClicked = !this.buttonClicked;
    };
    TestingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-testing',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\testing\testing.html"*/'<!--\n\n  Generated template for the TestingPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>testing</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-item>\n\n      <button ion-button full round (click)="onButtonClick()">Click</button>\n\n\n\n  </ion-item>\n\n\n\n<ion-item>\n\n        <ion-input *ngIf="buttonClicked" type="text" placeholder="type in" color="gray"></ion-input>\n\n      <button ion-button *ngIf="buttonClicked" color="standard" > Block</button>\n\n      <button ion-button *ngIf="buttonClicked" color="yellowe" > Add</button>\n\n\n\n</ion-item>\n\n\n\n\n\n \n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\testing\testing.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], TestingPage);
    return TestingPage;
}());

//# sourceMappingURL=testing.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_take__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_take___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_take__);



Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupschannelProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the GroupsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var GroupschannelProvider = /** @class */ (function () {
    function GroupschannelProvider(evente, afAuth, afDB) {
        this.evente = evente;
        this.afAuth = afAuth;
        this.afDB = afDB;
        this.UserUid = window.localStorage.getItem('userid');
        this.Groups = [];
        this.AllFriends = [];
        this.groupMembers = [];
        this.messages = [];
        this.users = [];
    }
    GroupschannelProvider.prototype.initialize = function (groupDetails) {
        this.groupDetails = groupDetails;
    };
    GroupschannelProvider.prototype.uploadGroupPicture = function (picURL) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.storage().ref('Channel Picture').child(_this.UserUid).child(_this.uid() + '.jpg').putString(picURL, __WEBPACK_IMPORTED_MODULE_2_firebase___default.a.storage.StringFormat.DATA_URL).then(function (snap) {
                snap.ref.getDownloadURL().then(function (downloadURL) {
                    resolve(true);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GroupschannelProvider.prototype.uid = function () {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    };
    GroupschannelProvider.prototype.creaeGroup = function (groupDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('cccc').push({
                key: "ket"
            }).then(function (snap) {
                var key = snap.key;
                _this.afDB.database.ref('cccc').child(key).remove().then(function () {
                    _this.afDB.database.ref('Channels').child(_this.UserUid).child(key).set({
                        Owner: _this.UserUid,
                        Key: key,
                        Name: groupDetails.Name,
                        Picture: groupDetails.Picture
                    }).then(function () {
                        _this.afDB.database.ref('Channels').child(_this.UserUid).child(key).child('Members').push({
                            Id: _this.UserUid
                        }).then(function () {
                            resolve(true);
                        });
                    });
                }).catch(function (err) {
                    reject(err);
                });
            });
        });
        return promise;
    };
    GroupschannelProvider.prototype.getGroups = function () {
        var _this = this;
        this.afDB.database.ref('Channels').child(this.UserUid).on('value', function (snap) {
            _this.Groups = [];
            var res = snap.val();
            for (var i in res) {
                _this.Groups.push(res[i]);
            }
            _this.evente.publish('Channels');
        });
    };
    GroupschannelProvider.prototype.getAllFriends = function (groupDetails) {
        var _this = this;
        this.afDB.database.ref('Users').on('value', function (snap) {
            var res = snap.val();
            var userDetails = [];
            for (var i in res) {
                userDetails.push(res[i]);
            }
            _this.afDB.database.ref('Friends').child(_this.UserUid).on('value', function (snap) {
                var res = snap.val();
                var array = [];
                for (var i in res) {
                    array.push(res[i]);
                }
                _this.afDB.database.ref('Channels').child(groupDetails.Owner).child(groupDetails.Key).child('Members').on('value', function (snap) {
                    _this.AllFriends = [];
                    var res = snap.val();
                    var array1 = [];
                    for (var io in res) {
                        array1.push(res[io]);
                    }
                    for (var bb = array.length - 1; bb >= 0; bb--) {
                        for (var cc = 0; cc < array1.length; cc++) {
                            if (array[bb].Id === array1[cc].Id) {
                                array.splice(bb, 1);
                            }
                        }
                    }
                    for (var c in array) {
                        for (var d in userDetails) {
                            if (array[c].Id === userDetails[d].Id) {
                                _this.AllFriends.push(userDetails[d]);
                            }
                        }
                    }
                    _this.evente.publish('AllFriends');
                });
            });
        });
    };
    GroupschannelProvider.prototype.addMember = function (userDetails, groupDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Channels').child(groupDetails.Owner).child(groupDetails.Key).child('Members').push({
                Id: userDetails.Id
            }).then(function () {
                _this.afDB.database.ref('Channels').child(userDetails.Id).child(groupDetails.Key).set(groupDetails).then(function () {
                    _this.afDB.database.ref('Channels').child(groupDetails.Owner).child(groupDetails.Key).child('Members').once('value', function (snap) {
                        var res = snap.val();
                        for (var i in res) {
                            _this.afDB.database.ref('Channels').child(res[i].Id).child(groupDetails.Key).child('Members').set(res).then(function () {
                                _this.afDB.database.ref('Channels').child(groupDetails.Owner).child(groupDetails.Key).child('Chats').once('value', function (snap) {
                                    var res1 = snap.val();
                                    _this.afDB.database.ref('Channels').child(userDetails.Id).child(groupDetails.Key).child('Chats').set(res1).then(function () {
                                        resolve(true);
                                    }).catch(function (err) {
                                        reject(err);
                                    });
                                }).catch(function (err) {
                                    reject(err);
                                });
                            });
                        }
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            });
        });
        return promise;
    };
    GroupschannelProvider.prototype.deleteGroup = function (groupDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Channels').child(groupDetails.Owner).child(groupDetails.Key).child('Members').once('value', function (snap) {
                var res = snap.val();
                for (var i in res) {
                    _this.afDB.database.ref('Channels').child(res[i].Id).child(groupDetails.Key).remove().then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                }
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GroupschannelProvider.prototype.leaveGroup = function (groupDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Channels').child(groupDetails.Owner).child(groupDetails.Key).child('Members').once('value', function (snap) {
                var res = snap.val();
                _this.afDB.database.ref('Channels').child(groupDetails.Owner).child(groupDetails.Key).child('Members').orderByChild('Id').equalTo(_this.UserUid).once('value', function (snapshot) {
                    var res1 = snapshot.val();
                    var temp = Object.keys(res1);
                    for (var i in res) {
                        _this.afDB.database.ref('Channels').child(res[i].Id).child(groupDetails.Key).child('Members').child(temp[0]).remove().then(function () {
                            _this.afDB.database.ref('Channels').child(_this.UserUid).child(groupDetails.Key).remove().then(function () {
                                resolve(true);
                            }).catch(function (err) {
                                reject(err);
                            });
                        }).catch(function (err) {
                            reject(err);
                        });
                    }
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GroupschannelProvider.prototype.getGroupMembers = function (groupDetails) {
        var _this = this;
        this.afDB.database.ref('Users').on('value', function (snap) {
            _this.groupMembers = [];
            var res = snap.val();
            var userDetails = [];
            for (var i in res) {
                userDetails.push(res[i]);
            }
            _this.afDB.database.ref('Channels').child(groupDetails.Owner).child(groupDetails.Key).child('Members').on('value', function (snap) {
                _this.groupMembers = [];
                var res = snap.val();
                var array = [];
                for (var i in res) {
                    array.push(res[i]);
                }
                for (var ia in userDetails) {
                    for (var ii in array) {
                        if (userDetails[ia].Id === array[ii].Id) {
                            _this.groupMembers.push(userDetails[ia]);
                        }
                    }
                }
                _this.evente.publish('GroupMembers');
            });
        });
    };
    GroupschannelProvider.prototype.deleteMember = function (userDetails, groupDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Channels').child(groupDetails.Owner).child(groupDetails.Key).child('Members').once('value', function (snap) {
                var res = snap.val();
                _this.afDB.database.ref('Channels').child(groupDetails.Owner).child(groupDetails.Key).child('Members').orderByChild('Id').equalTo(userDetails.Id).once('value', function (snapshot) {
                    var res1 = snapshot.val();
                    var temp = Object.keys(res1);
                    for (var i in res) {
                        _this.afDB.database.ref('Channels').child(res[i].Id).child(groupDetails.Key).child('Members').child(temp[0]).remove().then(function () {
                            _this.afDB.database.ref('Channels').child(userDetails.Id).child(groupDetails.Key).remove().then(function () {
                                resolve(true);
                            }).catch(function (err) {
                                reject(err);
                            });
                        }).catch(function (err) {
                            reject(err);
                        });
                    }
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GroupschannelProvider.prototype.sendMessage = function (messageDetails, groupDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Channels').child(groupDetails.Owner).child(groupDetails.Key).child('Members').once('value', function (snap) {
                var res = snap.val();
                _this.afDB.database.ref('neoe').push({
                    Id: 'fneof'
                }).then(function (snapshot) {
                    var key = snapshot.key;
                    for (var i in res) {
                        _this.afDB.database.ref('Channels').child(res[i].Id).child(groupDetails.Key).child('Chats').child(key).set({
                            Body: messageDetails.body,
                            Id: _this.UserUid,
                            Time: new Date().toString(),
                            Key: key
                        }).then(function () {
                            _this.afDB.database.ref('neoe').child(key).remove().then(function () {
                                resolve(true);
                            }).catch(function (err) {
                                reject(err);
                            });
                        });
                    }
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GroupschannelProvider.prototype.getAllMessages = function () {
        var _this = this;
        var array1 = [];
        this.afDB.database.ref('Channels').child(this.UserUid).child(this.groupDetails.Key).child('Chats').on('value', function (snap) {
            _this.messages = [];
            array1 = [];
            var res = snap.val();
            for (var i in res) {
                _this.messages.push(res[i]);
                array1.push(res[i].Id);
            }
            _this.users = [];
            _this.afDB.database.ref('Users').once('value', function (snapshot) {
                var res = snapshot.val();
                var array = [];
                for (var a in res) {
                    array.push(res[a]);
                }
                for (var c in array1) {
                    for (var d in array) {
                        if (array1[c] === array[d].Id) {
                            _this.users.push(array[d]);
                        }
                    }
                }
                _this.evente.publish('Messages');
            });
        });
    };
    GroupschannelProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], GroupschannelProvider);
    return GroupschannelProvider;
}());

//# sourceMappingURL=groupschannel.js.map

/***/ }),

/***/ 565:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DirectivesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__swipe_vertical_swipe_vertical__ = __webpack_require__(566);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var DirectivesModule = /** @class */ (function () {
    function DirectivesModule() {
    }
    DirectivesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__swipe_vertical_swipe_vertical__["a" /* SwipeVerticalDirective */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__swipe_vertical_swipe_vertical__["a" /* SwipeVerticalDirective */]]
        })
    ], DirectivesModule);
    return DirectivesModule;
}());

//# sourceMappingURL=directives.module.js.map

/***/ }),

/***/ 566:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SwipeVerticalDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SwipeVerticalDirective = /** @class */ (function () {
    function SwipeVerticalDirective(el) {
        this.onSwipeUp = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.onSwipeDown = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.el = el.nativeElement;
    }
    SwipeVerticalDirective.prototype.ngOnInit = function () {
        var _this = this;
        this.swipeGesture = new __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Gesture */](this.el, {
            recognizers: [
                [Hammer.Swipe, { direction: Hammer.DIRECTION_VERTICAL }]
            ]
        });
        this.swipeGesture.listen();
        this.swipeGesture.on("swipeup", function () {
            _this.onSwipeUp.emit({ el: _this.el });
        });
        this.swipeGesture.on("swipedown", function () {
            _this.onSwipeDown.emit({ el: _this.el });
        });
    };
    SwipeVerticalDirective.prototype.ngOnDestroy = function () {
        this.swipeGesture.destroy();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], SwipeVerticalDirective.prototype, "onSwipeUp", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], SwipeVerticalDirective.prototype, "onSwipeDown", void 0);
    SwipeVerticalDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[swipe-vertical]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], SwipeVerticalDirective);
    return SwipeVerticalDirective;
}());

//# sourceMappingURL=swipe-vertical.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConnectivityProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__ = __webpack_require__(595);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ConnectivityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ConnectivityProvider = /** @class */ (function () {
    function ConnectivityProvider(platform, network) {
        this.platform = platform;
        this.network = network;
        this.selected_place = new __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__["BehaviorSubject"](null); // true is your initial value
        this.selectedPlaceObserve$ = this.selected_place.asObservable();
        this.onDevice = this.platform.is('cordova');
    }
    ConnectivityProvider.prototype.isOnline = function () {
        if (this.onDevice && this.network.type) {
            return this.network.type !== "none";
        }
        else {
            return navigator.onLine;
        }
    };
    ConnectivityProvider.prototype.isOffline = function () {
        if (this.onDevice && this.network.type) {
            return this.network.type === "none";
        }
        else {
            return !navigator.onLine;
        }
    };
    //update or set selected place
    ConnectivityProvider.prototype.setSelectedPlace = function (value) {
        this.selected_place.next(value);
        console.log('isFixed changed', value);
    };
    ConnectivityProvider.prototype.getSelectedPlace = function () {
        return this.selected_place.getValue();
    };
    ConnectivityProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
    ], ConnectivityProvider);
    return ConnectivityProvider;
}());

//# sourceMappingURL=connectivity.js.map

/***/ }),

/***/ 58:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__panic_panic__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__accountprofilecot_accountprofilecot__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__post_post__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_auth_auth__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__nearby_nearby__ = __webpack_require__(386);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { AboutPage } from '../about/about';







//import { UpdateProfilePage } from '../update-profile/update-profile';
//import { BuddychatPage } from '../buddychat/buddychat';
//import { ChatsPage } from '../chats/chats';
var TabsPage = /** @class */ (function () {
    function TabsPage(InAppBrowser, afAuth, authProvider, navCtrl) {
        this.InAppBrowser = InAppBrowser;
        this.afAuth = afAuth;
        this.authProvider = authProvider;
        this.navCtrl = navCtrl;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_4__accountprofilecot_accountprofilecot__["a" /* AccountProfilePagecotcot */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_8__nearby_nearby__["a" /* NearbyPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_3__panic_panic__["a" /* PanicPage */];
        this.userDetails = {
            Name: '',
            Email: '',
            Phone: '',
            Id: '',
            Status: '',
            proPhoto: '',
            bgPhoto: ''
        };
    }
    TabsPage.prototype.profile = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__post_post__["a" /* PostPage */], {
            userDetails: this.userDetails
        });
    };
    TabsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // this.superTabsCtrl.showToolbar(true)
        this.authProvider.getUserDetails().then(function (res) {
            _this.userDetails.Name = res.Name;
            _this.userDetails.proPhoto = res.Photo;
            _this.userDetails.Phone = res.Phone;
            _this.userDetails.Status = res.Status;
            _this.userDetails.Id = res.Id;
            _this.userDetails.bgPhoto = res.bgPhoto;
            _this.userDetails.Email = res.Email;
        }).catch(function (err) {
            console.log(err);
        });
    };
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tabs',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\tabs\tabs.html"*/'<ion-tabs color="col" >\n\n  <ion-tab [root]="tab1Root" tabTitle="Posts"  tabIcon="customicon6" ></ion-tab>\n\n  <ion-tab [root]="tab2Root" tabTitle="Nearby"  tabIcon="customicon7"></ion-tab>\n\n  <ion-tab [root]="tab3Root" tabTitle="Profile"  tabIcon="customicon8" (ionSelect)="profile()"></ion-tab>\n\n  <ion-tab [root]="tab4Root" tabTitle="Panic Button"  tabIcon="customicon9"></ion-tab>\n\n</ion-tabs>\n\n\n\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\tabs\tabs.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__["a" /* AngularFireAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__["a" /* AngularFireAuth */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_7__providers_auth_auth__["a" /* AuthProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__providers_auth_auth__["a" /* AuthProvider */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */]) === "function" && _d || Object])
    ], TabsPage);
    return TabsPage;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 584:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_menugp_menugp__ = __webpack_require__(219);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    //rootPage ;
    function MyApp(platform, statusBar, authProvider, splashScreen) {
        var _this = this;
        this.authProvider = authProvider;
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            splashScreen.hide();
            _this.checkUserState();
            //this.authProvider.onlineStatus()
            //this.authProvider.offlineStatus()
            //console.log(window.localStorage.getItem('userid'))
        });
    }
    MyApp.prototype.checkUserState = function () {
        var userstate = window.localStorage.getItem('userstate');
        if (userstate == 'logedIn') {
            this.authProvider.onlineStatus();
            this.authProvider.offlineStatus();
            this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_menugp_menugp__["a" /* MenugpPage */];
        }
        else {
            this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        }
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 591:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PreferencePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { AboutPage } from '../about/about';

/**
 * Generated class for the PreferencePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PreferencePage = /** @class */ (function () {
    function PreferencePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PreferencePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PreferencePage');
    };
    PreferencePage.prototype.account = function () {
        // this.navCtrl.push(AboutPage);
    };
    PreferencePage.prototype.back = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    PreferencePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-preference',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\preference\preference.html"*/'\n\n<ion-header>\n\n\n\n  <ion-navbar color="purple">\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Your Profile</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n\n\n<ion-content>\n\n  \n\n<!--Profile Picture -->\n\n<div class="spacer" style="height: 10px;"></div>\n\n  <div class="profile-image" (click)="editimage()">\n\n    <p (click)="editnamel()"  style="text-align: right; color: blue; text-align: center; margin-right: 50px; margin-bottom: -12px; margin-top: -158px; font-weight:lighter;  font-size: 18px;"> {{(profileData | async)?.lastName}}</p>\n\n  <div style=" margin-bottom: -10px; margin-top: -35px; vertical-align: text-top; text-align: 40px;margin-left: 25px;">\n\n  <img src="{{avatar}}" >\n\n  </div>\n\n   </div>\n\n\n\n<!--Username-->\n\n<div style=" margin-bottom: -10px; margin-top: -100px;">\n\n<div  >\n\n  <button ion-button round outline color="purple"  style="width: 240px; height:10vw;"  (click)="editnamel()" >\n\n    <ion-item >\n\n      <ion-input placeholder="Username" class="text" style=" font-size: 12px; text-align: left;"></ion-input>\n\n    </ion-item>\n\n  \n\n  </button>\n\n  </div>\n\n\n\n<!--Email Address-->\n\n<div>\n\n  <button ion-button round outline color="purple"  style="width: 240px; height:10vw;" class="center" (click)="editnamen()"  >\n\n    <ion-item >\n\n      <ion-input placeholder="Email Address" class="text" style=" font-size: 12px; text-align: left;" ></ion-input>\n\n    </ion-item>\n\n  </button>\n\n</div>\n\n<!---Change Password-->\n\n<div>\n\n  <button ion-button round outline color="purple"  style="width: 240px; height:10vw; text-align: left;" class="center" (click)="editnameid()">\n\n    <ion-item >\n\n      <ion-input placeholder="Change Password" class="text" style=" font-size: 12px;" ></ion-input>\n\n    </ion-item>  \n\n  </button>\n\n</div>\n\n\n\n<!--Update Profile-->\n\n<div class="update">\n\n  <button ion-button round color="standard" class="update" (click)="profile()" >Update</button>\n\n  </div>\n\n  <div class="spacer" style="height: 3vw;"></div>\n\n  \n\n  <!--Profile Settings-->\n\n\n\n<div class="color"><button ion-button round  style="height:10vw; width:200vw; text-transform: none; " center color="very"  (click)="updated()">Account settings</button></div>  \n\n \n\n  \n\n</div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\preference\preference.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], PreferencePage);
    return PreferencePage;
}());

//# sourceMappingURL=preference.js.map

/***/ }),

/***/ 597:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoogleMapsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__connectivity_connectivity__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GoogleMapsProvider = /** @class */ (function () {
    function GoogleMapsProvider(connectivityService, geolocation) {
        this.connectivityService = connectivityService;
        this.geolocation = geolocation;
        this.mapInitialised = false;
        this.markers = [];
        this.apiKey = "AIzaSyBkK4dKGUIh5qkHR6FacFJTKbLR_nyEftk";
    }
    GoogleMapsProvider.prototype.init = function (mapElement, pleaseConnect) {
        this.mapElement = mapElement;
        this.pleaseConnect = pleaseConnect;
        return this.loadGoogleMaps();
    };
    /*
     @Description: set up and load google map on the device with API details
    */
    GoogleMapsProvider.prototype.loadGoogleMaps = function () {
        var _this = this;
        return new Promise(function (resolve) {
            if (typeof google == "undefined" || typeof google.maps == "undefined") {
                console.log("Google maps JavaScript needs to be loaded.");
                _this.disableMap();
                if (_this.connectivityService.isOnline()) {
                    window['mapInit'] = function () {
                        _this.initMap().then(function () {
                            resolve(true);
                        });
                        _this.enableMap();
                    };
                    var script = document.createElement("script");
                    script.id = "mapView";
                    if (_this.apiKey) {
                        script.src = 'https://maps.googleapis.com/maps/api/js?key=' + _this.apiKey + '&libraries=places&callback=mapInit';
                    }
                    else {
                        script.src = 'https://maps.googleapis.com/maps/api/js?callback=mapInit';
                    }
                    document.body.appendChild(script);
                }
            }
            else {
                if (_this.connectivityService.isOnline()) {
                    _this.initMap();
                    _this.enableMap();
                }
                else {
                    _this.disableMap();
                }
            }
            _this.addConnectivityListeners();
        });
    };
    /*
     @Description:Initial Google Maps once its set up has completed to display on screen
    */
    GoogleMapsProvider.prototype.initMap = function () {
        var _this = this;
        this.mapInitialised = true;
        return new Promise(function (resolve) {
            _this.geolocation.getCurrentPosition().then(function (position) {
                // UNCOMMENT FOR NORMAL USE
                var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                // let latLng = new google.maps.LatLng(40.713744, -74.009056);
                var mapOptions = {
                    center: latLng,
                    zoom: 15,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                _this.map = new google.maps.Map(_this.mapElement, mapOptions);
                resolve(true);
            });
        });
    };
    /*
     @Description:Disable the load map process when there is no internet by hiding the map
    */
    GoogleMapsProvider.prototype.disableMap = function () {
        if (this.pleaseConnect) {
            this.pleaseConnect.style.display = "block";
        }
    };
    /*
     @Description:Enable the load map process when there is  internet by showing the map
    */
    GoogleMapsProvider.prototype.enableMap = function () {
        if (this.pleaseConnect) {
            this.pleaseConnect.style.display = "none";
        }
    };
    /*
    @Author:Dieudonne Dengun
    @Date: 09/04/2018
    @Description: constantly listen for when the user comes back online, and when they do it will trigger the whole loading process
  
  */
    GoogleMapsProvider.prototype.addConnectivityListeners = function () {
        var _this = this;
        document.addEventListener('online', function () {
            console.log("online");
            setTimeout(function () {
                if (typeof google == "undefined" || typeof google.maps == "undefined") {
                    _this.loadGoogleMaps();
                }
                else {
                    if (!_this.mapInitialised) {
                        _this.initMap();
                    }
                    _this.enableMap();
                }
            }, 2000);
        }, false);
        document.addEventListener('offline', function () {
            console.log("offline");
            _this.disableMap();
        }, false);
    };
    /*
     @Author:Dieudonne Dengun
     @Date: 10/04/2018
     @Description: Get nearby activities to  a user's current location
     @Param: @place types
  
  
    */
    GoogleMapsProvider.prototype.getNearbyActivities = function (place_type, user_current_location) {
        var request = {
            location: user_current_location,
            radius: '500',
            type: [place_type]
        };
        this.place_service = new google.maps.places.PlacesService(this.map);
        this.place_service.nearbySearch(request, this.handleResultsCallback);
    };
    GoogleMapsProvider.prototype.handleResultsCallback = function (results, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
                var place = results[i];
                this.addMarker(place);
            }
        }
    };
    /*
      @Author:Dieudonne Dengun
      @Date: 09/04/2018
      @Description: add google marker for a search place on Google Map
    
    */
    GoogleMapsProvider.prototype.addMarker = function (place) {
        var marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: place.geometry.location
        });
        this.markers.push(marker);
    };
    GoogleMapsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__connectivity_connectivity__["a" /* ConnectivityProvider */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */]])
    ], GoogleMapsProvider);
    return GoogleMapsProvider;
}());

//# sourceMappingURL=google-maps.js.map

/***/ }),

/***/ 599:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ImageModalPage = /** @class */ (function () {
    function ImageModalPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ImageModalPage.prototype.ionViewDidLoad = function () {
        this.image = this.navParams.get('img');
    };
    ImageModalPage.prototype.close = function () {
        this.navCtrl.pop();
    };
    ImageModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-image-modal',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\image-modal\image-modal.html"*/'<ion-content>\n  <div class="content" (click)="close()" tappable>\n    <img src={{image}}/>\n  </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\image-modal\image-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], ImageModalPage);
    return ImageModalPage;
}());

//# sourceMappingURL=image-modal.js.map

/***/ }),

/***/ 68:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_call_number__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_block_block__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__editprofile_editprofile__ = __webpack_require__(181);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { PhotoViewer } from '@ionic-native/photo-viewer';


//import { SuperTabsController } from 'ionic2-super-tabs';



var ProfilePage = /** @class */ (function () {
    //photoViewer: any;
    function ProfilePage(ngZone, events, camera, callNumber, actionSheetCtrl, loadCtrl, platform, blockProvider, navCtrl, authProvider, navParams) {
        var _this = this;
        this.ngZone = ngZone;
        this.events = events;
        this.camera = camera;
        this.callNumber = callNumber;
        this.actionSheetCtrl = actionSheetCtrl;
        this.loadCtrl = loadCtrl;
        this.platform = platform;
        this.blockProvider = blockProvider;
        this.navCtrl = navCtrl;
        this.authProvider = authProvider;
        this.navParams = navParams;
        this.userDetails = {
            Name: '',
            Email: '',
            Phone: '',
            Id: '',
            Status: '',
            proPhoto: '',
            bgPhoto: ''
        };
        this.details = {
            Name: '',
            Email: '',
            Phone: '',
            Id: '',
            Status: '',
            proPhoto: '',
            about: '',
            bgPhoto: ''
        };
        this.onlineStatus = 'Online';
        this.offlineStatus = 'Offline';
        this.myProfile = false;
        this.userDetails = this.navParams.get('userDetails');
        this.events.subscribe('ProfileDetails', function () {
            _this.ngZone.run(function () {
                _this.details = _this.authProvider.ProfileDetails;
                if (_this.details.Id == _this.authProvider.UserUid) {
                    _this.myProfile = true;
                }
                else {
                    _this.myProfile = false;
                }
            });
        });
    }
    ProfilePage.prototype.showToast = function (message) {
        this.platform.ready().then(function () {
            window.plugins.toast.show(message, "short", 'bottom');
        });
    };
    ProfilePage.prototype.ionViewDidLeave = function () {
        this.events.subscribe('ProfileDetails');
    };
    ProfilePage.prototype.ionViewDidEnter = function () {
        this.authProvider.getProfileDetails(this.userDetails);
    };
    ProfilePage.prototype.bgPic = function () {
        var _this = this;
        if (this.details.Id == this.authProvider.UserUid) {
            var actionSheet = this.actionSheetCtrl.create({
                title: this.details.Name,
                buttons: [
                    {
                        text: 'View Cover',
                        icon: 'person',
                        role: 'destructive',
                        handler: function () {
                            _this.viewProfilePicture(_this.details.bgPhoto, _this.details.Name);
                        }
                    }, {
                        text: 'Select Cover',
                        icon: 'albums',
                        handler: function () {
                            _this.selectCover();
                        }
                    }, {
                        text: 'Cancel',
                        icon: 'close',
                        role: 'cancel',
                        handler: function () {
                            _this.showToast('Cancel');
                        }
                    }
                ]
            });
            actionSheet.present();
        }
        else {
            this.viewProfilePicture(this.details.bgPhoto, this.details.Name);
        }
    };
    ProfilePage.prototype.proPic = function () {
        var _this = this;
        if (this.details.Id == this.authProvider.UserUid) {
            var actionSheet = this.actionSheetCtrl.create({
                title: this.details.Name,
                buttons: [
                    {
                        text: 'View Profile Picture',
                        icon: 'person',
                        role: 'destructive',
                        handler: function () {
                            _this.viewProfilePicture(_this.details.proPhoto, _this.details.Name);
                        }
                    }, {
                        text: 'Select Profile Picture',
                        icon: 'albums',
                        handler: function () {
                            _this.selectProfilePicture();
                        }
                    }, {
                        text: 'Cancel',
                        icon: 'close',
                        role: 'cancel',
                        handler: function () {
                            _this.showToast('Cancel');
                        }
                    }
                ]
            });
            actionSheet.present();
        }
        else {
            this.viewProfilePicture(this.details.proPhoto, this.details.Name);
        }
    };
    ProfilePage.prototype.viewProfilePicture = function (photo, name) {
        var options = {
            share: true,
            closeButton: true,
            copyToReference: false // default is false
        };
        //this.photoViewer.show(photo, name, options);    
    };
    ProfilePage.prototype.selectProfilePicture = function () {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Uploading Picture ...'
        });
        var cameraOptions = {
            quality: 50,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            encodingType: this.camera.EncodingType.PNG,
            destinationType: this.camera.DestinationType.DATA_URL
        };
        this.camera.getPicture(cameraOptions).then(function (ImageData) {
            _this.captureDataUrl = 'data:image/jpeg;base64,' + ImageData;
            load.present();
            _this.authProvider.uploadProfilePhoto(_this.captureDataUrl).then(function () {
                _this.details.proPhoto = _this.captureDataUrl;
                load.dismiss();
                _this.showToast('Profile Picture has been updated');
            }).catch(function (err) {
                load.dismiss();
                _this.showToast(err);
            });
        }, function (err) {
            var error = JSON.stringify(err);
            _this.showToast(error);
        });
    };
    ProfilePage.prototype.selectCover = function () {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Uploading Cover ...'
        });
        var cameraOptions = {
            quality: 50,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            encodingType: this.camera.EncodingType.PNG,
            destinationType: this.camera.DestinationType.DATA_URL
        };
        this.camera.getPicture(cameraOptions).then(function (ImageData) {
            _this.captureDataUrll = 'data:image/jpeg;base64,' + ImageData;
            load.present();
            _this.authProvider.uploadCover(_this.captureDataUrll).then(function () {
                _this.details.bgPhoto = _this.captureDataUrll;
                load.dismiss();
                _this.showToast('Cover has been updated');
            }).catch(function (err) {
                load.dismiss();
                _this.showToast(err);
            });
        }, function (err) {
            var error = JSON.stringify(err);
            _this.showToast(error);
        });
    };
    ProfilePage.prototype.editProfile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__editprofile_editprofile__["a" /* EditprofilePage */], {
            userDetails: this.details
        });
    };
    ProfilePage.prototype.callPhoneNumber = function (Phone) {
        var _this = this;
        this.callNumber.callNumber(Phone, true)
            .then(function (res) { return _this.showToast('Launched dialer! ' + res); })
            .catch(function (err) { return _this.showToast('Error launching dialer ' + err); });
    };
    ProfilePage.prototype.block = function () {
        var _this = this;
        var load = this.loadCtrl.create({
            content: 'Blocking ' + this.details.Name + ' ...'
        });
        load.present();
        this.blockProvider.blockUser(this.details).then(function () {
            load.dismiss();
            _this.navCtrl.pop();
            _this.showToast(_this.details.Name + ' has been blocked');
        }).catch(function (err) {
            _this.showToast(err);
            load.dismiss();
        });
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\profile\profile.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{details.Name}}</ion-title>\n    <div class="online" *ngIf="details.Status == onlineStatus"></div>\n    <div class="offline" *ngIf="details.Status == offlineStatus"></div>\n    <span class="Status">{{details.Status}}</span>\n    <ion-buttons end *ngIf="myProfile">\n    	<button ion-button ion-icon icon-only icon-left (click)="editProfile()">\n    		<ion-icon name="settings"></ion-icon>\n    	</button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n\n	<div class="bg-image">\n		<img src="{{details.bgPhoto}}" width="100%" height="100%" (click)="bgPic()">\n	</div>\n	<div class="image">\n		<img src="{{details.Photo}}" (click)="proPic()">\n	</div>\n	<div class="followe">\n		<ul>\n			<li>\n				<span id="number">450</span>\n				<span id="followe">Followers</span>\n			</li>\n			<li>\n				<span id="number">200</span>\n				<span id="followe">Following</span>\n			</li>\n		</ul>\n	</div>\n\n	<div class="about">\n		<h3 class="title">About</h3>\n		<p class="body">{{details.about}}</p>\n	</div>\n	<div class="Phone">\n		<h3 class="title">Mobile Number</h3>\n		<p class="mob">{{details.Phone}}</p>\n		<button color="header" class="call" icon-left ion-icon ion-button (click)="callPhoneNumber(details.Phone)">\n			<ion-icon name="call"></ion-icon>\n		</button>\n	</div>\n	<div class="group" ion-item (click)="createGroup()"  *ngIf="!myProfile">\n		<span class="icons">\n			<ion-icon color="header" name="add"></ion-icon>\n			<ion-icon color="header" name="contacts"></ion-icon>\n		</span>\n		Create group with {{details.Name}}\n	</div>\n\n\n\n	<div class="block" ion-item (click)="block()" *ngIf="!myProfile">\n		<span class="icon"><ion-icon color="header" name="trash"></ion-icon></span>\n		Block\n	</div>\n\n\n\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_call_number__["a" /* CallNumber */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Platform */], __WEBPACK_IMPORTED_MODULE_5__providers_block_block__["a" /* BlockProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GroupProvider = /** @class */ (function () {
    function GroupProvider(afAuth, afDB, evente) {
        this.afAuth = afAuth;
        this.afDB = afDB;
        this.evente = evente;
        this.UserUid = window.localStorage.getItem('userid');
        this.Groups = [];
        this.AllFriends = [];
        this.groupMembers = [];
        this.messages = [];
        this.users = [];
    }
    GroupProvider.prototype.initialize = function (groupDetails) {
        this.groupDetails = groupDetails;
    };
    GroupProvider.prototype.uploadGroupPicture = function (picURL) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.storage().ref('Group Picture').child(_this.UserUid).child(_this.uid() + '.jpg').putString(picURL, __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.storage.StringFormat.DATA_URL).then(function (snap) {
                snap.ref.getDownloadURL().then(function (downloadURL) {
                    resolve(true);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GroupProvider.prototype.uid = function () {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    };
    GroupProvider.prototype.creaeGroup = function (groupDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('hhhh').push({
                key: "ket"
            }).then(function (snap) {
                var key = snap.key;
                _this.afDB.database.ref('hhhh').child(key).remove().then(function () {
                    _this.afDB.database.ref('Groups').child(_this.UserUid).child(key).set({
                        Owner: _this.UserUid,
                        Key: key,
                        Name: groupDetails.Name,
                        Picture: groupDetails.Picture
                    }).then(function () {
                        _this.afDB.database.ref('Groups').child(_this.UserUid).child(key).child('Members').push({
                            Id: _this.UserUid
                        }).then(function () {
                            resolve(true);
                        });
                    });
                }).catch(function (err) {
                    reject(err);
                });
            });
        });
        return promise;
    };
    GroupProvider.prototype.getGroups = function () {
        var _this = this;
        this.afDB.database.ref('Groups').child(this.UserUid).on('value', function (snap) {
            _this.Groups = [];
            var res = snap.val();
            for (var i in res) {
                _this.Groups.push(res[i]);
            }
            _this.evente.publish('Groups');
        });
    };
    GroupProvider.prototype.getAllFriends = function (groupDetails) {
        var _this = this;
        this.afDB.database.ref('Users').on('value', function (snap) {
            var res = snap.val();
            var userDetails = [];
            for (var i in res) {
                userDetails.push(res[i]);
            }
            _this.afDB.database.ref('Friends').child(_this.UserUid).on('value', function (snap) {
                var res = snap.val();
                var array = [];
                for (var i in res) {
                    array.push(res[i]);
                }
                _this.afDB.database.ref('Groups').child(groupDetails.Owner).child(groupDetails.Key).child('Members').on('value', function (snap) {
                    _this.AllFriends = [];
                    var res = snap.val();
                    var array1 = [];
                    for (var io in res) {
                        array1.push(res[io]);
                    }
                    for (var bb = array.length - 1; bb >= 0; bb--) {
                        for (var cc = 0; cc < array1.length; cc++) {
                            if (array[bb].Id === array1[cc].Id) {
                                array.splice(bb, 1);
                            }
                        }
                    }
                    for (var c in array) {
                        for (var d in userDetails) {
                            if (array[c].Id === userDetails[d].Id) {
                                _this.AllFriends.push(userDetails[d]);
                            }
                        }
                    }
                    _this.evente.publish('AllFriends');
                });
            });
        });
    };
    GroupProvider.prototype.addMember = function (userDetails, groupDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Groups').child(groupDetails.Owner).child(groupDetails.Key).child('Members').push({
                Id: userDetails.Id
            }).then(function () {
                _this.afDB.database.ref('Groups').child(userDetails.Id).child(groupDetails.Key).set(groupDetails).then(function () {
                    _this.afDB.database.ref('Groups').child(groupDetails.Owner).child(groupDetails.Key).child('Members').once('value', function (snap) {
                        var res = snap.val();
                        for (var i in res) {
                            _this.afDB.database.ref('Groups').child(res[i].Id).child(groupDetails.Key).child('Members').set(res).then(function () {
                                _this.afDB.database.ref('Groups').child(groupDetails.Owner).child(groupDetails.Key).child('Chats').once('value', function (snap) {
                                    var res1 = snap.val();
                                    _this.afDB.database.ref('Groups').child(userDetails.Id).child(groupDetails.Key).child('Chats').set(res1).then(function () {
                                        resolve(true);
                                    }).catch(function (err) {
                                        reject(err);
                                    });
                                }).catch(function (err) {
                                    reject(err);
                                });
                            });
                        }
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            });
        });
        return promise;
    };
    GroupProvider.prototype.deleteGroup = function (groupDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Groups').child(groupDetails.Owner).child(groupDetails.Key).child('Members').once('value', function (snap) {
                var res = snap.val();
                for (var i in res) {
                    _this.afDB.database.ref('Groups').child(res[i].Id).child(groupDetails.Key).remove().then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                }
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GroupProvider.prototype.leaveGroup = function (groupDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Groups').child(groupDetails.Owner).child(groupDetails.Key).child('Members').once('value', function (snap) {
                var res = snap.val();
                _this.afDB.database.ref('Groups').child(groupDetails.Owner).child(groupDetails.Key).child('Members').orderByChild('Id').equalTo(_this.UserUid).once('value', function (snapshot) {
                    var res1 = snapshot.val();
                    var temp = Object.keys(res1);
                    for (var i in res) {
                        _this.afDB.database.ref('Groups').child(res[i].Id).child(groupDetails.Key).child('Members').child(temp[0]).remove().then(function () {
                            _this.afDB.database.ref('Groups').child(_this.UserUid).child(groupDetails.Key).remove().then(function () {
                                resolve(true);
                            }).catch(function (err) {
                                reject(err);
                            });
                        }).catch(function (err) {
                            reject(err);
                        });
                    }
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GroupProvider.prototype.getGroupMembers = function (groupDetails) {
        var _this = this;
        this.afDB.database.ref('Users').on('value', function (snap) {
            _this.groupMembers = [];
            var res = snap.val();
            var userDetails = [];
            for (var i in res) {
                userDetails.push(res[i]);
            }
            _this.afDB.database.ref('Groups').child(groupDetails.Owner).child(groupDetails.Key).child('Members').on('value', function (snap) {
                _this.groupMembers = [];
                var res = snap.val();
                var array = [];
                for (var i in res) {
                    array.push(res[i]);
                }
                for (var ia in userDetails) {
                    for (var ii in array) {
                        if (userDetails[ia].Id === array[ii].Id) {
                            _this.groupMembers.push(userDetails[ia]);
                        }
                    }
                }
                _this.evente.publish('GroupMembers');
            });
        });
    };
    GroupProvider.prototype.deleteMember = function (userDetails, groupDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Groups').child(groupDetails.Owner).child(groupDetails.Key).child('Members').once('value', function (snap) {
                var res = snap.val();
                _this.afDB.database.ref('Groups').child(groupDetails.Owner).child(groupDetails.Key).child('Members').orderByChild('Id').equalTo(userDetails.Id).once('value', function (snapshot) {
                    var res1 = snapshot.val();
                    var temp = Object.keys(res1);
                    for (var i in res) {
                        _this.afDB.database.ref('Groups').child(res[i].Id).child(groupDetails.Key).child('Members').child(temp[0]).remove().then(function () {
                            _this.afDB.database.ref('Groups').child(userDetails.Id).child(groupDetails.Key).remove().then(function () {
                                resolve(true);
                            }).catch(function (err) {
                                reject(err);
                            });
                        }).catch(function (err) {
                            reject(err);
                        });
                    }
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GroupProvider.prototype.sendMessage = function (messageDetails, groupDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Groups').child(groupDetails.Owner).child(groupDetails.Key).child('Members').once('value', function (snap) {
                var res = snap.val();
                _this.afDB.database.ref('dsad').push({
                    Id: 'fasdf'
                }).then(function (snapshot) {
                    var key = snapshot.key;
                    for (var i in res) {
                        _this.afDB.database.ref('Groups').child(res[i].Id).child(groupDetails.Key).child('Chats').child(key).set({
                            Body: messageDetails.body,
                            Id: _this.UserUid,
                            Time: __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.database.ServerValue.TIMESTAMP,
                            Key: key
                        }).then(function () {
                            _this.afDB.database.ref('dsad').child(key).remove().then(function () {
                                resolve(true);
                            }).catch(function (err) {
                                reject(err);
                            });
                        });
                    }
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    GroupProvider.prototype.getAllMessages = function () {
        var _this = this;
        var array1 = [];
        this.afDB.database.ref('Groups').child(this.UserUid).child(this.groupDetails.Key).child('Chats').on('value', function (snap) {
            _this.messages = [];
            array1 = [];
            var res = snap.val();
            for (var i in res) {
                _this.messages.push(res[i]);
                array1.push(res[i].Id);
            }
            _this.users = [];
            _this.afDB.database.ref('Users').once('value', function (snapshot) {
                var res = snapshot.val();
                var array = [];
                for (var a in res) {
                    array.push(res[a]);
                }
                for (var c in array1) {
                    for (var d in array) {
                        if (array1[c] === array[d].Id) {
                            _this.users.push(array[d]);
                        }
                    }
                }
                _this.evente.publish('Messages');
            });
        });
    };
    GroupProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], GroupProvider);
    return GroupProvider;
}());

//# sourceMappingURL=group.js.map

/***/ }),

/***/ 86:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoadingProvider = /** @class */ (function () {
    function LoadingProvider(loadingController) {
        this.loadingController = loadingController;
        // Loading Provider
        // This is the provider class for most of the loading spinners screens on the app.
        // Set your spinner/loading indicator type here
        // List of Spinners: https://ionicframework.com/docs/v2/api/components/spinner/Spinner/
        this.spinner = {
            spinner: 'circles'
        };
    }
    //Show loading
    LoadingProvider.prototype.show = function () {
        if (!this.loading) {
            this.loading = this.loadingController.create(this.spinner);
            this.loading.present();
        }
    };
    //Hide loading
    LoadingProvider.prototype.hide = function () {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    };
    LoadingProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */]])
    ], LoadingProvider);
    return LoadingProvider;
}());

//# sourceMappingURL=loading.js.map

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_toast__ = __webpack_require__(307);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import {LogoutProvider} from "./logout";

//import { Validators } from "@angular/forms";
var errorMessages = {
    // Alert Provider
    // This is the provider class for most of the success and error messages in the app.
    // If you added your own messages don't forget to make a function for them or add them in the showErrorMessage switch block.
    // Firebase Error Messages
    accountExistsWithDifferentCredential: {
        title: "Account Exists!",
        subTitle: "An account with the same credential already exists."
    },
    invalidCredential: {
        title: "Invalid Credential!",
        subTitle: "An error occured logging in with this credential."
    },
    operationNotAllowed: {
        title: "Login Failed!",
        subTitle: "Logging in with this provider is not allowed! Please contact support."
    },
    userDisabled: {
        title: "Account Disabled!",
        subTitle: "Sorry! But this account has been suspended! Please contact support."
    },
    userNotFound: {
        title: "Account Not Found!",
        subTitle: "Sorry, but an account with this credential could not be found."
    },
    wrongPassword: {
        title: "Incorrect Password!",
        subTitle: "Sorry, but the password you have entered is incorrect."
    },
    invalidEmail: {
        title: "Invalid Email!",
        subTitle: "Sorry, but you have entered an invalid email address."
    },
    emailAlreadyInUse: {
        title: "Email Not Available!",
        subTitle: "Sorry, but this email is already in use."
    },
    weakPassword: {
        title: "Weak Password!",
        subTitle: "Sorry, but you have entered a weak password."
    },
    requiresRecentLogin: {
        title: "Credential Expired!",
        subTitle: "Sorry, but this credential has expired! Please login again."
    },
    userMismatch: {
        title: "User Mismatch!",
        subTitle: "Sorry, but this credential is for another user!"
    },
    providerAlreadyLinked: {
        title: "Already Linked!",
        subTitle: "Sorry, but your account is already linked to this credential."
    },
    credentialAlreadyInUse: {
        title: "Credential Not Available!",
        subTitle: "Sorry, but this credential is already used by another user."
    },
    // Profile Error Messages
    changeName: {
        title: "Change Name Failed!",
        subTitle: "Sorry, but we've encountered an error changing your name."
    },
    updateProfile: {
        title: "Update Profile Failed",
        subTitle: "Sorry, but we've encountered an error updating your profile."
    },
    usernameExists: {
        title: "Username Already Exists!",
        subTitle: "Sorry, but this username is already taken by another user."
    },
    phoneNumberExists: {
        title: "Phone Number Already Exists!",
        subTitle: "Sorry, but this phone number is already taken by another user."
    },
    // Image Error Messages
    imageUpload: {
        title: "Image Upload Failed!",
        subTitle: "Sorry but we've encountered an error uploading selected image."
    },
    // Group Error Messages
    groupUpdate: {
        title: "Update Group Failed!",
        subTitle: "Sorry, but we've encountered an error updating this group."
    },
    groupLeave: {
        title: "Leave Group Failed!",
        subTitle: "Sorry, but you've encountered an error leaving this group."
    },
    groupDelete: {
        title: "Delete Group Failed!",
        subTitle: "Sorry, but we've encountered an error deleting this group."
    }
};
var successMessages = {
    passwordResetSent: {
        title: "Password Reset Sent!",
        subTitle: "A password reset email has been sent to: "
    },
    profileUpdated: {
        title: "Profile Updated!",
        subTitle: "Your profile has been successfully updated!"
    },
    phoneNumberUpdated: {
        title: "Phone Number Updated!",
        subTitle: "Your phone number has been successfully updated!"
    },
    emailVerified: {
        title: "Email Confirmed!",
        subTitle: "Congratulations! Your email has been confirmed!"
    },
    emailVerificationSent: {
        title: "Email Confirmation Sent!",
        subTitle: "An email confirmation has been sent to: "
    },
    accountDeleted: {
        title: "Account Deleted!",
        subTitle: "Your account has been successfully deleted."
    },
    passwordChanged: {
        title: "Password Changed!",
        subTitle: "Your password has been successfully changed."
    },
    friendRequestSent: {
        title: "Friend Request Sent!",
        subTitle: "Your friend request has been successfully sent!"
    },
    friendRequestRemoved: {
        title: "Friend Request Deleted!",
        subTitle: "Your friend request has been successfully deleted."
    },
    groupUpdated: {
        title: "Group Updated!",
        subTitle: "This group has been successfully updated!"
    },
    groupLeft: {
        title: "Leave Group",
        subTitle: "You have successfully left this group."
    }
};
var AlertProvider = /** @class */ (function () {
    function AlertProvider(alertCtrl, toast) {
        this.alertCtrl = alertCtrl;
        this.toast = toast;
    }
    // Show profile updated
    AlertProvider.prototype.showProfileUpdatedMessage = function () {
        this.alert = this.alertCtrl
            .create({
            title: successMessages.profileUpdated["title"],
            subTitle: successMessages.profileUpdated["subTitle"],
            buttons: ["OK"]
        })
            .present();
    };
    AlertProvider.prototype.showPhoneNumberUpdatedMessage = function () {
        this.alert = this.alertCtrl
            .create({
            title: successMessages.phoneNumberUpdated["title"],
            subTitle: successMessages.phoneNumberUpdated["subTitle"],
            buttons: ["OK"]
        })
            .present();
    };
    // Show password reset sent
    AlertProvider.prototype.showPasswordResetMessage = function (email) {
        this.alert = this.alertCtrl
            .create({
            title: successMessages.passwordResetSent["title"],
            subTitle: successMessages.passwordResetSent["subTitle"] + email,
            buttons: ["OK"]
        })
            .present();
    };
    // Show email verified and redirect to homePage
    AlertProvider.prototype.showEmailVerifiedMessageAndRedirect = function (navCtrl) {
        this.alert = this.alertCtrl
            .create({
            title: successMessages.emailVerified["title"],
            subTitle: successMessages.emailVerified["subTitle"],
            buttons: [
                {
                    text: "OK",
                    handler: function () {
                        //navCtrl.setRoot(Login.homePage);
                    }
                }
            ]
        })
            .present();
    };
    // Show email verification sent
    AlertProvider.prototype.showEmailVerificationSentMessage = function (email) {
        this.alert = this.alertCtrl
            .create({
            title: successMessages.emailVerificationSent["title"],
            subTitle: successMessages.emailVerificationSent["subTitle"] + email,
            buttons: ["OK"]
        })
            .present();
    };
    // Show account deleted
    AlertProvider.prototype.showAccountDeletedMessage = function () {
        this.alert = this.alertCtrl
            .create({
            title: successMessages.accountDeleted["title"],
            subTitle: successMessages.accountDeleted["subTitle"],
            buttons: ["OK"]
        })
            .present();
    };
    // Show password changed
    AlertProvider.prototype.showPasswordChangedMessage = function () {
        this.alert = this.alertCtrl
            .create({
            title: successMessages.passwordChanged["title"],
            subTitle: successMessages.passwordChanged["subTitle"],
            buttons: ["OK"]
        })
            .present();
    };
    // show alert
    AlertProvider.prototype.showAlert = function (title, subTitle) {
        this.alert = this.alertCtrl
            .create({
            title: title,
            subTitle: subTitle,
            buttons: ["OK"]
        })
            .present();
    };
    // Show friend request sent
    AlertProvider.prototype.showFriendRequestSent = function () {
        this.alert = this.alertCtrl
            .create({
            title: successMessages.friendRequestSent["title"],
            subTitle: successMessages.friendRequestSent["subTitle"],
            buttons: ["OK"]
        })
            .present();
    };
    // Show friend request removed
    AlertProvider.prototype.showFriendRequestRemoved = function () {
        this.alert = this.alertCtrl
            .create({
            title: successMessages.friendRequestRemoved["title"],
            subTitle: successMessages.friendRequestRemoved["subTitle"],
            buttons: ["OK"]
        })
            .present();
    };
    // Show group updated.
    AlertProvider.prototype.showGroupUpdatedMessage = function () {
        this.alert = this.alertCtrl
            .create({
            title: successMessages.groupUpdated["title"],
            subTitle: successMessages.groupUpdated["subTitle"],
            buttons: ["OK"]
        })
            .present();
    };
    // Show error messages depending on the code
    // If you added custom error codes on top, make sure to add a case block for it.
    /* showErrorMessage(code) {
      switch (code) {
        // Firebase Error Messages
        case "auth/account-exists-with-different-credential":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.accountExistsWithDifferentCredential["title"],
              subTitle:
                errorMessages.accountExistsWithDifferentCredential["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "auth/invalid-credential":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.invalidCredential["title"],
              subTitle: errorMessages.invalidCredential["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "auth/operation-not-allowed":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.operationNotAllowed["title"],
              subTitle: errorMessages.operationNotAllowed["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "auth/user-disabled":
          this.alert = this.alertCtrl.create({
            title: errorMessages.userDisabled["title"],
            subTitle: errorMessages.userDisabled["subTitle"],
            buttons: ["OK"]
          });
          this.alert.present();
          break;
        case "auth/user-not-found":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.userNotFound["title"],
              subTitle: errorMessages.userNotFound["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "auth/wrong-password":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.wrongPassword["title"],
              subTitle: errorMessages.wrongPassword["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "auth/invalid-email":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.invalidEmail["title"],
              subTitle: errorMessages.invalidEmail["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "auth/email-already-in-use":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.emailAlreadyInUse["title"],
              subTitle: errorMessages.emailAlreadyInUse["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "auth/weak-password":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.weakPassword["title"],
              subTitle: errorMessages.weakPassword["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "auth/requires-recent-login":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.requiresRecentLogin["title"],
              subTitle: errorMessages.requiresRecentLogin["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "auth/user-mismatch":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.userMismatch["title"],
              subTitle: errorMessages.userMismatch["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "auth/provider-already-linked":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.providerAlreadyLinked["title"],
              subTitle: errorMessages.providerAlreadyLinked["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "auth/credential-already-in-use":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.credentialAlreadyInUse["title"],
              subTitle: errorMessages.credentialAlreadyInUse["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        // Profile Error Messages
        case "profile/error-change-name":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.changeName["title"],
              subTitle: errorMessages.changeName["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "profile/invalid-chars-name":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.invalidCharsName["title"],
              subTitle: errorMessages.invalidCharsName["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "profile/name-too-short":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.nameTooShort["title"],
              subTitle: errorMessages.nameTooShort["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "profile/error-change-email":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.changeEmail["title"],
              subTitle: errorMessages.changeEmail["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "profile/invalid-email":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.invalidProfileEmail["title"],
              subTitle: errorMessages.invalidProfileEmail["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "profile/error-change-photo":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.changePhoto["title"],
              subTitle: errorMessages.changePhoto["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "profile/password-too-short":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.passwordTooShort["title"],
              subTitle: errorMessages.passwordTooShort["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "profile/invalid-chars-password":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.invalidCharsPassword["title"],
              subTitle: errorMessages.invalidCharsPassword["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "profile/passwords-do-not-match":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.passwordsDoNotMatch["title"],
              subTitle: errorMessages.passwordsDoNotMatch["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "profile/error-update-profile":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.updateProfile["title"],
              subTitle: errorMessages.updateProfile["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "profile/error-same-username":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.usernameExists["title"],
              subTitle: errorMessages.usernameExists["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "profile/error-same-phoneNumber":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.phoneNumberExists["title"],
              subTitle: errorMessages.phoneNumberExists["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        //Image Error Messages
        case "image/error-image-upload":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.imageUpload["title"],
              subTitle: errorMessages.imageUpload["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        // Group Error MEssages
        case "group/error-update-group":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.groupUpdate["title"],
              subTitle: errorMessages.groupUpdate["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "group/error-leave-group":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.groupLeave["title"],
              subTitle: errorMessages.groupLeave["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
        case "group/error-delete-group":
          this.alert = this.alertCtrl
            .create({
              title: errorMessages.groupDelete["title"],
              subTitle: errorMessages.groupDelete["subTitle"],
              buttons: ["OK"]
            })
            .present();
          break;
      }
    } */
    AlertProvider.prototype.showToast = function (msg) {
        this.toast.show(msg, "5000", "bottom").subscribe(function (toast) { });
    };
    AlertProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_toast__["a" /* Toast */]])
    ], AlertProvider);
    return AlertProvider;
}());

//# sourceMappingURL=alert.js.map

/***/ }),

/***/ 88:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import {Contacts} from "@ionic-native/contacts";


var DataProvider = /** @class */ (function () {
    function DataProvider(angularDb, 
        //private contacts: Contacts,
        storage) {
        this.angularDb = angularDb;
        this.storage = storage;
        this.userContactsList = [];
        this.userOnlyContacts = [];
        this.exitsUserList = [];
        this.inviteUserList = [];
        this.userContactsListWithCountryCode = [];
        this.isContactGet = false;
        this.countryCode = "+1";
    }
    // set webRTCClient
    DataProvider.prototype.setWebRTCClient = function (val) {
        this.webRTCClient = val;
    };
    // get webRTCClient
    DataProvider.prototype.getwebRTCClient = function () {
        return this.webRTCClient;
    };
    // set Incoming Call id
    DataProvider.prototype.setIncomingCallId = function (id) {
        this.incomingCallId = id;
    };
    // get incoming call id
    DataProvider.prototype.getIncomingCallid = function () {
        return this.incomingCallId;
    };
    // Get all users
    DataProvider.prototype.getUsers = function () {
        return this.angularDb.list("Users", {
            query: {
                orderByChild: "Name"
            }
        });
    };
    // Get user with username
    DataProvider.prototype.getUserWithUsername = function (Email) {
        return this.angularDb.list("Users", {
            query: {
                orderByChild: "Email",
                equalTo: Email
            }
        });
    };
    // Get user with phonenumber
    /*  getUserWithPhonenumber(Phone) {
       return this.angularDb.list('Users', {
         query: {
           orderByChild: "Phone",
           equalTo: Phone
         }
       });
     } */
    // Get logged in user data
    DataProvider.prototype.getCurrentUser = function () {
        return this.angularDb.object("Users" + __WEBPACK_IMPORTED_MODULE_2_firebase__["auth"]().currentUser.uid);
    };
    // Get user by their userId
    DataProvider.prototype.getUser = function (userId) {
        return this.angularDb.object("Users" + userId);
    };
    // Get requests given the userId.
    DataProvider.prototype.getRequests = function (userId) {
        return this.angularDb.object("/requests/" + userId);
    };
    // Get friend requests given the userId.
    DataProvider.prototype.getFriendRequests = function (userId) {
        return this.angularDb.list("/requests", {
            query: {
                orderByChild: "receiver",
                equalTo: userId
            }
        });
    };
    // Get conversation given the conversationId.
    DataProvider.prototype.getConversation = function (conversationId) {
        return this.angularDb.object("/conversations/" + conversationId);
    };
    // Get conversations of the current logged in user.
    DataProvider.prototype.getConversations = function () {
        return this.angularDb.list("/accounts/" + __WEBPACK_IMPORTED_MODULE_2_firebase__["auth"]().currentUser.uid + "/conversations");
    };
    // Get messages of the conversation given the Id.
    DataProvider.prototype.getConversationMessages = function (conversationId) {
        return this.angularDb.object("/conversations/" + conversationId + "/messages");
    };
    // Get messages of the group given the Id.
    DataProvider.prototype.getGroupMessages = function (groupId) {
        return this.angularDb.object("/groups/" + groupId + "/messages");
    };
    // Get groups of the logged in user.
    DataProvider.prototype.getGroups = function () {
        return this.angularDb.list("/accounts/" + __WEBPACK_IMPORTED_MODULE_2_firebase__["auth"]().currentUser.uid + "/groups");
    };
    // Get group info given the groupId.
    DataProvider.prototype.getGroup = function (groupId) {
        return this.angularDb.object("/groups/" + groupId);
    };
    // Get Timeline of user
    DataProvider.prototype.getTimelines = function () {
        return this.angularDb.list("Users" + __WEBPACK_IMPORTED_MODULE_2_firebase__["auth"]().currentUser.uid + "/timeline");
    };
    // Get Timeline by user id
    DataProvider.prototype.getTimelineByUid = function (id) {
        return this.angularDb.object("Users" + id + "/timeline");
    };
    // Get Timeline post
    DataProvider.prototype.getTimelinePost = function () {
        return this.angularDb.list("/timeline");
    };
    DataProvider.prototype.getAllReportedPost = function () {
        return this.angularDb.list("/reportPost");
    };
    // Get time line by id
    DataProvider.prototype.getTimeline = function (timelineId) {
        return this.angularDb.object("/timeline/" + timelineId);
    };
    // Get Friend List
    DataProvider.prototype.getFriends = function () {
        return this.angularDb.list("Users" + __WEBPACK_IMPORTED_MODULE_2_firebase__["auth"]().currentUser.uid + 'Friends');
    };
    // Get comments list
    DataProvider.prototype.getComments = function (postId) {
        return this.angularDb.list("/comments/" + postId);
    };
    // Get likes
    DataProvider.prototype.getLike = function (postId) {
        return this.angularDb.list("/likes/" + postId);
    };
    DataProvider.prototype.postLike = function (postId) {
        return this.angularDb.object("/likes/" + postId);
    };
    // Get likes
    DataProvider.prototype.getdisLike = function (postId) {
        return this.angularDb.list("/dislikes/" + postId);
    };
    DataProvider.prototype.postdisLike = function (postId) {
        return this.angularDb.object("/dislikes/" + postId);
    };
    // post Comments
    DataProvider.prototype.postComments = function (postId) {
        return this.angularDb.object("/comments/" + postId);
    };
    // report post to admin
    DataProvider.prototype.getReportPost = function (postId) {
        console.log("postId", postId);
        return this.angularDb.object("/reportPost/" + postId);
    };
    DataProvider.prototype.setData = function (key, val) {
        this.storage.set(key, val);
    };
    DataProvider.prototype.getData = function (key) {
        return this.storage.get(key).then(function (val) {
            return val;
        });
    };
    DataProvider.prototype.clearData = function () {
        this.storage.clear();
    };
    DataProvider.prototype.removePost = function (post) {
        var _this = this;
        this.getUser(post.postBy).take(1).subscribe(function (account) {
            console.log("before timeline", timeline);
            var timeline = account.timeline;
            __WEBPACK_IMPORTED_MODULE_4_lodash___default.a.remove(timeline, function (n) {
                return n == post.$key;
            });
            console.log("after timeline", timeline);
            // Add both users as friends.
            _this.getUser(post.postBy).update({
                timeline: timeline
            }).then(function (success) {
                /**
                 * Remove post from time line
                //  */
                _this.getTimeline(post.$key).remove().then(function (success) {
                    _this.angularDb.object('/reportPost/' + post.$key).remove();
                }).catch(function (error) {
                });
            });
        });
    };
    DataProvider.prototype.ignorePost = function (post) {
        console.log("ingnore post ", post);
        this.angularDb.object('/reportPost/' + post.$key).remove();
    };
    DataProvider.prototype.unFriend = function (userId) {
        var _this = this;
        /**
         * Remove friend id from friend account
         */
        this.getUser(userId).take(1).subscribe(function (account) {
            var friends = account.friends;
            console.log("==friend List before", friends);
            if (friends) {
                __WEBPACK_IMPORTED_MODULE_4_lodash___default.a.remove(friends, function (n) {
                    return n == __WEBPACK_IMPORTED_MODULE_2_firebase__["auth"]().currentUser.uid;
                });
                _this.getUser(userId).update({
                    friends: friends
                }).then(function (success) {
                });
            }
            console.log("==friend List after", friends);
        });
        /**
         * Remove friend id from login user account
         */
        this.getUser(__WEBPACK_IMPORTED_MODULE_2_firebase__["auth"]().currentUser.uid).take(1).subscribe(function (account) {
            var friends = account.friends;
            console.log("==user List before", friends);
            if (friends) {
                __WEBPACK_IMPORTED_MODULE_4_lodash___default.a.remove(friends, function (n) {
                    return n == userId;
                });
                _this.getUser(__WEBPACK_IMPORTED_MODULE_2_firebase__["auth"]().currentUser.uid).update({
                    friends: friends
                }).then(function (success) {
                });
            }
            console.log("==user List after", friends);
        });
    };
    DataProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__["a" /* AngularFireDatabase */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__["a" /* AngularFireDatabase */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]) === "function" && _b || Object])
    ], DataProvider);
    return DataProvider;
    var _a, _b;
}());

//# sourceMappingURL=data.js.map

/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FriendsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FriendsProvider = /** @class */ (function () {
    function FriendsProvider(afAuth, afDB, evente) {
        this.afAuth = afAuth;
        this.afDB = afDB;
        this.evente = evente;
        this.UserUid = window.localStorage.getItem('userid');
        this.Friends = [];
    }
    FriendsProvider.prototype.getAllFriends = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            var Details = [];
            _this.afDB.database.ref('Users').once('value', function (snap) {
                var res = snap.val();
                var userDetails = [];
                for (var i in res) {
                    userDetails.push(res[i]);
                }
                _this.afDB.database.ref('Friends').child(_this.UserUid).once('value', function (snap) {
                    var res = snap.val();
                    var array = [];
                    for (var i in res) {
                        array.push(res[i]);
                    }
                    for (var ia in userDetails) {
                        for (var ii in array) {
                            if (userDetails[ia].Id === array[ii].Id) {
                                Details.push(userDetails[ia]);
                            }
                        }
                    }
                    resolve(Details);
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    FriendsProvider.prototype.getFriends = function () {
        var _this = this;
        this.afDB.database.ref('Users').on('value', function (snap) {
            var res = snap.val();
            var userDetails = [];
            for (var i in res) {
                userDetails.push(res[i]);
            }
            _this.afDB.database.ref('Friends').child(_this.UserUid).on('value', function (snap) {
                _this.Friends = [];
                var res = snap.val();
                var array = [];
                for (var i in res) {
                    array.push(res[i]);
                }
                for (var ia in userDetails) {
                    for (var ii in array) {
                        if (userDetails[ia].Id === array[ii].Id) {
                            _this.Friends.push(userDetails[ia]);
                        }
                    }
                }
                _this.evente.publish('Friends');
            });
        });
    };
    FriendsProvider.prototype.unFriend = function (userDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Friends').child(_this.UserUid).orderByChild('Id').equalTo(userDetails.Id).once('value', function (snap) {
                var res = snap.val();
                var tempstore = Object.keys(res);
                _this.afDB.database.ref('Friends').child(_this.UserUid).child(tempstore[0]).remove().then(function () {
                    _this.afDB.database.ref('Friends').child(userDetails.Id).orderByChild('Id').equalTo(_this.UserUid).once('value', function (snap) {
                        var res = snap.val();
                        var tempstore = Object.keys(res);
                        _this.afDB.database.ref('Friends').child(userDetails.Id).child(tempstore[0]).remove().then(function () {
                            resolve(true);
                        }).catch(function (err) {
                            reject(err);
                        });
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    FriendsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], FriendsProvider);
    return FriendsProvider;
}());

//# sourceMappingURL=friends.js.map

/***/ }),

/***/ 90:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__notification_notification__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_auth__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RequestProvider = /** @class */ (function () {
    function RequestProvider(afAuth, afDB, authProvider, notificationProvider) {
        this.afAuth = afAuth;
        this.afDB = afDB;
        this.authProvider = authProvider;
        this.notificationProvider = notificationProvider;
        this.UserUid = window.localStorage.getItem('userid');
    }
    RequestProvider.prototype.getSentRequests = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            var Details = [];
            _this.afDB.database.ref('Users').once('value', function (snap) {
                var res = snap.val();
                var userDetails = [];
                for (var i in res) {
                    userDetails.push(res[i]);
                }
                _this.afDB.database.ref('Requests').child(_this.UserUid).child('Sent Requests').once('value', function (snap) {
                    var res = snap.val();
                    var sentArray = [];
                    for (var i in res) {
                        sentArray.push(res[i]);
                    }
                    for (var ia in userDetails) {
                        for (var ii in sentArray) {
                            if (userDetails[ia].Id === sentArray[ii].Id) {
                                Details.push(userDetails[ia]);
                            }
                        }
                    }
                    resolve(Details);
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    RequestProvider.prototype.getReceivedRequests = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            var Details = [];
            _this.afDB.database.ref('Users').once('value', function (snap) {
                var res = snap.val();
                var userDetails = [];
                for (var i in res) {
                    userDetails.push(res[i]);
                }
                _this.afDB.database.ref('Requests').child(_this.UserUid).child('Received Requests').once('value', function (snap) {
                    var res = snap.val();
                    var receivedArray = [];
                    for (var i in res) {
                        receivedArray.push(res[i]);
                    }
                    for (var ia in userDetails) {
                        for (var ii in receivedArray) {
                            if (userDetails[ia].Id === receivedArray[ii].Id) {
                                Details.push(userDetails[ia]);
                            }
                        }
                    }
                    resolve(Details);
                    resolve();
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    RequestProvider.prototype.makeRequest = function (userDetails) {
        var _this = this;
        var notificationDetails = {
            Type: 'Request'
        };
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Requests').child(_this.UserUid).child('Sent Requests').push({
                Id: userDetails.Id
            }).then(function () {
                _this.afDB.database.ref('Requests').child(userDetails.Id).child('Received Requests').push({
                    Id: _this.UserUid
                }).then(function () {
                    _this.notificationProvider.pushNotification(userDetails, notificationDetails).then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            });
        });
        return promise;
    };
    RequestProvider.prototype.deleteSentRequest = function (userDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Requests').child(_this.UserUid).child('Sent Requests').orderByChild('Id').equalTo(userDetails.Id).once('value', function (snap) {
                var res = snap.val();
                var tempstore = Object.keys(res);
                _this.afDB.database.ref('Requests').child(_this.UserUid).child('Sent Requests').child(tempstore[0]).remove().then(function () {
                    _this.afDB.database.ref('Requests').child(userDetails.Id).child('Received Requests').orderByChild('Id').equalTo(_this.UserUid).once('value', function (snap) {
                        var res = snap.val();
                        var tempstore = Object.keys(res);
                        _this.afDB.database.ref('Requests').child(userDetails.Id).child('Received Requests').child(tempstore[0]).remove().then(function () {
                            resolve(true);
                        }).catch(function (err) {
                            reject(err);
                        });
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            });
        });
        return promise;
    };
    RequestProvider.prototype.deleteReceivedRequest = function (userDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Requests').child(_this.UserUid).child('Received Requests').orderByChild('Id').equalTo(userDetails.Id).once('value', function (snap) {
                var res = snap.val();
                var tempstore = Object.keys(res);
                _this.afDB.database.ref('Requests').child(_this.UserUid).child('Received Requests').child(tempstore[0]).remove().then(function () {
                    _this.afDB.database.ref('Requests').child(userDetails.Id).child('Sent Requests').orderByChild('Id').equalTo(_this.UserUid).once('value', function (snap) {
                        var res = snap.val();
                        var tempstore = Object.keys(res);
                        _this.afDB.database.ref('Requests').child(userDetails.Id).child('Sent Requests').child(tempstore[0]).remove().then(function () {
                            resolve(true);
                        }).catch(function (err) {
                            reject(err);
                        });
                    });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    RequestProvider.prototype.acceptRquest = function (userDetails) {
        var _this = this;
        var notificationDetails = {
            Type: 'Friend'
        };
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Friends').child(userDetails.Id).push({
                Id: _this.UserUid
            }).then(function () {
                _this.afDB.database.ref('Friends').child(_this.UserUid).push({
                    Id: userDetails.Id
                }).then(function () {
                    _this.deleteReceivedRequest(userDetails).then(function () {
                        _this.notificationProvider.pushNotification(userDetails, notificationDetails).then(function () {
                            resolve(true);
                        }).catch(function (err) {
                            reject(err);
                        });
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            });
        });
        return promise;
    };
    RequestProvider.prototype.blockSentRequest = function (userDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Block List').child(userDetails.Id).push({
                Id: _this.UserUid
            }).then(function () {
                _this.afDB.database.ref('Block List').child(_this.UserUid).push({
                    Id: userDetails.Id
                }).then(function () {
                    _this.deleteSentRequest(userDetails).then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            });
        });
        return promise;
    };
    RequestProvider.prototype.blockReceivedRequest = function (userDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Block List').child(userDetails.Id).push({
                Id: _this.UserUid
            }).then(function () {
                _this.afDB.database.ref('Block List').child(_this.UserUid).push({
                    Id: userDetails.Id
                }).then(function () {
                    _this.deleteReceivedRequest(userDetails).then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            });
        });
        return promise;
    };
    RequestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_4__auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_3__notification_notification__["a" /* NotificationProvider */]])
    ], RequestProvider);
    return RequestProvider;
}());

//# sourceMappingURL=request.js.map

/***/ }),

/***/ 91:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NotificationProvider = /** @class */ (function () {
    function NotificationProvider(afAuth, afDB, evente) {
        this.afAuth = afAuth;
        this.afDB = afDB;
        this.evente = evente;
        this.UserUid = window.localStorage.getItem('userid');
        this.NotificationsUnread = [];
        this.buddyUsersUnread = [];
        this.NotificationsRead = [];
        this.buddyUsersRead = [];
    }
    NotificationProvider.prototype.getMyNotificationsUnread = function () {
        var _this = this;
        this.afDB.database.ref('Notifications').child(this.UserUid).child('Un Read').on('value', function (snap) {
            _this.NotificationsUnread = [];
            var res = snap.val();
            var array1 = [];
            for (var i in res) {
                _this.NotificationsUnread.push(res[i]);
                array1.push(res[i].Id);
            }
            _this.afDB.database.ref('Users').on('value', function (snap) {
                _this.buddyUsersUnread = [];
                var res = snap.val();
                var array = [];
                for (var i in res) {
                    array.push(res[i]);
                }
                for (var d in array1) {
                    for (var c in array) {
                        if (array[c].Id === array1[d]) {
                            _this.buddyUsersUnread.push(array[c]);
                        }
                    }
                }
                _this.evente.publish('Notifications');
            });
        });
    };
    NotificationProvider.prototype.getMyNotificationsRead = function () {
        var _this = this;
        this.afDB.database.ref('Notifications').child(this.UserUid).child('Read').on('value', function (snap) {
            _this.NotificationsRead = [];
            var res = snap.val();
            var array1 = [];
            for (var i in res) {
                _this.NotificationsRead.push(res[i]);
                array1.push(res[i].Id);
            }
            _this.afDB.database.ref('Users').on('value', function (snap) {
                _this.buddyUsersRead = [];
                var res = snap.val();
                var array = [];
                for (var i in res) {
                    array.push(res[i]);
                }
                for (var d in array1) {
                    for (var c in array) {
                        if (array[c].Id === array1[d]) {
                            _this.buddyUsersRead.push(array[c]);
                        }
                    }
                }
                _this.evente.publish('Notifications');
            });
        });
    };
    NotificationProvider.prototype.pushNotification = function (userDetails, notificationDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('dsa').push({
                Id: 'fdf'
            }).then(function (snap) {
                var key = snap.key;
                _this.afDB.database.ref('Notifications').child(userDetails.Id).child('Un Read').child(key).set({
                    Type: notificationDetails.Type,
                    Id: _this.UserUid,
                    Key: key
                }).then(function () {
                    _this.afDB.database.ref('dsa').child(key).remove().then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            });
        });
        return promise;
    };
    NotificationProvider.prototype.makeNotificationAsRead = function (notificationDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Notifications').child(_this.UserUid).child('Read').child(notificationDetails.Key).set(notificationDetails).then(function () {
                _this.afDB.database.ref('Notifications').child(_this.UserUid).child('Un Read').child(notificationDetails.Key).remove().then(function () {
                    resolve(true);
                }).catch(function (err) {
                    reject(err);
                });
            });
        });
        return promise;
    };
    NotificationProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], NotificationProvider);
    return NotificationProvider;
}());

//# sourceMappingURL=notification.js.map

/***/ }),

/***/ 92:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ChatProvider = /** @class */ (function () {
    function ChatProvider(afAuth, afDB, evente) {
        this.afAuth = afAuth;
        this.afDB = afDB;
        this.evente = evente;
        this.UserUid = window.localStorage.getItem('userid');
        this.allMessages = [];
        this.Conversations = [];
        this.buddyUsers = [];
    }
    ChatProvider.prototype.initialize = function (userDetails) {
        this.userDetails = userDetails;
    };
    ChatProvider.prototype.getMessages = function (userDetails) {
        var _this = this;
        this.afDB.database.ref('Chat').child(this.UserUid).child(userDetails.Id).on('value', function (snap) {
            _this.allMessages = [];
            var res = snap.val();
            for (var i in res) {
                _this.allMessages.push(res[i]);
            }
            _this.evente.publish('messages');
        });
    };
    ChatProvider.prototype.sendMessage = function (messageDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Chat').child(_this.UserUid).child(_this.userDetails.Id).push({
                Body: messageDetails.body,
                Id: _this.UserUid,
                Time: new Date().toString()
            }).then(function (snap) {
                var key = snap.key;
                _this.afDB.database.ref('Chat').child(_this.userDetails.Id).child(_this.UserUid).child(key).set({
                    Body: messageDetails.body,
                    Id: _this.UserUid,
                    //Time: new Date().toString()
                    Time: new Date().toString(),
                    Key: key
                }).then(function () {
                    _this.afDB.database.ref('Chat').child(_this.UserUid).child(_this.userDetails.Id).child(key).update({
                        Key: key
                    }).then(function () {
                        _this.afDB.database.ref('Conversations').child(_this.UserUid).orderByChild('Id').equalTo(_this.userDetails.Id).once('value', function (snapshot) {
                            var res = snapshot.val();
                            if (res != null) {
                                var store = Object.keys(res);
                                _this.afDB.database.ref('Conversations').child(_this.UserUid).child(store[0]).remove().then(function () {
                                    _this.afDB.database.ref('Conversations').child(_this.UserUid).push({
                                        Id: _this.userDetails.Id,
                                        Body: messageDetails.body,
                                        Time: new Date().toString()
                                    }).then(function () {
                                        _this.afDB.database.ref('Conversations').child(_this.userDetails.Id).orderByChild('Id').equalTo(_this.UserUid).once('value', function (snapshot) {
                                            var res = snapshot.val();
                                            if (res != null) {
                                                var store_1 = Object.keys(res);
                                                _this.afDB.database.ref('Conversations').child(_this.userDetails.Id).child(store_1[0]).remove().then(function () {
                                                    _this.afDB.database.ref('Conversations').child(_this.userDetails.Id).push({
                                                        Id: _this.UserUid,
                                                        Body: messageDetails.body,
                                                        Time: new Date().toString()
                                                    }).then(function () {
                                                        resolve(true);
                                                    });
                                                }).catch(function (err) {
                                                    reject(err);
                                                });
                                            }
                                            else {
                                                _this.afDB.database.ref('Conversations').child(_this.userDetails.Id).push({
                                                    Id: _this.UserUid,
                                                    Body: messageDetails.body,
                                                    Time: new Date().toString()
                                                }).then(function () {
                                                    resolve(true);
                                                });
                                            }
                                        }).catch(function (err) {
                                            reject(err);
                                        });
                                    });
                                }).catch(function (err) {
                                    reject(err);
                                });
                            }
                            else {
                                _this.afDB.database.ref('Conversations').child(_this.UserUid).push({
                                    Id: _this.userDetails.Id,
                                    Body: messageDetails.body,
                                    Time: new Date().toString()
                                }).then(function () {
                                    _this.afDB.database.ref('Conversations').child(_this.userDetails.Id).orderByChild('Id').equalTo(_this.userDetails.Id).once('value', function (snapshot) {
                                        var res = snapshot.val();
                                        if (res != null) {
                                            var store_2 = Object.keys(res);
                                            _this.afDB.database.ref('Conversations').child(_this.userDetails.Id).child(store_2[0]).remove().then(function () {
                                                _this.afDB.database.ref('Conversations').child(_this.userDetails.Id).push({
                                                    Id: _this.UserUid,
                                                    Body: messageDetails.body,
                                                    Time: new Date().toString()
                                                }).then(function () {
                                                    resolve(true);
                                                });
                                            }).catch(function (err) {
                                                reject(err);
                                            });
                                        }
                                        else {
                                            _this.afDB.database.ref('Conversations').child(_this.userDetails.Id).push({
                                                Id: _this.UserUid,
                                                Body: messageDetails.body,
                                                Time: new Date().toString()
                                            }).then(function () {
                                                resolve(true);
                                            });
                                        }
                                    }).catch(function (err) {
                                        reject(err);
                                    });
                                });
                            }
                        }).catch(function (err) {
                            reject(err);
                        });
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            });
        });
        return promise;
    };
    ChatProvider.prototype.getConversations = function () {
        var _this = this;
        this.afDB.database.ref('Conversations').child(this.UserUid).on('value', function (snap) {
            _this.Conversations = [];
            var res = snap.val();
            var array1 = [];
            for (var i in res) {
                _this.Conversations.push(res[i]);
                _this.Conversations;
                array1.push(res[i].Id);
            }
            _this.afDB.database.ref('Users').on('value', function (snap) {
                _this.buddyUsers = [];
                var res = snap.val();
                var array = [];
                for (var i in res) {
                    array.push(res[i]);
                }
                array1.reverse();
                for (var d in array1) {
                    for (var c in array) {
                        if (array[c].Id === array1[d]) {
                            _this.buddyUsers.push(array[c]);
                            _this.buddyUsers;
                        }
                    }
                }
                _this.evente.publish('Conversations');
            });
        });
    };
    ChatProvider.prototype.deleteMessageForMe = function (message, myDetails, friendDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Chat').child(myDetails.Id).child(friendDetails.Id).child(message.Key).remove().then(function () {
                _this.afDB.database.ref('Chat').child(myDetails.Id).child(friendDetails.Id).limitToLast(1).once('value', function (snap) {
                    var res = snap.val();
                    _this.afDB.database.ref('Conversations').child(myDetails.Id).orderByChild('Id').equalTo(friendDetails.Id).once('value', function (snapshot) {
                        var res1 = snapshot.val();
                        var temp = Object.keys(res1);
                        for (var i in res) {
                            _this.afDB.database.ref('Conversations').child(myDetails.Id).child(temp[0]).update({
                                Body: res[i].Body,
                                Time: res[i].Time
                            }).then(function () {
                                resolve(true);
                            });
                        }
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    ChatProvider.prototype.deleteMessageForAll = function (message, myDetails, friendDetails) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.afDB.database.ref('Chat').child(myDetails.Id).child(friendDetails.Id).child(message.Key).remove().then(function () {
                _this.afDB.database.ref('Chat').child(friendDetails.Id).child(myDetails.Id).child(message.Key).remove().then(function () {
                    resolve(true);
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    ChatProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], ChatProvider);
    return ChatProvider;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_geolocation__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(593);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_location_accuracy__ = __webpack_require__(377);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the LocationsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var LocationsProvider = /** @class */ (function () {
    function LocationsProvider(http, geolocation, actionSheetCtrl, location, alert, toast, loader) {
        this.http = http;
        this.geolocation = geolocation;
        this.actionSheetCtrl = actionSheetCtrl;
        this.location = location;
        this.alert = alert;
        this.toast = toast;
        this.loader = loader;
    }
    /*
      General Utility functions to be
   */
    /*
    @Author: Dieudonne Dengun
    @Date: 12/04/2017
    @Description: Show  and close loading screen to a component
  
   */
    LocationsProvider.prototype.showLoader = function (message) {
        this.loading = this.loader.create({
            content: message
        });
        this.loading.present();
    };
    /*
     @Description: Close alert dialog
    */
    LocationsProvider.prototype.closeLoader = function () {
        this.loading.dismiss();
    };
    /*
     @Author: Dieudonne Dengun
     @Date: 12/04/2017
     @Description: Show Toast message to the bottom of the screen
     @param : $message, $position //top, bottom and middle
     */
    LocationsProvider.prototype.showToastMessage = function (message, position, duration, class_name) {
        if (position === void 0) { position = "bottom"; }
        if (duration === void 0) { duration = 4000; }
        if (class_name === void 0) { class_name = "toast-default"; }
        var toast_message = this.toast.create({
            message: message,
            duration: duration,
            position: position,
        });
        toast_message.present();
    };
    /*
     @Author: Dieudonne Dengun
     @Date: 12/04/2017
     @Description: Show Toast message to the bottom of the screen
     @param : $message
    */
    LocationsProvider.prototype.showSimpleAlertDialog = function (title, msg) {
        if (title === void 0) { title = ""; }
        if (msg === void 0) { msg = ""; }
        var alert = this.alert.create({
            title: title,
            message: msg,
            buttons: ['OK']
        });
        alert.present();
    };
    /*
      @AUthor:Dieudonne Dengun
      @Date:07/05/0218
      @Description:prompt and enable user to enable location service
    
    */
    LocationsProvider.prototype.enableLocationService = function () {
        var _this = this;
        var enabled = false;
        var alert = this.alert.create({
            title: 'To continue, please you will need to turn on your device location to enable Location Service',
            message: '',
            buttons: [
                // {
                //   text: 'Disagree',
                //   role: 'cancel',
                //   handler: () => {
                //   }
                // },
                {
                    text: 'Turn On Now',
                    handler: function () {
                        //show loader
                        _this.showLoader('Enabling location service, please wait...');
                        _this.location.canRequest().then(function (canRequest) {
                            if (canRequest) {
                                // the accuracy option will be ignored by iOS
                                _this.location.request(_this.location.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
                                    console.log('Request successful');
                                    enabled = true;
                                    _this.closeLoader();
                                    return enabled;
                                }, function (error) {
                                    console.log('Error requesting location permissions', error);
                                });
                            }
                        });
                    }
                }
            ],
        });
        alert.present();
        return enabled;
    };
    /*
      @Description:Filter an array of nearby places in order of closeness to the user current location
    */
    LocationsProvider.prototype.calculateNearbyPlacesByApplyHaversine = function (locations, usersLocation) {
        // let usersLocation = {
        //   lat: 40.713744, 
        //   lng: -74.009056
        // };
        var locations_object = locations;
        console.log(locations[0]);
        // locations_object.forEach(function (location) {
        //     console.log(location);
        // });
        for (var i = 0; i < locations_object.length; i++) {
            console.log("dengun");
            var placeLocation = {
                lat: locations_object[i].location.latitude,
                lng: locations_object[i].location.longitude
            };
            var distance = this.getDistanceBetweenPoints(usersLocation, placeLocation, 'miles').toFixed(2);
            // location.distance 
            console.log(distance);
        }
        // locations.map((location) => {
        // let placeLocation = {
        //   lat: location.latitude,
        //   lng: location.longitude
        // };
        // let distance = this.getDistanceBetweenPoints(
        //     usersLocation,
        //     placeLocation,
        //     'miles'
        //   ).toFixed(2);
        //  // location.distance 
        //  console.log(distance);
        // });
        return locations_object;
    };
    /*
       @Description:Calculate the distance between two points using latitudes and longitudes
    */
    LocationsProvider.prototype.getDistanceBetweenPoints = function (start, end, units) {
        var earthRadius = {
            miles: 3958.8,
            km: 6371
        };
        var R = earthRadius[units || 'miles'];
        var lat1 = start.lat;
        var lon1 = start.lng;
        var lat2 = end.lat;
        var lon2 = end.lng;
        var dLat = this.toRad((lat2 - lat1));
        var dLon = this.toRad((lon2 - lon1));
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
                Math.sin(dLon / 2) *
                Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d;
    };
    LocationsProvider.prototype.toRad = function (x) {
        return x * Math.PI / 180;
    };
    /*
    @Author:Dieudonne Dengun
    @Date:21/05/2018
    @Description:Display actionsheet
    */
    LocationsProvider.prototype.showActionSheeet = function (buttons) {
        buttons.push({
            icon: 'close',
            text: 'Cancel',
            role: 'cancel',
        });
        var actionSheet = this.actionSheetCtrl.create({
            buttons: buttons
        });
        actionSheet.present();
    };
    LocationsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_location_accuracy__["a" /* LocationAccuracy */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["u" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* LoadingController */]])
    ], LocationsProvider);
    return LocationsProvider;
}());

//# sourceMappingURL=locations.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DonationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payments_payments__ = __webpack_require__(212);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DonationPage = /** @class */ (function () {
    function DonationPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DonationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DonationPage');
    };
    DonationPage.prototype.payment = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__payments_payments__["a" /* PaymentsPage */]);
    };
    DonationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-donation',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\donation\donation.html"*/'\n<ion-header>\n  <ion-navbar color="purple">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title> #Donation Funds</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  \n\n  <div  style="text-align:start; font-size:small;">\n    <h6 ion-text color="white" >Donation Funds</h6>\n  </div>\n\n<!-- <ion-text color="#ffffff"> -->\n  <div   style="font-weight:lighter;font-size: smaller;">\n  <label\n   ion-text color="white" >Due to the pandemic of Coronavirus, this project caters for those in need of basic needs,those unable to work, and lost their jobs that feed their families.</label>\n  </div>\n\n  <div class="spacer" style="height: 3px;" ></div>\n   <!-- </ion-text> -->\n  <div style="font-weight:lighter;font-size:smaller;">\n<p ion-text color="white">Your donation would be highly appreciated</p>\n</div>\n<div class="spacer" style="height: 5px;" ></div>\n  <div  style="text-align:start; font-size:small;" >\n    <h6 ion-text color="white">Banking Details:</h6>\n  </div>\n\n  <div style="font-weight:lighter;font-size: smaller;">\n  <p ion-text color="white">Banking Name:  FNB</p> \n  <div class="spacer" style="height:0em;" ></div>\n  <p ion-text color="white">Branch Code:   24007</p>\n  <div class="spacer" style="height: 0em;" ></div>\n  <p ion-text color="white">Account No:   624000932312 </p>\n  <div class="spacer" style="height: 0em;" ></div>\n  <p ion-text color="white">Ref:  "CF" + Your Name</p>\n  </div>\n  <div class="spacer" style="height: 10px;" ></div>\n <!--  <button ion-button  full round style="height:10vw; width:40vw;  text-transform: none; " color="standard"  (click)="payment()" center >Donate</button> -->\n  <div class="color" ion-text color="white"><button ion-button round  style="height:10vw; width:40vw; text-transform: none; " center color="standard"  (click)="payment()">Donate</button>\n  </div>\n\n\n</ion-content>\n\n<ion-footer no-border>\n  <ion-toolbar>\n    <div class="donation">\n    <button ion-button  color="darkgrey" full round style=" height:10vw;  text-transform: none; font-size: smaller; font-weight: lighter;"   >\n     <p ion-text color="#000000" style="text-align:start;">Channel is Read-Only</p> \n    </button>\n  </div>\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\donation\donation.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */]])
    ], DonationPage);
    return DonationPage;
}());

//# sourceMappingURL=donation.js.map

/***/ }),

/***/ 95:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReactionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_loading__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_data__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_firebase__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_image__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_alert__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_google_maps__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_geolocation__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the ReactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReactionsPage = /** @class */ (function () {
    function ReactionsPage(navCtrl, navParams, loadingProvider, dataProvider, angularDb, firebaseProvider, alertCtrl, imageProvider, alertProvider, googleMaps, geolocation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingProvider = loadingProvider;
        this.dataProvider = dataProvider;
        this.angularDb = angularDb;
        this.firebaseProvider = firebaseProvider;
        this.alertCtrl = alertCtrl;
        this.imageProvider = imageProvider;
        this.alertProvider = alertProvider;
        this.googleMaps = googleMaps;
        this.geolocation = geolocation;
        this.userDetails = {
            Name: '',
            Email: '',
            Phone: '',
            Id: '',
            Status: '',
            proPhoto: '',
            bgPhoto: ''
        };
    }
    ReactionsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // Observe the userData on database to be used by our markup html.
        // Whenever the userData on the database is updated, it will automatically reflect on our user variable.
        this.dataProvider.getCurrentUser().then(function (res) {
            _this.userDetails.Name = res.Name;
            _this.userDetails.proPhoto = res.Photo;
            _this.userDetails.Phone = res.Phone;
            _this.userDetails.Status = res.Status;
            _this.userDetails.Id = res.Id;
            _this.userDetails.bgPhoto = res.bgPhoto;
            _this.userDetails.Email = res.Email;
        }).catch(function (err) {
            console.log(err);
        });
    };
    ReactionsPage.prototype.post = function () {
        var _this = this;
        if (this.image) {
            this.loadingProvider.show();
            this.imageProvider.uploadPostImage(this.image).then(function (url) {
                // ======= push new post in 'timeline' ====
                _this.angularDb.list('timeline').push({
                    dateCreated: new Date().toString(),
                    postBy: __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid,
                    postText: _this.postText,
                    image: url
                }).then(function (success) {
                    _this.postText = '';
                    var timelineId = success.key;
                    _this.firebaseProvider.timeline(timelineId);
                    _this.alertProvider.showToast('Add post successfully ..');
                    _this.loadingProvider.hide();
                    _this.navCtrl.pop();
                });
            });
        }
        else if (this.location) {
            this.loadingProvider.show();
            // ======= push new post in 'timeline' ====
            this.angularDb.list('timeline').push({
                dateCreated: new Date().toString(),
                postBy: __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid,
                postText: this.postText,
                location: this.location
            }).then(function (success) {
                _this.postText = '';
                var timelineId = success.key;
                _this.firebaseProvider.timeline(timelineId);
                _this.alertProvider.showToast('Add post successfully ..');
                _this.loadingProvider.hide();
                _this.navCtrl.pop();
            });
        }
        else {
            this.loadingProvider.show();
            // ======= push new post in 'timeline' ====
            this.angularDb.list('timeline').push({
                dateCreated: new Date().toString(),
                postBy: __WEBPACK_IMPORTED_MODULE_5_firebase__["auth"]().currentUser.uid,
                postText: this.postText,
            }).then(function (success) {
                _this.postText = '';
                var timelineId = success.key;
                _this.firebaseProvider.timeline(timelineId);
                _this.alertProvider.showToast('Add post successfully ..');
                _this.loadingProvider.hide();
                _this.navCtrl.pop();
            });
        }
    };
    ReactionsPage.prototype.imageShare = function () {
        var _this = this;
        this.imageProvider.setImage().then(function (url) {
            _this.image = url;
        });
    };
    ReactionsPage.prototype.locationShare = function () {
        var _this = this;
        this.loadingProvider.show();
        this.geolocation.getCurrentPosition().then(function (position) {
            var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            var mapOptions = {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            _this.location = JSON.stringify({ lat: position.coords.latitude, long: position.coords.longitude });
            _this.map = new google.maps.Map(_this.mapElement.nativeElement, mapOptions);
            var marker = new google.maps.Marker({
                map: _this.map,
                animation: google.maps.Animation.DROP,
                position: _this.map.getCenter()
            });
            _this.loadingProvider.hide();
        }, function (err) {
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], ReactionsPage.prototype, "mapElement", void 0);
    ReactionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-reactions',template:/*ion-inline-start:"C:\Users\mokat\SACC-APP\src\pages\reactions\reactions.html"*/'<!--\n  Generated template for the AddPostPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>add-post</ion-title>\n    <ion-buttons end (click)="post()">\n      <button  [disabled]="!postText">\n      POST\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-line>\n  <ion-item >\n    <ion-thumbnail item-left  >\n      <img src="{{userDetails.proPhoto}}"  >\n    </ion-thumbnail>\n    <p><b>{{userDetails.Name}}</b></p>\n  </ion-item>\n  <ion-item>\n    <textarea placeholder="Please enter your message" [(ngModel)]="postText"> </textarea>\n  </ion-item>\n  <ion-item *ngIf="image">\n    <img src="{{image}}">\n  </ion-item>\n  <div #map id="map" style="height:50%;"></div>\n</ion-content>\n<ion-footer>\n  <ion-grid>\n    <ion-row>\n      <ion-col width-50>\n        <button ion-button icon-left block color="primary" (click)="imageShare()" [disabled]="location">\n          <ion-icon name=\'camera\'></ion-icon> Image\n        </button>\n        <!-- <button ion-button icon-only outline color="custom" [disabled]="shareImage">\n          <ion-icon name=\'navigate\'></ion-icon>\n\n        </button> -->\n      </ion-col>\n      <ion-col width-50>\n        <button ion-button icon-left block color="primary" (click)="locationShare()"  [disabled]="image"> <!---(click)="locationShare()" -->\n            <ion-icon name=\'navigate\'></ion-icon>\n            Location\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-footer>\n'/*ion-inline-end:"C:\Users\mokat\SACC-APP\src\pages\reactions\reactions.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_data__["a" /* DataProvider */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_6__providers_firebase__["a" /* FirebaseProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_image__["a" /* ImageProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_alert__["a" /* AlertProvider */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_google_maps__["a" /* GoogleMaps */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_geolocation__["a" /* Geolocation */]])
    ], ReactionsPage);
    return ReactionsPage;
}());

//# sourceMappingURL=reactions.js.map

/***/ })

},[388]);
//# sourceMappingURL=main.js.map