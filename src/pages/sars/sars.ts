import { Component, ViewChild,OnInit,Renderer,NgZone } from '@angular/core';
import { NavController, NavParams,ToastOptions ,ToastController ,AlertController,ViewController,ModalController,Events} from 'ionic-angular';
import { Report } from './../../model/report';
import { first } from 'rxjs/operators';
import { UserProvider } from '../../providers/user/user';
import firebase from 'firebase';
import { BookedPage } from '../booked/booked';
import { NearcentrePage } from '../nearcentre/nearcentre';




@Component({
  selector: 'page-sars',
  templateUrl: 'sars.html',
})
export class SarsPage {

  accordionExpanded = false;
  @ViewChild("cc") cardContent :any;
 // @ViewChild("lc") listContent :any;
  icon: string = "arrow-forward";
  /* report = {} as Report;
  alignuid;
  photoURL;*/
  randomNumberFloor;
  rawRandomNumber;
  toastOptions: ToastOptions;
  @ViewChild('content') childNavCtrl: NavController; 
 

  constructor(public renderers: Renderer,private toast:ToastController,public navCtrl: NavController,public zone: NgZone,public userservice: UserProvider, private alertCtrl: AlertController) {
    
    /* var maxV=10000
    var minV=1000
    this.rawRandomNumber = Math.random() * (maxV - minV) + minV
    this.randomNumberFloor =Math.floor(this.rawRandomNumber)
    this.toastOptions = {
    message: ' Appointment send!',
    duration: 3000 ;*/
  }


 OnInit()
  {
    console.log(this.cardContent.nativeElement);
    //console.log(this.listContent.nativeElement);
  
  }
 
   toggleAccordion()
  {
      if(this.accordionExpanded){
   

      this.renderers.setElementStyle(this.cardContent.nativeElement,"max-height","0px");
    

      }else{

        this.renderers.setElementStyle(this.cardContent.nativeElement,"max-height","500px");

      }

      this.accordionExpanded = !this.accordionExpanded;
  }  
   alert(message: string) {
    this.alertCtrl.create({
     
      title: 'Appointment!',
      message: 'Your appointment is successfully Scheduled. ' ,
      subTitle: message,
      buttons: ['OK']
    }).present();
    this.navCtrl.setRoot(BookedPage)
  } 

  map()
  {
    this.navCtrl.push(NearcentrePage)
  }

  popup(message:string)
  {
    this.alertCtrl.create({
     
      message: 'Tax Service Debt Management:Taxpayers can be assisted with Debt arrangments.Dedicated Small,Micro and Medium Enterprices (SMMEs) service deskTo Qualify as an SMME,you need to be the actual owner with a per annum business turnover of between R0 and R20M.Where to park:There is ample parking space at the new branch',
      subTitle: message,
      buttons: ['OK']
    }).present();
  }
  /*  toggleAccordion1()
  {
    if(this.accordionExpanded){
   

      this.renderers.setElementStyle(this.listContent.nativeElement,"max-height","0px");
    

      }else{

        this.renderers.setElementStyle(this.listContent.nativeElement,"max-height","500px");

      }

      this.accordionExpanded = !this.accordionExpanded;
      this.icon = this.icon == "arrow-forward" ? "arrow-down" : "arrow-forward";
  }  
 */
}
