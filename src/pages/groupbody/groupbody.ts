import { Component, NgZone } from '@angular/core';
import { NavController, NavParams, Events,ModalController, ActionSheetController, LoadingController, Platform } from 'ionic-angular';
//import { GroupProvider } from '../../providers/group/group';

import { GroupaddmemberPage } from '../groupaddmember/groupaddmember';
import { GroupmemberPage } from '../groupmember/groupmember';
import { GroupschannelProvider } from '../../providers/groups/groupschannel';
import { NewgroupchannelPage } from '../newgroupchannel/newgroupchannel';
import { GroupchatchannelPage } from '../groupchatchannel/groupchatchannel';

//import { SuperTabsController } from 'ionic2-super-tabs';


declare var window: any;


@Component({
  selector: 'page-groupbody',
  templateUrl: 'groupbody.html',
})
export class GroupbodyPage {
	Groups = []
	
  groupDetails

	User_Uid = this.groupProvider.UserUid


	looding = true

  constructor( public ngZone: NgZone,private myModal:ModalController, public events: Events, public loadCtrl: LoadingController, private platform: Platform, public actionSheetCtrl: ActionSheetController, public navCtrl: NavController, public groupProvider: GroupschannelProvider, public navParams: NavParams) {
    this.groupDetails = this.navParams.get('groupDetails')
    this.events.subscribe('Channels', () => {
		this.ngZone.run(() => {
		  this.looding = false
		  this.Groups = this.groupProvider.Groups
		})
	  })   

  }



  ionViewDidLeave(){
    this.events.subscribe('Channels')
  }


  ionViewDidEnter(){
    this.groupProvider.getGroups()
  }






  openGroupBody(groupDetails) {
    this.groupProvider.initialize(groupDetails)
    const modal = this.myModal.create(GroupchatchannelPage, {
      groupDetails: groupDetails
    })
    modal.present();

  }


  openNewGroupPage() {
	this.navCtrl.push(NewgroupchannelPage)
}
showActionSheet() {
  if (this.groupDetails.Owner == this.groupProvider.UserUid) {

    const actionSheet = this.actionSheetCtrl.create({
      title: this.groupDetails.Name,
      buttons: [
        {
          text: 'Group Members',
          icon:'contacts',
          handler: () => {
            this.showMembers(this.groupDetails)
          }
        },{
          text: 'Add Member',
          icon:'add',
          handler: () => {
            this.addMember(this.groupDetails)
          }
        },{
          text: 'Group Info',
          icon:'alert',
          handler: () => {
            this.groupInfo(this.groupDetails)
          }
        },{
          text: 'Delete Group',
          icon:'trash',
          handler: () => {
            this.deleteGroup(this.groupDetails)
          }
        },{
          text: 'Cancel',
          icon:'close',
          handler: () => {
            this.showToast('Cancel clicked');
          }
        }
      ]
    })
    actionSheet.present()
  } else {
    const actionSheet = this.actionSheetCtrl.create({
      title: this.groupDetails.Name,
      buttons: [
        {
          text: 'Group Members',
          icon:'contacts',
          handler: () => {
            this.showMembers(this.groupDetails)
          }
        },{
          text: 'Group Info',
          icon:'alert',
          handler: () => {
            this.groupInfo(this.groupDetails)
          }
        },{
          text: 'Leave Group',
          icon:'log-out',
          handler: () => {
            this.leaveGroup(this.groupDetails)
          }
        },{
          text: 'Cancel',
          icon:'close',
          handler: () => {
            this.showToast('Cancel clicked');
          }
        }
      ]
    })
    actionSheet.present()
  }
}
leaveGroup(groupDetails){
  let load = this.loadCtrl.create({
    content: 'Leaving Group ...'
  })

  load.present()
  this.groupProvider.leaveGroup(groupDetails).then(() => {
    load.dismiss()
    this.showToast('You have been leaved')
    this.navCtrl.pop()
  }).catch((err) => {
    load.dismiss()
    this.showToast(err)
  })

}

groupInfo(groupDetails){
  
}


deleteGroup(groupDetails){
  let load = this.loadCtrl.create({
    content: 'Deleting Group ...'
  })

  load.present()
  this.groupProvider.deleteGroup(groupDetails).then(() => {
    load.dismiss()
    this.showToast('Group has been deleted')
    this.navCtrl.pop()
  }).catch((err) => {
    load.dismiss()
    this.showToast(err)
  })

}
showToast(message){
  this.platform.ready().then(() => {
      window.plugins.toast.show(message, "short", 'bottom');
  })    
}

showMembers(groupDetails){
  this.navCtrl.push(GroupmemberPage, {
    groupDetails: groupDetails
  })
}

addMember(groupDetails){
  this.navCtrl.push(GroupaddmemberPage, {
    groupDetails: groupDetails
  })
}





}