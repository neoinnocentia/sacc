import { Component,NgZone } from '@angular/core';
import {  NavController, NavParams,Events } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { ChatProvider } from '../../providers/chat/chat';
//import { RequestProvider } from '../../providers/requests/requests';
import { SettingsPage } from '../settings/settings';
//import { NotificationsPage } from '../notifications/notifications';
import { NotificationProvider } from '../../providers/notification/notification';
import { NotificationsPage } from '../notifications/notifications';
import { RequestProvider } from '../../providers/request/request';
import { AuthProvider } from '../../providers/auth/auth';
import { FriendsProvider } from '../../providers/friends/friends';
import { UsersPage } from '../users/users';


/**
 * Generated class for the PresencePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-presence',
  templateUrl: 'presence.html',
})
export class PresencePage {

 
  public buttonClicked: boolean = false;
  myrequests;
  myfriends;
  notifications

  Friends = []
  allUsers = []
  Conversations = []
  userDetails: any;
  onlineStatus = 'Online'
  offlineStatus = "Offline"


  looding = true

  constructor(public requestservice:RequestProvider,public chatProvider: ChatProvider,public authProvider: AuthProvider, public friendsProvider: FriendsProvider,public navCtrl: NavController,public notificationsProvider: NotificationProvider,public events: Events, public ngZone: NgZone,public chatservice: ChatProvider,private afDatabase: AngularFireDatabase,private afAuth: AngularFireAuth, public navParams: NavParams) {
    this.events.subscribe('Notifications', () => {
      this.ngZone.run(() => {
        var res = this.notificationsProvider.NotificationsUnread
        this.notifications = res.length
      })
    })
    
    
    this.events.subscribe('Friends', () => {
      this.ngZone.run(() => {
        this.Friends = this.friendsProvider.Friends
      })
    })
    var days = ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri']
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']

    var today = new Date
    var toYear = today.getFullYear()
    var toMonth = 1 + today.getMonth()
    var toDays = today.getDate()

    var todayRes1 = toYear + '/' + toMonth + '/' + toDays
    var todayRes2 = toYear + '/' + toMonth
    var todayRes3 = toYear


    this.events.subscribe('Conversations', () => {
      this.ngZone.run(() => {
        this.looding = false
        this.Conversations = this.chatProvider.Conversations
        this.allUsers = this.chatProvider.buddyUsers


        for (var key in this.Conversations) {

          var d = new Date(this.Conversations[key].Time)

          var years = d.getFullYear()
          var month = 1 + d.getMonth()
          var day = d.getDate()
          var hours = d.getHours()
          var minutes = '0' + d.getMinutes()

          var messageTime1 = years + '/' + month + '/' + day
          var messageTime2 = years + '/' + month
          var messageTime3 = years

          var DN = toDays - day

          if (messageTime1 == todayRes1) {
            this.Conversations[key].Time = hours + ":" + minutes.substr(-2)
          } else {
            if (messageTime2 == todayRes2) {
              if (DN == 1) {
                this.Conversations[key].Time = 'Yestersay,' + hours + ":" + minutes.substr(-2)
              } else if (DN < 7) {
                this.Conversations[key].Time = days[DN] + hours + ":" + minutes.substr(-2)
              } else if (DN > 7) {
                this.Conversations[key].Time = months[month] + ',' + day + ',' + years
              }
            } else {
              this.Conversations[key].Time = months[month] + ',' + day + ',' + years
            }
          }

        }





      })
    })
  }
  ionViewWillEnter() {

    this.requestservice.getReceivedRequests();
    this.requestservice.getReceivedRequests();
    this.myfriends = [];
    this.events.subscribe('gotrequests', () => {
      this.myrequests = [];
      this.myrequests = this.requestservice.myDetails;
    })
    this.events.subscribe('friends', () => {
      this.myfriends = [];
      this.ngZone.run(() => {
        this.myfriends = this.requestservice.getReceivedRequests;
      })

    })
  }
  ionViewDidLeave() {
    this.events.unsubscribe('gotrequests');
    this.events.unsubscribe('friends');
    this.events.subscribe('friends');
    this.events.subscribe('Friends');
    this.events.subscribe('Conversations');
    this.events.subscribe('Notifications')



  }
  ionViewDidEnter(){
    this.friendsProvider.getFriends();
    this.chatProvider.getConversations();

    this.requestservice.getReceivedRequests();
    this.notificationsProvider.getMyNotificationsUnread()

  } 
 

  notification(){
    this.navCtrl.setRoot(NotificationsPage)
  }
 
  settings(){
    this.navCtrl.push(SettingsPage);
  }
  openUsersPage(){
    this.navCtrl.push(UsersPage);
  }
  
 
}
