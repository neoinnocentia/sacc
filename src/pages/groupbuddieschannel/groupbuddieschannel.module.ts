import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupbuddieschannelPage } from './groupbuddieschannel';

@NgModule({
  declarations: [
    GroupbuddieschannelPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupbuddieschannelPage),
  ],
  exports: [
    GroupbuddieschannelPage
  ]
})
export class GroupbuddieschannelPageModule {}
