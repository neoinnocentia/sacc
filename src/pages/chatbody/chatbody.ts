import { Component, NgZone, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, Platform, Events, AlertController, LoadingController } from 'ionic-angular';

//import { SuperTabsController } from 'ionic2-super-tabs';
import { AuthProvider } from '../../providers/auth/auth';
import { ChatProvider } from '../../providers/chat/chat';
//import { Clipboard } from '@ionic-native/clipboard';



declare var window: any;

@Component({
  selector: 'page-chatbody',
  templateUrl: 'chatbody.html',
})
export class ChatbodyPage {
  [x: string]: any;


	userDetails


  details ={}

  myDetails ={}

  Conversations = []
  onlineStatus = 'Online'
  offlineStatus = 'Offline'


  newMessage = {
  	body: ''
  }

  allMessages = []


  @ViewChild('content') content: Content

  constructor(  public loadCtrl: LoadingController, private platform: Platform, public chatProvider: ChatProvider, public alertCtrl: AlertController, public authProvider: AuthProvider, public navCtrl: NavController, public navParams: NavParams, public ngZone: NgZone, public events: Events) {
    
 /*    var days = ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri']
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']

    var today = new Date
    var toYear = today.getFullYear()
    var toMonth = 1 + today.getMonth()
    var toDays = today.getDate()

    var todayRes1 = toYear + '/' + toMonth + '/' + toDays
    var todayRes2 = toYear + '/' + toMonth
    var todayRes3 = toYear


 */
    this.events.subscribe('Conversations', () => {
      this.ngZone.run(() => {
        this.looding = false
        this.allMessages = this.chatProvider.allMessages
        this.allUsers = this.chatProvider.buddyUsers


        for (var key in this.allMessages) {

          var d = new Date(this.allMessages[key].Time)

          var years = d.getFullYear()
          var month = d.getMonth()
          var day = d.getDate()
          var hours = d.getHours()
          var minutes = '0' + d.getMinutes()

          var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
          var formattedTime = monthNames[month] + "-" + day + "-" + hours + ":" + minutes.substr(-2) ;

           this.allMessages[key].Time = formattedTime;
        }





      })
    })


    this.userDetails = this.navParams.get('userDetails')

    this.events.subscribe('ProfileDetails', () => {
      this.ngZone.run(() => {
        this.details = this.authProvider.ProfileDetails
      })
    })

    this.events.subscribe('myDetails', () => {
      this.ngZone.run(() => {
        this.myDetails = this.authProvider.myDetails
      })
    })

    this.events.subscribe('messages', () => {
      this.ngZone.run(() => {
        this.allMessages = this.chatProvider.allMessages
      })
    })

    if (this.allMessages.length > 6) {
      setTimeout(() => {
        for(let i = 0; i < 10; i++){
          this.allMessages[i]
        }
      }, 300)
    }


  }





  showToast(message){
        this.platform.ready().then(() => {
            window.plugins.toast.show(message, "short", 'bottom');
        })    
  }



  /* callFunction(){
    this.content.scrollToBottom(0)
  } */


  ionViewDidLeave(){
    this.events.subscribe('ProfileDetails')
    this.events.subscribe('myDetails')
    this.events.subscribe('messages')
  }


  ionViewDidEnter(){
    this.authProvider.getProfileDetails(this.userDetails)
    this.authProvider.getMyDetails()
    this.chatProvider.getMessages(this.userDetails)
  }

  sendMessage() {
  	var res = this.newMessage.body
  	var res1 = res.trim()
  	if (res1 == '') {
  		this.showToast("can't send empty message")
  		this.newMessage.body = ''
  	} else {
  		this.chatProvider.sendMessage(this.newMessage).then(() => {
  			this.newMessage.body = ''
  		}).catch((err) => {
  			console.log(err)
  		})
  	}
  }

  showMessageConfirm(message, myDetails, friendDetails){
    const confirm = this.alertCtrl.create({
      title: 'Message',
      message: 'Tap On Option',
      buttons: [
 
        {
          text: 'Delete For Me',
          handler: () => {
            this.deleteMessageForMe(message, myDetails, friendDetails)
          }
        },
        {
          text: 'Delete For All',
          handler: () => {
            this.deleteMessageForAll(message, myDetails, friendDetails)
          }
        },
        {
          text: 'Cancel',
          handler: () => {
            this.showToast('Cancel')
          }
        }
      ]
    });
    confirm.present();
  }




 
  copyMessage(message) {
    this.clipboard.copy(message.Body).then(() => {
      this.showToast('Message coppied to clipboard')
    })
  }
 


  deleteMessageForMe(message, myDetails, friendDetails){
    let load = this.loadCtrl.create({
      content: 'Deleting Message ...'
    })
    load.present()
    this.chatProvider.deleteMessageForMe(message, myDetails, friendDetails).then(() => {
      load.dismiss()
      this.showToast('Message has been deleted')
    }).catch((err) => {
      load.dismiss()
      this.showToast(err)
    })
  }


  deleteMessageForAll(message, myDetails, friendDetails){
    let load = this.loadCtrl.create({
      content: 'Deleting Message ...'
    })
    load.present()    
    this.chatProvider.deleteMessageForAll(message, myDetails, friendDetails).then(() => {
      load.dismiss()
      this.showToast('Message has been deleted')
    }).catch((err) => {
      load.dismiss()
      this.showToast(err)
    })
  }
}