import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastOptions, AlertController, Events, LoadingController, Platform, ActionSheetController } from 'ionic-angular';
import { ImghandlerProvider } from '../../providers/imghandler/imghandler';
import { UserProvider } from '../../providers/user/user';
import firebase from 'firebase';
import { LoginPage } from '../login/login';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { Profile } from './../../model/profile';
//import { WelcomePage } from '../welcome/welcome';
import { ToastController } from 'ionic-angular';
import { EditprofilePage } from '../editprofile/editprofile';
import { AuthProvider } from '../../providers/auth/auth';
import { Camera } from '@ionic-native/camera';
import { TabsPage } from '../tabs/tabs';
import { AccountProfilePagecotcot } from '../accountprofilecot/accountprofilecot';
import { ChatPage } from '../chat/chat';
import { PanicPage } from '../panic/panic';

/**
 * Generated class for the PostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var window: any;
@IonicPage()
@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class PostPage {
  tab1Root= AccountProfilePagecotcot;
  tab2Root = ChatPage;
  //tab3Root = PostPage;
  tab4Root = PanicPage;
  userDetails ={
		Name: '',
    Email: '',
    Phone: '',
    Id: '',
    Status: '',
    proPhoto: '',
    bgPhoto: ''
	}


  details ={
    Name: '',
    Email: '',
    Phone: '',
    Id: '',
    Status: '',
    proPhoto: '',
    about: '',
    bgPhoto: ''
  }

  myProfile = false

  captureDataUrl;
  captureDataUrll

  constructor( public ngZone: NgZone, public events: Events, public camera: Camera,   public actionSheetCtrl: ActionSheetController, public loadCtrl: LoadingController, private platform: Platform, public navCtrl: NavController, public authProvider: AuthProvider, public navParams: NavParams) {
  	this.userDetails = this.navParams.get('userDetails')

    this.events.subscribe('ProfileDetails', () => {
      this.ngZone.run(() => {
        this.details = this.authProvider.ProfileDetails
        if (this.details.Id == this.authProvider.UserUid) {
          this.myProfile = true
        } else {
          this.myProfile = false
        }
      })
    })    
  }





  showToast(message){
        this.platform.ready().then(() => {
            window.plugins.toast.show(message, "short", 'bottom');
        })    
  }




  ionViewDidLeave(){
    this.events.subscribe('ProfileDetails')
  }




  ionViewDidEnter(){
    this.authProvider.getProfileDetails(this.userDetails)
  }





  proPic(){
    if (this.details.Id == this.authProvider.UserUid) {
      const actionSheet = this.actionSheetCtrl.create({
        title: this.details.Name,
        buttons: [
          {
            text: 'View Profile Picture',
            icon: 'person',
            role: 'destructive',
            handler: () => {
              this.viewProfilePicture(this.details.proPhoto, this.details.Name)
            }
          },{
            text: 'Select Profile Picture',
            icon: 'albums',
            handler: () => {
              this.selectProfilePicture()
            }
          },{
            text: 'Cancel',
            icon: 'close',
            role: 'cancel',
            handler: () => {
              this.showToast('Cancel')
            }
          }
        ]
      })
      actionSheet.present() 
    } else {
      this.viewProfilePicture(this.details.proPhoto, this.details.Name)
    }
  }



  viewProfilePicture(photo, name){
    var options = {
        share: true, // default is false
        closeButton: true, // default is true
        copyToReference: false // default is false
    }
    //this.photoViewer.show(photo, name, options);    
  }



  selectProfilePicture() {
    let load = this.loadCtrl.create({
      content: 'Uploading Picture ...'
    })

    const cameraOptions = {
      quality : 50,
      sourceType : this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: this.camera.EncodingType.PNG,
      destinationType: this.camera.DestinationType.DATA_URL
    }

    this.camera.getPicture(cameraOptions).then((ImageData) => {
      this.captureDataUrl = 'data:image/jpeg;base64,' + ImageData
      load.present()
      this.authProvider.uploadProfilePhoto(this.captureDataUrl).then(() => {
        this.details.proPhoto = this.captureDataUrl
        load.dismiss()
        this.showToast('Profile Picture has been updated')
      }).catch((err) => {
        load.dismiss()
        this.showToast(err)
      })
    }, (err) => {
      var error = JSON.stringify(err)
      this.showToast(error)
    })
  }



  





  editProfile() {
    this.navCtrl.push(EditprofilePage, {
      userDetails : this.details
    })
  }




 

  tabs(){
    this.navCtrl.setRoot(TabsPage);

  }
}