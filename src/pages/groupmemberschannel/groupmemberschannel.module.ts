import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupmembersChannelPage } from './groupmemberschannel';

@NgModule({
  declarations: [
    GroupmembersChannelPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupmembersChannelPage),
  ],
  exports: [
    GroupmembersChannelPage
  ]
})
export class GroupmembersChannelPageModule {}
