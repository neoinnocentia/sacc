import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { SarsPage } from '../sars/sars';
import { BookingsPage } from '../bookings/bookings';



@Component({
  selector: 'page-booked',
  templateUrl: 'booked.html',
})
export class BookedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookedPage');
  }

  goback()
  {
    this.navCtrl.setRoot(BookingsPage);
  }

}
