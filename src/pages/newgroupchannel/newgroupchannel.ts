import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,Platform, LoadingController } from 'ionic-angular';
import { GroupschannelProvider } from '../../providers/groups/groupschannel';
import { ImghandlerProvider } from '../../providers/imghandler/imghandler';
import { GroupchatchannelPage } from '../groupchatchannel/groupchatchannel';
import { GroupbodyPage } from '../groupbody/groupbody';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { GroupschannelPage } from '../groupschannel/groupschannel';

declare var window: any;

/**
 * Generated class for the NewgroupchannelPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-newgroupchannel',
  templateUrl: 'newgroupchannel.html',
})
export class NewgroupchannelPage {

  group = {
		Name : 'Group Name',
		Picture: 'https://firebasestorage.googleapis.com/v0/b/chat-app-f6a12.appspot.com/o/user.png?alt=media&token=79419e48-423a-42c3-92b2-16027651b931'
  }

  captureDataUrl

  constructor( public camera: Camera,public navCtrl: NavController,public platform: Platform, public navParams: NavParams, public alertCtrl: AlertController,
    public groupProvider: GroupschannelProvider, public imghandler: ImghandlerProvider,
    public loadingCtrl: LoadingController) {
  }
  showToast(message){
    this.platform.ready().then(() => {
        window.plugins.toast.show(message, "short", 'bottom');
    })  	
}
editName() {
  const prompt = this.alertCtrl.create({
    title: 'Channel Name',
    message: 'Enter a name for this channel',
    inputs: [
      {
        name: 'Name',
        placeholder: 'Group Name'
      }
    ],
    buttons : [
      {
        text: 'Cancel',
        cssClass: 'half-alert-button',
        handler: data => {
          this.showToast('Cancel')
        }
      },
      {
        text: 'Save',
        cssClass: 'half-alert-button',
        handler: data => {
          var res = data.Name
          var result = res.trim()
          if (result == '') {
            this.showToast('Enter Channel Name')
          } else {
            this.group.Name = result
          }
        }
      }
    ]
  })
  prompt.present()
}




selectPicture() {
  let load = this.loadingCtrl.create({
    content: 'Uploading channel picture ....'
  })

  const cameraoptions: CameraOptions = {
    quality : 50,
    sourceType : this.camera.PictureSourceType.PHOTOLIBRARY,
    encodingType: this.camera.EncodingType.PNG,
    destinationType: this.camera.DestinationType.DATA_URL
  }


  this.camera.getPicture(cameraoptions).then((ImageData) => {
    this.captureDataUrl = 'data:image/jpeg;base64,' + ImageData
    load.present()
    this.groupProvider.uploadGroupPicture(this.captureDataUrl).then(() => {
      this.group.Picture = this.captureDataUrl
      load.dismiss()
    }).catch((err) => {
      load.dismiss()
      this.showToast(err)
    })
  }, (err) => {
    var res = JSON.stringify(err)
    load.dismiss()
    this.showToast(res)
  })


}




createGroup() {
  let load = this.loadingCtrl.create({
    content: 'Creating Channel ....'
  })


  if (this.group.Picture == 'hsttps://firebasestorage.googleapis.com/v0/b/chat-app-f6a12.appspot.com/o/user.png?alt=media&token=79419e48-423a-42c3-92b2-16027651b931') {
    console.log('Select Channel Picture')
  } else {
    if (this.group.Name == 'Channel Name') {
      console.log('Enter Channel Name')
    } else {
      load.present()
      this.groupProvider.creaeGroup(this.group).then(() => {
        load.dismiss()
        console.log('Channel has been created')
        this.navCtrl.setRoot(GroupschannelPage)
      }).catch((err) => {
        load.dismiss()
        console.log(err)
      })
    }
  }
}

 }