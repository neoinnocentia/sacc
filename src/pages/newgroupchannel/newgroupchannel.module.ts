import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewgroupchannelPage } from './newgroupchannel';

@NgModule({
  declarations: [
    NewgroupchannelPage,
  ],
  imports: [
    IonicPageModule.forChild(NewgroupchannelPage),
  ],
  exports: [
    NewgroupchannelPage
  ]
})
export class NewgroupchannelPageModule {}
