import { Component,NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NearcentrePage } from '../nearcentre/nearcentre';
import { BookingsPage } from '../bookings/bookings';
import { LocationsProvider } from '../../providers/locations/locations';
import { Geolocation } from '@ionic-native/geolocation';
import { SarsPage } from '../sars/sars';
//import {Geoloc}
declare var google;

@IonicPage()
@Component({
  selector: 'page-nearby',
  templateUrl: 'nearby.html',
})
export class NearbyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public geolocation: Geolocation,private ngzone:NgZone) {
   
  }
}
 /*  updateSearchResults(){
    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
    (predictions, status) => {
      this.autocompleteItems = [];
      this.ngzone.run(() => {
        predictions.forEach((prediction) => {
          this.autocompleteItems.push(prediction);
        });
      });
    });
  }
  selectSearchResult(item){
    this.clearMarkers();
    this.autocompleteItems = [];
  
    this.geocoder.geocode({'placeId': item.place_id}, (results, status) => {
      if(status === 'OK' && results[0]){
        let position = {
            lat: results[0].geometry.location.lat,
            lng: results[0].geometry.location.lng
        };
        let marker = new google.maps.Marker({
          position: results[0].geometry.location,
          map: this.map,
        });
        this.markers.push(marker);
        this.map.setCenter(results[0].geometry.location);
      }
    })
  }
  
  tryGeolocation(){
    this.clearMarkers();
    this.geolocation.getCurrentPosition().then((resp) => {
      let pos = {
        lat: resp.coords.latitude,
        lng: resp.coords.longitude
      };
      let marker = new google.maps.Marker({
        position: pos,
        map: this.map,
        title: 'I am here!'
      });
      this.markers.push(marker);
      this.map.setCenter(pos);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  clearMarkers():void {
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: this.map.getCenter()
    });

    let content = "<h4>Information!</h4>";          

   this.addInfoWindow(marker, content);

  }
  appointment()
  {
    this.navCtrl.push(BookingsPage)
  }
  

  ionViewDidLoad() {
   // this.tryGeolocation();
   this.map = new google.maps.Map(document.getElementById('map'), {
      center: { lat:-26.107567, lng: 28.056702},
      zoom: 15
    }); 
   // this.findUserLocation();
  }
  

}
 */