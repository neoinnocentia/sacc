import { Component, ViewChild } from '@angular/core';
import {CallNumber} from '@ionic-native/call-number';
import { IonicPage, NavController, AlertController,NavParams,ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {map} from 'rxjs/operators';
import { StorageProvider, Item } from '../../providers/storage/storage';


@IonicPage()
@Component({
  selector: 'page-panic',
  templateUrl: 'panic.html',
})
export class PanicPage {

  Name: string;
  phoneNumber: number;
  phone: string;
  User: any;
  res:any;
  contact: any;
  contacts:any[] = [];
  Data: any[] = [];
 items: Item[] = [];
 newItem: Item = <Item>{};

  constructor(public navCtrl: NavController,private storageService: StorageProvider,private alertCtrl: AlertController,private toastController: ToastController, public navParams: NavParams, public callNumber : CallNumber) {
  
     this.loadItems();
  }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
// CREATE
addItem() {
  this.newItem.modified = Date.now();
  this.newItem.id = Date.now();

  this.storageService.addItem(this.newItem).then(item => {
    this.newItem = <Item>{};
    this.showToast('Item added!')
    this.loadItems(); // Or add it to the array directly
  });
}  
  // READ
  loadItems() {
    this.storageService.getItems().then(items => {
      this.items = items;
      console.log(this.items)
    });
  }

  deleteItem(item: Item) {
    this.storageService.deleteItem(item.id).then(item => {
      this.showToast('Contact removed!');
      this.loadItems(); // Or splice it from the array directly
    });
  }
  

  police(){
    this.callNumber.callNumber("0117589100", true)
  .then(res => console.log('Launched dialer!', res))
  .catch(err => console.log('Error launching dialer', err));
    
  }
  ambulance(){
    this.callNumber.callNumber("084124", true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
  fire(){
 this.callNumber.callNumber("0114580911", true)
  .then(res => console.log('Launched dialer!', res))
  .catch(err => console.log('Error launching dialer', err));
  
  }
   child(){
   this.callNumber.callNumber("0114524110", true)
  .then(res => console.log('Launched dialer!', res))
  .catch(err => console.log('Error launching dialer', err));
   
  }
  coronaCare()
  {   
    this.callNumber.callNumber("0828839920", true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));

  }
  church()
  {
    this.callNumber.callNumber("01188422987", true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));

  }

  call(Phone){
    this.callNumber.callNumber(Phone, true)
   .then(res => console.log('Launched dialer!', res))
   .catch(err => console.log('Error launching dialer', err));
    
   }

   async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Add Emergency Contact',
      inputs: [
        {
          name: 'Name',
          placeholder: 'Name'
        },
        {
          name: 'Phone',
          placeholder: 'Phone Number',
          type: 'string'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            this.newItem.modified = Date.now();
            this.newItem.id = Date.now();
            this.newItem.Name = data.Name;
            this.newItem.Number = data.Phone;
              this.storageService.addItem(this.newItem).then(item => {
              this.newItem = <Item>{};
              this.showToast('Contact added!')
              this.loadItems(); // Or add it to the array directly
            });
              //console.log(data.Name,data.phone);
            
          }
        }
      ]
    });
    alert.present();
  }
 

  ionViewDidLoad() {
    console.log('ionViewDidLoad PanicPage');
  }  

}
