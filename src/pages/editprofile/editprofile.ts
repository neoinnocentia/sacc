import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Platform } from 'ionic-angular';


import { AuthProvider } from '../../providers/auth/auth';


declare var window: any;


@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html',
})
export class EditprofilePage {

	userDetails = {
		Name: '',
		Email: '',
		Phone: ''
	}



  constructor(public navCtrl: NavController, public loadCtrl: LoadingController, private platform: Platform, public navParams: NavParams, public authProvider: AuthProvider) {
  	this.userDetails = this.navParams.get('userDetails')

  	var res = this.navParams.get('userDetails')
  	this.userDetails.Name = res.Name
  	this.userDetails.Email = res.Email
  	this.userDetails.Phone = res.Phone

  }




  showToast(message){
        this.platform.ready().then(() => {
            window.plugins.toast.show(message, "short", 'bottom');
        })  	
  }



  updateProfile() {
  	let load = this.loadCtrl.create({
		content: 'Processing ....'
	})

  	if (this.userDetails.Name.length < 6) {
  		this.showToast('Name should be at least 6 characters')
  	} else {
  		if (this.userDetails.Email == '') {
  			this.showToast('Email should be xxxx@megatech.co.za')
  		} else {
  			if (this.userDetails.Phone == '') {
				 	this.showToast('Number should be 10 digit')			
  			} else {
  				load.present()
			  	this.authProvider.updateProfile(this.userDetails).then(() => {
			  		load.dismiss()
			  		this.navCtrl.pop()
			  		this.showToast('Details have been updated')
			  	}).catch((err) => {
			  		load.dismiss()
			  		this.showToast(err)
			  	}) 
  			}
  		}
  	}


  }








}