import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { AboutPage } from '../about/about';

import { LoginPage } from '../login/login';

/**
 * Generated class for the PreferencePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-preference',
  templateUrl: 'preference.html',
})
export class PreferencePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreferencePage');
  }
  account(){
   // this.navCtrl.push(AboutPage);

  }
 
  back(){
    this.navCtrl.setRoot(LoginPage);

  }

}
