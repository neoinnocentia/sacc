import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
//import { ProfilepicPage } from '../profilepic/profilepic';
import { LoginPage } from '../login/login';
import { AngularFireAuth} from 'angularfire2/auth';
import { AngularFireDatabase} from 'angularfire2/database';
import { Profile } from '../../model/profile';
import { DonationPage } from '../donation/donation';

@IonicPage()
@Component({
  selector: 'page-payments',
  templateUrl: 'payments.html',
})
export class PaymentsPage {

  selectedLeave : string = '';
  profile = {} as Profile;
  newuser = {
    email: '',
    password: '',
    amount:'',
    number:'',
    displayName: ''
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public userservice: UserProvider,
    public loadingCtrl: LoadingController,private afDatabase:AngularFireDatabase ,private fire:AngularFireAuth, public toastCtrl: ToastController) {
}

pay(){
  var toaster = this.toastCtrl.create({
    duration: 3000,
    position: 'bottom'
  });
  if (this.newuser.email == '' || this.newuser.amount == '' || this.newuser.displayName == '') {
    toaster.setMessage('All fields are required dude');
    toaster.present();
  }
  else if (this.newuser.amount.length >= 10) {
    toaster.setMessage('Password is not strong. Try giving more than six characters');
    toaster.present();
  }
  else {
    let loader = this.loadingCtrl.create({
      content: 'Please wait'
    });
    loader.present();
    this.userservice.adduser(this.newuser).then((res: any) => {
      loader.dismiss();
      if (res.success){
        this.fire.authState.take(1).subscribe(auth => {
          this.afDatabase.object(`profile/${auth.uid}`);
        })
        //this.navCtrl.push(MenugpPage);
      }
      
        
      else{
         alert('Error' + res);
      } 
    })
  }
  this.fire.authState.take(1).subscribe(auth => {
    this.afDatabase.object(`profile/${auth.uid}`);
    

  })
}  

goback() {
  this.navCtrl.setRoot(DonationPage);
}

}

    
