import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupinfochannelPage } from './groupinfochannel';

@NgModule({
  declarations: [
    GroupinfochannelPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupinfochannelPage),
  ],
  exports: [
    GroupinfochannelPage
  ]
})
export class GroupinfochannelPageModule {}
