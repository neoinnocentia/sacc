import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TestingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-testing',
  templateUrl: 'testing.html',
})
export class TestingPage {
public buttonClicked: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  public onButtonClick(){
    this.buttonClicked = !this.buttonClicked;
  }

}
