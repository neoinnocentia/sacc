import { Component,NgZone } from '@angular/core';
import {  NavController, ModalController,PopoverController ,MenuController ,Platform,NavParams ,Events} from 'ionic-angular';
import { TimelinePage } from '../timeline/timeline';
import { ReactionsPage } from '../../pages/reactions/reactions';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { SocialSharing } from '@ionic-native/social-sharing';
//import { CommentPage } from '../comment/comment';
import { AddCommentPage } from '../add-comment/add-comment';
import { AngularFireDatabase } from 'angularfire2/database'
import { TabsPage } from '../tabs/tabs';
import { PresencePage } from '../presence/presence';
import { DonationPage } from '../donation/donation';

@Component({
  selector: 'page-accountprofilecot',
  templateUrl: 'accountprofilecot.html',
})
export class AccountProfilePagecotcot {


  private stories = new Array<any>();
  username: string = '';
  message: string = '';
  _chatSubscription;
  messages: object[] = [];
  LikeValue: number;
  Like: number;
  comment: number;
  comments: number;
  
  quotes :any;

  constructor(public navCtrl: NavController,public ngZone: NgZone, public events: Events,public menu: MenuController, private file: File,public db: AngularFireDatabase, public navParams: NavParams, 
    private fileOpener: FileOpener,platform: Platform,private socialSharing: SocialSharing,private popoverCtrl: PopoverController,private modalCtrl: ModalController) {
   
   
    window.addEventListener("contextmenu", (e) => { e.preventDefault(); });
    this.LikeValue = 0;
    this.Like = 0;
    this.comment= 6;
  this.comments= 6 ;
   // this.getQuotes();
    let storyItem1 = {
      userPicture: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.04.jpeg?alt=media&token=377986ea-ce66-460a-b56e-3a15d10a42a1",
      userId: 1,
      userName: "Bishop Joshua",
      currentItem: 0,
      items: [{
        date: "12/08/20",
        duration: "3",
        id: "3",
        media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.25.58.jpeg?alt=media&token=b5a120a4-2977-4256-b93c-8f54dad845f8",
        seen: false,
        type: "0",
        views: 5
      }],
      seen: true
    };

    let storyItem2 = {
      userPicture: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.00.jpeg?alt=media&token=ffc0d97e-5432-4b18-874e-be0b10424c45",
      userId: 2,
      userName: "Pastor James",
      currentItem: 0,
      seen: false,
      items: [{
        date: "12/08/20",
        duration: "4",
        id: "64",
        media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.02%20(1).jpeg?alt=media&token=930ddfd3-f935-4633-a551-a91fe8eb16be",
        seen: false,
        type: "0",
        views: null
      },{
        date: "12/08/20",
        duration: "3",
        id: "74",
        media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.02.jpeg?alt=media&token=35c25663-4614-4f9e-bb78-09362891427d",
        seen: false,
        type: "0",
        views: null
      },{
        date: "11/08/20",
        duration: null,
        id: "84",
        media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.03%20(1).jpeg?alt=media&token=268fe7e6-3d4d-497b-9361-dae30eb6636f",
        seen: false,
        type: "1",
        views: null
      }]
    };

    let storyItem3 = {
      userPicture: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.03%20(2).jpeg?alt=media&token=44a21fa6-db47-4c64-979c-0f475b815af1",
      userId: 1,
      userName: "prophet MJB",
      currentItem: 0,
      items: [{
        date: "20 minutes",
        duration: "5",
        id: "3",
        media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.03.jpeg?alt=media&token=de137fc9-466c-41bf-b2f5-c506f3cea61e",
        seen: true,
        type: "0",
        views: 5
      }],
      seen: true
    };

    let storyItem4 = {
      userPicture: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.04%20(1).jpeg?alt=media&token=af620806-2cb6-4603-9418-d9f337e3bc65",
      userId: 2,
      userName: "Pastor TK",
      currentItem: 0,
      seen: false,
      items: [{
        date: "30 minutes",
        duration: "4",
        id: "64",
        media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.04%20(2).jpeg?alt=media&token=959ae903-261c-4dbc-b860-3f14652d1286",
        seen: false,
        type: "0",
        views: null
      },{
        date: "há 30 minutos",
        duration: "3",
        id: "74",
        media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.04%20(3).jpeg?alt=media&token=552e4beb-b249-4461-82e8-63d43b9506fe",
        seen: false,
        type: "0",
        views: null
      },{
        date: "há 1 hora",
        duration: null,
        id: "84",
        media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.03.jpeg?alt=media&token=de137fc9-466c-41bf-b2f5-c506f3cea61e",
        seen: false,
        type: "1",
        views: null
      }]
    };

    let storyItem5 = {
      userPicture: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.04%20(2).jpeg?alt=media&token=959ae903-261c-4dbc-b860-3f14652d1286",
      userId: 1,
      userName: "AFM Youth",
      currentItem: 0,
      items: [{
        date: "há 20 minutos",
        duration: "5",
        id: "3",
        media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.04%20(1).jpeg?alt=media&token=af620806-2cb6-4603-9418-d9f337e3bc65",
        seen: true,
        type: "0",
        views: 5
      }],
      seen: true
    };

    let storyItem6 = {
      userPicture: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.03%20(1).jpeg?alt=media&token=268fe7e6-3d4d-497b-9361-dae30eb6636f",
      userId: 2,
      userName: "Pastor Moses",
      currentItem: 0,
      seen: false,
      items: [{
        date: "há 30 minutos",
        duration: "4",
        id: "64",
        media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.04.jpeg?alt=media&token=377986ea-ce66-460a-b56e-3a15d10a42a1",
        seen: false,
        type: "0",
        views: null
      },{
        date: "há 30 minutos",
        duration: "3",
        id: "74",
        media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.25.51%20(1).jpeg?alt=media&token=13e1427e-5105-4715-85e8-9c9b80c5ec3a",
        seen: false,
        type: "0",
        views: null
      },{
        date: "há 1 hora",
        duration: null,
        id: "84",
        media: "https://firebasestorage.googleapis.com/v0/b/municipality-732a0.appspot.com/o/WhatsApp%20Image%202020-08-12%20at%2007.26.03%20(1).jpeg?alt=media&token=268fe7e6-3d4d-497b-9361-dae30eb6636f",
        seen: false,
        type: "1",
        views: null
      }]
    };

    this.stories.push(storyItem1);
    this.stories.push(storyItem2);
    this.stories.push(storyItem3);
    this.stories.push(storyItem4);
    this.stories.push(storyItem5);
    this.stories.push(storyItem6);

    this.reorderStories();
  }
  openStoryViewer(index: number) {
    let modal = this.modalCtrl.create(TimelinePage, { stories: this.stories, tapped: index });
    modal.onDidDismiss(() => {
      this.reorderStories();
    });
    modal.present();
  }

  reorderStories() {
    this.stories.sort((a, b) => {
      if (a.seen) return 1;
      if (b.seen) return -1;

      return 0;
    });
  }
  showReactions(ev){

    let reactions = this.popoverCtrl.create(ReactionsPage);

    reactions.present({
        ev: ev
    });

}

handleLike(){
  this.LikeValue++;
 
 }
 handlingLike(){
  this.Like++;
 
 }


 saveImg() {
  let imageName = "logo.jpg";
  const ROOT_DIRECTORY = 'file:///sdcard//';
  const downloadFolderName = 'tempDownloadFolder';
  
  //Create a folder in memory location
  this.file.createDir(ROOT_DIRECTORY, downloadFolderName, true)
    .then((entries) => {

      //Copy our asset/img/FreakyJolly.jpg to folder we created
      this.file.copyFile(this.file.applicationDirectory + "www/assets/imgs/", imageName, ROOT_DIRECTORY + downloadFolderName + '//', imageName)
        .then((entries) => {

          //Open copied file in device's default viewer
          this.fileOpener.open(ROOT_DIRECTORY + downloadFolderName + "/" + imageName, 'image/jpeg')
            .then(() => console.log('File is opened'))
            .catch(e => alert('Error' + JSON.stringify(e)));
        })
        .catch((error) => {
          alert('error ' + JSON.stringify(error));
        });
    })
    .catch((error) => {
      alert('error' + JSON.stringify(error));
    });
}

shareImg() { 
  let imageName = "logo.jpg";
  const ROOT_DIRECTORY = 'file:///sdcard//';
  const downloadFolderName = 'tempDownloadFolder';
  
  //Create a folder in memory location
  this.file.createDir(ROOT_DIRECTORY, downloadFolderName, true)
    .then((entries) => {

      //Copy our asset/img/FreakyJolly.jpg to folder we created
      this.file.copyFile(this.file.applicationDirectory + "www/assets/imgs/", imageName, ROOT_DIRECTORY + downloadFolderName + '//', imageName)
        .then((entries) => {

          //Common sharing event will open all available application to share
          this.socialSharing.share("Message","Subject", ROOT_DIRECTORY + downloadFolderName + "/" + imageName, imageName)
            .then((entries) => {
              console.log('success ' + JSON.stringify(entries));
            })
            .catch((error) => {
              alert('error ' + JSON.stringify(error));
            });
        })
        .catch((error) => {
          alert('error ' + JSON.stringify(error));
        });
    })
    .catch((error) => {
      alert('error ' + JSON.stringify(error));
    });
}
shareImgg() { 
  let imageName = "gender.jpg";
  const ROOT_DIRECTORY = 'file:///sdcard//';
  const downloadFolderName = 'tempDownloadFolder';
  
  //Create a folder in memory location
  this.file.createDir(ROOT_DIRECTORY, downloadFolderName, true)
    .then((entries) => {

      //Copy our asset/img/FreakyJolly.jpg to folder we created
      this.file.copyFile(this.file.applicationDirectory + "www/assets/imgs/", imageName, ROOT_DIRECTORY + downloadFolderName + '//', imageName)
        .then((entries) => {

          //Common sharing event will open all available application to share
          this.socialSharing.share("Message","Subject", ROOT_DIRECTORY + downloadFolderName + "/" + imageName, imageName)
            .then((entries) => {
              console.log('success ' + JSON.stringify(entries));
            })
            .catch((error) => {
              alert('error ' + JSON.stringify(error));
            });
        })
        .catch((error) => {
          alert('error ' + JSON.stringify(error));
        });
    })
    .catch((error) => {
      alert('error ' + JSON.stringify(error));
    });
}

public toCommentSection(){
  let commentsModal = this.modalCtrl.create(AddCommentPage);
  commentsModal.present();
  this.comments++;
}

 public toCommentSections(){
  let commentsModal = this.modalCtrl.create(AddCommentPage);
  commentsModal.present();
  this.comment++;
}

 

call(){
  this.navCtrl.setRoot(TabsPage)
}

donate(){
  this.navCtrl.push(DonationPage);
}





}
