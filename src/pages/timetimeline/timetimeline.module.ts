import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimetimelinePage } from './timetimeline';

@NgModule({
  declarations: [
    TimetimelinePage,
  ],
  imports: [
    IonicPageModule.forChild(TimetimelinePage),
  ],
})
export class TimetimelinePageModule {}
