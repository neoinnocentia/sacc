import { Component } from '@angular/core';
import {
  ActionSheetController, AlertController, IonicPage, ModalController, NavController, NavParams,Platform} from "ionic-angular";
  import firebase from 'firebase';
import { AngularFireAuth} from 'angularfire2/auth';
import { MenugpPage } from '../menugp/menugp';
import { Camera } from '@ionic-native/camera';
import {CommentsPage} from "../comments/comments";
import {ImageModalPage} from "../image-modal/image-modal";
import {AlertProvider} from "../../providers/alert";
//import {AddPostPage} from "../add-post/add-post";
import {LoadingProvider} from "../../providers/loading";
import {DataProvider} from "../../providers/data";
import {AngularFireDatabase} from "angularfire2/database";
//mport * as firebase from "firebase";
import {FirebaseProvider} from "../../providers/firebase";
import * as _ from "lodash";
import { ReactionsPage } from '../reactions/reactions';
//import { load } from 'google-maps';
/**
 * Generated class for the TimetimelinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var AccountKitPlugin: any;

@IonicPage()
@Component({
  selector: 'page-timetimeline',
  templateUrl: 'timetimeline.html',
})
export class TimetimelinePage {

  private user: any;
  public timelineData: any;
  public friendsList: any;
  isFirstTime;
  constructor(    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingProvider: LoadingProvider,
    public angularDb: AngularFireDatabase,
    public dataProvider: DataProvider,
    public firebaseProvider: FirebaseProvider,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public alertProvider: AlertProvider,
    public platform: Platform) {
      this.platform.ready().then(() => {
        this.platform.pause.subscribe(() => {
          //this.isFirstTime = false;
          if (this.user.userId) {
            // Update userData on Database.
            this.angularDb
              .object('Users' + this.user.userId)
              .update({
                isOnline: false
              })
              .then(success => {})
              .catch(error => {
                //this.alertProvider.showErrorMessage('profile/error-update-profile');
              });
          }
        });
  
        this.platform.resume.subscribe(() => {
          this.isFirstTime = false;
          if (this.user.userId) {
            // Update userData on Database.
            this.angularDb
              .object('Users' + this.user.userId)
              .update({
                isOnline: true
              })
              .then(success => {})
              .catch(error => {
                //this.alertProvider.showErrorMessage('profile/error-update-profile');
              });
          }
        });
      });
  }

  
  ionViewDidLoad() {
   // this.isFirstTime = true;
    this.getTimeline();

  }
  getTimeline() {
    // Observe the userData on database to be used by our markup html.
    // Whenever the userData on the database is updated, it will automatically reflect on our user variable.
    this.loadingProvider.show();
    //this.createUserData();

    let userData = this.dataProvider.getCurrentUser().subscribe(user => {
      this.user = <any>user;
      console.log("timeline user", this.user);
       
      this.dataProvider.setData("userData", this.user);
      userData.unsubscribe();

      //  Update userData on Database.
    });

    // Get Friend  List
    this.dataProvider.getFriends().subscribe(friends => {
      // Get timeline by user
      this.dataProvider.getTimelinePost().subscribe(post => {
        this.loadingProvider.hide();

        if (this.timelineData) {
          let timeline = post[post.length - 1];
          let tempData = <any>{};
          tempData = timeline;

          let friendIndex = _.findKey(friends, data => {
            let _tempData = <any>data;
            return _tempData.$value == timeline.postBy;
          });
          if (
            friendIndex ||
            timeline.postBy == firebase.auth().currentUser.uid
          ) {
            this.dataProvider.getUser(timeline.postBy).subscribe(user => {
              tempData.avatar = user.img;
              tempData.name = user.name;
            });

            // Check Locaion

            if (timeline.location) {
              let tempLocaion = JSON.parse(timeline.location);
              tempData.lat = tempLocaion.lat;
              tempData.long = tempLocaion.long;
              // tempData.location="https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=500x200&maptype=roadmap&markers=color:red|label:S|"+tempLocaion.lat+","+tempLocaion.long+"&key=AIzaSyAt0edUAx4S2d7z8wh1Xe04yE9Xml1ZLPY"
              tempData.location =
                "https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=500x200&maptype=roadmap&markers=color:red|label:S|40.702147,-74.015794&key=AIzaSyAt0edUAx4S2d7z8wh1Xe04yE9Xml1ZLPY";
            }

            //  ===== check like and commnets ===
            this.dataProvider.getLike(tempData.$key).subscribe(likes => {
              tempData.likes = likes.length;

              let isLike = _.findKey(likes, like => {
                let _tempLike = <any>like;
                return _tempLike.$value == firebase.auth().currentUser.uid;
              });

              if (isLike) {
                tempData.isLike = true;
              } else {
                tempData.isLike = false;
              }
            });

            //  ===== check dilike
            this.dataProvider.getdisLike(tempData.$key).subscribe(dislikes => {
              tempData.dislikes = dislikes.length;
              // Check post like or not

              let isdisLike = _.findKey(dislikes, dislike => {
                let _tempLike = <any>dislike;
                return _tempLike.$value == firebase.auth().currentUser.uid;
              });

              if (isdisLike) {
                tempData.isdisLike = true;
              } else {
                tempData.isdisLike = false;
              }
            });

            //  ===== check commnets
            this.dataProvider.getComments(tempData.$key).subscribe(comments => {
              tempData.comments = comments.length;
              // Check post like or not

              let isComments = _.findKey(comments, comment => {
                let _tempComment = <any>comment;
                return (
                  _tempComment.commentBy == firebase.auth().currentUser.uid
                );
              });

              if (isComments) {
                tempData.isComment = true;
              } else {
                tempData.isComment = false;
              }
            });

            // this.addOrUpdateTimeline(tempData)
            this.timelineData.unshift(tempData);
          }
        } else {
          this.timelineData = [];
          post.forEach(data => {
            this.dataProvider.getTimeline(data.$key).subscribe(timeline => {
              if (timeline.$exists()) {
                let tempData = <any>{};
                tempData = timeline;
                let friendIndex = _.findKey(friends, data => {
                  let _tempData = <any>data;
                  return _tempData.$value == timeline.postBy;
                });
                if (
                  friendIndex ||
                  timeline.postBy == firebase.auth().currentUser.uid
                ) {
                  this.dataProvider.getUser(timeline.postBy).subscribe(user => {
                    tempData.avatar = user.img;
                    tempData.name = user.name;
                  });

                  // Check Location
                  if (timeline.location) {
                    let tempLocaion = JSON.parse(timeline.location);
                    tempData.lat = tempLocaion.lat;
                    tempData.long = tempLocaion.long;
                    tempData.location =
                      "https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=500x300&maptype=roadmap&markers=color:red|label:S|" +
                      tempLocaion.lat +
                      "," +
                      tempLocaion.long;
                  }

                  //  ===== check like
                  this.dataProvider.getLike(tempData.$key).subscribe(likes => {
                    tempData.likes = likes.length;
                    // Check post like or not

                    let isLike = _.findKey(likes, like => {
                      let _tempLike = <any>like;
                      return (
                        _tempLike.$value == firebase.auth().currentUser.uid
                      );
                    });

                    if (isLike) {
                      tempData.isLike = true;
                    } else {
                      tempData.isLike = false;
                    }
                  });

                  //  ===== check dilike
                  this.dataProvider
                    .getdisLike(tempData.$key)
                    .subscribe(dislikes => {
                      tempData.dislikes = dislikes.length;
                      // Check post like or not

                      let isdisLike = _.findKey(dislikes, dislike => {
                        let _tempLike = <any>dislike;
                        return (
                          _tempLike.$value == firebase.auth().currentUser.uid
                        );
                      });

                      if (isdisLike) {
                        tempData.isdisLike = true;
                      } else {
                        tempData.isdisLike = false;
                      }
                    });

                  //  ===== check commnets
                  this.dataProvider
                    .getComments(tempData.$key)
                    .subscribe(comments => {
                      tempData.comments = comments.length;
                      // Check post like or not

                      let isComments = _.findKey(comments, comment => {
                        let _tempComment = <any>comment;
                        return (
                          _tempComment.commentBy ==
                          firebase.auth().currentUser.uid
                        );
                      });

                      if (isComments) {
                        tempData.isComment = true;
                      } else {
                        tempData.isComment = false;
                      }
                    });

                  this.timelineData.unshift(tempData);
                }
              }
            });
          });
        }
      });
    });

  }
  reportPost(item) {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Report post to admin",
      buttons: [
        {
          text: "Report",
          role: "destructive",
          handler: () => {
            console.log(" report Post ", item);
            this.loadingProvider.show();
            this.firebaseProvider.reportPost(item, this.user).then(
              res => {
                this.loadingProvider.hide();
                this.alertProvider.showToast("Post reported successfully");
              },
              err => {
                this.loadingProvider.hide();
              }
            );
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });

    actionSheet.present();
  }
  addOrUpdateTimeline(timeline) {
    if (!this.timelineData) {
      this.timelineData = [timeline];
    } else {
      var index = -1;
      for (var i = 0; i < this.timelineData.length; i++) {
        if (this.timelineData[i].$key == timeline.$key) {
          index = i;
        }
      }
      if (index > -1) {
        this.timelineData[index] = timeline;
      } else {
        this.timelineData.unshift(timeline);
      }
    }
  }
  addPost() {
    this.navCtrl.push(ReactionsPage);
  }

  likePost(post) {
    this.firebaseProvider.likePost(post.$key);
  }

  delikePost(post) {
    this.firebaseProvider.delikePost(post.$key);
  }

  disikePost(post) {
    this.firebaseProvider.dislikePost(post.$key);
  }
  dedislikePost(post) {
    this.firebaseProvider.dedislikePost(post.$key);
  }

  commentPost(post) {
    let modal = this.modalCtrl.create(CommentsPage, { postKey: post.$key });
    modal.present();
  }

  openMap(lat, long) {
    window.open(
      "http://maps.google.com/maps?q=" + lat + "," + long,
      "_system",
      "location=yes"
    );
  }

}
