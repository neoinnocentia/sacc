import { Component, NgZone } from '@angular/core';
import { NavController, Events, NavParams ,ModalController} from 'ionic-angular';


//import { SuperTabsController } from 'ionic2-super-tabs';
import { FriendsProvider } from '../../providers/friends/friends';

import { UsersPage } from '../users/users';
import { ChatbodyPage } from '../chatbody/chatbody';
import { AuthProvider } from '../../providers/auth/auth';

import { ChatProvider } from '../../providers/chat/chat';

import * as moment from 'moment';
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

	Friends = []
  allUsers = []
  Conversations = []
  userDetails: any;
  onlineStatus = 'Online'
  offlineStatus = "Offline"


  looding = true

  constructor( public chatProvider: ChatProvider,private myModal:ModalController, public authProvider: AuthProvider, public ngZone: NgZone, public events: Events, public friendsProvider: FriendsProvider, public navCtrl: NavController, public navParams: NavParams) {
   
    this.events.subscribe('Friends', () => {
      this.ngZone.run(() => {
        this.Friends = this.friendsProvider.Friends
      })
    })
  this.events.subscribe('Conversations', () => {
      this.ngZone.run(() => {
        this.looding = false
        this.Conversations = this.chatProvider.Conversations
        this.allUsers = this.chatProvider.buddyUsers
        console.log(this.Conversations);
      

//Time: new Date().toString()
        for (var key in this.Conversations) {

          

          var d = new Date(this.Conversations[key].Time);
          

          var month = d.getMonth();
          var day = d.getDate();
          var hours = d.getHours();
          var minutes = '0' + d.getMinutes();
          

          var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
          var formattedTime = monthNames[month] + "-" + day + "-" + hours + ":" + minutes.substr(-2) ;
         // day + "/" + monthNames[month] + "/" + hours + " : " + minutes.substr(-2) 
         

        this.Conversations[key].Time = formattedTime;
      
        
      
        } 





      })
    })

  }
  ionViewDidLeave(){
    this.events.subscribe('Friends')
    this.events.subscribe('Conversations')
  }


  ionViewDidEnter(){
    this.friendsProvider.getFriends()
    this.chatProvider.getConversations()

  }

  openUsersPage() {
  	this.navCtrl.push(UsersPage)
  }


  openChatBody(userDetails) {
    this.chatProvider.initialize(userDetails);
    const modal = this.myModal.create(ChatbodyPage,{
      userDetails: userDetails
     
    })
    modal.present();
  }

	


}