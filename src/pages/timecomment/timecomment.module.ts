import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimecommentPage } from './timecomment';

@NgModule({
  declarations: [
    TimecommentPage,
  ],
  imports: [
    IonicPageModule.forChild(TimecommentPage),
  ],
})
export class TimecommentPageModule {}
