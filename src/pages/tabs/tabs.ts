import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';

//import { AboutPage } from '../about/about';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { PanicPage } from '../panic/panic';
import { AccountProfilePagecotcot } from '../accountprofilecot/accountprofilecot';
import { ChatPage } from '../chat/chat';
import { PreferencePage } from '../preference/preference';
import { NewgroupPage } from '../newgroup/newgroup';
import { GroupPage } from '../group/group';
import { PostPage } from '../post/post';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthProvider } from '../../providers/auth/auth';
import { NearbyPage } from '../nearby/nearby';
import { ReactionsPage } from '../reactions/reactions';
import { TimetimelinePage } from '../timetimeline/timetimeline';

//import { UpdateProfilePage } from '../update-profile/update-profile';
//import { BuddychatPage } from '../buddychat/buddychat';
//import { ChatsPage } from '../chats/chats';




@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  tab1Root= AccountProfilePagecotcot;
  tab2Root = NearbyPage;
  tab4Root = PanicPage;

  image: any;
  userDetails = {
    Name: '',
    Email: '',
    Phone: '',
    Id: '',
    Status: '',
    proPhoto: '',
    bgPhoto: ''
  }

  constructor(public InAppBrowser:InAppBrowser,private afAuth: AngularFireAuth, public authProvider: AuthProvider,public navCtrl: NavController, ) {
  }
  profile(){
    this.navCtrl.setRoot(PostPage, {
      userDetails: this.userDetails
    })
  }
  ionViewWillEnter() {
    // this.superTabsCtrl.showToolbar(true)
     this.authProvider.getUserDetails().then((res: any) => {
       this.userDetails.Name = res.Name
       this.userDetails.proPhoto = res.Photo
       this.userDetails.Phone = res.Phone
       this.userDetails.Status = res.Status
       this.userDetails.Id = res.Id
       this.userDetails.bgPhoto = res.bgPhoto
       this.userDetails.Email = res.Email
     }).catch((err) => {
       console.log(err)
     })
   }
 


}
