import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
//import { CommentsProvider } from '../../providers/comments/comments';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database'
import { Profile } from '../../model/profile';
import { AngularFireAuth } from 'angularfire2/auth';
//import { UserProvidercot } from '../../providers/user/usercot';
import firebase from 'firebase';
import {snapshotToArray} from '../../app/app.module';
 


/**
 * Generated class for the AddCommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-add-comment',
  templateUrl: 'add-comment.html',
})
export class AddCommentPage {
items = [];
ref = firebase.database().ref('items/');
inputText:string = '';

  constructor(public db: AngularFireDatabase,
    public navCtrl: NavController, public navParams: NavParams) {
      
   this.ref.on('value', resp => {
     this.items = snapshotToArray(resp);
   });
    }

    addItem(item){
      if(item!==undefined && item!==null){
        let newItem = this.ref.push();
      newItem.set(item);
      this.inputText = '';
      }
      
    }

    

 
}
