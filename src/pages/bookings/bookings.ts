import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SarsPage } from '../sars/sars';


@Component({
  selector: 'page-bookings',
  templateUrl: 'bookings.html',
})
export class BookingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookingsPage');
  }
  sars()
  {
    this.navCtrl.push(SarsPage)
  }

}
