import { Component,ElementRef, ViewChild } from '@angular/core';
import {  NavController, Platform } from 'ionic-angular';
import { LocationsProvider } from '../../providers/locations/locations';
import { Http} from '@angular/http';
import { HttpModule } from '@angular/http';
//import { MapPage } from '../map/map';
//import { ListPage } from '../list/list';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { PlaceDetailsPage } from '../place-details/place-details';
import { PlaceDirectionPage } from '../place-direction/place-direction';
import { RestaurantsPage } from '../restaurants/restaurants';
//import { SupermarketPage } from '../supermarket/supermarket';
//import { HospitalPage } from '../hospital/hospital';
//import { PharmacyPage } from '../pharmacy/pharmacy';
//import { ChurchPage } from '../church/church';
import { SchoolPage } from '../school/school';
//import { GasStationPage } from '../gas-station/gas-station';
import { SarsPage } from '../sars/sars';
//import { PostooficePage } from '../postoofice/postoofice';


@Component({
  selector: 'page-nearcentre',
  templateUrl: 'nearcentre.html',
})
export class NearcentrePage {
  
  tab1Root: any  = RestaurantsPage;
  tab2Root: any  = SchoolPage;
  //tab3Root: any  = HospitalPage;
  //tab4Root: any  = PharmacyPage;
 // tab5Root: any = SarsPage;
 // tab6Root: any = PostooficePage;
 // tab7Root: any = GasStationPage ;
  

 // tab1Root: any = MapPage;
  //tab2Root: any = ListPage;

  
  selected_place:any;
  rating:any=0;
  showFooter:boolean=true; //show and hide footer using the Fab Icon
  constructor(public connectionService:ConnectivityProvider, public navCtrl:NavController) {
     this.connectionService.selectedPlaceObserve$.subscribe(place=>{

       this.selected_place=place;

       if(this.selected_place)
       this.rating=this.selected_place.rating;
       //this.rating=4;
     })
  }


  /*
    @Author:Dieudonne Dengun
    @Date:03/05/2018
    @Description:Navigate to Place Details Page
  */
   showPlaceDetailsPage(place:any){
      
    this.navCtrl.push(PlaceDetailsPage,{place:place});
    
   }

   /*
    @Author:Dieudonne Dengun
    @Date:03/05/2018
    @Description:Navigate to Place Direction Page
  */
 showDirectionPage(place:any){
   
   this.navCtrl.push(PlaceDirectionPage ,{direction:place});
    
}

/*
   @Description:Handle Fab Icon click
*/
   
}

