import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenugpPage } from './menugp';

@NgModule({
  declarations: [
    MenugpPage,
  ],
  imports: [
    IonicPageModule.forChild(MenugpPage),
  ],
})
export class MenugpPageModule {}
