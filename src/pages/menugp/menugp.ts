 import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController,ModalController, NavParams, Nav, AlertController, MenuController,Platform ,Events, LoadingController, Tabs} from 'ionic-angular';

import { ProfilePage } from '../profile/profile';
import firebase from 'firebase';
import { LoginPage } from '../login/login';
//import { LoggedinPage } from '../loggedin/loggedin';

import {InAppBrowser } from '@ionic-native/in-app-browser';
import { PanicPage } from '../panic/panic';
//import { AboutPage } from '../about/about';
//import { CojpaymentsPage } from '../cojpayments/cojpayments';
//import { CojhistoryPage } from '../cojhistory/cojhistory';
//import { AboutPage } from '../about/about';
//import { ImghandlerProvider } from '../../providers/imghandler/imghandler';
//import { UserProvider } from '../../providers/user/user';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { Profile } from './../../model/profile';
import { TabsPage } from '../tabs/tabs';
//import { HomePage } from '../home/home';
import { AccountProfilePagecotcot } from '../accountprofilecot/accountprofilecot';
//import { UpdateProfilePage } from '../update-profile/update-profile';
//import { SignupPage } from '../signup/signup';
//import { ChatsPage } from '../chats/chats';
//import { BuddiesPage } from '../buddies/buddies';
//import { BuddychatPage } from '../buddychat/buddychat';
//import { GroupsProvider } from '../../providers/groups/groups';
import { NewgroupPage } from '../newgroup/newgroup';
//import { GroupchatPage } from '../groupchat/groupchat';
//import { GroupsPage } from '../groups/groups';
import { group } from '@angular/core/src/animation/dsl';
import { Tab } from 'ionic-angular/umd/navigation/nav-interfaces';
import { ChatPage } from '../chat/chat';
//import { GroupinfoPage } from '../groupinfo/groupinfo';
//import { ChatProvider } from '../../providers/chat/chat';
//import { GroupchatchannelPage } from '../groupchatchannel/groupchatchannel';

import { GroupbodyPage } from '../groupbody/groupbody';

import { GroupProvider } from '../../providers/group/group';
import { GroupPage } from '../group/group';

export interface PageInterface {
  title: string;
  pageName: any;
  icon: string;
  tabComponent?: any;
  index?: number;
  component?: any;
  
}

@IonicPage()
@Component({
  selector: 'page-menugp',
  templateUrl: 'menugp.html',
})
export class MenugpPage {
  Groups = []


  User_Uid = this.groupProvider.UserUid
  //
  allmygroups;
  tab1Root= ChatPage;
  //tab2Root = NewgroupPage;
  tab3Root= GroupPage;
rootPage = TabsPage;
avatar: string;
img:string;
@ViewChild(Nav) nav: Nav;
  constructor(public navCtrl: NavController,public groupProvider: GroupProvider,
    public platform: Platform, private myModal:ModalController,public loadingCtrl: LoadingController,public events: Events,
    public menu: MenuController,private afAuth: AngularFireAuth,private database: AngularFireDatabase,public alertCtrl: AlertController,  public zone: NgZone, public navParams: NavParams, private InAppBrowser: InAppBrowser) {
      this.events.subscribe('Groups', () => {
        this.zone.run(() => {
          
          this.Groups = this.groupProvider.Groups
        })
      })
    }
    pages: PageInterface[]= [
      {title: 'Chats' , pageName: ChatPage ,icon:'assets/icons8-chat.png'}, 
      {title: 'Group Chats' , pageName:NewgroupPage, icon:'assets/icons8-user_group.png'}

     
     
    ]


  
    newgroup(){
     
      const modal = this.myModal.create(NewgroupPage);
      modal.present();
  
    } 
    
 
    ionViewDidLeave(){
      this.events.subscribe('Groups')
    }
  
  
  
  
    ionViewDidEnter(){
      this.groupProvider.getGroups()
    }
  
  
  
  
    openGroupBody(groupDetails) {
      this.groupProvider.initialize(groupDetails)
      this.navCtrl.push(GroupbodyPage, {
        groupDetails: groupDetails
      })
    }
  

}
