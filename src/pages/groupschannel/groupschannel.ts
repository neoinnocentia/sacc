import { Component,NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController } from 'ionic-angular';
import { GroupschannelProvider } from '../../providers/groups/groupschannel';
import { NewgroupchannelPage } from '../newgroupchannel/newgroupchannel';
import { GroupchatchannelPage } from '../groupchatchannel/groupchatchannel';
import { NewgroupPage } from '../newgroup/newgroup';

import { GroupbodyPage } from '../groupbody/groupbody';

import { GroupProvider } from '../../providers/group/group';
@IonicPage()
@Component({
  selector: 'page-groupschannel',
  templateUrl: 'groupschannel.html',
})
export class GroupschannelPage {

 
 
  Groups = []


  User_Uid = this.groupProvider.UserUid

looding = true

  constructor(public navCtrl: NavController,public ngZone : NgZone, public navParams: NavParams, public events: Events,
              public myModal: ModalController, public groupProvider: GroupschannelProvider) {
                
    this.events.subscribe('Channels', () => {
      this.ngZone.run(() => {
        this.looding = false
        this.Groups = this.groupProvider.Groups
      })
    })    

  }

  
  openNewGroupPage() {
  	this.navCtrl.push(NewgroupchannelPage)
  }







  ionViewDidLeave(){
    this.events.subscribe('Channels')
  }




  ionViewDidEnter(){
    this.groupProvider.getGroups()
  }

  openGroupBody(groupDetails) {
    this.groupProvider.initialize(groupDetails)
    const modal = this.myModal.create(GroupchatchannelPage, {
      groupDetails: groupDetails
    })
    modal.present();
  }
  
  
}
