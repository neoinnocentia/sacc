import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupschannelPage } from './groupschannel';

@NgModule({
  declarations: [
    GroupschannelPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupschannelPage),
  ],
  exports: [
    GroupschannelPage
  ]
})
export class GroupschannelPageModule {}
