import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimelinePage } from './timeline';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    TimelinePage,
   
  ],
  imports: [
    IonicPageModule.forChild(TimelinePage),
    DirectivesModule
  ],
})
export class TimelinePageModule {}
