import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController,Platform, ActionSheetController, LoadingController, Content, Events } from 'ionic-angular';
import { GroupschannelProvider } from '../../providers/groups/groupschannel';
import { ImghandlerProvider } from '../../providers/imghandler/imghandler';
import firebase from 'firebase';
import { GroupbuddieschannelPage } from '../groupbuddieschannel/groupbuddieschannel';
import { GroupmembersChannelPage } from '../groupmemberschannel/groupmemberschannel';
import { GroupinfochannelPage } from '../groupinfochannel/groupinfochannel';
import { PresencePage } from '../presence/presence';
import { DonationPage } from '../donation/donation';
declare var window: any;

/**
 * Generated class for the GroupchatchannelPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-groupchatchannel',
  templateUrl: 'groupchatchannel.html',
})
export class GroupchatchannelPage {
	groupDetails


	User_Uid = this.groupProvider.UserUid


	newMessage = {
		body: ''
	}


	allMessages = []
	allUsers = []
  constructor(public ngZone: NgZone, private platform: Platform,public navCtrl: NavController,public menu: MenuController, public navParams: NavParams, public groupProvider: GroupschannelProvider,
    public actionSheetCtrl: ActionSheetController, public events: Events, public imgstore: ImghandlerProvider, public loadCtrl: LoadingController) {
     /*  var days = ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri']
      var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
  
      var today = new Date
      var toYear = today.getFullYear()
      var toMonth = 1 + today.getMonth()
      var toDays = today.getDate()
  
      var todayRes1 = toYear + '/' + toMonth + '/' + toDays
      var todayRes2 = toYear + '/' + toMonth
      var todayRes3 = toYear
  
  
   */
      this.events.subscribe('Messages', () => {
        this.ngZone.run(() => {
         // this.looding = false
          this.allMessages = this.groupProvider.messages
          this.allUsers = this.groupProvider.users
  
  
          for (var key in this.allMessages) {
  
            var d = new Date(this.allMessages[key].Time)
  
            var years = d.getFullYear()
            var month =  d.getMonth()
            var day = d.getDate()
            var hours = d.getHours()
            var minutes = '0' + d.getMinutes()
  
         
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var formattedTime = monthNames[month] + "-" + day + "-" + hours + ":" + minutes.substr(-2) ;
  
  this.allMessages[key].Time = formattedTime;
          }

        })
      })

      this.groupDetails = this.navParams.get('groupDetails')
      this.events.subscribe('Messages', () => {
        this.ngZone.run(() => {
          this.allMessages = this.groupProvider.messages
          this.allUsers = this.groupProvider.users
        })
      })
  
    }

    ionViewDidLeave(){
      this.events.subscribe('Messages')
    }
  
  
    ionViewDidEnter(){
      this.groupProvider.getAllMessages()
    }
  
  
  
    showToast(message){
          this.platform.ready().then(() => {
              window.plugins.toast.show(message, "short", 'bottom');
          })    
    }
    showActionSheet() {
      if (this.groupDetails.Owner == this.groupProvider.UserUid) {
  
        const actionSheet = this.actionSheetCtrl.create({
          title: this.groupDetails.Name,
          buttons: [
            {
              text: 'Group Members',
              icon:'contacts',
              handler: () => {
                this.showMembers(this.groupDetails)
              }
            },{
              text: 'Add Member',
              icon:'add',
              handler: () => {
                this.addMember(this.groupDetails)
              }
            },{
              text: 'Delete Group',
              icon:'trash',
              handler: () => {
                this.deleteGroup(this.groupDetails)
              }
            },{
              text: 'Cancel',
              icon:'close',
              handler: () => {
                this.showToast('Cancel clicked');
              }
            }
          ]
        })
        actionSheet.present()
      } else {
        const actionSheet = this.actionSheetCtrl.create({
          title: this.groupDetails.Name,
          buttons: [
            {
              text: 'Group Members',
              icon:'contacts',
              handler: () => {
                this.showMembers(this.groupDetails)
              }
            },{
              text: 'Leave Group',
              icon:'log-out',
              handler: () => {
                this.leaveGroup(this.groupDetails)
              }
            },{
              text: 'Cancel',
              icon:'close',
              handler: () => {
                this.showToast('Cancel clicked');
              }
            }
          ]
        })
        actionSheet.present()
      }
    }
    leaveGroup(groupDetails){
      let load = this.loadCtrl.create({
        content: 'Leaving Group ...'
      })
  
      load.present()
      this.groupProvider.leaveGroup(groupDetails).then(() => {
        load.dismiss()
        this.showToast('You have been leaved')
        this.navCtrl.pop()
      }).catch((err) => {
        load.dismiss()
        this.showToast(err)
      })
  
    }
  
  
  
  
    deleteGroup(groupDetails){
      let load = this.loadCtrl.create({
        content: 'Deleting Group ...'
      })
  
      load.present()
      this.groupProvider.deleteGroup(groupDetails).then(() => {
        load.dismiss()
        this.showToast('Group has been deleted')
        this.navCtrl.pop()
      }).catch((err) => {
        load.dismiss()
        this.showToast(err)
      })
  
    }
  
  
    showMembers(groupDetails){
      this.navCtrl.push(GroupmembersChannelPage, {
        groupDetails: groupDetails
      })
    }
  
    addMember(groupDetails){
      this.navCtrl.push(GroupbuddieschannelPage, {
        groupDetails: groupDetails
      })
    }
  
    donate(){
      this.navCtrl.push(DonationPage);
    }
  
  
    groupInfo(groupDetails){
  
    }
    sendMessage() {
      var res = this.newMessage.body
      var res1 = res.trim()
      if (res1 == '') {
        this.showToast("can't send empty message")
        this.newMessage.body = ''
      } else {
        this.groupProvider.sendMessage(this.newMessage, this.groupDetails).then(() => {
          this.newMessage.body = ''
        }).catch((err) => {
          console.log(err)
        })
      }  	
    }

    opensettings(){
      this.navCtrl.push(PresencePage);
    }
  
  


    }