import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupchatchannelPage } from './groupchatchannel';

@NgModule({
  declarations: [
    GroupchatchannelPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupchatchannelPage),
  ],
  exports: [
    GroupchatchannelPage
  ]
})
export class GroupchatchannelPageModule {}
