export interface Profile {
    firstName: string;
    lastName: string;
    number: string;
    accountNo: string;
    municipality: string;
    idNo: string;
    address: string;

    
}