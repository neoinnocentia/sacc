export interface Issue {
    problem: string;
    dateOfEvent: string;
    location: string;
  
}