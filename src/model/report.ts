export interface Report {
    contact: string;
    department: string;
    firstName: string;
    dateOfEvent: string;
    randomNumberFloor:any;
}