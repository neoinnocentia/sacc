import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "angularfire2/database";
import * as firebase from "firebase";
//import {Contacts} from "@ionic-native/contacts";
import {Storage} from "@ionic/storage";
import async from "async";
import _ from 'lodash';

@Injectable()
export class DataProvider {
  // Data Provider
  // This is the provider class for most of the Firebase observables in the app.
  webRTCClient;
  incomingCallId;
  userContactsList = [];
  userOnlyContacts = [];
  exitsUserList = [];
  inviteUserList = [];
  userContactsListWithCountryCode = [];
  isContactGet = false;
  countryCode = "+1";
  constructor(
    public angularDb: AngularFireDatabase,
    //private contacts: Contacts,
    private storage: Storage,
  ) {
  }

  // set webRTCClient
  setWebRTCClient(val) {
    this.webRTCClient = val;
  }

  // get webRTCClient
  getwebRTCClient() {
    return this.webRTCClient;
  }

  // set Incoming Call id
  setIncomingCallId(id) {
    this.incomingCallId = id;
  }

  // get incoming call id
  getIncomingCallid() {
    return this.incomingCallId;
  }

  // Get all users
  getUsers() {
    return this.angularDb.list("Users", {
      query: {
        orderByChild: "Name"
      }
    });
  }

  // Get user with username
  getUserWithUsername(Email) {
    return this.angularDb.list("Users", {
      query: {
        orderByChild: "Email",
        equalTo: Email
      }
    });
  }

  // Get user with phonenumber
 /*  getUserWithPhonenumber(Phone) {
    return this.angularDb.list('Users', {
      query: {
        orderByChild: "Phone",
        equalTo: Phone
      }
    });
  } */

  // Get logged in user data
  getCurrentUser() {
    return this.angularDb.object(
      "Users" + firebase.auth().currentUser.uid
    );
  }

  // Get user by their userId
  getUser(userId) {
    return this.angularDb.object("Users"+ userId);
  }


  // Get requests given the userId.
  getRequests(userId) {
    return this.angularDb.object("/requests/" + userId);
  }

  // Get friend requests given the userId.
  getFriendRequests(userId) {
    return this.angularDb.list("/requests", {
      query: {
        orderByChild: "receiver",
        equalTo: userId
      }
    });
  }

  // Get conversation given the conversationId.
  getConversation(conversationId) {
    return this.angularDb.object("/conversations/" + conversationId);
  }

  // Get conversations of the current logged in user.
  getConversations() {
    return this.angularDb.list(
      "/accounts/" + firebase.auth().currentUser.uid + "/conversations"
    );
  }

  // Get messages of the conversation given the Id.
  getConversationMessages(conversationId) {
    return this.angularDb.object(
      "/conversations/" + conversationId + "/messages"
    );
  }

  // Get messages of the group given the Id.
  getGroupMessages(groupId) {
    return this.angularDb.object("/groups/" + groupId + "/messages");
  }

  // Get groups of the logged in user.
  getGroups() {
    return this.angularDb.list(
      "/accounts/" + firebase.auth().currentUser.uid + "/groups"
    );
  }

  // Get group info given the groupId.
  getGroup(groupId) {
    return this.angularDb.object("/groups/" + groupId);
  }

  // Get Timeline of user
  getTimelines() {
    return this.angularDb.list(
      "Users" + firebase.auth().currentUser.uid + "/timeline"
    );
  }

  // Get Timeline by user id
  getTimelineByUid(id) {
    return this.angularDb.object(
      "Users" + id + "/timeline"
    );
  }

  // Get Timeline post
  getTimelinePost() {
    return this.angularDb.list("/timeline");
  }

  getAllReportedPost() {
    return this.angularDb.list("/reportPost");

  }

  // Get time line by id
  getTimeline(timelineId) {
    return this.angularDb.object("/timeline/" + timelineId);
  }

  // Get Friend List
  getFriends() {
    return this.angularDb.list(
      "Users" + firebase.auth().currentUser.uid + 'Friends'
    );
  }

  // Get comments list
  getComments(postId) {
    return this.angularDb.list("/comments/" + postId);
  }

  // Get likes
  getLike(postId) {
    return this.angularDb.list("/likes/" + postId);
  }

  postLike(postId) {
    return this.angularDb.object("/likes/" + postId);
  }

  // Get likes
  getdisLike(postId) {
    return this.angularDb.list("/dislikes/" + postId);
  }

  postdisLike(postId) {
    return this.angularDb.object("/dislikes/" + postId);
  }
  // post Comments
  postComments(postId) {
    return this.angularDb.object("/comments/" + postId);
  }

  // report post to admin
  getReportPost(postId) {
    console.log("postId", postId)
    return this.angularDb.object("/reportPost/" + postId);
  }

 
  setData(key, val) {
    this.storage.set(key, val);
  }

  getData(key) {
    return this.storage.get(key).then(val => {
      return val;
    });
  }

  clearData() {
    this.storage.clear();
  }


  removePost(post) {
    this.getUser(post.postBy).take(1).subscribe((account) => {
      console.log("before timeline", timeline)
      var timeline = account.timeline;

      _.remove(timeline, (n) => {
        return n == post.$key
      });
      console.log("after timeline", timeline)
      // Add both users as friends.
      this.getUser(post.postBy).update({
        timeline: timeline
      }).then((success) => {

        /**
         * Remove post from time line
        //  */
        this.getTimeline(post.$key).remove().then((success) => {
          this.angularDb.object('/reportPost/' + post.$key).remove();
        }).catch((error) => {
        })
      })
    })
  }

  ignorePost(post){
    console.log("ingnore post ", post)
      this.angularDb.object('/reportPost/' + post.$key).remove()
  }

  unFriend(userId){
    /**
     * Remove friend id from friend account
     */
    this.getUser(userId).take(1).subscribe((account) => {
      var friends = account.friends;
      console.log("==friend List before", friends)
      if(friends){
        _.remove(friends, (n) => {
          return n == firebase.auth().currentUser.uid
        });
        this.getUser(userId).update({
          friends: friends
        }).then((success) => {
        })
      }
      console.log("==friend List after", friends)

    })
   /**
    * Remove friend id from login user account
    */
    this.getUser(firebase.auth().currentUser.uid).take(1).subscribe((account) => {
      var friends = account.friends;
      console.log("==user List before", friends)
      if(friends){
        _.remove(friends, (n) => {
          return n == userId
        });
        this.getUser(firebase.auth().currentUser.uid).update({
          friends: friends
        }).then((success) => {
        })
      }
      console.log("==user List after", friends)

    })
  }
}
