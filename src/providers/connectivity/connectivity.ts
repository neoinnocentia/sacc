import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network';
import { Platform } from 'ionic-angular';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

/*
  Generated class for the ConnectivityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class ConnectivityProvider {

  onDevice :boolean;

  private selected_place = new BehaviorSubject<any>(null); // true is your initial value
  selectedPlaceObserve$ = this.selected_place.asObservable();

  constructor(public platform: Platform, private network: Network) {
    this.onDevice = this.platform.is('cordova');
  }

  isOnline(): boolean {
    if (this.onDevice && this.network.type) {
      return this.network.type !== "none";
    } else {
      return navigator.onLine;
    }
  }

  isOffline(): boolean {
    if (this.onDevice && this.network.type) {
      return this.network.type === "none";
    } else {
      return !navigator.onLine;
    }
  }

  //update or set selected place


   setSelectedPlace(value: any) {
    this.selected_place.next(value);
    console.log('isFixed changed', value);
  }

   getSelectedPlace():any {
    return this.selected_place.getValue()
  }
}
