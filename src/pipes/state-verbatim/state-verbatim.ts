import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the StateVerbatimPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'stateVerbatim',
})
export class StateVerbatimPipe implements PipeTransform {
 
public states: object = {
  'Ek': 'Ekurhuleni',
  'SD': 'Sedibeng',
  'CT': 'City of Tshwane'
};

  transform(value: string, ...args) {
    if(value == null) return;

    if(this.states[value]){
      return this.states[value];
    } else {
      return value;
    }
  }
}
