import { NgModule } from '@angular/core';
import { StateVerbatimPipe } from './state-verbatim/state-verbatim';
@NgModule({
	declarations: [StateVerbatimPipe],
	imports: [],
	exports: [StateVerbatimPipe]
})
export class PipesModule {}
