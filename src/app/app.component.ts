import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import { WelcomePage } from '../pages/welcome/welcome';
//import { AppointmentDashPage } from '../pages/appointment-dash/appointment-dash';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';


import { AuthProvider } from '../providers/auth/auth';
import { MenugpPage } from '../pages/menugp/menugp';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
rootPage;
//rootPage ;
 

  constructor(platform: Platform, statusBar: StatusBar,public authProvider: AuthProvider, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      splashScreen.hide();
      this.checkUserState()
     //this.authProvider.onlineStatus()
      //this.authProvider.offlineStatus()

//console.log(window.localStorage.getItem('userid'))
    })

  }
  checkUserState(){
    var userstate = window.localStorage.getItem('userstate')
    if (userstate == 'logedIn') {
      this.authProvider.onlineStatus()
      this.authProvider.offlineStatus()
      this.rootPage = MenugpPage
    } else {
      this.rootPage = LoginPage
    }



  }
}
