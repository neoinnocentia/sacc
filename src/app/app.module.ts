import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { ProfilePage } from '../pages/profile/profile';
import { AngularFireDatabaseModule } from "angularfire2/database";
import { Geolocation} from '@ionic-native/geolocation';
import { Network } from '@ionic-native/network';
import { HttpModule } from '@angular/http';
import { Camera } from '@ionic-native/camera';
import { ChatProvider } from '../providers/chat/chat';
import { AuthProvider } from '../providers/auth/auth';
import { File } from '@ionic-native/file';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { Device } from '@ionic-native/device';
import { Diagnostic } from '@ionic-native/diagnostic'; 
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AccountProfilePagecotcot } from '../pages/accountprofilecot/accountprofilecot';
import {CallNumber} from '@ionic-native/call-number';
import { PanicPage } from '../pages/panic/panic';

import { IChat } from '../model/chat';
import { FormsModule } from '@angular/forms';
import { IonicSelectableModule } from 'ionic-selectable';
import { NativeStorage } from '@ionic-native/native-storage';
import { PreferencePage } from '../pages/preference/preference';
import { StateVerbatimPipe} from '../pipes/state-verbatim/state-verbatim';

import { TestingPage } from '../pages/testing/testing';
import { TimelinePageModule } from '../pages/timeline/timeline.module';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ReactionsPage } from '../pages/reactions/reactions';
import { FileOpener } from '@ionic-native/file-opener';
import { AddCommentPage } from '../pages/add-comment/add-comment';
import { NewgroupPage } from '../pages/newgroup/newgroup';
import { PresencePage } from '../pages/presence/presence';
import { SettingsPage } from '../pages/settings/settings';
import { UsersPage } from '../pages/users/users';
import { NotificationProvider } from '../providers/notification/notification';
import { RequestProvider } from '../providers/request/request';
import { BlockProvider } from '../providers/block/block';
import { UsersProvider } from '../providers/users/users';
import { FriendsProvider } from '../providers/friends/friends';
import { GroupProvider } from '../providers/group/group';
import { EditprofilePage } from '../pages/editprofile/editprofile';
import { NotificationUnreadPage } from '../pages/notification-unread/notification-unread';
import { NotificationReadPage } from '../pages/notification-read/notification-read';
import { GroupaddmemberPage } from '../pages/groupaddmember/groupaddmember';
import { GroupmemberPage } from '../pages/groupmember/groupmember';
import { ChatPage } from '../pages/chat/chat';
import { GroupPage } from '../pages/group/group';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { FriendsPage } from '../pages/friends/friends';
import { BlockPage } from '../pages/block/block';
import { RequestsPage } from '../pages/requests/requests';
import { NotificationsPage } from '../pages/notifications/notifications';
import { ChatbodyPage } from '../pages/chatbody/chatbody';
import { GroupbodyPage } from '../pages/groupbody/groupbody';
import { IonicStorageModule } from '@ionic/storage';

import { DatePicker } from '@ionic-native/date-picker';
import { Toast } from '@ionic-native/toast';
import { MenugpPage } from '../pages/menugp/menugp';
import { StorageProvider } from '../providers/storage/storage';
import { PostPage } from '../pages/post/post';
import { ImghandlerProvider } from '../providers/imghandler/imghandler';
import { GroupschannelProvider } from '../providers/groups/groupschannel';
import { GroupbuddieschannelPage } from '../pages/groupbuddieschannel/groupbuddieschannel';
import { GroupchatchannelPage } from '../pages/groupchatchannel/groupchatchannel';
import { GroupinfochannelPage } from '../pages/groupinfochannel/groupinfochannel';
import { GroupmembersChannelPage } from '../pages/groupmemberschannel/groupmemberschannel';
import { GroupschannelPage } from '../pages/groupschannel/groupschannel';
import { NewgroupchannelPage } from '../pages/newgroupchannel/newgroupchannel';
import { DonationPage } from '../pages/donation/donation';
import { PaymentsPage } from '../pages/payments/payments';
import { UserProvider } from '../providers/user/user';
import { LocationsProvider } from '../providers/locations/locations';
import { ConnectivityProvider } from '../providers/connectivity/connectivity';
import { GoogleMapsProvider } from '../providers/google-maps/google-maps';
import { NearbyPage } from '../pages/nearby/nearby';
import { BookedPage } from '../pages/booked/booked';
import { BookingsPage } from '../pages/bookings/bookings';
import { SarsPage } from '../pages/sars/sars';
import { NearcentrePage } from '../pages/nearcentre/nearcentre';
import { s } from '@angular/core/src/render3';
import { SchoolPage } from '../pages/school/school';
import { RestaurantsPage } from '../pages/restaurants/restaurants';
import { PlaceDetailsPage } from '../pages/place-details/place-details';
import { PlaceDirectionPage } from '../pages/place-direction/place-direction';
import { DataProvider } from '../providers/data';
import { TimetimelinePage } from '../pages/timetimeline/timetimeline';
import { AlertProvider } from '../providers/alert';
import { FirebaseProvider } from '../providers/firebase';
import { ImageProvider } from '../providers/image';
import { LoadingProvider } from '../providers/loading';
//import { VideoProvider } from '../providers/video';
import { CommentsPage } from '../pages/comments/comments';
import { ImageModalPage } from '../pages/image-modal/image-modal';
import { GoogleMaps } from '@ionic-native/google-maps';







//import { RequestsProvider } from '../providers/requests/requests';

//import { Clipboard } from '@ionic-native/clipboard';

//import { ClipboardOriginal } from '@ionic-native/clipboard';
//import { PhotoViewer } from '@ionic-native/photo-viewer';
//mport { ClipboardOriginal } from '@ionic-native/clipboard';



const firebaseAuth = {
  apiKey:"AIzaSyB2NbbCFdW5z5Mh_YNY_K1bOip8sKMrPM8",
  authDomain:"municipality-732a0.firebaseapp.com",
  databaseURL:"https://municipality-732a0.firebaseio.com",
  projectId:"municipality-732a0",
  storageBucket:"municipality-732a0.appspot.com",
  messagingSenderId:"1033486321710"
  
  
  };
  export const snapshotToArray = snapshot => {
    let returnArray = [];
    snapshot.forEach(element => {
      let item = element.val();
      item.key = element.key;
      returnArray.push(item);
    });
    return returnArray;
  }

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    LoginPage,
    RegisterPage,
    ProfilePage,
    AddCommentPage,
    ReactionsPage,
    PreferencePage,  
    AccountProfilePagecotcot,
   PanicPage,
  TestingPage,
  NewgroupPage,
  PresencePage,
  SettingsPage,
  UsersPage,
  EditprofilePage,
  NotificationUnreadPage,
  NotificationReadPage,
  GroupaddmemberPage,
  GroupmemberPage,
  ChatPage,
  GroupPage,
  ResetPasswordPage,
  FriendsPage,
  BlockPage,
  RequestsPage,
  NotificationsPage,
  ChatbodyPage,
  GroupbodyPage,
  GroupaddmemberPage,
  GroupmemberPage,
  MenugpPage,
  PostPage,
  GroupbuddieschannelPage,
GroupchatchannelPage,
GroupinfochannelPage,
GroupmembersChannelPage,
GroupschannelPage,
NewgroupchannelPage,
DonationPage,
PaymentsPage,
NearbyPage,
BookedPage,
BookingsPage,
SarsPage,
NearcentrePage,
SchoolPage,
RestaurantsPage,
PlaceDetailsPage,
PlaceDirectionPage,
TimetimelinePage,
CommentsPage,
ImageModalPage


  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseAuth),
    IonicStorageModule.forRoot(),
    AngularFireAuthModule,
    FormsModule,
    AngularFireDatabaseModule,
    HttpClientModule,
    HttpModule,
    IonicSelectableModule,
    TimelinePageModule,
    HttpClientModule
  
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
       TabsPage,
    LoginPage,
    RegisterPage,
    ProfilePage,
       ReactionsPage,
    PreferencePage,
    AccountProfilePagecotcot,
  PanicPage,
  TestingPage,
  AddCommentPage,
  NewgroupPage,
  PresencePage,
  SettingsPage ,
  UsersPage,
  EditprofilePage,
  NotificationUnreadPage,
  NotificationReadPage,
  RegisterPage,
  ChatPage,
  GroupPage,
  ResetPasswordPage,
  FriendsPage,
  BlockPage,
  RequestsPage,
  NotificationsPage,
  ChatbodyPage,
  GroupbodyPage,
  GroupaddmemberPage,
  GroupmemberPage,
  MenugpPage,
  PostPage,
  GroupbuddieschannelPage,
GroupchatchannelPage,
GroupinfochannelPage,
GroupmembersChannelPage,
GroupschannelPage,
NewgroupchannelPage,
DonationPage,
PaymentsPage,
NearbyPage,
BookedPage,
BookingsPage,
SarsPage,
NearcentrePage,
SchoolPage,
RestaurantsPage,
PlaceDetailsPage,
PlaceDirectionPage,
TimetimelinePage,
CommentsPage,
ImageModalPage
    

  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    File,
    FilePath,
    FileOpener,
    FileChooser,
    Camera,
    Toast,
    DatePicker,
    CallNumber,
    NativeStorage,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ChatProvider,
    AuthProvider,
    Device,
    Network,
    Diagnostic,
    UsersProvider,
    LocationAccuracy,
    InAppBrowser,
    SocialSharing,
    NotificationProvider,
    RequestProvider,
    BlockProvider,
    UsersProvider,
    FriendsProvider,
    GroupProvider,
    StorageProvider,
    ImghandlerProvider,
    GroupschannelProvider,
    UserProvider,
    ConnectivityProvider,
    GoogleMapsProvider,
    LocationsProvider,
    DataProvider,
    AlertProvider,
    FirebaseProvider,
    ImageProvider,
    LoadingProvider,
    //VideoProvider
    GoogleMaps
    

   
  ]
})
export class AppModule {
 
}
